package com.yueai.activity;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.alibaba.fastjson.JSON;
import com.library.imageloader.ImageLoader;
import com.library.imageloader.ImageLoaderUtil;
import com.library.utils.LogUtil;
import com.library.utils.ToastUtil;
import com.yueai.R;
import com.yueai.base.BaseTitleActivity;
import com.yueai.bean.BaseModel;
import com.yueai.bean.MarkPhoto;
import com.yueai.bean.MarkPhotoBean;
import com.yueai.constant.IUrlConstant;
import com.yueai.xml.PlatformInfoXml;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.Callback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import okhttp3.Call;
import okhttp3.Response;

/**
 * Created by Administrator on 2017/1/7.
 */

public class TestImageActivity extends BaseTitleActivity {
    @BindView(R.id.test_gv)
    GridView test_gv;
    @BindView(R.id.upLoad)
    Button upLoad;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_test_image;
    }

    @Override
    protected String getCenterTitle() {
        return null;
    }

    TestMarkPhotoAdapter adapter;

    @Override
    protected void initViewsAndVariables() {

        getMarkPhotoResource();
        upLoad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtil.e(LogUtil.TAG_WHC, "选中" + selectList.toString());
                upMarkPhotoResource(selectList);
            }
        });
    }

    @Override
    protected void addListeners() {

    }

    public List<MarkPhoto> mDataList = new ArrayList<MarkPhoto>();
    private List<String> selectList = new ArrayList<String>();
    // 用来控制CheckBox的选中状况
    private HashMap<Integer, Boolean> isSelected;

    public class TestMarkPhotoAdapter extends BaseAdapter {

        class ViewHolder {
            RelativeLayout test_rl;
            ImageView ivName;
            CheckBox cb;
        }

        public TestMarkPhotoAdapter() {
            isSelected = new HashMap<Integer, Boolean>();
            // 初始化数据
            if (mDataList != null && mDataList.size() > 0) {
                initDate();
            }
        }

        // 初始化isSelected的数据
        private void initDate() {
            for (int i = 0; i < mDataList.size(); i++) {
                getIsSelected().put(i, false);
            }
        }

        @Override
        public int getCount() {
            return mDataList.size();
        }

        @Override
        public Object getItem(int position) {
            return mDataList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            final MarkPhoto bean = mDataList.get(position);
            LayoutInflater inflater = LayoutInflater.from(TestImageActivity.this);
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.item_test_image, null);
                holder = new ViewHolder();
                holder.ivName = (ImageView) convertView
                        .findViewById(R.id.test_image);
                holder.test_rl = (RelativeLayout) convertView
                        .findViewById(R.id.test_rl);
                holder.cb = (CheckBox) convertView
                        .findViewById(R.id.cb);
                convertView.setTag(holder);
            } else {
                // 取出holder
                holder = (ViewHolder) convertView.getTag();
            }

            ImageLoaderUtil.getInstance().loadImage(TestImageActivity.this, new ImageLoader.Builder().url(bean.getImageUrl()).imageView(holder.ivName).build());
            // 监听checkBox并根据原来的状态来设置新的状态

            holder.cb.setOnClickListener(new View.OnClickListener() {

                public void onClick(View v) {
                    if (isSelected.get(position)) {
                        isSelected.put(position, false);
                        setIsSelected(isSelected);
                        selectList.remove(mDataList.get(position).getId());
                    } else {
                        isSelected.put(position, true);
                        setIsSelected(isSelected);
                        selectList.add(mDataList.get(position).getId());
                    }

                }
            });
            holder.test_rl.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isSelected.get(position)) {
                        isSelected.put(position, false);
                        setIsSelected(isSelected);
                        selectList.remove(mDataList.get(position).getId());
                    } else {
                        isSelected.put(position, true);
                        setIsSelected(isSelected);
                        selectList.add(mDataList.get(position).getId());
                    }
                    notifyDataSetChanged();
                }
            });
            // 根据isSelected来设置checkbox的选中状况
            holder.cb.setChecked(getIsSelected().get(position));
            return convertView;
        }

        public HashMap<Integer, Boolean> getIsSelected() {
            return isSelected;
        }

        public void setIsSelected(HashMap<Integer, Boolean> isSelect) {
            isSelected = isSelect;
        }
    }

    public void getMarkPhotoResource() {
        OkHttpUtils.post()
                .url(IUrlConstant.URL_TESTIMAGE)
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .build()
                .execute(new TestMarkPhotoCallBack());
    }

    public class TestMarkPhotoCallBack extends Callback<MarkPhotoBean> {
        @Override
        public MarkPhotoBean parseNetworkResponse(Response response, int id) throws Exception {
            String resultJson = response.body().string();
            LogUtil.e(LogUtil.TAG_WHC, "markPhoto" + resultJson.toString());
            if (!TextUtils.isEmpty(resultJson)) {
                return JSON.parseObject(resultJson, MarkPhotoBean.class);
            }
            return null;
        }

        @Override
        public void onError(Call call, Exception e, int id) {
            LogUtil.e(LogUtil.TAG_WHC, "markPhotoError" + e.toString());
        }

        @Override
        public void onResponse(MarkPhotoBean response, int id) {
            if (response != null) {
                if (response.getMarkPhotos() != null && response.getMarkPhotos().size() > 0) {
                    for (int i = 0; i < response.getMarkPhotos().size(); i++) {
                        mDataList.add(response.getMarkPhotos().get(i));
                    }
                    adapter = new TestMarkPhotoAdapter();
                    test_gv.setAdapter(adapter);
                }
            }
        }
    }

    public void upMarkPhotoResource(List<String> list) {
        if (selectList != null && selectList.size() > 0) {
            //上傳選擇的
            LogUtil.e(LogUtil.TAG_WHC, "选项中上传参数" + list.toString().substring(1, list.toString().length() - 1));
            OkHttpUtils.post()
                    .url(IUrlConstant.URL_UP_TESTIMAGE)
                    .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                    .addParams("photoIds", list.toString().substring(1, list.toString().length() - 1))
                    .build()
                    .execute(new UpMarkPhotoCallBack());
        } else {
            ToastUtil.showShortToast(TestImageActivity.this, "必須選擇");
        }
    }

    public class UpMarkPhotoCallBack extends Callback<BaseModel> {
        @Override
        public BaseModel parseNetworkResponse(Response response, int id) throws Exception {
            String resultJson = response.body().string();
            LogUtil.e(LogUtil.TAG_WHC, "markPhoto" + resultJson.toString());
            if (!TextUtils.isEmpty(resultJson)) {
                return JSON.parseObject(resultJson, BaseModel.class);
            }
            return null;
        }

        @Override
        public void onError(Call call, Exception e, int id) {
            LogUtil.e(LogUtil.TAG_WHC, "markPhotoError" + e.toString());
        }

        @Override
        public void onResponse(BaseModel response, int id) {
            if (response != null) {
                ToastUtil.showShortToast(TestImageActivity.this, "成功" + response.getIsSucceed());
            }
        }
    }
}
