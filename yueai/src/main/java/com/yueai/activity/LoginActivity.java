package com.yueai.activity;

import android.graphics.Color;
import android.os.Build;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.library.dialog.OnSingleDialogClickListener;
import com.library.utils.DialogUtil;
import com.library.utils.ToastUtil;
import com.library.utils.Util;
import com.yueai.R;
import com.yueai.base.BaseFragmentActivity;
import com.yueai.bean.FindPwdBean;
import com.yueai.bean.LoginEntity;
import com.yueai.constant.IUrlConstant;
import com.yueai.event.FinishEvent;
import com.yueai.utils.AppsflyerUtils;
import com.yueai.utils.CommonRequestUtil;
import com.yueai.xml.PlatformInfoXml;
import com.yueai.xml.UserInfoXml;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.Callback;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import okhttp3.Call;
import okhttp3.Response;

/**
 * Created by Administrator on 2016/10/18.
 */
public class LoginActivity extends BaseFragmentActivity implements View.OnClickListener {
    @BindView(R.id.login_account)
    EditText login_account;
    @BindView(R.id.login_password)
    EditText login_password;
    @BindView(R.id.register)
    TextView register;
    @BindView(R.id.login)
    TextView login;
    @BindView(R.id.forget_password)
    TextView forget_password;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_login;
    }

    @Override
    protected void initViewsAndVariables() {
        // 透明状态栏
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {// 5.0及以上
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            // 设置状态栏背景
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {// 4.4
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        if (UserInfoXml.getUsername() != null) {
            login_account.setText(UserInfoXml.getUsername());
        }
        if (UserInfoXml.getPassword() != null) {
            login_password.setText(UserInfoXml.getPassword());
        }
    }

    @Override
    protected void addListeners() {
        register.setOnClickListener(this);
        login.setOnClickListener(this);
        forget_password.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.register:
                Util.gotoActivity(LoginActivity.this, RegisterActivity.class, false);
                break;

            case R.id.login:
                login();
                break;

            case R.id.forget_password:
                findPwd();
                break;
        }
    }

    private void login() {
        String account = login_account.getText().toString();
        String password = login_password.getText().toString();
        if (TextUtils.isEmpty(account) || TextUtils.isEmpty(password)) {
            ToastUtil.showShortToast(LoginActivity.this, getString(R.string.login_error));
            return;
        }
        showLoading();
        OkHttpUtils.post()
                .url(IUrlConstant.URL_LOGIN)
                .addHeader("token", TextUtils.isEmpty(PlatformInfoXml.getToken()) ? "" : PlatformInfoXml.getToken())
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .addParams("pushToken", PlatformInfoXml.getPushToken())
                .addParams("account", login_account.getText().toString())
                .addParams("password", login_password.getText().toString())
                .build()
                .execute(new LoginCallBack());
    }

    private class LoginCallBack extends Callback<LoginEntity> {
        @Override
        public LoginEntity parseNetworkResponse(Response response, int id) throws Exception {
            String resultJson = response.body().string();
            if (!TextUtils.isEmpty(resultJson)) {
                return JSON.parseObject(resultJson, LoginEntity.class);
            }
            return null;
        }

        @Override
        public void onError(Call call, Exception e, int id) {
            dismissLoading();
            ToastUtil.showShortToast(LoginActivity.this, getString(R.string.login_fail));
        }

        @Override
        public void onResponse(LoginEntity response, int id) {
            if (response != null) {
                if (!TextUtils.isEmpty(response.getMsg())) {
                    ToastUtil.showShortToast(LoginActivity.this, response.getMsg());
                }
                String isSucceed = response.getIsSucceed();
                if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
                    PlatformInfoXml.setToken(response.getToken());
                    UserInfoXml.setUserInfo(response.getUserEnglish());
                    // 设置页眉和互推开关
                    UserInfoXml.setIsShowHeadMsg(response.getIsShowHeadNotice());
                    UserInfoXml.setIsShowHeadMsgNew(response.getIsShowHeadNotice2());
                    UserInfoXml.setIsShowRecommendUser(response.getIsShowCommendUser());
                    // appsflyer登录
                    AppsflyerUtils.login();
                    CommonRequestUtil.initParamsDict(new CommonRequestUtil.OnCommonListener() {
                        @Override
                        public void onSuccess() {
                            gotoMainActivity();
                        }

                        @Override
                        public void onFail() {
                            gotoMainActivity();
                        }
                    });
                } else {
                    dismissLoading();
                }
            } else {
                dismissLoading();
            }
        }
    }

    private void gotoMainActivity() {
        // 发送事件，finish其它Activity
        EventBus.getDefault().post(new FinishEvent());
        dismissLoading();
        Util.gotoActivity(LoginActivity.this, MainActivity.class, true);
    }

    private void findPwd() {
        if (!TextUtils.isEmpty(PlatformInfoXml.getToken()) && !TextUtils.isEmpty(UserInfoXml.getUsername()) && !TextUtils.isEmpty(UserInfoXml.getPassword())) {
            showLoading();
            OkHttpUtils.post()
                    .url(IUrlConstant.URL_FIND_PWD)
                    .addHeader("token", PlatformInfoXml.getToken())
                    .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                    .build()
                    .execute(new FindPwdCallBack());
        } else {
            ToastUtil.showShortToast(LoginActivity.this, getString(R.string.never_login));
        }
    }

    public class FindPwdCallBack extends Callback<FindPwdBean> {
        @Override
        public FindPwdBean parseNetworkResponse(Response response, int id) throws Exception {
            String resultJson = response.body().string();
            if (!TextUtils.isEmpty(resultJson)) {
                return JSON.parseObject(resultJson, FindPwdBean.class);
            }
            return null;
        }

        @Override
        public void onError(Call call, Exception e, int id) {
            dismissLoading();
        }

        @Override
        public void onResponse(FindPwdBean response, int id) {
            if (response != null) {
                if (response.getAccount() != null && response.getPassword() != null) {
                    DialogUtil.showSingleBtnDialog(getSupportFragmentManager(), getString(R.string.find_pwd), getString(R.string.account) + response.getAccount() + "\n" + getString(R.string.password) + response.getPassword(), getString(R.string.sure), true, new OnSingleDialogClickListener() {
                        @Override
                        public void onPositiveClick(View view) {

                        }
                    });
                }
            }
            dismissLoading();
        }
    }

    @Override
    protected void doRegister() {
        super.doRegister();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void unregister() {
        super.unregister();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onEvent(FinishEvent event) {
        finish();
    }

}
