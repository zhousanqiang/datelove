package com.yueai.activity;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.library.dialog.OnCheckBoxDoubleDialogClickListener;
import com.library.dialog.OnEditDoubleDialogClickListener;
import com.library.dialog.OneWheelDialog;
import com.library.dialog.ThreeWheelDialog;
import com.library.utils.DialogUtil;
import com.library.utils.HeightUtils;
import com.library.utils.Util;
import com.yueai.R;
import com.yueai.base.BaseTitleActivity;
import com.yueai.bean.ChangeMyInfoBean;
import com.yueai.bean.User;
import com.yueai.constant.CommonData;
import com.yueai.event.UpdateUserInfoEvent;
import com.yueai.utils.CommonRequestUtil;
import com.yueai.utils.ParamsUtils;
import com.yueai.xml.UserInfoXml;

import org.greenrobot.eventbus.EventBus;

import java.util.List;
import java.util.Map;

import butterknife.BindView;

/**
 * 个人资料页
 * Modified by zhangdroid on 2017/03/11.
 */
public class MyInfoActivity extends BaseTitleActivity implements View.OnClickListener {
    @BindView(R.id.myInfo_progress)
    ProgressBar mPbInfoProgress;
    @BindView(R.id.myInfo_precent)
    TextView mTvInfoPrecent;
    @BindView(R.id.myInfo_monologue_count)
    TextView mTvMonologueCount;
    @BindView(R.id.myInfo_monologue_et)
    EditText mEtMonologue;

    // 基本资料项
    @BindView(R.id.myInfo_nickname_ll)
    LinearLayout mLlNickname;
    @BindView(R.id.myInfo_nickname)
    TextView mTvNickname;
    @BindView(R.id.myInfo_birthday_ll)
    LinearLayout mLlBirthday;
    @BindView(R.id.myInfo_birthday)
    TextView mTvBirtyday;
    @BindView(R.id.meInfo_sign_ll)
    LinearLayout mLlSign;
    @BindView(R.id.meInfo_sign)
    TextView mTvSign;
    @BindView(R.id.meInfo_area_ll)
    LinearLayout meArea;
    @BindView(R.id.meInfo_area)
    TextView mTvArea;
    @BindView(R.id.myInfo_income_ll)
    LinearLayout mLlIncome;
    @BindView(R.id.myInfo_income)
    TextView mTvIncome;
    @BindView(R.id.myInfo_height_ll)
    LinearLayout mLlHeight;
    @BindView(R.id.myInfo_height)
    TextView mTvHeight;
    @BindView(R.id.myInfo_work_ll)
    LinearLayout mLlWork;
    @BindView(R.id.myInfo_work)
    TextView mTvWork;
    @BindView(R.id.myInfo_education_ll)
    LinearLayout mLlEducation;
    @BindView(R.id.myInfo_education)
    TextView mTvEducation;
    @BindView(R.id.myInfo_marriage_ll)
    LinearLayout mLlMarriage;
    @BindView(R.id.myInfo_marriage)
    TextView mTvMarriage;
    @BindView(R.id.myInfo_wantBaby_ll)
    LinearLayout mLlWantBaby;
    @BindView(R.id.myInfo_wantBaby)
    TextView mTvWantBaby;

    // 扩展资料项
    @BindView(R.id.myInfo_sport_ll)
    LinearLayout mLlSport;
    @BindView(R.id.myInfo_sport)
    TextView mTvSport;
    @BindView(R.id.myInfo_personal_ll)
    LinearLayout mLlPersonal;
    @BindView(R.id.myInfo_personal)
    TextView mTvPersonal;
    @BindView(R.id.myInfo_wantToGo_ll)
    LinearLayout mLlWantToGo;
    @BindView(R.id.myInfo_wantToGo)
    TextView mTvWantToGo;
    @BindView(R.id.myInfo_bookCartoonFilm_ll)
    LinearLayout mLlBookCartoonFilm;
    @BindView(R.id.myInfo_bookCartoonFilm)
    TextView mTvBookCartoonFilm;
    @BindView(R.id.myInfo_hobby_ll)
    LinearLayout mLlHobby;
    @BindView(R.id.myInfo_hobby)
    TextView mTvHobby;
    @BindView(R.id.myInfo_save)
    Button mBtnSave;

    // 临时保存修改后的资料项
    private ChangeMyInfoBean mChangeMyInfoBean = new ChangeMyInfoBean();

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_my_info;
    }

    @Override
    protected String getCenterTitle() {
        return getString(R.string.my_info);
    }

    @Override
    protected void initViewsAndVariables() {
        showLoading();
        setContent();
        CommonRequestUtil.loadUserInfo(new CommonRequestUtil.OnLoadUserInfoListener() {
            @Override
            public void onSuccess(User user) {
                if (user != null) {
                    setContent();
                }
                dismissLoading();
            }

            @Override
            public void onFail() {
                dismissLoading();
            }
        });
    }

    @Override
    protected void addListeners() {
        mLlNickname.setOnClickListener(this);
        mLlBirthday.setOnClickListener(this);
        meArea.setOnClickListener(this);
        mLlIncome.setOnClickListener(this);
        mLlHeight.setOnClickListener(this);
        mLlWork.setOnClickListener(this);
        mLlEducation.setOnClickListener(this);
        mLlMarriage.setOnClickListener(this);
        mLlWantBaby.setOnClickListener(this);
        mLlSport.setOnClickListener(this);
        mLlPersonal.setOnClickListener(this);
        mLlWantToGo.setOnClickListener(this);
        mLlBookCartoonFilm.setOnClickListener(this);
        mLlHobby.setOnClickListener(this);
        mBtnSave.setOnClickListener(this);
        mEtMonologue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String content = mEtMonologue.getText().toString();
                mTvMonologueCount.setText(content.length() + "/120");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void setContent() {
        // 计算当前用户填写的资料项数
        int i = 0;
        String monologue = Util.convertText(UserInfoXml.getMonologue());
        if (!TextUtils.isEmpty(monologue)) {
            mEtMonologue.setText(monologue);
            mTvMonologueCount.setText(monologue.length() + "/120");
            i++;
        }
        String nickname = UserInfoXml.getNickName();
        if (!TextUtils.isEmpty(nickname)) {
            mTvNickname.setText(nickname);
            if (!nickname.equals(getString(R.string.male)) && !nickname.equals(getString(R.string.female))) {
                i++;
            }
        }
        String birthday = UserInfoXml.getBirthday();
        if (!TextUtils.isEmpty(birthday)) {
            mTvBirtyday.setText(birthday);
            i++;
        }
        String constellation = UserInfoXml.getConstellation();
        if (!isTextEmpty(constellation)) {
            mTvSign.setText(ParamsUtils.getConstellationMap().get(constellation));
            i++;
        }
        String area = UserInfoXml.getProvinceName();
        if (!TextUtils.isEmpty(area)) {
            mTvArea.setText(area);
            i++;
        }
        String income = UserInfoXml.getIncome();
        if (!isTextEmpty(income)) {
            mTvIncome.setText("NT$" + ParamsUtils.getIncomeMap().get(income));
            i++;
        }
        String height = UserInfoXml.getHeight();
        if (!isTextEmpty(height)) {
            mTvHeight.setText(HeightUtils.getInchCmByCm(height));
            i++;
        }
        String work = UserInfoXml.getWork();
        if (!isTextEmpty(work)) {
            mTvWork.setText(ParamsUtils.getWorkMap().get(work));
            i++;
        }
        String education = UserInfoXml.getEducation();
        if (!isTextEmpty(education)) {
            mTvEducation.setText(ParamsUtils.getEducationMap().get(education));
            i++;
        }
        String marriage = UserInfoXml.getMarriage();
        if (!isTextEmpty(marriage)) {
            mTvMarriage.setText(ParamsUtils.getMarriageMap().get(marriage));
            i++;
        }
        String wantBaby = UserInfoXml.getWantBaby();
        if (!isTextEmpty(wantBaby)) {
            mTvWantBaby.setText(ParamsUtils.getWantBabyMap().get(wantBaby));
            i++;
        }

        String sport = UserInfoXml.getSport();
        if (!TextUtils.isEmpty(sport)) {
            mTvSport.setText(getValue(UserInfoXml.getSport(), ParamsUtils.getSportMap()));
            i++;
        }
        String personal = UserInfoXml.getPersonal();
        if (!TextUtils.isEmpty(personal)) {
            mTvPersonal.setText(getValue(personal, ParamsUtils.getPersonalMap()));
            i++;
        }
        String travel = UserInfoXml.getTravel();
        if (!TextUtils.isEmpty(travel)) {
            mTvWantToGo.setText(getValue(travel, ParamsUtils.getTravelMap()));
            i++;
        }
        String bookCartoon = UserInfoXml.getBookCartoon();
        if (!TextUtils.isEmpty(bookCartoon)) {
            mTvBookCartoonFilm.setText(getValue(bookCartoon, ParamsUtils.getBookCartoonMap()));
            i++;
        }
        String hobby = UserInfoXml.getInterest();
        if (!TextUtils.isEmpty(hobby)) {
            mTvHobby.setText(getValue(hobby, ParamsUtils.getInterestMap()));
            i++;
        }

        int progress = (int) ((i / 16.0f) * 100);
        mPbInfoProgress.setProgress(progress);
        mTvInfoPrecent.setText(getString(R.string.level, progress + "%"));
    }

    private boolean isTextEmpty(String text) {
        return TextUtils.isEmpty(text) || "0".equals(text) || "null".equals(text) || "".equals(text);
    }

    private String getValue(String key, Map<String, String> map) {
        if (key != null) {
            String[] spit = key.split("\\|");
            StringBuffer value = new StringBuffer();
            for (int i = 0; i < spit.length; i++) {
                if (i < spit.length - 1) {
                    value.append(map.get(spit[i]) + ",");
                } else {
                    value.append(map.get(spit[i]) + "");
                }
            }
            return value.toString();
        }
        return null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.myInfo_sport_ll:
                DialogUtil.showCheckBoxDoubleBtnDialog(getSupportFragmentManager(), getString(R.string.sport), getString(R.string.sure), getString(R.string.cancel),
                        false, ParamsUtils.getSportMapValue(), new OnCheckBoxDoubleDialogClickListener() {

                            @Override
                            public void onPositiveClick(View view, List<String> list) {
                                if (!Util.isListEmpty(list)) {
                                    mTvSport.setText(list.toString().substring(1, list.toString().length() - 1));
                                    StringBuilder stringBuilder = new StringBuilder();
                                    for (int i = 0; i < list.size(); i++) {
                                        if (i == list.size() - 1) {
                                            stringBuilder.append(ParamsUtils.getSportMapKeyByValue(list.get(i)));
                                        } else {
                                            stringBuilder.append(ParamsUtils.getSportMapKeyByValue(list.get(i)))
                                                    .append("|");
                                        }
                                    }
                                    mChangeMyInfoBean.setSport(stringBuilder.toString());
                                }
                            }

                            @Override
                            public void onNegativeClick(View view) {

                            }
                        });
                break;

            case R.id.myInfo_personal_ll:
                DialogUtil.showCheckBoxDoubleBtnDialog(getSupportFragmentManager(), getString(R.string.personal), getString(R.string.sure), getString(R.string.cancel),
                        false, ParamsUtils.getPersonalMapValue(), new OnCheckBoxDoubleDialogClickListener() {
                            @Override
                            public void onPositiveClick(View view, List<String> list) {
                                if (!Util.isListEmpty(list)) {
                                    mTvPersonal.setText(list.toString().substring(1, list.toString().length() - 1));
                                    StringBuilder stringBuilder = new StringBuilder();
                                    for (int i = 0; i < list.size(); i++) {
                                        if (i == list.size() - 1) {
                                            stringBuilder.append(ParamsUtils.getPersonalMapKeyByValue(list.get(i)));
                                        } else {
                                            stringBuilder.append(ParamsUtils.getPersonalMapKeyByValue(list.get(i)))
                                                    .append("|");
                                        }
                                    }
                                    mChangeMyInfoBean.setPersonal(stringBuilder.toString());
                                }
                            }

                            @Override
                            public void onNegativeClick(View view) {

                            }
                        });
                break;

            case R.id.myInfo_wantToGo_ll:
                DialogUtil.showCheckBoxDoubleBtnDialog(getSupportFragmentManager(), getString(R.string.want_to_go), getString(R.string.sure), getString(R.string.cancel),
                        false, ParamsUtils.getTravelMapValue(), new OnCheckBoxDoubleDialogClickListener() {
                            @Override
                            public void onPositiveClick(View view, List<String> list) {
                                if (!Util.isListEmpty(list)) {
                                    mTvWantToGo.setText(list.toString().substring(1, list.toString().length() - 1));
                                    StringBuilder stringBuilder = new StringBuilder();
                                    for (int i = 0; i < list.size(); i++) {
                                        if (i == list.size() - 1) {
                                            stringBuilder.append(ParamsUtils.getTravelMapKeyByValue(list.get(i)));
                                        } else {
                                            stringBuilder.append(ParamsUtils.getTravelMapKeyByValue(list.get(i)))
                                                    .append("|");
                                        }
                                    }
                                    mChangeMyInfoBean.setWantToGo(stringBuilder.toString());
                                }
                            }

                            @Override
                            public void onNegativeClick(View view) {

                            }
                        });
                break;

            case R.id.myInfo_bookCartoonFilm_ll:
                DialogUtil.showCheckBoxDoubleBtnDialog(getSupportFragmentManager(), getString(R.string.book_cartoon_film), getString(R.string.sure), getString(R.string.cancel),
                        false, ParamsUtils.getBookCartoonMapValue(), new OnCheckBoxDoubleDialogClickListener() {
                            @Override
                            public void onPositiveClick(View view, List<String> list) {
                                if (!Util.isListEmpty(list)) {
                                    mTvBookCartoonFilm.setText(list.toString().substring(1, list.toString().length() - 1));
                                    StringBuilder stringBuilder = new StringBuilder();
                                    for (int i = 0; i < list.size(); i++) {
                                        if (i == list.size() - 1) {
                                            stringBuilder.append(ParamsUtils.getBookCartoonMapKeyByValue(list.get(i)));
                                        } else {
                                            stringBuilder.append(ParamsUtils.getBookCartoonMapKeyByValue(list.get(i)))
                                                    .append("|");
                                        }
                                    }
                                    mChangeMyInfoBean.setBookCartoon(stringBuilder.toString());
                                }
                            }

                            @Override
                            public void onNegativeClick(View view) {

                            }
                        });
                break;

            case R.id.myInfo_hobby_ll:
                DialogUtil.showCheckBoxDoubleBtnDialog(getSupportFragmentManager(), getString(R.string.hobby), getString(R.string.sure), getString(R.string.cancel),
                        false, ParamsUtils.getInterestMapValue(), new OnCheckBoxDoubleDialogClickListener() {
                            @Override
                            public void onPositiveClick(View view, List<String> list) {
                                if (!Util.isListEmpty(list)) {
                                    mTvHobby.setText(list.toString().substring(1, list.toString().length() - 1));
                                    StringBuilder stringBuilder = new StringBuilder();
                                    for (int i = 0; i < list.size(); i++) {
                                        if (i == list.size() - 1) {
                                            stringBuilder.append(ParamsUtils.getInterestMapKeyByValue(list.get(i)));
                                        } else {
                                            stringBuilder.append(ParamsUtils.getInterestMapKeyByValue(list.get(i)))
                                                    .append("|");
                                        }
                                    }
                                    mChangeMyInfoBean.setInterest(stringBuilder.toString());
                                }
                            }

                            @Override
                            public void onNegativeClick(View view) {

                            }
                        });
                break;

            case R.id.myInfo_nickname_ll:
                DialogUtil.showEditDoubleBtnDialog(getSupportFragmentManager(), getString(R.string.nickname), getString(R.string.nick_name_hint), getString(R.string.sure), getString(R.string.cancel),
                        false, new OnEditDoubleDialogClickListener() {
                            @Override
                            public void onPositiveClick(View view, String content) {
                                if (!TextUtils.isEmpty(content)) {
                                    mTvNickname.setText(content);
                                    mChangeMyInfoBean.setNickname(content);
                                }
                            }

                            @Override
                            public void onNegativeClick(View view) {

                            }
                        });
                break;
            case R.id.myInfo_birthday_ll:
                DialogUtil.showDateSelectDialog(getSupportFragmentManager(), getString(R.string.birthday), getString(R.string.sure), getString(R.string.cancel),
                        false, new ThreeWheelDialog.OnThreeWheelDialogClickListener() {
                            @Override
                            public void onPositiveClick(View view, String selectedText1, String selectedText2, String selectedText3) {
                                if (selectedText1 != null && selectedText2 != null && selectedText3 != null) {
                                    mTvBirtyday.setText(selectedText1 + "-" + selectedText2 + "-" + selectedText3);
                                    mChangeMyInfoBean.setBirthday(selectedText1 + "-" + selectedText2 + "-" + selectedText3);
                                }
                            }

                            @Override
                            public void onNegativeClick(View view) {

                            }
                        });
                break;
            case R.id.meInfo_area_ll:
                String area = mTvArea.getText().toString();
                DialogUtil.showSingleWheelDialog(getSupportFragmentManager(), CommonData.getProvinceList(), TextUtils.isEmpty(area) ? 0 : CommonData.getProvinceList().indexOf(area),
                        getString(R.string.area), getString(R.string.sure), getString(R.string.cancel), false, new OneWheelDialog.OnOneWheelDialogClickListener() {
                            @Override
                            public void onPositiveClick(View view, String selectedText) {
                                if (!TextUtils.isEmpty(selectedText)) {
                                    mTvArea.setText(selectedText);
                                    mChangeMyInfoBean.setArea(selectedText);
                                }
                            }

                            @Override
                            public void onNegativeClick(View view) {

                            }
                        });
                break;
            case R.id.myInfo_income_ll:
                String income = mTvIncome.getText().toString();
                if (!TextUtils.isEmpty(income) && income.contains("NT$")) {
                    income = income.replace("NT$", "");
                }
                DialogUtil.showSingleWheelDialog(getSupportFragmentManager(), ParamsUtils.getIncomeMapValue(), TextUtils.isEmpty(income) ? 0 : ParamsUtils.getIncomeMapValue().indexOf(income),
                        getString(R.string.income), getString(R.string.sure), getString(R.string.cancel), false, new OneWheelDialog.OnOneWheelDialogClickListener() {
                            @Override
                            public void onPositiveClick(View view, String selectedText) {
                                if (!TextUtils.isEmpty(selectedText)) {
                                    mTvIncome.setText("NT$" + selectedText);
                                    mChangeMyInfoBean.setIncome(ParamsUtils.getIncomeMapKeyByValue(selectedText));
                                }
                            }

                            @Override
                            public void onNegativeClick(View view) {

                            }
                        });
                break;
            case R.id.myInfo_height_ll:
                String height = mTvHeight.getText().toString();
                DialogUtil.showSingleWheelDialog(getSupportFragmentManager(), HeightUtils.getInchCmList(), TextUtils.isEmpty(height) ? 0 : HeightUtils.getInchCmList().indexOf(height),
                        getString(R.string.height), getString(R.string.sure), getString(R.string.cancel), false, new OneWheelDialog.OnOneWheelDialogClickListener() {
                            @Override
                            public void onPositiveClick(View view, String selectedText) {
                                if (!TextUtils.isEmpty(selectedText)) {
                                    mTvHeight.setText(selectedText);
                                    mChangeMyInfoBean.setHeight(HeightUtils.getCmByInchCm(selectedText));
                                }
                            }

                            @Override
                            public void onNegativeClick(View view) {

                            }
                        });
                break;
            case R.id.myInfo_work_ll:
                String work = mTvWork.getText().toString();
                DialogUtil.showSingleWheelDialog(getSupportFragmentManager(), ParamsUtils.getWorkMapValue(), TextUtils.isEmpty(work) ? 0 : ParamsUtils.getWorkMapValue().indexOf(work),
                        getString(R.string.work), getString(R.string.sure), getString(R.string.cancel), false, new OneWheelDialog.OnOneWheelDialogClickListener() {
                            @Override
                            public void onPositiveClick(View view, String selectedText) {
                                if (!TextUtils.isEmpty(selectedText)) {
                                    mTvWork.setText(selectedText);
                                    mChangeMyInfoBean.setWork(ParamsUtils.getWorkMapKeyByValue(selectedText));
                                }
                            }

                            @Override
                            public void onNegativeClick(View view) {

                            }
                        });
                break;
            case R.id.myInfo_education_ll:
                String education = mTvEducation.getText().toString();
                DialogUtil.showSingleWheelDialog(getSupportFragmentManager(), ParamsUtils.getEducationMapValue(), TextUtils.isEmpty(education) ? 0 : ParamsUtils.getEducationMapValue().indexOf(education),
                        getString(R.string.education), getString(R.string.sure), getString(R.string.cancel), false, new OneWheelDialog.OnOneWheelDialogClickListener() {
                            @Override
                            public void onPositiveClick(View view, String selectedText) {
                                if (!TextUtils.isEmpty(selectedText)) {
                                    mTvEducation.setText(selectedText);
                                    mChangeMyInfoBean.setEducation(ParamsUtils.getEducationMapKeyByValue(selectedText));
                                }
                            }

                            @Override
                            public void onNegativeClick(View view) {

                            }
                        });
                break;
            case R.id.myInfo_marriage_ll:
                String marriage = mTvMarriage.getText().toString();
                DialogUtil.showSingleWheelDialog(getSupportFragmentManager(), ParamsUtils.getMarriageMapValue(), TextUtils.isEmpty(marriage) ? 0 : ParamsUtils.getMarriageMapValue().indexOf(marriage),
                        getString(R.string.marriage), getString(R.string.sure), getString(R.string.cancel), false, new OneWheelDialog.OnOneWheelDialogClickListener() {
                            @Override
                            public void onPositiveClick(View view, String selectedText) {
                                if (!TextUtils.isEmpty(selectedText)) {
                                    mTvMarriage.setText(selectedText);
                                    mChangeMyInfoBean.setMarriage(ParamsUtils.getMarriageMapKeyByValue(selectedText));
                                }
                            }

                            @Override
                            public void onNegativeClick(View view) {

                            }
                        });
                break;
            case R.id.myInfo_wantBaby_ll:
                String wantBaby = mTvWantBaby.getText().toString();
                DialogUtil.showSingleWheelDialog(getSupportFragmentManager(), ParamsUtils.getWantBabyMapValue(), TextUtils.isEmpty(wantBaby) ? 0 : ParamsUtils.getWantBabyMapValue().indexOf(wantBaby),
                        getString(R.string.wantBaby), getString(R.string.sure), getString(R.string.cancel), false, new OneWheelDialog.OnOneWheelDialogClickListener() {
                            @Override
                            public void onPositiveClick(View view, String selectedText) {
                                if (!TextUtils.isEmpty(selectedText)) {
                                    mTvWantBaby.setText(selectedText);
                                    mChangeMyInfoBean.setWantBaby(ParamsUtils.getWantBabyMapKeyByValue(selectedText));
                                }
                            }

                            @Override
                            public void onNegativeClick(View view) {

                            }
                        });
                break;

            case R.id.myInfo_save:
                uploadMyInfo();
                break;
        }
    }

    private void uploadMyInfo() {
        if (mChangeMyInfoBean != null) {
            showLoading();
            String monologue = mEtMonologue.getText().toString();
            if (!TextUtils.isEmpty(monologue) && !monologue.equals(UserInfoXml.getMonologue())) {
                UserInfoXml.setMonologue(monologue);
            }
            String nickname = mChangeMyInfoBean.getNickname();
            if (!TextUtils.isEmpty(nickname) && !nickname.equals(UserInfoXml.getNickName())) {
                UserInfoXml.setNickName(nickname);
            }
            String birthday = mChangeMyInfoBean.getBirthday();
            if (!TextUtils.isEmpty(birthday) && !birthday.equals(UserInfoXml.getBirthday())) {
                UserInfoXml.setBirthday(birthday);
            }
            String sign = mChangeMyInfoBean.getSign();
            if (!TextUtils.isEmpty(sign) && !sign.equals(UserInfoXml.getConstellation())) {
                UserInfoXml.setConstellation(sign);
            }
            String area = mChangeMyInfoBean.getArea();
            if (!TextUtils.isEmpty(area) && !area.equals(UserInfoXml.getProvinceName())) {
                UserInfoXml.setProvinceName(area);
            }
            String income = mChangeMyInfoBean.getIncome();
            if (!TextUtils.isEmpty(income) && !income.equals(UserInfoXml.getIncome())) {
                UserInfoXml.setIncome(income);
            }
            String height = mChangeMyInfoBean.getHeight();
            if (!TextUtils.isEmpty(height) && !height.equals(UserInfoXml.getHeight())) {
                UserInfoXml.setHeight(height);
            }
            String work = mChangeMyInfoBean.getWork();
            if (!TextUtils.isEmpty(work) && !work.equals(UserInfoXml.getWork())) {
                UserInfoXml.setWork(work);
            }
            String education = mChangeMyInfoBean.getEducation();
            if (!TextUtils.isEmpty(education) && !education.equals(UserInfoXml.getEducation())) {
                UserInfoXml.setEducation(education);
            }
            String marriage = mChangeMyInfoBean.getMarriage();
            if (!TextUtils.isEmpty(marriage) && !marriage.equals(UserInfoXml.getMarriage())) {
                UserInfoXml.setMarriage(marriage);
            }
            String wantBaby = mChangeMyInfoBean.getWantBaby();
            if (!TextUtils.isEmpty(wantBaby) && !wantBaby.equals(UserInfoXml.getWantBaby())) {
                UserInfoXml.setWantBaby(wantBaby);
            }
            String sport = mChangeMyInfoBean.getSport();
            if (!TextUtils.isEmpty(sport) && !sport.equals(UserInfoXml.getSport())) {
                UserInfoXml.setSport(sport);
            }
            String personal = mChangeMyInfoBean.getPersonal();
            if (!TextUtils.isEmpty(personal) && !personal.equals(UserInfoXml.getPersonal())) {
                UserInfoXml.setPersonal(personal);
            }
            String wantToGo = mChangeMyInfoBean.getWantToGo();
            if (!TextUtils.isEmpty(wantToGo) && !wantToGo.equals(UserInfoXml.getTravel())) {
                UserInfoXml.setTravel(wantToGo);
            }
            String bookCartoon = mChangeMyInfoBean.getBookCartoon();
            if (!TextUtils.isEmpty(bookCartoon) && !bookCartoon.equals(UserInfoXml.getBookCartoon())) {
                UserInfoXml.setBookCartoon(bookCartoon);
            }
            String hobby = mChangeMyInfoBean.getInterest();
            if (!TextUtils.isEmpty(hobby) && !hobby.equals(UserInfoXml.getInterest())) {
                UserInfoXml.setInterest(hobby);
            }
            CommonRequestUtil.uploadUserInfo(true, new CommonRequestUtil.OnCommonListener() {
                @Override
                public void onSuccess() {
                    // 发送事件，更新用户信息
                    EventBus.getDefault().post(new UpdateUserInfoEvent());
                    setContent();
                    dismissLoading();
                }

                @Override
                public void onFail() {
                    dismissLoading();
                }
            });
        }
    }

}
