package com.yueai.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.library.adapter.recyclerview.CommonRecyclerViewAdapter;
import com.library.adapter.recyclerview.RecyclerViewHolder;
import com.library.imageloader.CircleTransformation;
import com.library.imageloader.ImageLoader;
import com.library.imageloader.ImageLoaderUtil;
import com.library.utils.HeightUtils;
import com.library.utils.SharedPreferenceUtil;
import com.library.utils.Util;
import com.library.widgets.CircleImageView;
import com.library.widgets.CollapsingTextView;
import com.yueai.R;
import com.yueai.bean.FateUser;
import com.yueai.bean.HeadMsgNotice;
import com.yueai.bean.Image;
import com.yueai.bean.MsgBox;
import com.yueai.bean.NewHeadMsgNotice;
import com.yueai.bean.RecommendUser;
import com.yueai.bean.User;
import com.yueai.bean.UserBase;
import com.yueai.bean.UserDetail;
import com.yueai.constant.IConfigConstant;
import com.yueai.constant.IUrlConstant;
import com.yueai.dialog.ReportDialog;
import com.yueai.event.HeadMsgEvent;
import com.yueai.event.MessageChangedEvent;
import com.yueai.event.SayHelloEvent;
import com.yueai.utils.CommonRequestUtil;
import com.yueai.utils.ParamsUtils;
import com.yueai.utils.Utils;
import com.yueai.xml.PlatformInfoXml;
import com.yueai.xml.UserInfoXml;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.Callback;
import com.zhy.http.okhttp.callback.StringCallback;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;
import okhttp3.Response;

/**
 * 用户详情页
 * Created by zhangdroid on 2016/10/22.
 */
public class UserInfoDetailActivity extends AppCompatActivity {
    public static final String INTENT_KEY_UID = "intent_key_uId";
    public static final String INTENT_KEY_ISRECORD = "intent_key_isRecord";
    public static final String INTENT_KEY_SOURCETAG = "intent_key_sourceTag";
    /**
     * 缘分页面
     */
    public static final String SOURCE_FROM_FATE = "1";
    /**
     * 搜索页面
     */
    public static final String SOURCE_FROM_SEARCH = "2";
    /**
     * 聊天列表页面
     */
    public static final String SOURCE_FROM_CHAT_LIST = "3";
    /**
     * 聊天内容页面
     */
    public static final String SOURCE_FROM_CHAT_CONTENT = "4";
    /**
     * 附近页面
     */
    public static final String SOURCE_FROM_NEARBY = "5";
    /**
     * 谁看过我
     */
    public static final String SOURCE_FROM_SEEME = "6";
    /**
     * 关注
     */
    public static final String SOURCE_CARE = "7";
    /**
     * 頁眉
     */
    public static final String SOURCE_HEAD_MSG = "8";
    /**
     * 推荐用户
     */
    public static final String SOURCE_FROM_RECOMMEND_USER = "9";

    @BindView(R.id.userDetail_CollapsingToolbarLayout)
    CollapsingToolbarLayout mCollapsingToolbarLayout;
    @BindView(R.id.userDetail_toolbar)
    Toolbar mToolbar;
    // FAB 下一位用户
    @BindView(R.id.userDetail_fab_next)
    FloatingActionButton mFabNext;
    // 用户头像和照片墙
    @BindView(R.id.userDetail_avatar)
    ImageView mIvUserAvatar;
    @BindView(R.id.userDetail_pic_wall)
    RecyclerView mPicRecyclerView;
    // 展示的基本信息
    @BindView(R.id.userDetail_base_info)
    TextView mTvBaseInfo;

    // 关注、打招呼和聊天
    @BindView(R.id.userDetail_follow)
    Button mBtnFollow;
    @BindView(R.id.userDetail_sayHello)
    Button mBtnSayHello;
    @BindView(R.id.userDetail_chat)
    Button mBtnChat;
    // 内心独白
    @BindView(R.id.userDetail_monologue_ll)
    LinearLayout mLlMonologue;
    @BindView(R.id.userDetail_monologue)
    CollapsingTextView mMonologue;
    // 用户个人资料
    // 地区
    @BindView(R.id.userDetail_area_ll)
    LinearLayout mLlArea;
    @BindView(R.id.userDetail_area)
    TextView mTvArea;
    @BindView(R.id.userDetail_area_divider)
    View mAreaDivider;
    // 星座
    @BindView(R.id.userDetail_sign_ll)
    LinearLayout mLlSign;
    @BindView(R.id.userDetail_sign)
    TextView mTvSign;
    @BindView(R.id.userDetail_sign_divider)
    View mSignDivider;
    // 学历
    @BindView(R.id.userDetail_education_ll)
    LinearLayout mLlEducation;
    @BindView(R.id.userDetail_education)
    TextView mTvEducation;
    @BindView(R.id.userDetail_education_divider)
    View mEducationDivider;
    // 收入
    @BindView(R.id.userDetail_income_ll)
    LinearLayout mLlIncome;
    @BindView(R.id.userDetail_income)
    TextView mTvIncome;
    @BindView(R.id.userDetail_income_divider)
    View mIncomeDivider;
    // 职业
    @BindView(R.id.userDetail_occupation_ll)
    LinearLayout mLlOccupation;
    @BindView(R.id.userDetail_occupation)
    TextView mTvOccupation;
    @BindView(R.id.userDetail_occupation_divider)
    View mOccupationDivider;
    // 是否想要小孩
    @BindView(R.id.userDetail_wantBaby_ll)
    LinearLayout mLlWantBaby;
    @BindView(R.id.userDetail_wantBaby)
    TextView mTvWantBaby;
    @BindView(R.id.userDetail_want_baby_divider)
    View mWantBabyDivider;
    // 喜欢的运动
    @BindView(R.id.userDetail_sport_ll)
    LinearLayout mLlSport;
    @BindView(R.id.userDetail_sport)
    TextView mTvSport;
    @BindView(R.id.userDetail_sport_divider)
    View mSportDivider;
    // 个性特征
    @BindView(R.id.userDetail_personal_ll)
    LinearLayout mLlPersonal;
    @BindView(R.id.userDetail_personal)
    TextView mTvPersonal;
    @BindView(R.id.userDetail_personal_divider)
    View mPersonalDivider;
    // 旅行
    @BindView(R.id.userDetail_travel_ll)
    LinearLayout mLlTravel;
    @BindView(R.id.userDetail_travel)
    TextView mTvTravel;
    @BindView(R.id.userDetail_travel_divider)
    View mTravelDivider;
    // 喜欢的书和动漫
    @BindView(R.id.userDetail_book_cartoon_ll)
    LinearLayout mLlBookCartoon;
    @BindView(R.id.userDetail_book_cartoon)
    TextView mTvBookCartoon;
    @BindView(R.id.userDetail_book_cartoon_divider)
    View mBookCartoonDivider;
    // 兴趣爱好
    @BindView(R.id.userDetail_hobby_ll)
    LinearLayout mLlHobby;
    @BindView(R.id.userDetail_hobby)
    TextView mTvHobby;
    @BindView(R.id.userDetail_hobby_divider)
    View mHobbyDivider;

    // 页眉
    @BindView(R.id.head_msg_container)
    RelativeLayout mRlHeadMsgContainer;
    @BindView(R.id.head_msg_content)
    TextView mTvHeadMsgContent;
    @BindView(R.id.head_msg_clear)
    ImageView mIvHeadMsgClear;
    // 新页眉

    @BindView(R.id.headMsg_unread)
    RelativeLayout mRlHeadMsgUnread;
    @BindView(R.id.headMsg_avatar)
    CircleImageView mIvHeadMsgAvatar;
    @BindView(R.id.headMsg_nickname)
    TextView mTvNickname;
    @BindView(R.id.headMsg_age)
    TextView mTvAge;
    @BindView(R.id.headMsg_height)
    TextView mTvHeight;
    @BindView(R.id.headMsg_content)
    TextView mTvMsgContent;
    @BindView(R.id.headMsg_check)
    TextView mTvMsgCheck;
    // 访客
    @BindView(R.id.headMsg_visitor)
    RelativeLayout mRlHeadMsgVisitor;
    @BindView(R.id.visitor_avatar)
    CircleImageView mIvVisitorAvatar;
    @BindView(R.id.visitor_content)
    TextView mTvVisitorContent;
    @BindView(R.id.visitor_ignore)
    TextView mTvVisitorIgnore;
    @BindView(R.id.visitor_look)
    TextView mTvVisitorLook;
    // 推荐用户
    @BindView(R.id.recommend_user_container)
    LinearLayout mLlRecommendUser;
    @BindView(R.id.recommend_user_avatar)
    CircleImageView mIvRecommendAvatar;
    @BindView(R.id.recommend_user_content)
    TextView mTvRecommendContent;
    @BindView(R.id.recommend_user_ignore)
    TextView mTvRecommendIgnore;
    @BindView(R.id.recommend_user_look)
    TextView mTvRecommendLook;
    @BindView(R.id.headMsg_container)
    FrameLayout mFlHeadMsgNew;

    private String isRecord;
    private String sourceTag;
    private String uId;
    private String nickname;
    private String avatarUrl;
    /**
     * 进度加载对话框
     */
    private Dialog mLoadingDialog;
    private ImageWallAdapter mImageWallAdapter;
    private int mIsFollow;
    private int mIsSayHello;
    private int pageNum = 0;
    private final String pageSize = "1";

    private static final int HEAD_MSG_REFRESH_PERIOD = 20 * 1000;
    private static final int MSG_TYPE_LOAD_DATA = 1;
    private static final int MSG_TYPE_HEAD_MSG = 2;
    private static final int MSG_TYPE_HEAD_MSG_NEW = 3;
    private static final int MSG_TYPE_RECOMMEND_USER = 4;
    private static final int MSG_TYPE_SHOW_UNREAD_MSG = 5;
    private static final int MSG_TYPE_DISMISS_UNREAD_MSG = 6;
    private static final int MSG_TYPE_DISMISS_VISITOR = 7;
    private static final int MSG_TYPE_HIDE_RECOMMEND_USER = 8;

    /**
     * 页眉对象缓存
     */
    private HeadMsgNotice mHeadMsgNotice;
    /**
     * 新页眉对象缓存
     */
    private NewHeadMsgNotice mNewHeadMsgNotice;
    /**
     * 推荐用户对象缓存
     */
    private RecommendUser mRecommendUser;
    /**
     * 新页眉最后一条未读消息毫秒数
     */
    private long mLastMsgTime;
    /**
     * 推荐用户轮询周期（秒）
     */
    private int cycle;
    /**
     * 新页眉中当前正在显示的未读消息索引
     */
    private int mCurrDisplayItem = 0;
    private TimerHandler mTimerHandler;
    private boolean isShake = true;
    private Vibrator vibrator;
    private long mPreviousTime;


    private static class TimerHandler extends Handler {
        private WeakReference<UserInfoDetailActivity> mWeakReference;

        public TimerHandler(UserInfoDetailActivity userInfoDetailActivity) {
            this.mWeakReference = new WeakReference<UserInfoDetailActivity>(userInfoDetailActivity);
        }

        @Override
        public void handleMessage(Message msg) {
            final UserInfoDetailActivity userInfoDetailActivity = mWeakReference.get();
            switch (msg.what) {
                case MSG_TYPE_LOAD_DATA:// 加载数据
                    if (userInfoDetailActivity != null) {
                        userInfoDetailActivity.loadData();
                    }
                    break;

//                case MSG_TYPE_HEAD_MSG:// 页眉刷新
//                    if (userInfoDetailActivity != null) {
//                        userInfoDetailActivity.getHeadMsg();
//                    }
//                    sendEmptyMessageDelayed(MSG_TYPE_HEAD_MSG, HEAD_MSG_REFRESH_PERIOD);
//                    break;
                case MSG_TYPE_HEAD_MSG_NEW:// 新页眉刷新
                    if (userInfoDetailActivity != null) {
                        userInfoDetailActivity.getNewHeadMsg();
                    }
                    break;
                case MSG_TYPE_RECOMMEND_USER:// 推荐用户刷新
                    int cycle = 0;
                    if (userInfoDetailActivity != null) {
                        userInfoDetailActivity.getRecommendUsr();
                        cycle = userInfoDetailActivity.cycle;
                    }
                    sendEmptyMessageDelayed(MSG_TYPE_RECOMMEND_USER, cycle < 20 ? HEAD_MSG_REFRESH_PERIOD : cycle * 1000);
                    break;

                case MSG_TYPE_SHOW_UNREAD_MSG:// 显示未读消息页眉
                    // 先隐藏上一条
                    if (userInfoDetailActivity != null) {
                        userInfoDetailActivity.dismissWithAnim(userInfoDetailActivity.mFlHeadMsgNew, userInfoDetailActivity.mRlHeadMsgUnread);
                        userInfoDetailActivity.mFlHeadMsgNew.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                // 如果存在下一条，则显示
                                if (userInfoDetailActivity.mNewHeadMsgNotice != null && userInfoDetailActivity.mCurrDisplayItem < userInfoDetailActivity.mNewHeadMsgNotice.getUnreadMsgBoxList().size() - 1) {
                                    userInfoDetailActivity.mCurrDisplayItem++;
                                    userInfoDetailActivity.setNewHeadMsg();
                                } else {// 继续轮询
                                    sendEmptyMessageDelayed(MSG_TYPE_HEAD_MSG_NEW, HEAD_MSG_REFRESH_PERIOD);
                                }
                            }
                        }, 500);
                    }
                    break;
                case MSG_TYPE_DISMISS_UNREAD_MSG:// 隐藏未读消息页眉
                    if (userInfoDetailActivity != null) {
                        userInfoDetailActivity.dismissWithAnim(userInfoDetailActivity.mFlHeadMsgNew, userInfoDetailActivity.mRlHeadMsgUnread);
                    }
                    break;
                case MSG_TYPE_HIDE_RECOMMEND_USER:// 隐藏推荐用户页眉
                    if (userInfoDetailActivity != null) {
                        userInfoDetailActivity.dismissWithAnim(userInfoDetailActivity.mLlRecommendUser, userInfoDetailActivity.mLlRecommendUser);
                    }
                    sendEmptyMessageDelayed(MSG_TYPE_RECOMMEND_USER, HEAD_MSG_REFRESH_PERIOD);
                    break;
                case MSG_TYPE_DISMISS_VISITOR:// 隐藏访客页眉
                    if (userInfoDetailActivity != null) {
                        userInfoDetailActivity.dismissWithAnim(userInfoDetailActivity.mFlHeadMsgNew, userInfoDetailActivity.mRlHeadMsgVisitor);
                    }
                    sendEmptyMessageDelayed(MSG_TYPE_HEAD_MSG_NEW, HEAD_MSG_REFRESH_PERIOD);
                    break;
            }
        }

    }

    public static void toUserInfoDetailActivity(Activity from, String uId, String isRecord, String sourceTag) {
        Map<String, String> map = new HashMap<String, String>();
        map.put(INTENT_KEY_UID, uId);
        map.put(INTENT_KEY_ISRECORD, TextUtils.isEmpty(isRecord) ? "0" : isRecord);
        map.put(INTENT_KEY_SOURCETAG, sourceTag);
        Util.gotoActivity(from, UserInfoDetailActivity.class, false, map);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);
        ButterKnife.bind(this);

        // 透明状态栏
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {// 5.0及以上
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            // 设置状态栏背景
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {// 4.4
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        Intent intent = getIntent();
        if (intent != null) {
            uId = intent.getStringExtra(INTENT_KEY_UID);
            isRecord = intent.getStringExtra(INTENT_KEY_ISRECORD);
            sourceTag = intent.getStringExtra(INTENT_KEY_SOURCETAG);
        }
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowHomeEnabled(true);
        }

        // 初始化照片墙RecyclerView
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mPicRecyclerView.setLayoutManager(linearLayoutManager);
        mImageWallAdapter = new ImageWallAdapter(this, R.layout.item_pic_wall);
        mPicRecyclerView.setAdapter(mImageWallAdapter);
        // 已付费男用户和女用户显示聊天
        if (UserInfoXml.isMale()) {
            if (UserInfoXml.isMonthly()) {
                mBtnSayHello.setVisibility(View.GONE);
                mBtnChat.setVisibility(View.VISIBLE);
            } else {
                mBtnSayHello.setVisibility(View.VISIBLE);
                mBtnChat.setVisibility(View.GONE);
            }
        } else {
            mBtnSayHello.setVisibility(View.GONE);
            mBtnChat.setVisibility(View.VISIBLE);
        }

        addListeners();
        // 加载用户详情
        showLoading();
        loadUserDetailInfo();
        // 延时加载数据
        mTimerHandler = new TimerHandler(this);
        mTimerHandler.sendEmptyMessageDelayed(MSG_TYPE_LOAD_DATA, 1000 * 2);
    }

    /**
     * 加载页眉和推荐用户
     */
    private void loadData() {
//        // 获取页眉
//        if (UserInfoXml.isShowHeadMsg()) {
//            getHeadMsg();
//            mTimerHandler.sendEmptyMessageDelayed(MSG_TYPE_HEAD_MSG, HEAD_MSG_REFRESH_PERIOD);
//        }
        // 获取新页眉
        if (UserInfoXml.isShowHeadMsgNew()) {
            getNewHeadMsg();
        }
        // 获取推荐用户
        if (UserInfoXml.isShowRecommendUser()) {
            getRecommendUsr();
//            mTimerHandler.sendEmptyMessageDelayed(MSG_TYPE_RECOMMEND_USER, HEAD_MSG_REFRESH_PERIOD);
        }
    }

    private void getHeadMsg() {
        CommonRequestUtil.getHeadMsg(new CommonRequestUtil.OnGetHeadMsgListener() {
            @Override
            public void onSuccess(HeadMsgNotice headMsgNotice) {
                if (headMsgNotice != null) {
                    mHeadMsgNotice = headMsgNotice;
                    String message = headMsgNotice.getMessage();
                    if (!TextUtils.isEmpty(message)) {
                        mRlHeadMsgContainer.setVisibility(View.VISIBLE);
                        mTvHeadMsgContent.setText(message);
                    } else {
                        mRlHeadMsgContainer.setVisibility(View.GONE);
                    }
                    String noticeType = headMsgNotice.getNoticeType();
                    if (!TextUtils.isEmpty(noticeType) && "1".equals(noticeType)) {
                        // 有未读消息时刷新消息列表
                        EventBus.getDefault().post(new MessageChangedEvent());
                    }
                }
            }

            @Override
            public void onFail() {

            }
        });
    }

    /**
     * 获取新页眉
     */
    private void getNewHeadMsg() {
        String lastTime = new SharedPreferenceUtil(UserInfoDetailActivity.this).takeLastTime();
        if (!TextUtils.isEmpty(lastTime)) {
            mLastMsgTime = Long.parseLong(lastTime);
        }
        CommonRequestUtil.getNewHeadMsg(mLastMsgTime, new CommonRequestUtil.OnGetNewHeadMsgListener() {
            @Override
            public void onSuccess(NewHeadMsgNotice newHeadMsgNotice) {
                if (newHeadMsgNotice != null) {
                    mNewHeadMsgNotice = newHeadMsgNotice;
                    //上一次轮训时最后一条未读消息的毫秒数
//                    mPreviousTime=mLastMsgTime;
                    //此次轮训获得的最后一条未读消息的毫秒数
                    mLastMsgTime = newHeadMsgNotice.getLastMsgTime();
                    new SharedPreferenceUtil(UserInfoDetailActivity.this).saveLastTime(mLastMsgTime);
                    mCurrDisplayItem = 0;
                    String noticeType = newHeadMsgNotice.getNoticeType();
                    if (!TextUtils.isEmpty(noticeType)) {
                        switch (noticeType) {
                            case "0":// 没有页眉，继续轮询
                                mTimerHandler.sendEmptyMessageDelayed(MSG_TYPE_HEAD_MSG_NEW, HEAD_MSG_REFRESH_PERIOD);
                                break;
                            case "1":// 未读消息

                                // 刷新聊天列表
                                EventBus.getDefault().post(new MessageChangedEvent());
                                setNewHeadMsg();

                                break;

                            case "2":// 访客
                                FateUser fateUser = newHeadMsgNotice.getRemoteYuanfenUserBase();
                                if (fateUser != null) {
                                    UserBase userBase = fateUser.getUserBaseEnglish();
                                    if (userBase != null) {
                                        ImageLoaderUtil.getInstance().loadImage(UserInfoDetailActivity.this, new ImageLoader.Builder()
                                                .url(userBase.getImage().getThumbnailUrl()).imageView(mIvVisitorAvatar).build());
                                        mTvVisitorContent.setText(getString(R.string.visitor_tip, userBase.getNickName()));
                                    }
                                }
                                mRlHeadMsgUnread.setVisibility(View.GONE);
                                mRlHeadMsgVisitor.setVisibility(View.VISIBLE);
                                showWithAnim(mFlHeadMsgNew);
                                mTimerHandler.sendEmptyMessageDelayed(MSG_TYPE_DISMISS_VISITOR, newHeadMsgNotice.getDisplaySecond() * 1000);
                                break;
                        }
                    }
                }
            }

            @Override
            public void onFail() {
                mTimerHandler.sendEmptyMessageDelayed(MSG_TYPE_HEAD_MSG_NEW, HEAD_MSG_REFRESH_PERIOD);
            }
        });
    }

    /**
     * 按照未读消息列表顺序显示未读信
     */
    private void setNewHeadMsg() {
        if (mNewHeadMsgNotice != null) {
            List<MsgBox> msgBoxList = mNewHeadMsgNotice.getUnreadMsgBoxList();
            if (!Util.isListEmpty(msgBoxList) && mCurrDisplayItem < msgBoxList.size()) {
                MsgBox msgBox = msgBoxList.get(mCurrDisplayItem);
                if (msgBox != null) {
                    UserBase userBase = msgBox.getUserBaseEnglish();
                    if (userBase != null) {
                        Image image = userBase.getImage();
                        if (image != null) {
                            ImageLoaderUtil.getInstance().loadImage(UserInfoDetailActivity.this, new ImageLoader.Builder()
                                    .url(image.getThumbnailUrl()).imageView(mIvHeadMsgAvatar).build());
                        }
                        mTvNickname.setText(userBase.getNickName());
                        mTvAge.setText(TextUtils.concat(String.valueOf(userBase.getAge()), getString(R.string.age)));
                        mTvAge.setSelected(userBase.getSex() == 0);
                        mTvHeight.setText(HeightUtils.getInchCmByCm(userBase.getHeightCm()));
                    }
                    String msgContent = msgBox.getMsg();
                    if (!TextUtils.isEmpty(msgContent)) {
                        mTvMsgContent.setText(msgContent);
                    }
                    mRlHeadMsgUnread.setVisibility(View.VISIBLE);
                    mRlHeadMsgVisitor.setVisibility(View.GONE);
                    showWithAnim(mFlHeadMsgNew);
                }
                // 一段时间后显示下一条未读消息
                mTimerHandler.sendEmptyMessageDelayed(MSG_TYPE_SHOW_UNREAD_MSG, mNewHeadMsgNotice.getDisplaySecond() * 1000);
            }
        }
    }

    private void showWithAnim(View view) {

        if (isShake) {
            view.setVisibility(View.VISIBLE);
            vibrator = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(200);
            view.startAnimation(AnimationUtils.loadAnimation(UserInfoDetailActivity.this, R.anim.anim_in));
        }


    }

    private void dismissWithAnim(final View view, final View view2) {
        Animation outAnimation = AnimationUtils.loadAnimation(UserInfoDetailActivity.this, R.anim.push_bottom_out);
        view.startAnimation(outAnimation);
        outAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(View.GONE);
                view2.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    /**
     * 获取推荐用户
     */
    private void getRecommendUsr() {
        CommonRequestUtil.getRecommendUser(cycle, new CommonRequestUtil.OnGetRecommendUserListener() {
            @Override
            public void onSuccess(RecommendUser recommendUser) {
                if (recommendUser != null) {
                    mRecommendUser = recommendUser;
                    cycle = recommendUser.getCycle();
                    User user = recommendUser.getUser();
                    if (user != null) {
                        UserBase userBase = user.getUserBaseEnglish();
                        if (userBase != null) {
                            // 显示推荐用户
                            mLlRecommendUser.setVisibility(View.VISIBLE);
                            mTvRecommendContent.setText(TextUtils.concat(userBase.getNickName(), " ", recommendUser.getSentence()));
                            ImageLoaderUtil.getInstance().loadImage(UserInfoDetailActivity.this, new ImageLoader.Builder()
                                    .url(userBase.getImage().getThumbnailUrl()).imageView(mIvRecommendAvatar).build());
                        }
                    }
                }
                // 一段时间后隐藏推荐用户
                mTimerHandler.sendEmptyMessageDelayed(MSG_TYPE_HIDE_RECOMMEND_USER, 5 * 1000);

            }


            @Override
            public void onFail() {
                // 一段时间后隐藏推荐用户
                mTimerHandler.sendEmptyMessageDelayed(MSG_TYPE_HIDE_RECOMMEND_USER, 5 * 1000);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tool_bar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_report:// 举报
                ReportDialog.newInstance(getString(R.string.report_title) + nickname, uId).show(getSupportFragmentManager(), "report");
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void addListeners() {
        // 左菜单点击事件处理
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        // 点击查看图片大图
        mIvUserAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PictureBrowseActivity.toPictureBrowseActivity(UserInfoDetailActivity.this, 0, mImageWallAdapter.getAdapterData());
            }
        });
        mImageWallAdapter.setOnItemClickListener(new CommonRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, RecyclerViewHolder viewHolder) {
                PictureBrowseActivity.toPictureBrowseActivity(UserInfoDetailActivity.this, position, mImageWallAdapter.getAdapterData());
            }
        });
        // 查看页眉消息
        mRlHeadMsgContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mHeadMsgNotice != null) {
                    mRlHeadMsgContainer.setVisibility(View.GONE);
                    String noticeType = mHeadMsgNotice.getNoticeType();
                    if (!TextUtils.isEmpty(noticeType)) {
                        switch (noticeType) {
                            case "1": // 未读消息
                                // 切换到聊天tab
                                EventBus.getDefault().post(new HeadMsgEvent());
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        finish();
                                    }
                                }, 200);
                                break;

                            case "2":// 正在看我
                                showLoading();
                                uId = mHeadMsgNotice.getRemoteUserId();
                                sourceTag = SOURCE_HEAD_MSG;
                                loadUserDetailInfo();
                                break;
                        }
                    }
                }
            }
        });
        mIvHeadMsgClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRlHeadMsgContainer.setVisibility(View.GONE);
            }
        });

        // 新页眉
        mRlHeadMsgUnread.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // 查看未读消息，进入聊天页面
                mFlHeadMsgNew.setVisibility(View.GONE);
                if (mNewHeadMsgNotice != null && mCurrDisplayItem < mNewHeadMsgNotice.getUnreadMsgBoxList().size()) {
                    List<MsgBox> msgBoxList = mNewHeadMsgNotice.getUnreadMsgBoxList();
                    if (!Util.isListEmpty(msgBoxList)) {
                        MsgBox msgBox = msgBoxList.get(mCurrDisplayItem);
                        if (msgBox != null) {
                            UserBase userBase = msgBox.getUserBaseEnglish();
                            if (userBase != null) {
                                ChatActivity.toChatActivity(UserInfoDetailActivity.this, userBase.getId(), userBase.getNickName(), userBase.getImage().getThumbnailUrl());
                                finish();
                            }
                        }
                    }
                }
            }
        });
        mTvVisitorLook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 隐藏新页眉
                mRlHeadMsgVisitor.setVisibility(View.GONE);
                mFlHeadMsgNew.setVisibility(View.GONE);
                // 查看用户详情
                if (mNewHeadMsgNotice != null) {
                    FateUser fateUser = mNewHeadMsgNotice.getRemoteYuanfenUserBase();
                    if (fateUser != null) {
                        UserBase userBase = fateUser.getUserBaseEnglish();
                        if (userBase != null) {
                            showLoading();
                            uId = userBase.getId();
                            sourceTag = SOURCE_HEAD_MSG;
                            loadUserDetailInfo();
                        }
                    }
                }
            }
        });
        mTvVisitorIgnore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 隐藏新页眉
                mRlHeadMsgVisitor.setVisibility(View.GONE);
                mFlHeadMsgNew.setVisibility(View.GONE);
            }
        });
        // 推荐用户
        mTvRecommendLook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mRecommendUser != null) {
                    UserBase userBase = mRecommendUser.getUser().getUserBaseEnglish();
                    if (userBase != null) {
                        // 隐藏推荐用户
                        mLlRecommendUser.setVisibility(View.GONE);
                        Image image = userBase.getImage();
                        if (UserInfoXml.isMale()) {
                            if (UserInfoXml.isMonthly()) {// 已付费男用户进入聊天页
                                ChatActivity.toChatActivity(UserInfoDetailActivity.this, userBase.getId(), userBase.getNickName(),
                                        image == null ? null : image.getThumbnailUrl());
                            } else {// 未付费男用户，进入对方空间页
                                showLoading();
                                uId = userBase.getId();
                                sourceTag = SOURCE_FROM_RECOMMEND_USER;
                                loadUserDetailInfo();
                            }
                        } else {// 女用户进入聊天页
                            ChatActivity.toChatActivity(UserInfoDetailActivity.this, userBase.getId(), userBase.getNickName(),
                                    image == null ? null : image.getThumbnailUrl());
                        }
                    }
                }
            }
        });
        mTvRecommendIgnore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 隐藏推荐用户
                mLlRecommendUser.setVisibility(View.GONE);
            }
        });
    }

    /**
     * 显示加载对话框
     */
    protected void showLoading() {
        mLoadingDialog = Utils.showLoadingDialog(this, true);
    }

    /**
     * 隐藏加载对话框
     */
    protected void dismissLoading() {
        if (mLoadingDialog != null && mLoadingDialog.isShowing()) {
            mLoadingDialog.dismiss();
        }
    }

    @OnClick({R.id.userDetail_fab_next, R.id.userDetail_follow, R.id.userDetail_sayHello, R.id.userDetail_chat})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.userDetail_fab_next:// 下一位用户
                loadNextUser();
                break;

            case R.id.userDetail_follow:
                if (mIsFollow == 1) {// 取消关注
                    CommonRequestUtil.cancelFollow(uId, true, new CommonRequestUtil.OnCommonListener() {
                        @Override
                        public void onSuccess() {
                            mIsFollow = 0;
                            mBtnFollow.setSelected(false);
                            mBtnFollow.setText(getString(R.string.follow));
                        }

                        @Override
                        public void onFail() {

                        }
                    });
                } else {// 关注
                    CommonRequestUtil.follow(uId, true, new CommonRequestUtil.OnCommonListener() {
                        @Override
                        public void onSuccess() {
                            mIsFollow = 1;
                            mBtnFollow.setSelected(true);
                            mBtnFollow.setText(getString(R.string.cancel_follow));
                        }

                        @Override
                        public void onFail() {

                        }
                    });
                }
                break;

            case R.id.userDetail_sayHello:// 打招呼
                CommonRequestUtil.sayHello(this, uId, CommonRequestUtil.SAY_HELLO_TYPE_USER_INFO, true, new CommonRequestUtil.OnCommonListener() {

                    @Override
                    public void onSuccess() {
                        mIsSayHello = 1;
                        mBtnSayHello.setEnabled(false);
                        mBtnSayHello.setSelected(true);
                        Utils.setDrawableLeft(mBtnSayHello, R.drawable.icon_say_hello_gray);
                        // 发送事件，更新状态
                        EventBus.getDefault().post(new SayHelloEvent());
                    }

                    @Override
                    public void onFail() {

                    }
                });
                break;

            case R.id.userDetail_chat:// 聊天
                ChatActivity.toChatActivity(this, uId, nickname, avatarUrl);
                break;
        }
    }

    /**
     * 设置用户信息
     */
    private void setUserInfo(User user) {
        if (user != null) {
            UserBase userBase = user.getUserBaseEnglish();
            if (userBase != null) {
                // 更新ID
                uId = userBase.getId();
                // 设置用户名为标题
                nickname = Util.convertText(userBase.getNickName());
                if (!TextUtils.isEmpty(nickname)) {
                    mCollapsingToolbarLayout.setTitle(nickname);
                }
                // 设置头像
                Image image = userBase.getImage();
                if (image != null) {
                    avatarUrl = image.getImageUrl();
                    if (!TextUtils.isEmpty(avatarUrl)) {
                        ImageLoaderUtil.getInstance().loadImage(UserInfoDetailActivity.this, new ImageLoader.Builder()
                                .url(avatarUrl).imageView(mIvUserAvatar).build());
                    } else {
                        mIvUserAvatar.setImageResource(IConfigConstant.COMMON_DEFAULT_AVATAR);
                    }
                } else {
                    mIvUserAvatar.setImageResource(IConfigConstant.COMMON_DEFAULT_AVATAR);
                }
                // 设置年龄，身高，婚姻状况为展示基本信息
                StringBuilder stringBuilder = new StringBuilder();
                int age = userBase.getAge();
                if (age < 18) {
                    age = 18;
                }
                stringBuilder.append(age)
                        .append(getString(R.string.years));
                String heightCm = userBase.getHeightCm();
                if (!TextUtils.isEmpty(heightCm)) {
                    stringBuilder.append(" | ")
                            .append(HeightUtils.getInchCmByCm(heightCm));
                }
                String marriage = ParamsUtils.getMarriageMap().get(String.valueOf(userBase.getMaritalStatus()));
                if (!TextUtils.isEmpty(marriage)) {
                    if (!TextUtils.isEmpty(heightCm)) {
                        stringBuilder.append(" | ");
                    }
                    stringBuilder.append(marriage);
                }
                mTvBaseInfo.setText(stringBuilder.toString());
                // 内心独白
                String monologue = userBase.getMonologue();
                if (!TextUtils.isEmpty(monologue)) {
                    mMonologue.setText(monologue);
                    mLlMonologue.setVisibility(View.VISIBLE);
                } else {
                    mLlMonologue.setVisibility(View.GONE);
                }
                // 地区
                setTextInfo(mLlArea, mTvArea, mAreaDivider, userBase.getArea().getProvinceName());
                // 星座
                setTextInfo(mLlSign, mTvSign, mSignDivider, ParamsUtils.getConstellationMap().get(String.valueOf(userBase.getSign())));
                // 学历
//                setTextInfo(mLlEducation, mTvEducation, mEducationDivider, ParamsUtils.getEducationMap().get(String.valueOf(userBase.getEducation())));
                // 收入
                String income = ParamsUtils.getIncomeMap().get(String.valueOf(userBase.getIncome()));
                setTextInfo(mLlIncome, mTvIncome, mIncomeDivider, TextUtils.isEmpty(income) ? null : "NT$" + income);
                // 职业
                setTextInfo(mLlOccupation, mTvOccupation, mOccupationDivider, ParamsUtils.getWorkMap().get(String.valueOf(userBase.getOccupation())));
            }
            // 设置照片墙
            setUserPicWall(user.getListImage());
            // 设置用户详细信息
            setUserDetailInfo(user);
            // 设置关注和打招呼
            mIsFollow = user.getIsFollow();
            if (mIsFollow == 1) {// 已关注
                mBtnFollow.setSelected(true);
                mBtnFollow.setText(getString(R.string.cancel_follow));
            } else {
                mBtnFollow.setSelected(false);
                mBtnFollow.setText(getString(R.string.follow));
            }
            mIsSayHello = user.getIsSayHello();
            if (mIsSayHello == 1) {// 已关注
                mBtnSayHello.setEnabled(false);
                mBtnSayHello.setSelected(true);
                Utils.setDrawableLeft(mBtnSayHello, R.drawable.icon_say_hello_gray);
            } else {
                mBtnSayHello.setEnabled(true);
                mBtnSayHello.setSelected(false);
                Utils.setDrawableLeft(mBtnSayHello, R.drawable.icon_say_hello);
            }
        }
    }

    /**
     * 设置用户照片墙
     */
    private void setUserPicWall(List<Image> imageList) {
        if (!Util.isListEmpty(imageList) && mImageWallAdapter != null) {
            mImageWallAdapter.replaceAll(imageList);
        }
    }

    /**
     * 设置用户详细资料
     */
    private void setUserDetailInfo(User user) {
        // 是否想要小孩
        setTextInfo(mLlWantBaby, mTvWantBaby, mWantBabyDivider, ParamsUtils.getWantBabyMap().get(String.valueOf(user.getWantsKids())));
        // 喜欢的运动
        setTextInfo(mLlSport, mTvSport, mSportDivider, ParamsUtils.getSportMap().get(user.getSports()));
        // 个性特征
        setTextInfo(mLlPersonal, mTvPersonal, mPersonalDivider, ParamsUtils.getPersonalMap().get(user.getCharacteristics()));
        // 旅行
        setTextInfo(mLlTravel, mTvTravel, mTravelDivider, ParamsUtils.getTravelMap().get(user.getTravel()));
        // 喜欢的书和漫画
        setTextInfo(mLlBookCartoon, mTvBookCartoon, mBookCartoonDivider, ParamsUtils.getBookCartoonMap().get(user.getBooks()));
        // 兴趣爱好
        setTextInfo(mLlHobby, mTvHobby, mHobbyDivider, getValue(user.getListHobby(), ParamsUtils.getInterestMap()));
    }

    private void setTextInfo(LinearLayout llContainer, TextView tvInfo, View dividerView, String text) {
        if (!TextUtils.isEmpty(text) && !"null".equals(text) && !"".equals(text)) {
            tvInfo.setText(text);
            llContainer.setVisibility(View.VISIBLE);
            dividerView.setVisibility(View.VISIBLE);
        } else {
            llContainer.setVisibility(View.GONE);
            dividerView.setVisibility(View.GONE);
        }
    }

    private String getValue(String key, Map<String, String> map) {
        if (key != null) {
            String[] spit = key.split("\\|");
            StringBuffer value = new StringBuffer();
            for (int i = 0; i < spit.length; i++) {
                if (i < spit.length - 1) {
                    value.append(map.get(spit[i]) + ",");
                } else {
                    value.append(map.get(spit[i]));
                }
            }
            return value.toString();
        }
        return null;
    }

    private static class ImageWallAdapter extends CommonRecyclerViewAdapter<Image> {

        public ImageWallAdapter(Context context, int layoutResId) {
            this(context, layoutResId, null);
        }

        public ImageWallAdapter(Context context, int layoutResId, List<Image> list) {
            super(context, layoutResId, list);
        }

        @Override
        protected void convert(int position, RecyclerViewHolder viewHolder, Image bean) {
            ImageView ivPic = (ImageView) viewHolder.getView(R.id.item_user_pic);
            if (bean != null) {
                String imgUrl = bean.getImageUrl();
                if (!TextUtils.isEmpty(imgUrl)) {
                    ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder()
                            .url(imgUrl).placeHolder(IConfigConstant.COMMON_DEFAULT_AVATAR_CIRCLE).transform(new CircleTransformation()).imageView(ivPic).build());
                } else {
                    ivPic.setImageResource(IConfigConstant.COMMON_DEFAULT_AVATAR_CIRCLE);
                }
            } else {
                ivPic.setImageResource(IConfigConstant.COMMON_DEFAULT_AVATAR_CIRCLE);
            }
        }
    }

    private void loadUserDetailInfo() {
        OkHttpUtils.post()
                .url(IUrlConstant.URL_GET_USER_INFO)
                .addHeader("token", PlatformInfoXml.getToken())
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .addParams("uid", TextUtils.isEmpty(uId) ? "" : uId)
                .addParams("isRecord", TextUtils.isEmpty(isRecord) ? "0" : isRecord)
                .addParams("sourceTag", TextUtils.isEmpty(sourceTag) ? "1" : sourceTag)
                .build()
                .execute(new Callback<UserDetail>() {
                    @Override
                    public UserDetail parseNetworkResponse(Response response, int id) throws Exception {
                        String resultJson = response.body().string();
                        if (!TextUtils.isEmpty(resultJson)) {
                            return JSON.parseObject(resultJson, UserDetail.class);
                        }
                        return null;
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        stopRefresh(3);
                    }

                    @Override
                    public void onResponse(UserDetail response, int id) {
                        if (response != null) {
                            String isSucceed = response.getIsSucceed();
                            if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
                                setUserInfo(response.getUserEnglish());
                            }
                        }
                        stopRefresh(1);
                    }
                });
    }

    private void loadNextUser() {
        showLoading();
        pageNum++;
        OkHttpUtils.post()
                .url(IUrlConstant.URL_NEXT_USER)
                .addHeader("token", PlatformInfoXml.getToken())
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .addParams("pageNum", String.valueOf(pageNum))
                .addParams("pageSize", pageSize)
                .build()
                .execute(new StringCallback() {

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        stopRefresh(3);
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        if (!TextUtils.isEmpty(response)) {
                            JSONObject jsonObject = (JSONObject) JSON.parse(response);
                            String listUser = jsonObject.getString("listUser");
                            if (!TextUtils.isEmpty(listUser)) {
                                List<User> userList = JSON.parseArray(listUser, User.class);
                                if (!Util.isListEmpty(userList)) {
                                    setUserInfo(userList.get(0));
                                } else {
                                    loadNextUser();
                                }
                            } else {
                                loadNextUser();
                            }
                        } else {
                            loadNextUser();
                        }
                        stopRefresh(1);
                    }
                });
    }

    private void stopRefresh(int seconds) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dismissLoading();
            }
        }, seconds * 1000);
    }

    /**
     * 接收跳转到此类时新页眉信息未读数量，以及提醒此类继续读取未读信息，如果没有未读信息，则
     **/

    @Subscribe
    public void onEvent(MessageChangedEvent event) {

    }


    @Override
    protected void onStart() {
        super.onStart();
        isShake = true;
    }
//
//    @Override
//    protected void onPause() {
//        super.onPause();
//        isShake=false;
//    }

    @Override
    protected void onStop() {
        super.onStop();
        isShake = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isShake = true;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        isShake = true;
    }

    /**
     * 监听是否点击了home键将客户端推到后台
     */
    private BroadcastReceiver mHomeKeyEventReceiver = new BroadcastReceiver() {
        String SYSTEM_REASON = "reason";
        String SYSTEM_HOME_KEY = "homekey";
        String SYSTEM_HOME_KEY_LONG = "recentapps";

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Intent.ACTION_CLOSE_SYSTEM_DIALOGS)) {
                String reason = intent.getStringExtra(SYSTEM_REASON);
                if (TextUtils.equals(reason, SYSTEM_HOME_KEY)) {
                    //表示按了home键,程序到了后台
                    isShake = false;
                } else if (TextUtils.equals(reason, SYSTEM_HOME_KEY_LONG)) {
                    //表示长按home键,显示最近使用的程序列表
                }
            }
        }
    };
}
