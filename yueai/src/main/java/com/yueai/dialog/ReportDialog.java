package com.yueai.dialog;

import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.library.app.BaseDialogFragment;
import com.library.utils.ToastUtil;
import com.yueai.R;
import com.yueai.bean.BaseModel;
import com.yueai.constant.IUrlConstant;
import com.yueai.xml.PlatformInfoXml;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.Callback;

import okhttp3.Call;
import okhttp3.Response;

/**
 * 举报对话框
 * Created by zhangdroid on 2016/7/4.
 */
public class ReportDialog extends BaseDialogFragment {
    private String mUserId;

    public static ReportDialog newInstance(String title, String uId) {
        ReportDialog reportDialog = new ReportDialog();
        reportDialog.mUserId = uId;
        reportDialog.setArguments(getDialogBundle(title, null, null, null, true));
        return reportDialog;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.dialog_report;
    }

    @Override
    protected void setDialogContentView(View view) {
        TextView tvTitle = (TextView) view.findViewById(R.id.dialog_report_title);
        View dividerView = view.findViewById(R.id.dialog_report_divider);// 标题栏下分割线
        final RadioGroup radioGroupReason = (RadioGroup) view.findViewById(R.id.dialog_reason_group);
        final EditText etMore = (EditText) view.findViewById(R.id.dialog_report_reason_more);// 补充说明
        Button btnPositive = (Button) view.findViewById(R.id.dialog_report_sure);
        Button btnNegative = (Button) view.findViewById(R.id.dialog_report_cancel);

        String title = getDialogTitle();
        if (!TextUtils.isEmpty((title))) {
            tvTitle.setText(title);
            tvTitle.setVisibility(View.VISIBLE);
            dividerView.setVisibility(View.VISIBLE);
        } else {
            tvTitle.setVisibility(View.GONE);
            dividerView.setVisibility(View.GONE);
        }

        String positive = getDialogPositive();
        if (!TextUtils.isEmpty((positive))) {
            btnPositive.setText(positive);
        }

        String negative = getDialogNegative();
        if (!TextUtils.isEmpty((negative))) {
            btnNegative.setText(negative);
        }

        btnPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RadioButton checkBtn = (RadioButton) radioGroupReason.findViewById(radioGroupReason.getCheckedRadioButtonId());
                if(checkBtn==null){
                    ToastUtil.showShortToast(getActivity(), getActivity().getString(R.string.report_reason_empty));
                    return;
                }
                String reason = checkBtn.getText().toString();
                if (TextUtils.isEmpty(reason)) {
                    ToastUtil.showShortToast(getActivity(), getActivity().getString(R.string.report_reason_empty));
                    return;
                }
                reportUser(reason, etMore.getText().toString().trim());
            }
        });
        btnNegative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    private void reportUser(String reason, String contentMore) {
        if (TextUtils.isEmpty(mUserId)) {
            dismiss();
            return;
        }
        OkHttpUtils.post()
                .url(IUrlConstant.URL_REPORT)
                .addHeader("token", PlatformInfoXml.getToken())
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .addParams("contentId", reason)// 举报原因
                .addParams("content", contentMore)// 补充说明
                .build()
                .execute(new Callback<BaseModel>() {

                    @Override
                    public BaseModel parseNetworkResponse(Response response, int id) throws Exception {
                        String resultJson = response.body().string();
                        if (!TextUtils.isEmpty(resultJson)) {
                            return JSON.parseObject(resultJson, BaseModel.class);
                        }
                        return null;
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        ToastUtil.showShortToast(getActivity(), getString(R.string.report_fail));
                        dismiss();
                    }

                    @Override
                    public void onResponse(BaseModel response, int id) {
                        if (response != null) {
                            String isSucceed = response.getIsSucceed();
                            if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
                                ToastUtil.showShortToast(getActivity(), getString(R.string.report_success));
                                dismiss();
                            }
                        }
                    }
                });
    }

}
