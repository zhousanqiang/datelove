package com.yueai.dialog;

import android.view.View;
import android.widget.TextView;

import com.library.app.BaseDialogFragment;
import com.yueai.R;

/**
 * 相册操作对话框
 * Created by zhangdroid on 2017/4/28.
 */
public class AlbumMoreDialog extends BaseDialogFragment {
    private OnAlbumMoreClickLister mOnAlbumMoreClickLister;

    public static AlbumMoreDialog newInstance(OnAlbumMoreClickLister onAlbumMoreClickLister) {
        AlbumMoreDialog albumMoreDialog = new AlbumMoreDialog();
        albumMoreDialog.mOnAlbumMoreClickLister = onAlbumMoreClickLister;
        return albumMoreDialog;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.dialog_album_more;
    }

    @Override
    protected void setDialogContentView(View view) {
        TextView tvSet = (TextView) view.findViewById(R.id.dialog_album_set);// 设置为头像
        TextView tvDelete = (TextView) view.findViewById(R.id.popup_album_delete);// 删除聊天记录
        TextView tvCancel = (TextView) view.findViewById(R.id.popup_album_cancel);// 取消

        tvSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (mOnAlbumMoreClickLister != null) {
                    mOnAlbumMoreClickLister.setToAvatar();
                }
            }
        });
        tvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (mOnAlbumMoreClickLister != null) {
                    mOnAlbumMoreClickLister.deleteImage();
                }
            }
        });
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    public interface OnAlbumMoreClickLister {
        /**
         * 设置头像
         */
        void setToAvatar();

        /**
         * 删除图片
         */
        void deleteImage();
    }

}
