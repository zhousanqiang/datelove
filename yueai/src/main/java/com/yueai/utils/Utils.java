package com.yueai.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.library.utils.DensityUtil;
import com.library.utils.Util;
import com.yueai.R;
import com.yueai.base.BaseApplication;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhangdroid on 2016/8/9.
 */
public class Utils {

    /**
     * @return Application Context
     */
    public static Context getContext() {
        return BaseApplication.getGlobalContext();
    }

    public static boolean isHttpUrl(String url) {
        if (!TextUtils.isEmpty(url) && url.startsWith("http")) {
            return true;
        }
        return false;
    }

    /**
     * 显示公用加载对话框
     *
     * @param context      上下文对象
     * @param isCancelable 是否可以取消
     * @return dialog对象，用来隐藏
     */
    public static Dialog showLoadingDialog(Context context, boolean isCancelable) {
        RelativeLayout relativeLayout = new RelativeLayout(context);
        relativeLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        relativeLayout.setGravity(Gravity.CENTER);
        ImageView imageView = new ImageView(context);
        int size = DensityUtil.getScreenWidth(context) / 5;
        imageView.setLayoutParams(new RelativeLayout.LayoutParams(size, size));
        AnimationDrawable animationDrawable = Util.getFrameAnim(getLoadingDrawableList(context), true, 50);
        animationDrawable.start();
        imageView.setImageDrawable(animationDrawable);
        relativeLayout.addView(imageView);
        AlertDialog dialog = new AlertDialog.Builder(context, android.R.style.Theme_Panel)
                .setView(relativeLayout)
                .setCancelable(isCancelable)
                .show();
        return dialog;
    }

    /**
     * 列表加载更多
     */
    public static View getLoadingView(Context context) {
        RelativeLayout relativeLayout = new RelativeLayout(context);
        relativeLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        relativeLayout.setGravity(Gravity.CENTER);
        int padding = DensityUtil.dip2px(context, 15);
        relativeLayout.setPadding(0, padding, 0, padding);
        AnimationDrawable animationDrawable = Util.getFrameAnim(getLoadingDrawableList(context), true, 75);
        animationDrawable.start();
        ImageView imageView = new ImageView(context);
        imageView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        imageView.setImageDrawable(animationDrawable);
        relativeLayout.addView(imageView);
        return relativeLayout;
    }

    public static List<Drawable> getLoadingDrawableList(Context context) {
        List<Drawable> drawableList = new ArrayList<Drawable>();
        drawableList.add(getDrawable(context, R.drawable.loading_01));
        drawableList.add(getDrawable(context, R.drawable.loading_02));
        drawableList.add(getDrawable(context, R.drawable.loading_03));
        drawableList.add(getDrawable(context, R.drawable.loading_04));
        drawableList.add(getDrawable(context, R.drawable.loading_05));
        drawableList.add(getDrawable(context, R.drawable.loading_06));
        drawableList.add(getDrawable(context, R.drawable.loading_07));
        drawableList.add(getDrawable(context, R.drawable.loading_08));
        drawableList.add(getDrawable(context, R.drawable.loading_09));
        drawableList.add(getDrawable(context, R.drawable.loading_10));
        drawableList.add(getDrawable(context, R.drawable.loading_11));
        drawableList.add(getDrawable(context, R.drawable.loading_12));
        return drawableList;
    }

    private static Drawable getDrawable(Context context, int resId) {
        return context.getResources().getDrawable(resId);
    }

    public static void setDrawableLeft(TextView textView, int leftDrawableResId) {
        Drawable leftDrawable = getDrawable(textView.getContext(), leftDrawableResId);
        leftDrawable.setBounds(0, 0, leftDrawable.getMinimumWidth(), leftDrawable.getMinimumHeight());
        textView.setCompoundDrawables(leftDrawable, null, null, null);
    }

    /**
     * 获得AnimationDrawable，图片动画
     *
     * @param list         动画需要播放的图片集合
     * @param isRepeatable 是否可以重复
     * @param duration     帧间隔（毫秒）
     * @return
     */
    public static AnimationDrawable getFrameAnim(List<Drawable> list, boolean isRepeatable, int duration) {
        AnimationDrawable animationDrawable = new AnimationDrawable();
        animationDrawable.setOneShot(!isRepeatable);
        if (!Util.isListEmpty(list)) {
            for (int i = 0; i < list.size(); i++) {
                animationDrawable.addFrame(list.get(i), duration);
            }
        }
        return animationDrawable;
    }
}
