package com.yueai.adapter;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.library.adapter.recyclerview.CommonRecyclerViewAdapter;
import com.library.adapter.recyclerview.RecyclerViewHolder;
import com.library.imageloader.CircleTransformation;
import com.library.imageloader.ImageLoader;
import com.library.imageloader.ImageLoaderUtil;
import com.library.utils.DensityUtil;
import com.library.utils.HeightUtils;
import com.library.utils.Util;
import com.yueai.R;
import com.yueai.bean.Image;
import com.yueai.bean.SearchUser;
import com.yueai.bean.UserBase;
import com.yueai.constant.IConfigConstant;
import com.yueai.utils.CommonRequestUtil;
import com.yueai.utils.ParamsUtils;
import com.yueai.utils.Utils;

import java.util.List;

/**
 * 搜索适配器
 * Created by zhangdroid on 2016/10/29.
 */
public class SearchAdapter extends CommonRecyclerViewAdapter<SearchUser> {

    public SearchAdapter(Context context, int layoutResId) {
        super(context, layoutResId);
    }

    public SearchAdapter(Context context, int layoutResId, List list) {
        super(context, layoutResId, list);
    }

    @Override
    protected void convert(final int position, RecyclerViewHolder viewHolder, final SearchUser bean) {
        if (bean != null) {
            final UserBase userBase = bean.getUserBaseEnglish();
            if (userBase != null) {
                ImageView ivAvatar = (ImageView) viewHolder.getView(R.id.item_search_avatar);
                Image image = userBase.getImage();
                if (image != null) {
                    String imgUrl = image.getThumbnailUrl();
                    if (!TextUtils.isEmpty(imgUrl)) {
                        ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(imgUrl).imageView(ivAvatar)
                                .placeHolder(IConfigConstant.COMMON_DEFAULT_AVATAR_CIRCLE).transform(new CircleTransformation()).build());
                    } else {
                        ivAvatar.setImageResource(IConfigConstant.COMMON_DEFAULT_AVATAR_CIRCLE);
                    }
                } else {
                    ivAvatar.setImageResource(IConfigConstant.COMMON_DEFAULT_AVATAR_CIRCLE);
                }
                // 昵称
                String nickname = userBase.getNickName();
                if (!TextUtils.isEmpty(nickname)) {
                    viewHolder.setText(R.id.item_search_nickname, nickname);
                }
                // 年龄、身高
                int age = userBase.getAge();
                if (age < 18) {
                    age = 18;
                }
                StringBuilder age_height = new StringBuilder();
                age_height.append(age)
                        .append(mContext.getString(R.string.age))
                        .append("  ");
                String heightCm = userBase.getHeightCm();
                if (!TextUtils.isEmpty(heightCm)) {
                    age_height.append(HeightUtils.getInchCmByCm(heightCm));
                }
                viewHolder.setText(R.id.item_search_age_height, age_height.toString());
                // 地区，收入
                StringBuilder area_income = new StringBuilder();
                String area = userBase.getArea().getProvinceName();
                if (!TextUtils.isEmpty(area)) {
                    area_income.append(area);
                }
                String income = ParamsUtils.getIncomeMap().get(String.valueOf(userBase.getIncome()));
                if (!TextUtils.isEmpty(income)) {
                    if (!TextUtils.isEmpty(area)) {
                        area_income.append("  ");
                    }
                    area_income.append("NT$" + income);
                }
                viewHolder.setText(R.id.item_search_location_income, area_income.toString());
                // 标签
                LinearLayout llLabelContainer = (LinearLayout) viewHolder.getView(R.id.item_search_label);
                llLabelContainer.removeAllViews();
                List<String> labelList = bean.getListLabel();
                if (!Util.isListEmpty(labelList)) {
                    llLabelContainer.setVisibility(View.VISIBLE);
                    llLabelContainer.addView(createLabelTextView(ParamsUtils.getInterestMap().get(labelList.get(0))));
                } else {
                    llLabelContainer.setVisibility(View.GONE);
                }

                // 打招呼
                final TextView tvSayHello = (TextView) viewHolder.getView(R.id.item_search_say_hello);
                String isSayHello = bean.getIsSayHello();
                if (!TextUtils.isEmpty(isSayHello) && "1".equals(isSayHello)) {
                    tvSayHello.setSelected(true);
                    Utils.setDrawableLeft(tvSayHello, R.drawable.icon_say_hello_gray_small);
                } else {
                    tvSayHello.setSelected(false);
                    Utils.setDrawableLeft(tvSayHello, R.drawable.icon_say_hello_small);
                }
                // 打招呼
                tvSayHello.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        CommonRequestUtil.sayHello(mContext, userBase.getId(), CommonRequestUtil.SAY_HELLO_TYPE_SEARCH,
                                true, new CommonRequestUtil.OnCommonListener() {

                                    @Override
                                    public void onSuccess() {
                                        tvSayHello.setSelected(false);
                                        bean.setIsSayHello("1");
                                        updateItem(position, bean);
                                    }

                                    @Override
                                    public void onFail() {

                                    }
                                });
                    }
                });
            }
        }
    }

    private TextView createLabelTextView(String label) {
        TextView textView = new TextView(mContext);
        int padding15 = DensityUtil.dip2px(mContext, 15);
        int padding5 = DensityUtil.dip2px(mContext, 5);
        textView.setPadding(padding15, padding5, padding15, padding5);
        textView.setText(label);
        textView.setTextColor(Color.WHITE);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
        textView.setBackgroundResource(R.drawable.shape_round_rectangle_light_yellow_bg);
        textView.setGravity(Gravity.CENTER);
        return textView;
    }

}
