package com.yueai.base;

import android.app.Dialog;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.yueai.R;
import com.yueai.utils.Utils;

import butterknife.ButterKnife;

/**
 * base title activity
 * Created by zhangdroid on 2016/8/12.
 */
public abstract class BaseTitleActivity extends FragmentActivity {
    // 标题栏
    protected TextView mTvTitle;
    protected TextView mTvLeft;
    protected ImageView mIvLeft;
    protected TextView mTvRight;
    protected ImageView mIvRight;
    // 内容
    FrameLayout mFlContentContainer;
    /**
     * 进度加载对话框
     */
    private Dialog mLoadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setWindowFeature();
        setScreenOrientation();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_title);
        // 透明状态栏
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {// 5.0及以上
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            // 设置状态栏背景
            getWindow().setStatusBarColor(getResources().getColor(R.color.main_color));
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {// 4.4
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        initTitleBar();
        if (getLayoutResId() > 0) {
            View contentView = LayoutInflater.from(this).inflate(getLayoutResId(), null);
            ButterKnife.bind(this, contentView);
            if (mFlContentContainer.getChildCount() > 0) {
                mFlContentContainer.removeAllViews();
            }
            mFlContentContainer.addView(contentView);
        }
        initViewsAndVariables();
        addListeners();
        doRegister();
    }

    private void initTitleBar() {
        mTvTitle = (TextView) findViewById(R.id.activity_base_tv_title);
        mTvLeft = (TextView) findViewById(R.id.activity_base_tv_left);
        mIvLeft = (ImageView) findViewById(R.id.activity_base_iv_left);
        mTvRight = (TextView) findViewById(R.id.activity_base_tv_right);
        mIvRight = (ImageView) findViewById(R.id.activity_base_iv_right);
        mFlContentContainer = (FrameLayout) findViewById(R.id.activity_base_content);

        if (!TextUtils.isEmpty(getCenterTitle())) {
            mTvTitle.setText(getCenterTitle());
        }
        mIvLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    /**
     * 设置无标题栏等属性
     */
    protected void setWindowFeature() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
    }

    /**
     * 设置屏幕方向
     */
    protected void setScreenOrientation() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregister();
    }

    /**
     * 获得布局文件资源id
     *
     * @return R.layout.xx
     */
    protected abstract int getLayoutResId();

    /**
     * 设置标题
     */
    protected abstract String getCenterTitle();

    protected abstract void initViewsAndVariables();

    protected abstract void addListeners();

    /**
     * 注册/初始化receiver或第三方库
     */
    protected void doRegister() {
    }

    /**
     * 注销/释放相关资源
     */
    protected void unregister() {
    }

    //********************************************** 公用方法 **********************************************//

    /**
     * 设置标题
     *
     * @param title
     */
    protected void setTitle(String title) {
        if (!TextUtils.isEmpty(title)) {
            mTvTitle.setText(title);
        }
    }

    protected void showNoCancelLoading() {
        mLoadingDialog = Utils.showLoadingDialog(BaseTitleActivity.this, false);
    }

    /**
     * 显示加载对话框
     */
    protected void showLoading() {
        mLoadingDialog = Utils.showLoadingDialog(BaseTitleActivity.this, true);
    }

    /**
     * 隐藏加载对话框
     */
    protected void dismissLoading() {
        if (mLoadingDialog != null && mLoadingDialog.isShowing()) {
            mLoadingDialog.dismiss();
        }
    }

}
