package com.yueai.base;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.yueai.R;
import com.yueai.utils.Utils;

import butterknife.ButterKnife;

/**
 * base title fragment
 * Created by zhangdroid on 2016/8/9.
 */
public abstract class BaseTitleFragment extends Fragment {
    private View mRootView;
    // 标题栏
    TextView mTvTitle;
    protected TextView mTvLeft;
    protected ImageView mIvLeft;
    protected TextView mTvRight;
    protected ImageView mIvRight;
    // 分割线
    View mDividerView;
    // 内容
    FrameLayout mFlContentContainer;
    /**
     * 进度加载对话框
     */
    private Dialog mLoadingDialog;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        doRegister();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.fragment_base_title, container, false);
            mTvTitle = (TextView) mRootView.findViewById(R.id.fragment_base_tv_title);
            mTvLeft = (TextView) mRootView.findViewById(R.id.fragment_base_tv_left);
            mIvLeft = (ImageView) mRootView.findViewById(R.id.fragment_base_iv_left);
            mTvRight = (TextView) mRootView.findViewById(R.id.fragment_base_tv_right);
            mIvRight = (ImageView) mRootView.findViewById(R.id.fragment_base_iv_right);
            mDividerView = mRootView.findViewById(R.id.fragment_base_divider);
            mFlContentContainer = (FrameLayout) mRootView.findViewById(R.id.fragment_base_content);
            if (getLayoutResId() > 0) {
                View contentView = inflater.inflate(getLayoutResId(), null);
                ButterKnife.bind(this, contentView);
                if (mFlContentContainer.getChildCount() > 0) {
                    mFlContentContainer.removeAllViews();
                }
                mFlContentContainer.addView(contentView);
            }
        }
        initViewsAndVariables();
        addListeners();
        return mRootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregister();
    }

    /**
     * 获得布局文件资源id
     *
     * @return R.layout.xx
     */
    protected abstract int getLayoutResId();

    protected abstract void initViewsAndVariables();

    protected abstract void addListeners();

    /**
     * 注册/初始化receiver或第三方库
     */
    protected void doRegister() {
    }

    /**
     * 注销/释放相关资源
     */
    protected void unregister() {
    }


    //********************************************** 公用方法 **********************************************//

    /**
     * 设置标题
     *
     * @param title
     */
    public final void setTitle(String title) {
        if (!TextUtils.isEmpty(title)) {
            mTvTitle.setText(title);
        }
    }

    /**
     * 设置是否显示分割线
     *
     * @param isVisible
     */
    public final void setDividerVisble(boolean isVisible) {
        mDividerView.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    protected void showNoCancelLoading() {
        mLoadingDialog = Utils.showLoadingDialog(getActivity(), false);
    }

    /**
     * 显示加载对话框
     */
    protected void showLoading() {
        mLoadingDialog = Utils.showLoadingDialog(getActivity(), true);
    }

    /**
     * 隐藏加载对话框
     */
    protected void dismissLoading() {
        if (mLoadingDialog != null && mLoadingDialog.isShowing()) {
            mLoadingDialog.dismiss();
        }
    }

}
