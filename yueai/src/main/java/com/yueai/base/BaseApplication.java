package com.yueai.base;

import android.app.Application;
import android.content.Context;

import com.facebook.stetho.Stetho;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.library.utils.CrashHandler;
import com.yueai.utils.GreenDaoUtil;
import com.zhy.http.okhttp.OkHttpUtils;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

/**
 * Created by zhangdroid on 2016/5/25.
 */
public class BaseApplication extends Application {
    private static Context sApplicationContext;

    public static Context getGlobalContext() {
        return sApplicationContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sApplicationContext = getApplicationContext();
        // 初始化异常收集
        CrashHandler.getInstance().init(this);
        // 初始化OkHttpClient
        initOkhttpClient();
        // 初始化GreenDao
        GreenDaoUtil.initDatabase(this, "datelove-db");
        // 初始化Stetho
        Stetho.initializeWithDefaults(this);
    }

    /**
     * 配置网络请求相关参数
     */
    private void initOkhttpClient() {
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .readTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(15, TimeUnit.SECONDS)
                .addNetworkInterceptor(new StethoInterceptor())
                .build();
        OkHttpUtils.initClient(okHttpClient);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }
}
