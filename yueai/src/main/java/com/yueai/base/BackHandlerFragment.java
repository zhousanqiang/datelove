package com.yueai.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;

/**
 * 需要在Fragment中处理宿主Activity中返回键事件的Fragment的基类
 * <p>宿主Activity必须实现BackHandlerListener接口</p>
 * Created by zhangdroid on 2016/3/7.
 */
public abstract class BackHandlerFragment extends Fragment {
    private BackHandlerListener mBackHandlerListener;

    public interface BackHandlerListener {
        void setSelectedFragment(BackHandlerFragment selectedFragment);
    }

    /**
     * @return 当前子类Fragment消费了返回键则返回true，默认返回false
     */
    public abstract boolean onBackPressed();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!(getActivity() instanceof BackHandlerListener)) {
            throw new ClassCastException("Hosting Activity must implement BackHandlerListener");
        } else {
            mBackHandlerListener = (BackHandlerListener) getActivity();
        }
        doRegister();
    }

    @Override
    public void onStart() {
        super.onStart();
        // Mark this fragment as the selected Fragment.
        mBackHandlerListener.setSelectedFragment(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutResId(), container, false);
        ButterKnife.bind(this, view);
        initViewsAndVariables();
        addListeners();
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregister();
    }

    /**
     * 获得布局文件资源id
     *
     * @return R.layout.xx
     */
    protected abstract int getLayoutResId();

    protected abstract void initViewsAndVariables();

    protected abstract void addListeners();

    /**
     * 注册/初始化receiver或第三方库
     */
    protected void doRegister() {
    }

    /**
     * 注销/释放相关资源
     */
    protected void unregister() {
    }

}
