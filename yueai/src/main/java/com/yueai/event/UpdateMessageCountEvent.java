package com.yueai.event;

/**
 * Created by Administrator on 2016/12/27.
 */
public class UpdateMessageCountEvent {
    public int mUnreadMsgCount;

    public UpdateMessageCountEvent(int unreadCount) {
        this.mUnreadMsgCount = unreadCount;
    }

    public int getUnreadMsgCount() {
        return mUnreadMsgCount;
    }

    public void setUnreadMsgCount(int unreadMsgCount) {
        this.mUnreadMsgCount = unreadMsgCount;
    }

}
