package com.yueai.fragment;

import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.TextUtils;
import android.view.View;

import com.alibaba.fastjson.JSON;
import com.library.adapter.recyclerview.CommonRecyclerViewAdapter;
import com.library.adapter.recyclerview.RecyclerViewHolder;
import com.library.widgets.ItemTouchHelperCallback;
import com.library.widgets.RefreshRecyclerView;
import com.yueai.R;
import com.yueai.activity.ChatActivity;
import com.yueai.adapter.MessageAdapter;
import com.yueai.base.BaseTitleFragment;
import com.yueai.bean.Chat;
import com.yueai.bean.ChatInfo;
import com.yueai.bean.ChatMsg;
import com.yueai.bean.Image;
import com.yueai.bean.UserBase;
import com.yueai.constant.IUrlConstant;
import com.yueai.event.MessageChangedEvent;
import com.yueai.event.UpdateMessageCountEvent;
import com.yueai.utils.CommonRequestUtil;
import com.yueai.utils.Utils;
import com.yueai.xml.PlatformInfoXml;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.Callback;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import okhttp3.Call;
import okhttp3.Response;
/**
 * 信箱
 */
public class MessageFragment extends BaseTitleFragment {
    @BindView(R.id.message_recyclerview)
    RefreshRecyclerView mRefreshRecyclerView;
    private MessageAdapter mMessageAdapter;
    private int pageNum = 1;
    private final String pageSize = "30";
    private int mTotalUnreadMsgCnt;


    private Handler handler = new Handler();
    private Runnable runnable = new Runnable() {
        public void run() {
            this.update();
//            handler.postDelayed(this, 1000*10);// 间隔1秒
        }
        void update() {
            //刷新msg的内容
            loadMessageList();

        }
    };

    /**停止刷新**/
    public void stopRefish(){
        handler.removeCallbacks(runnable); //停止刷新
    }
    /**一进入此界面开始首次刷新**/
    public  void startFirstRefish(){
        handler.postDelayed(runnable, 100); //开始刷新
    }
    /**开始刷新**/
    public void startRefish(){
        handler.postDelayed(runnable, 1000*10); //开始刷新
    }
    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_message;
    }

    @Override
    protected void initViewsAndVariables() {
        setTitle(getString(R.string.tab_mail));
        // 设置下拉刷新样式
        mRefreshRecyclerView.setColorSchemeResources(R.color.main_color);
        mRefreshRecyclerView.setProgressBackgroundColorSchemeColor(Color.WHITE);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRefreshRecyclerView.setLayoutManager(linearLayoutManager);
        // 设置加载动画
        mRefreshRecyclerView.setLoadingView(Utils.getLoadingView(getActivity()));
        mMessageAdapter = new MessageAdapter(getActivity(), R.layout.item_fragment_message);
        mRefreshRecyclerView.setAdapter(mMessageAdapter);
    }


    @Override
    protected void addListeners() {
        // 设置侧滑监听
        mRefreshRecyclerView.setItemTouchHelperListener(new ItemTouchHelperCallback.ItemTouchHelperListener() {

            @Override
            public void onItemDrag(int fromPosition, int toPosition) {
                if (mMessageAdapter != null) {
                    mMessageAdapter.move(fromPosition, toPosition);
                }
            }

            @Override
            public void onItemSwipe(final int position) {
                if (mMessageAdapter != null) {
                    Chat chat = mMessageAdapter.getItemByPosition(position);
                    if (chat != null) {
                        UserBase userBase = chat.getUserBaseEnglish();
                        if (userBase != null) {
                            CommonRequestUtil.deleteChatHistory(userBase.getId(), true, new CommonRequestUtil.OnCommonListener() {
                                @Override
                                public void onSuccess() {
                                    mMessageAdapter.removeItem(position);
                                    loadMessageList();
                                }
                                @Override
                                public void onFail() {
                                }
                            });
                        }
                    }
                }
            }

            @Override
            public boolean onItemAnim(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState) {
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {// 侧滑
                    int distance = (int) Math.abs(dX);
                    RecyclerViewHolder recyclerViewHolder = ((RecyclerViewHolder) viewHolder);
                    if (distance <= recyclerViewHolder.getView(R.id.item_del_container).getWidth()) {// 滑动距离未超过删除方块的宽度
                        // 显示“左滑删除”提示
                        recyclerViewHolder.setVisibility(R.id.item_del_text, true);
                        recyclerViewHolder.setVisibility(R.id.item_del_icon, false);
                        viewHolder.itemView.scrollTo((int) -dX, 0);
                    } else if (distance < recyclerView.getWidth() / 2) {// 若方块全部显示后仍未超过item一半宽度，则显示删除图标
                        recyclerViewHolder.setVisibility(R.id.item_del_text, false);
                        recyclerViewHolder.setVisibility(R.id.item_del_icon, true);
                    }
                    return true;
                }
                return false;
            }

            @Override
            public void onItemClear(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                // 恢复状态
                viewHolder.itemView.setScrollX(0);
            }
        });
        // 下拉刷新
        mRefreshRecyclerView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshChatMsg();
            }
        });
        // 上拉加载更多
        mRefreshRecyclerView.setOnLoadingMoreListener(new RefreshRecyclerView.OnLoadingMoreListener() {
            @Override
            public void onLoadMore() {
                pageNum++;
                loadMessageList();
            }
        });
///**对触摸事件的监听，根据手指动作判断要不要进行自动刷新**/
//        mRefreshRecyclerView.setIsTouchListener(new RefreshRecyclerView.StopRefishListener() {
//            @Override
//            public void isTouch(boolean istouch) {
//                if(istouch){
//                   stopRefish();
//                }else {
//                    startRefish();
//                }
//            }
//        });
        mMessageAdapter.setOnItemClickListener(new CommonRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, RecyclerViewHolder viewHolder) {
                Chat chat = mMessageAdapter.getItemByPosition(position);
                if (chat != null) {
                    ChatMsg chatMsg = chat.getChat();
                    if (chatMsg != null) {
                        int unreadMsgCnt = chatMsg.getUnreadCount();
                        if (unreadMsgCnt > 0) {// 标记未读消息为已读
                            chatMsg.setUnreadCount(0);
                            chat.setChat(chatMsg);
                            mMessageAdapter.updateItem(position, chat);
                            // 更新总的未读消息数
                            mTotalUnreadMsgCnt = mTotalUnreadMsgCnt - unreadMsgCnt;
                            EventBus.getDefault().post(new UpdateMessageCountEvent(mTotalUnreadMsgCnt));
                        }
                    }
                    UserBase userBase = chat.getUserBaseEnglish();
                    if (userBase != null) {
                        Image image = userBase.getImage();
                        String imgUrl = "";
                        if (image != null) {
                            imgUrl = image.getThumbnailUrl();
                        }
                        ChatActivity.toChatActivity(getActivity(), userBase.getId(), userBase.getNickName(), imgUrl);
                    }
                }
            }
        });
    }

    private void refreshChatMsg() {
        pageNum = 1;
        loadMessageList();
    }

    /**
     * 加载聊天消息列表
     */
    private void loadMessageList() {
        OkHttpUtils.post()
                .url(IUrlConstant.URL_GET_ALL_CHAT_LIST)
                .addHeader("token", PlatformInfoXml.getToken())
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .addParams("pageNum", String.valueOf(pageNum))
                .addParams("pageSize", pageSize)
                .build()
                .execute(new Callback<ChatInfo>() {
                    @Override
                    public ChatInfo parseNetworkResponse(Response response, int id) throws Exception {
                        String resultJson = response.body().string();
                        if (!TextUtils.isEmpty(resultJson)) {
                            return JSON.parseObject(resultJson, ChatInfo.class);
                        }
                        return null;
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        stopRefresh(3);
                    }

                    @Override
                    public void onResponse(final ChatInfo response, int id) {
                        if (response != null) {
                            mTotalUnreadMsgCnt = response.getTotalUnread();
                            EventBus.getDefault().post(new UpdateMessageCountEvent(mTotalUnreadMsgCnt));
                            // 延时1秒显示
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    if (mMessageAdapter != null) {
                                        mMessageAdapter.setServerSystemTime(response.getSystemTime());
                                        if (pageNum == 1) {
                                            mMessageAdapter.replaceAll(response.getListChat());
                                        } else {
                                            mMessageAdapter.appendToList(response.getListChat());
                                        }
                                    }
                                }
                            }, 1000);
                        }
                        stopRefresh(1);
                    }
                });
    }


    private void stopRefresh(int secs) {
        if (mRefreshRecyclerView != null) {
            mRefreshRecyclerView.refreshCompleted(secs);
            mRefreshRecyclerView.loadMoreCompleted(secs);
        }
    }

    @Override
    protected void doRegister() {
        super.doRegister();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void unregister() {
        super.unregister();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onPause() {
        super.onPause();

    }

//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void onEvent(Message msg) {
//        if (msg.what == 1111111) {
//            //刷新msg的内容
//            loadMessageList();
//        }
//
//        }
    @Subscribe
    public void onEvent(MessageChangedEvent event) {
        refreshChatMsg();
    }


}

















///**
// * 信箱
// */
//public class MessageFragment extends BaseTitleFragment {
//    @BindView(R.id.message_recyclerview)
//    RefreshRecyclerView mRefreshRecyclerView;
//
//    private MessageAdapter mMessageAdapter;
//    private int pageNum = 1;
//    private final String pageSize = "30";
//    private int mTotalUnreadMsgCnt;
//
//    private Handler handler = new Handler();
//    private Runnable runnable = new Runnable() {
//        public void run() {
//            this.update();
////            handler.postDelayed(this, 1000*10);// 间隔1秒
//        }
//        void update() {
//            //刷新msg的内容
//            loadMessageList();
//
//        }
//    };
//
//    /**停止刷新**/
//    public void stopRefish(){
//        handler.removeCallbacks(runnable); //停止刷新
//    }
//    /**一进入此界面开始首次刷新**/
//    public void startFirstRefish(){
//        handler.postDelayed(runnable, 100); //开始刷新
//    }
//    /**开始刷新**/
//    public void startRefish(){
//        handler.postDelayed(runnable, 1000*10); //开始刷新
//    }
//    @Override
//    protected int getLayoutResId() {
//        return R.layout.fragment_message;
//    }
//
//    @Override
//    protected void initViewsAndVariables() {
//        setTitle(getString(R.string.tab_mail));
//        // 设置下拉刷新样式
//        mRefreshRecyclerView.setColorSchemeResources(R.color.main_color);
//        mRefreshRecyclerView.setProgressBackgroundColorSchemeColor(Color.WHITE);
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
//        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
//        mRefreshRecyclerView.setLayoutManager(linearLayoutManager);
//        // 设置加载动画
//        mRefreshRecyclerView.setLoadingView(Utils.getLoadingView(getActivity()));
//        mMessageAdapter = new MessageAdapter(getActivity(), R.layout.item_fragment_message);
//        mRefreshRecyclerView.setAdapter(mMessageAdapter);
//    }
//
//    @Override
//    protected void addListeners() {
//        // 设置侧滑监听
//        mRefreshRecyclerView.setItemTouchHelperListener(new ItemTouchHelperCallback.ItemTouchHelperListener() {
//
//            @Override
//            public void onItemDrag(int fromPosition, int toPosition) {
//                if (mMessageAdapter != null) {
//                    mMessageAdapter.move(fromPosition, toPosition);
//                }
//            }
//
//            @Override
//            public void onItemSwipe(final int position) {
//                if (mMessageAdapter != null) {
//                    Chat chat = mMessageAdapter.getItemByPosition(position);
//                    if (chat != null) {
//                        UserBase userBase = chat.getUserBaseEnglish();
//                        if (userBase != null) {
//                            CommonRequestUtil.deleteChatHistory(userBase.getId(), true, new CommonRequestUtil.OnCommonListener() {
//                                @Override
//                                public void onSuccess() {
//                                    mMessageAdapter.removeItem(position);
//                                }
//
//                                @Override
//                                public void onFail() {
//                                }
//                            });
//                        }
//                    }
//                }
//            }
//
//            @Override
//            public boolean onItemAnim(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState) {
//                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {// 侧滑
//                    int distance = (int) Math.abs(dX);
//                    RecyclerViewHolder recyclerViewHolder = ((RecyclerViewHolder) viewHolder);
//                    if (distance <= recyclerViewHolder.getView(R.id.item_del_container).getWidth()) {// 滑动距离未超过删除方块的宽度
//                        // 显示“左滑删除”提示
//                        recyclerViewHolder.setVisibility(R.id.item_del_text, true);
//                        recyclerViewHolder.setVisibility(R.id.item_del_icon, false);
//                        viewHolder.itemView.scrollTo((int) -dX, 0);
//                    } else if (distance < recyclerView.getWidth() / 2) {// 若方块全部显示后仍未超过item一半宽度，则显示删除图标
//                        recyclerViewHolder.setVisibility(R.id.item_del_text, false);
//                        recyclerViewHolder.setVisibility(R.id.item_del_icon, true);
//                    }
//                    return true;
//                }
//                return false;
//            }
//
//            @Override
//            public void onItemClear(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
//                // 恢复状态
//                viewHolder.itemView.setScrollX(0);
//            }
//        });
//        // 下拉刷新
//        mRefreshRecyclerView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                refreshChatMsg();
//            }
//        });
//        // 上拉加载更多
//        mRefreshRecyclerView.setOnLoadingMoreListener(new RefreshRecyclerView.OnLoadingMoreListener() {
//            @Override
//            public void onLoadMore() {
//                pageNum++;
//                loadMessageList();
//            }
//        });
//        mMessageAdapter.setOnItemClickListener(new CommonRecyclerViewAdapter.OnItemClickListener() {
//            @Override
//            public void onItemClick(View view, int position, RecyclerViewHolder viewHolder) {
//                Chat chat = mMessageAdapter.getItemByPosition(position);
//                if (chat != null) {
//                    ChatMsg chatMsg = chat.getChat();
//                    if (chatMsg != null) {
//                        int unreadMsgCnt = chatMsg.getUnreadCount();
//                        if (unreadMsgCnt > 0) {// 标记未读消息为已读
//                            chatMsg.setUnreadCount(0);
//                            chat.setChat(chatMsg);
//                            mMessageAdapter.updateItem(position, chat);
//                            // 更新总的未读消息数
//                            mTotalUnreadMsgCnt = mTotalUnreadMsgCnt - unreadMsgCnt;
//                            EventBus.getDefault().post(new UpdateMessageCountEvent(mTotalUnreadMsgCnt));
//                        }
//                    }
//                    UserBase userBase = chat.getUserBaseEnglish();
//                    if (userBase != null) {
//                        Image image = userBase.getImage();
//                        String imgUrl = "";
//                        if (image != null) {
//                            imgUrl = image.getThumbnailUrl();
//                        }
//                        ChatActivity.toChatActivity(getActivity(), userBase.getId(), userBase.getNickName(), imgUrl);
//                    }
//                }
//            }
//        });
//    }
//
//    private void refreshChatMsg() {
//        pageNum = 1;
//        loadMessageList();
//    }
//
//    /**
//     * 加载聊天消息列表
//     */
//    private void loadMessageList() {
//        OkHttpUtils.post()
//                .url(IUrlConstant.URL_GET_ALL_CHAT_LIST)
//                .addHeader("token", PlatformInfoXml.getToken())
//                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
//                .addParams("pageNum", String.valueOf(pageNum))
//                .addParams("pageSize", pageSize)
//                .build()
//                .execute(new Callback<ChatInfo>() {
//                    @Override
//                    public ChatInfo parseNetworkResponse(Response response, int id) throws Exception {
//                        String resultJson = response.body().string();
//                        if (!TextUtils.isEmpty(resultJson)) {
//                            return JSON.parseObject(resultJson, ChatInfo.class);
//                        }
//                        return null;
//                    }
//
//                    @Override
//                    public void onError(Call call, Exception e, int id) {
//                        stopRefresh(3);
//                    }
//
//                    @Override
//                    public void onResponse(final ChatInfo response, int id) {
//                        if (response != null) {
//                            mTotalUnreadMsgCnt = response.getTotalUnread();
//                            EventBus.getDefault().post(new UpdateMessageCountEvent(mTotalUnreadMsgCnt));
//                            // 延时1秒显示
//                            new Handler().postDelayed(new Runnable() {
//                                @Override
//                                public void run() {
//                                    if (mMessageAdapter != null) {
//                                        mMessageAdapter.setServerSystemTime(response.getSystemTime());
//                                        if (pageNum == 1) {
//                                            mMessageAdapter.replaceAll(response.getListChat());
//                                        } else {
//                                            mMessageAdapter.appendToList(response.getListChat());
//                                        }
//                                    }
//                                }
//                            }, 1000);
//                        }
//                        stopRefresh(1);
//                    }
//                });
//    }
//
//    private void stopRefresh(int secs) {
//        if (mRefreshRecyclerView != null) {
//            mRefreshRecyclerView.refreshCompleted(secs);
//            mRefreshRecyclerView.loadMoreCompleted(secs);
//        }
//    }
//
//    @Override
//    protected void doRegister() {
//        super.doRegister();
//        EventBus.getDefault().register(this);
//    }
//
//    @Override
//    protected void unregister() {
//        super.unregister();
//        EventBus.getDefault().unregister(this);
//    }
//
//    @Subscribe
//    public void onEvent(MessageChangedEvent event) {
//        refreshChatMsg();
//    }
//
//}
