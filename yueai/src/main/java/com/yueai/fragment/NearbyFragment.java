package com.yueai.fragment;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.library.adapter.recyclerview.CommonRecyclerViewAdapter;
import com.library.adapter.recyclerview.RecyclerViewHolder;
import com.library.widgets.RefreshRecyclerView;
import com.yueai.R;
import com.yueai.activity.UserInfoDetailActivity;
import com.yueai.adapter.NearbyAdapter;
import com.yueai.base.BaseTitleFragment;
import com.yueai.bean.MatcherInfo;
import com.yueai.bean.NearBy;
import com.yueai.bean.NearByUser;
import com.yueai.constant.IUrlConstant;
import com.yueai.event.UpdateUserProvince;
import com.yueai.utils.Utils;
import com.yueai.xml.PlatformInfoXml;
import com.yueai.xml.UserInfoXml;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.Callback;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import okhttp3.Call;
import okhttp3.Response;

/**
 * 附近
 */
public class NearbyFragment extends BaseTitleFragment {
    @BindView(R.id.nearby_recyclerview)
    RefreshRecyclerView mRefreshRecyclerView;
    @BindView(R.id.nearby_tip)
    TextView mTvNearbyTip;

    private StaggeredGridLayoutManager mStaggeredGridLayoutManager;
    private NearbyAdapter mNearbyAdapter;
    private int pageNum = 1;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_nearby;
    }

    @Override
    protected void initViewsAndVariables() {
        setTitle(getString(R.string.tab_near));
        // 设置下拉刷新样式
        mRefreshRecyclerView.setColorSchemeResources(R.color.main_color);
        mRefreshRecyclerView.setProgressBackgroundColorSchemeColor(Color.WHITE);
        // 设置加载动画
        mRefreshRecyclerView.setLoadingView(Utils.getLoadingView(getActivity()));
        mStaggeredGridLayoutManager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
        mRefreshRecyclerView.setLayoutManager(mStaggeredGridLayoutManager);
        mNearbyAdapter = new NearbyAdapter(getActivity(), R.layout.item_fragment_nearby);
        mRefreshRecyclerView.setAdapter(mNearbyAdapter);
        // 检查是否能定位
        checkGps();
    }

    @Override
    protected void addListeners() {
        // 下拉刷新
        mRefreshRecyclerView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh();
            }
        });
        // 上拉加载更多
        mRefreshRecyclerView.setOnLoadingMoreListener(new RefreshRecyclerView.OnLoadingMoreListener() {
            @Override
            public void onLoadMore() {
                pageNum++;
                loadNearbyData();
            }
        });
        mNearbyAdapter.setOnItemClickListener(new CommonRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, RecyclerViewHolder viewHolder) {
                NearByUser nearByUser = mNearbyAdapter.getItemByPosition(position);
                if (nearByUser != null) {
                    UserInfoDetailActivity.toUserInfoDetailActivity(getActivity(), nearByUser.getUserBaseEnglish().getId(),
                            null, UserInfoDetailActivity.SOURCE_FROM_NEARBY);
                }
            }
        });
        mTvNearbyTip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkGps()) {
                    refresh();
                } else {
                    startGPRS(getContext());
                }
            }
        });
    }

    public boolean checkGps() {
        LocationManager locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            mTvNearbyTip.setVisibility(View.GONE);
            mRefreshRecyclerView.setVisibility(View.VISIBLE);
            return true;
        } else {
            mTvNearbyTip.setVisibility(View.VISIBLE);
            mRefreshRecyclerView.setVisibility(View.GONE);
        }
        return false;
    }

    private void startGPRS(Context context) {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        try {
            context.startActivity(intent);
        } catch (ActivityNotFoundException ex) {
            // The Android SDK doc says that the location settings activity
            // may not be found. In that case show the general settings.
            // General settings activity
            intent.setAction(Settings.ACTION_SETTINGS);
            try {
                context.startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void refresh() {
        pageNum = 1;
        loadNearbyData();
    }

    private void loadNearbyData() {
        Map<String, MatcherInfo> map = new HashMap<>();
        MatcherInfo matcherInfo = new MatcherInfo();
        String matcherInfoStr = UserInfoXml.getMatchInfo();
        if (!TextUtils.isEmpty(matcherInfoStr)) {
            matcherInfo = JSON.parseObject(matcherInfoStr, MatcherInfo.class);
        }
        map.put("matcherInfo", matcherInfo);
        OkHttpUtils.post()
                .url(IUrlConstant.URL_NEARBY)
                .addHeader("token", PlatformInfoXml.getToken())
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .addParams("pageNum", String.valueOf(pageNum))
                .addParams("matcherInfo", JSON.toJSONString(map))
                .addParams("distance", "10")
                .build()
                .execute(new Callback<NearBy>() {
                    @Override
                    public NearBy parseNetworkResponse(Response response, int id) throws Exception {
                        String resultJson = response.body().string();
                        if (!TextUtils.isEmpty(resultJson)) {
                            return JSON.parseObject(resultJson, NearBy.class);
                        }
                        return null;
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        loadNearbyData();
                        stopRefresh(3);
                    }

                    @Override
                    public void onResponse(final NearBy response, int id) {
                        if (response != null) {
                            String isSucceed = response.getIsSucceed();
                            if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
                                // 延时1秒显示
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (mNearbyAdapter != null) {
                                            if (pageNum == 1) {
                                                if (response.getListUser() == null || (response.getListUser() != null && response.getListUser().size() == 0)) {
                                                    mTvNearbyTip.setVisibility(View.VISIBLE);
                                                }
                                                mNearbyAdapter.replaceAll(response.getListUser());
                                            } else {
                                                mNearbyAdapter.appendToList(response.getListUser());
                                            }
                                        }
                                    }
                                }, 1000);
                            }
                        }
                        stopRefresh(1);
                    }
                });
    }

    private void stopRefresh(int secs) {
        if (mRefreshRecyclerView != null) {
            mRefreshRecyclerView.refreshCompleted(secs);
            mRefreshRecyclerView.loadMoreCompleted(secs);
        }
    }
    @Subscribe
    public void onEvent(UpdateUserProvince event) {
        pageNum = 1;
        loadNearbyData();
    }
    @Override
    protected void doRegister() {
        EventBus.getDefault().register(this);
    }

    @Override
    protected void unregister() {
        EventBus.getDefault().unregister(this);
    }
}
