package com.yueai.fragment;


import android.os.Environment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.library.adapter.recyclerview.CommonRecyclerViewAdapter;
import com.library.adapter.recyclerview.RecyclerViewHolder;
import com.library.imageloader.CircleTransformation;
import com.library.imageloader.ImageLoader;
import com.library.imageloader.ImageLoaderUtil;
import com.library.utils.Util;
import com.yueai.R;
import com.yueai.activity.BuyServiceActivity;
import com.yueai.activity.GetPhotoActivity;
import com.yueai.activity.MatchInfoActivity;
import com.yueai.activity.MyCareActivity;
import com.yueai.activity.MyInfoActivity;
import com.yueai.activity.MyPhotoActivity;
import com.yueai.activity.SettingActivity;
import com.yueai.activity.UserInfoDetailActivity;
import com.yueai.adapter.SeeMeAdapter;
import com.yueai.base.BaseApplication;
import com.yueai.base.BaseTitleFragment;
import com.yueai.bean.Image;
import com.yueai.bean.SeeMe;
import com.yueai.bean.SeeMeList;
import com.yueai.bean.User;
import com.yueai.constant.IConfigConstant;
import com.yueai.constant.IUrlConstant;
import com.yueai.event.NoSeeMeEvent;
import com.yueai.event.UpdatePayInfoEvent;
import com.yueai.event.UpdateUserInfoEvent;
import com.yueai.event.UpdateUserProvince;
import com.yueai.utils.CommonRequestUtil;
import com.yueai.utils.FileUtil;
import com.yueai.xml.PlatformInfoXml;
import com.yueai.xml.UserInfoXml;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.Callback;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import okhttp3.Call;
import okhttp3.Response;

/**
 * 我
 */
public class MeFragment extends BaseTitleFragment implements View.OnClickListener {
    @BindView(R.id.head_icon)
    ImageView head_icon;
    @BindView(R.id.nick_name)
    TextView nick_name;
    @BindView(R.id.user_info)
    TextView user_info;
    @BindView(R.id.recyclerview_seeme)
    RecyclerView mRecyclerView;

    @BindView(R.id.buy_bean)
    TextView buy_bean;
    @BindView(R.id.buy_month)
    TextView buy_month;
    @BindView(R.id.my_info)
    TextView my_info;
    @BindView(R.id.me_vip)
    TextView me_vip;
    @BindView(R.id.my_photo)
    TextView my_photo;
    @BindView(R.id.my_match)
    TextView my_match;
    @BindView(R.id.my_care)
    TextView my_care;
    @BindView(R.id.my_set)
    TextView my_set;
    @BindView(R.id.no_person)
    TextView no_person;

    private SeeMeAdapter mSeeMeAdapter;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_me;
    }

    @Override
    protected void initViewsAndVariables() {
        setTitle(getString(R.string.tab_me));
        setMyInfo();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mSeeMeAdapter = new SeeMeAdapter(getActivity(), R.layout.item_seeme);
        mRecyclerView.setAdapter(mSeeMeAdapter);
        getSeeMe();
    }

    private void setMyInfo() {
        if (UserInfoXml.isMale()) {
            head_icon.setImageResource(R.drawable.icon_me_male);
        } else {
            head_icon.setImageResource(R.drawable.icon_me_female);
        }
        File avatarFile = new File(IConfigConstant.LOCAL_AVATAR_PATH);
        if (avatarFile != null && avatarFile.exists()) {
            ImageLoaderUtil.getInstance().loadCircleImage(getActivity(), head_icon, avatarFile);
        } else {
            ImageLoaderUtil.getInstance().loadImage(getActivity(), new ImageLoader.Builder().url(UserInfoXml.getAvatarThumbnailUrl()).
                    imageView(head_icon).transform(new CircleTransformation()).build());
            // 下载头像，缓存到本地
            new Thread(new Runnable() {
                @Override
                public void run() {
                    String imageUrl = UserInfoXml.getAvatarUrl();
                    if (!TextUtils.isEmpty(imageUrl)) {
                        FileUtil.downloadFile(imageUrl, BaseApplication.getGlobalContext().
                                getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath(), "avatar.jpg");
                    }
                }
            }).start();
        }
        nick_name.setText(UserInfoXml.getNickName());
        user_info.setText(UserInfoXml.getAge() + getString(R.string.years) + "\t\t" + UserInfoXml.getProvinceName());
        buy_bean.setSelected(UserInfoXml.isBeanUser());
        buy_month.setSelected(UserInfoXml.isMonthly());
    }

    @Override
    protected void addListeners() {
        buy_bean.setOnClickListener(this);
        buy_month.setOnClickListener(this);
        my_info.setOnClickListener(this);
        me_vip.setOnClickListener(this);
        my_photo.setOnClickListener(this);
        my_match.setOnClickListener(this);
        my_care.setOnClickListener(this);
        my_set.setOnClickListener(this);
        no_person.setOnClickListener(this);
        head_icon.setOnClickListener(this);
        mSeeMeAdapter.setOnItemClickListener(new CommonRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, RecyclerViewHolder viewHolder) {
                SeeMe seeMe = mSeeMeAdapter.getItemByPosition(position);
                if (seeMe != null) {
                    UserInfoDetailActivity.toUserInfoDetailActivity(getActivity(), seeMe.getUserBaseEnglish().getId(), null, UserInfoDetailActivity.SOURCE_FROM_SEEME);
                }
            }
        });
    }

    private void getSeeMe() {
        OkHttpUtils.post()
                .url(IUrlConstant.URL_GET_GUEST_LIST)
                .addHeader("token", PlatformInfoXml.getToken())
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .addParams("pageNum", "1")
                .addParams("pageSize", "100")
                .build()
                .execute(new Callback<SeeMeList>() {
                    @Override
                    public SeeMeList parseNetworkResponse(Response response, int id) throws Exception {
                        String resultJson = response.body().string();
                        if (!TextUtils.isEmpty(resultJson)) {
                            return JSON.parseObject(resultJson, SeeMeList.class);
                        }
                        return null;
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(SeeMeList response, int id) {
                        if (response != null) {
                            List<SeeMe> seeMeList = response.getSeeMeList();
                            if (!Util.isListEmpty(seeMeList)) {
                                mRecyclerView.setVisibility(View.VISIBLE);
                                no_person.setVisibility(View.GONE);
                                mSeeMeAdapter.replaceAll(seeMeList);
                            }
                        }
                    }
                });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.head_icon:
                GetPhotoActivity.toGetPhotoActivity(getActivity(), IConfigConstant.LOCAL_AVATAR_PATH, new GetPhotoActivity.OnGetPhotoListener() {
                    @Override
                    public void getSelectedPhoto(final File file) {
                        if (file != null) {
                            showLoading();
                            CommonRequestUtil.uploadImage(true, file, true, new CommonRequestUtil.OnUploadImageListener() {
                                @Override
                                public void onSuccess(Image image) {
                                    if (image != null) {
                                        // 保存头像信息
                                        UserInfoXml.setAvatarUrl(image.getImageUrl());
                                        UserInfoXml.setAvatarThumbnailUrl(image.getThumbnailUrl());
                                        UserInfoXml.setAvatarStatus(image.getStatus());
                                    }
                                    // 设置新头像
                                    ImageLoaderUtil.getInstance().loadCircleImage(getActivity(), head_icon, file);
                                    dismissLoading();
                                }

                                @Override
                                public void onFail() {
                                    dismissLoading();
                                }
                            });
                        }
                    }
                });
                break;
            case R.id.buy_bean:
                BuyServiceActivity.toBuyServiceActivity(getActivity(), BuyServiceActivity.INTENT_FROM_BEAN,
                        IConfigConstant.PAY_TAG_FROM_DETAIL, IConfigConstant.PAY_WAY_BEAN);
                break;
            case R.id.buy_month:
                BuyServiceActivity.toBuyServiceActivity(getActivity(), BuyServiceActivity.INTENT_FROM_MONTH,
                        IConfigConstant.PAY_TAG_FROM_DETAIL, IConfigConstant.PAY_WAY_MONTH);
                break;
            case R.id.me_vip:
                BuyServiceActivity.toBuyServiceActivity(getActivity(), BuyServiceActivity.INTENT_FROM_INTERCEPT,
                        IConfigConstant.PAY_TAG_FROM_DETAIL, IConfigConstant.PAY_WAY_ALL);
                break;

            case R.id.my_info:
                Util.gotoActivity(getActivity(), MyInfoActivity.class, false);
                break;
            case R.id.my_photo:
                Util.gotoActivity(getActivity(), MyPhotoActivity.class, false);
                break;
            case R.id.my_match:
                Util.gotoActivity(getActivity(), MatchInfoActivity.class, false);
                break;
            case R.id.my_care:
                Util.gotoActivity(getActivity(), MyCareActivity.class, false);
                break;
            case R.id.my_set:
                Util.gotoActivity(getActivity(), SettingActivity.class, false);
                break;
            case R.id.no_person:
                EventBus.getDefault().post(new NoSeeMeEvent());
                break;
        }
    }

    @Override
    protected void doRegister() {
        EventBus.getDefault().register(this);
    }

    @Override
    protected void unregister() {
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onEvent(UpdatePayInfoEvent event) {
        buy_bean.setSelected(UserInfoXml.isBeanUser());
        buy_month.setSelected(UserInfoXml.isMonthly());
    }

    @Subscribe
    public void onEvent(UpdateUserInfoEvent event) {
        CommonRequestUtil.loadUserInfo(new CommonRequestUtil.OnLoadUserInfoListener() {
            @Override
            public void onSuccess(User user) {
                setMyInfo();
            }

            @Override
            public void onFail() {

            }
        });
    }
    @Subscribe
    public void onEvent(UpdateUserProvince event) {
        user_info.setText(UserInfoXml.getAge() + getString(R.string.years) + "\t\t" + UserInfoXml.getProvinceName());

    }

}
