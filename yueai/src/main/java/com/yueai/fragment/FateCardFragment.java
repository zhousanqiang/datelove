package com.yueai.fragment;

import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.alibaba.fastjson.JSON;
import com.library.utils.Util;
import com.lorentzos.flingswipe.FlingCardListener;
import com.lorentzos.flingswipe.SwipeFlingAdapterView;
import com.yueai.R;
import com.yueai.activity.UserInfoDetailActivity;
import com.yueai.adapter.SwipeFlingViewAdapter;
import com.yueai.base.BaseTitleFragment;
import com.yueai.bean.Fate;
import com.yueai.bean.FateUser;
import com.yueai.bean.User;
import com.yueai.bean.UserBase;
import com.yueai.constant.IUrlConstant;
import com.yueai.event.UpdateUserProvince;
import com.yueai.utils.CommonRequestUtil;
import com.yueai.xml.PlatformInfoXml;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.Callback;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.Call;
import okhttp3.Response;

/**
 * 缘分页左右滑动卡片
 * Created by zhangdroid on 2016/8/11.
 */
public class FateCardFragment extends BaseTitleFragment implements SwipeFlingAdapterView.onFlingListener {
    @BindView(R.id.fragment_fate_SwipeView)
    SwipeFlingAdapterView mSwipeFlingAdapterView;
    @BindView(R.id.fragment_fate_dislike)
    ImageView mIvDislike;
    @BindView(R.id.fragment_fate_like)
    ImageView mIvLike;
    private SwipeFlingViewAdapter mSwipeFlingViewAdapter;
    private int pageNum = 1;
    private final String pageSize = "30";
    private boolean getProvince=true;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_fate_card;
    }

    @Override
    protected void initViewsAndVariables() {
        setTitle(getString(R.string.tab_fate));
        mSwipeFlingViewAdapter = new SwipeFlingViewAdapter(getActivity(), R.layout.item_swipefling_cardview);
        mSwipeFlingAdapterView.setAdapter(mSwipeFlingViewAdapter);

        // 加载缘分列表
        setBtnVisibility(false);
        showLoading();
        loadYuanFenData(false);
    }

    /**
     * 设置是否显示喜欢/不喜欢按钮
     *
     * @param isVisible
     */
    private void setBtnVisibility(boolean isVisible) {
        mIvDislike.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        mIvLike.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    @Override
    protected void addListeners() {
        mSwipeFlingAdapterView.setFlingListener(this);
        mSwipeFlingAdapterView.setOnItemClickListener(new SwipeFlingAdapterView.OnItemClickListener() {

            @Override
            public void onItemClicked(int itemPosition, Object dataObject) {
                FateUser fateUser = (FateUser) dataObject;
                if (fateUser != null) {
                    UserInfoDetailActivity.toUserInfoDetailActivity(getActivity(), fateUser.getUserBaseEnglish().getId(), null, UserInfoDetailActivity.SOURCE_FROM_FATE);
                }
            }
        });
    }

    @OnClick({R.id.fragment_fate_dislike, R.id.fragment_fate_like})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fragment_fate_dislike:// 左滑不感兴趣
                if (mSwipeFlingAdapterView != null) {
                    View selectedView = mSwipeFlingAdapterView.getSelectedView();
                    if (selectedView != null) {
                        ImageView ivDislike = (ImageView) selectedView.findViewById(R.id.item_swipefling_dislike);
                        ivDislike.setAlpha(1f);
                    }
                    FlingCardListener flingCardListener = mSwipeFlingAdapterView.getTopCardListener();
                    if (flingCardListener != null) {
                        flingCardListener.selectLeft();
                    }
                }
                break;

            case R.id.fragment_fate_like:// 右滑喜欢
                // 打招呼
                FateUser fateUser = (FateUser) mSwipeFlingAdapterView.getSelectedItem();
                if (fateUser != null) {
                    UserBase userBase = fateUser.getUserBaseEnglish();
                    if (userBase != null) {
                        sayHello(userBase.getId());
                    }
                }
                if (mSwipeFlingAdapterView != null) {
                    View selectedView = mSwipeFlingAdapterView.getSelectedView();
                    if (selectedView != null) {
                        ImageView ivLike = (ImageView) selectedView.findViewById(R.id.item_swipefling_like);
                        ivLike.setAlpha(1f);
                    }
                    FlingCardListener flingCardListener = mSwipeFlingAdapterView.getTopCardListener();
                    if (flingCardListener != null) {
                        flingCardListener.selectRight();
                    }
                }
                break;
        }
    }

    private void loadYuanFenData(final boolean getProvince) {
        OkHttpUtils.post()
                .url(IUrlConstant.URL_FATE)
                .addHeader("token", PlatformInfoXml.getToken())
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .addParams("pageNum", String.valueOf(pageNum))
                .addParams("pageSize", pageSize)
                .build()
                .execute(new Callback<Fate>() {
                    @Override
                    public Fate parseNetworkResponse(Response response, int id) throws Exception {
                        String resultJson = response.body().string();
                        if (!TextUtils.isEmpty(resultJson)) {
                            return JSON.parseObject(resultJson, Fate.class);
                        }
                        return null;
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        if (mSwipeFlingViewAdapter != null && mSwipeFlingViewAdapter.isEmpty()) {
                            setBtnVisibility(false);
                        }
                        loadYuanFenData(false);
                        stopRefresh(3);
                    }

                    @Override
                    public void onResponse(Fate response, int id) {
                        if (response != null) {
                            String isSucceed = response.getIsSucceed();
                            if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
                                List<FateUser> list = response.getListUser();
                                if (!Util.isListEmpty(list)) {
                                    setBtnVisibility(true);
                                    if (mSwipeFlingViewAdapter != null) {
                                        if (pageNum == 1) {
                                            mSwipeFlingViewAdapter.replaceAll(list);
                                        } else {
                                            mSwipeFlingViewAdapter.appendToList(list);
                                        }
                                    }
                                   if(getProvince) {
                                       if (mSwipeFlingAdapterView != null) {
                                           View selectedView = mSwipeFlingAdapterView.getSelectedView();
                                           if (selectedView != null) {
                                               ImageView ivDislike = (ImageView) selectedView.findViewById(R.id.item_swipefling_dislike);
                                               ivDislike.setAlpha(1f);
                                           }
                                           if(mSwipeFlingAdapterView!=null){
                                               FlingCardListener flingCardListener = mSwipeFlingAdapterView.getTopCardListener();
                                               if (flingCardListener != null) {
                                                   flingCardListener.selectLeft();
                                               }
                                           }

                                       }
                                   }
                                }
                            }
                        }
                        stopRefresh(1);
                    }
                });
    }

    /**
     * 隐藏加载对话框
     */
    private void stopRefresh(int secs) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dismissLoading();
            }
        }, secs * 1000);
    }

    @Override
    public void removeFirstObjectInAdapter() {
        if (mSwipeFlingViewAdapter != null) {
            mSwipeFlingViewAdapter.removeItem(0);
        }
    }

    @Override
    public void onLeftCardExit(Object dataObject) {
        // 拉黑，不喜欢
/*        FateUser FateUser = (FateUser) dataObject;
        if (FateUser != null) {

            UserBase userBase = FateUser.getUserBaseEnglish();
            if (userBase != null) {
                CommonRequestUtil.pull2Black(userBase.getId(), false, new CommonRequestUtil.OnPull2BlackListener() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onFail() {

                    }
                });
            }
        }*/
    }

    @Override
    public void onRightCardExit(Object dataObject) {
        // 打招呼
        FateUser FateUser = (FateUser) dataObject;

        if (FateUser != null) {
            UserBase userBase = FateUser.getUserBaseEnglish();
            if (userBase != null) {
                sayHello(userBase.getId());
            }
        }
    }

    @Override
    public void onAdapterAboutToEmpty(int itemsInAdapter) {
        if (itemsInAdapter == 9) {// 剩余10条数据时加载更多
            pageNum++;
            loadYuanFenData(false);
        }
    }

    @Override
    public void onScroll(float scrollProgressPercent) {
        if (mSwipeFlingAdapterView != null) {
            View selectedView = mSwipeFlingAdapterView.getSelectedView();
            if (selectedView != null) {
                // 右滑
                ImageView ivLike = (ImageView) selectedView.findViewById(R.id.item_swipefling_like);
                ivLike.setAlpha(scrollProgressPercent > 0 ? scrollProgressPercent : 0);
                // 左滑
                ImageView ivDislike = (ImageView) selectedView.findViewById(R.id.item_swipefling_dislike);
                ivDislike.setAlpha(scrollProgressPercent < 0 ? -scrollProgressPercent : 0);
            }
        }
    }

    private void sayHello(String userId) {
        CommonRequestUtil.sayHello(getActivity(), userId, CommonRequestUtil.SAY_HELLO_TYPE_FATE,
                true, new CommonRequestUtil.OnCommonListener() {

                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onFail() {

                    }
                });
    }
    @Subscribe
    public void onEvent(UpdateUserProvince event) {
        // 加载缘分列表
        setBtnVisibility(false);
        loadYuanFenData(getProvince);
    }
    @Override
    protected void doRegister() {
        EventBus.getDefault().register(this);
    }

    @Override
    protected void unregister() {
        EventBus.getDefault().unregister(this);
    }

}
