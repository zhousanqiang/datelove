package com.yueai.bean;

/**
 * 页眉对象
 * Created by zhangdroid on 2016/8/3.
 */
public class HeadMsgNotice {
    private String message;
    private String Msg;
    private String isSucceed;
    private int displaySecond;
    private int unreadMsg;
    // 通知类型：0 没有页眉通知
    //    1 未读信通知（跳转到信箱列表页）
    //    2 谁正在看我通知（推荐，同城上线，新注册，打招呼，符合你的条件）（跳转到对方空间页）
    //    3 支付失败、照片未审核通过通知（跳转到支付失败、照片未审核通过信箱详情页）
    //    4 诚信认证-身份证（军官证）审核失败通知（跳转到身份证（军官证）认证页面）
    //    5 照片通过审核，查看相册（跳转到“我”页面）；
    //    6 广告通知（跳转到advertiesUrl指定html5页面）
    private String noticeType;
    private String remoteUserId;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMsg() {
        return Msg;
    }

    public void setMsg(String msg) {
        Msg = msg;
    }

    public String getIsSucceed() {
        return isSucceed;
    }

    public void setIsSucceed(String isSucceed) {
        this.isSucceed = isSucceed;
    }

    public int getDisplaySecond() {
        return displaySecond;
    }

    public void setDisplaySecond(int displaySecond) {
        this.displaySecond = displaySecond;
    }

    public int getUnreadMsg() {
        return unreadMsg;
    }

    public void setUnreadMsg(int unreadMsg) {
        this.unreadMsg = unreadMsg;
    }

    public String getNoticeType() {
        return noticeType;
    }

    public void setNoticeType(String noticeType) {
        this.noticeType = noticeType;
    }

    public String getRemoteUserId() {
        return remoteUserId;
    }

    public void setRemoteUserId(String remoteUserId) {
        this.remoteUserId = remoteUserId;
    }

    @Override
    public String toString() {
        return "HeadMsgNotice{" +
                "message='" + message + '\'' +
                ", Msg='" + Msg + '\'' +
                ", isSucceed='" + isSucceed + '\'' +
                ", displaySecond=" + displaySecond +
                ", unreadMsg=" + unreadMsg +
                ", noticeType=" + noticeType +
                ", remoteUserId=" + remoteUserId +
                '}';
    }
}
