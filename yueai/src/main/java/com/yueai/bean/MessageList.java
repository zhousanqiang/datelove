package com.yueai.bean;

import java.util.List;

/**
 * 聊天列表对象
 * Created by zhangdroid on 2016/7/4.
 */
public class MessageList extends BaseModel {
    private long systemTime;// 系统当前时间戳
    private String lastMsgId;// 最新一条消息
    private String ownerIconUrl;// 用户头像URL
    private String showWriteMsgIntercept; // 展示写信拦截入口,1展示,2隐藏不展示
    private String payType; // 支付方式(支付关闭：-1；苹果内支付：1WEB支付中心支付：0)
    private String isShowSendVipByPraise;// 是否显示好评送会员拦截入口（0：不显示 1：显示）；
    private String payWay;// 支付渠道信息(苹果内支付：apple;支付关闭:-1;WEB支付中心支付：url地址)
    private List<Message> listMsg;

    public long getSystemTime() {
        return systemTime;
    }

    public void setSystemTime(long systemTime) {
        this.systemTime = systemTime;
    }

    public String getLastMsgId() {
        return lastMsgId;
    }

    public void setLastMsgId(String lastMsgId) {
        this.lastMsgId = lastMsgId;
    }

    public String getOwnerIconUrl() {
        return ownerIconUrl;
    }

    public void setOwnerIconUrl(String ownerIconUrl) {
        this.ownerIconUrl = ownerIconUrl;
    }

    public String getShowWriteMsgIntercept() {
        return showWriteMsgIntercept;
    }

    public void setShowWriteMsgIntercept(String showWriteMsgIntercept) {
        this.showWriteMsgIntercept = showWriteMsgIntercept;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getIsShowSendVipByPraise() {
        return isShowSendVipByPraise;
    }

    public void setIsShowSendVipByPraise(String isShowSendVipByPraise) {
        this.isShowSendVipByPraise = isShowSendVipByPraise;
    }

    public String getPayWay() {
        return payWay;
    }

    public void setPayWay(String payWay) {
        this.payWay = payWay;
    }

    public List<Message> getListMsg() {
        return listMsg;
    }

    public void setListMsg(List<Message> listMsg) {
        this.listMsg = listMsg;
    }

    @Override
    public String toString() {
        return "MessageList{" +
                "systemTime='" + systemTime + '\'' +
                ", lastMsgId='" + lastMsgId + '\'' +
                ", ownerIconUrl='" + ownerIconUrl + '\'' +
                ", showWriteMsgIntercept='" + showWriteMsgIntercept + '\'' +
                ", payType='" + payType + '\'' +
                ", isShowSendVipByPraise='" + isShowSendVipByPraise + '\'' +
                ", payWay='" + payWay + '\'' +
                ", listMsg=" + listMsg +
                '}';
    }

}
