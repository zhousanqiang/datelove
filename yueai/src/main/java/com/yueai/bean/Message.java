package com.yueai.bean;

import com.yueai.online.DaoSession;
import com.yueai.online.MessageDao;

import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Keep;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.ToOne;
import org.greenrobot.greendao.annotation.Transient;
import com.yueai.online.ImageDao;
import com.yueai.online.QaQuestionDao;

/**
 * 聊天消息对象
 * Created by zhangdroid on 2016/6/22.
 */
@Entity
public class Message {
    @Id(autoincrement = true)
    private Long _id;
    private String id;// 消息id
    private String uid;// 发信人id
    private String recevUserId;// 收信人id
    // 1->对方文本消息，2->对方语音消息，3->自己文本消息，4->文本消息, 7->语音信件,8->qa问答信,9->小助手问题
    // 10->图片, 11->qa解锁信）
    @NotNull
    private String msgType;
    private String content;// 消息内容
    private String audioUrl;// 语音url
    private String audioTime;// 语音时间
    private String recevStatus;// 语音状态(0是未读，1是已读)
    private String createDate;// 发送时间
    private long createDateMills;// 发送时间（毫秒数）
    @ToOne(joinProperty = "_id")
    private QaQuestion qaQuestion;// QA对象
    @ToOne(joinProperty = "_id")
    private Image chatImage;// 图片对象
    @Transient
    private String language;// 语言
    @Transient
    private String country;// 国家
    /**
     * Used to resolve relations
     */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;
    /**
     * Used for active entity operations.
     */
    @Generated(hash = 859287859)
    private transient MessageDao myDao;

    @Generated(hash = 728694940)
    public Message(Long _id, String id, String uid, String recevUserId,
            @NotNull String msgType, String content, String audioUrl, String audioTime,
            String recevStatus, String createDate, long createDateMills) {
        this._id = _id;
        this.id = id;
        this.uid = uid;
        this.recevUserId = recevUserId;
        this.msgType = msgType;
        this.content = content;
        this.audioUrl = audioUrl;
        this.audioTime = audioTime;
        this.recevStatus = recevStatus;
        this.createDate = createDate;
        this.createDateMills = createDateMills;
    }

    @Generated(hash = 637306882)
    public Message() {
    }

    @Generated(hash = 579984933)
    private transient Long qaQuestion__resolvedKey;
    @Generated(hash = 1969775772)
    private transient Long chatImage__resolvedKey;

    public Long get_id() {
        return this._id;
    }

    public void set_id(Long _id) {
        this._id = _id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getRecevUserId() {
        return recevUserId;
    }

    public void setRecevUserId(String recevUserId) {
        this.recevUserId = recevUserId;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAudioUrl() {
        return audioUrl;
    }

    public void setAudioUrl(String audioUrl) {
        this.audioUrl = audioUrl;
    }

    public String getAudioTime() {
        return audioTime;
    }

    public void setAudioTime(String audioTime) {
        this.audioTime = audioTime;
    }

    public String getRecevStatus() {
        return recevStatus;
    }

    public void setRecevStatus(String recevStatus) {
        this.recevStatus = recevStatus;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public long getCreateDateMills() {
        return createDateMills;
    }

    public void setCreateDateMills(long createDateMills) {
        this.createDateMills = createDateMills;
    }

    @Keep
    public QaQuestion getQaQuestion() {
        return qaQuestion;
    }

    @Keep
    public void setQaQuestion(QaQuestion qaQuestion) {
        this.qaQuestion = qaQuestion;
    }

    @Keep
    public Image getChatImage() {
        return chatImage;
    }

    @Keep
    public void setChatImage(Image chatImage) {
        this.chatImage = chatImage;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return "Message{" +
                "_id=" + _id +
                ", id='" + id + '\'' +
                ", uid='" + uid + '\'' +
                ", recevUserId='" + recevUserId + '\'' +
                ", msgType='" + msgType + '\'' +
                ", content='" + content + '\'' +
                ", audioUrl='" + audioUrl + '\'' +
                ", audioTime='" + audioTime + '\'' +
                ", recevStatus='" + recevStatus + '\'' +
                ", createDate='" + createDate + '\'' +
                ", createDateMills=" + createDateMills +
                ", qaQuestion=" + qaQuestion +
                ", chatImage=" + chatImage +
                ", language='" + language + '\'' +
                ", country='" + country + '\'' +
                ", daoSession=" + daoSession +
                ", myDao=" + myDao +
                ", qaQuestion__resolvedKey=" + qaQuestion__resolvedKey +
                ", chatImage__resolvedKey=" + chatImage__resolvedKey +
                '}';
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    /**
     * called by internal mechanisms, do not call yourself.
     */
    @Generated(hash = 747015224)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getMessageDao() : null;
    }

}
