package com.yueai.bean;

import java.util.List;

/**
 * 搜爱列表对象
 * Created by zhangdroid on 2016/8/12.
 */
public class Search extends BaseModel {
    int totalCount;// 总数
    int pageSize;// 该页内容数,
    int pageNum;// 页码
    int resultType;// 结果是否为补充用户，1->正常搜索结果；-1 ->补充的搜索结果（放宽搜索条件）
    List<SearchUser> listSearch;// 搜索数据对象列表
    int randomNum;// 搜索随机因数,每次查询时服务端返回

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getResultType() {
        return resultType;
    }

    public void setResultType(int resultType) {
        this.resultType = resultType;
    }

    public List<SearchUser> getListSearch() {
        return listSearch;
    }

    public void setListSearch(List<SearchUser> listSearch) {
        this.listSearch = listSearch;
    }

    public int getRandomNum() {
        return randomNum;
    }

    public void setRandomNum(int randomNum) {
        this.randomNum = randomNum;
    }

    @Override
    public String toString() {
        return "Search{" +
                "totalCount=" + totalCount +
                ", pageSize=" + pageSize +
                ", pageNum=" + pageNum +
                ", resultType=" + resultType +
                ", listSearch=" + listSearch +
                ", randomNum=" + randomNum +
                '}';
    }

}
