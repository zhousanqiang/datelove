package com.yueai.bean;

import java.util.List;

/**
 * 注册成功后返回的数据对象
 * Created by zhangdroid on 2016/6/23.
 */
public class Register extends BaseModel {
    String userName;// 用户名
    String password;// 密码
    String regTime;// 用户注册时间
    String token;// 用户token，之后所有的请求接口都要传入token
    String isQaSwitch; // 注册成功QA界面开关：1->显示，0->不显示
    List<String> recallTags; // 召回tag列表
    List<String> listName; // 随机昵称列表
    List<UserBase> listUser;// QA用户列表
    User userPandora;// 用户对象
    String introduceTemplate;
    String isInterceptSteps2;
    String isShowOneYuanDialog;
    String isShowUploadAudioInformation;
    String sessionId;
    String type;
    String isQuickSayHello;// 登陆时是否可以批量打招呼：1、可以 0、不可以
    String isShowHeadNotice;// 老页眉开关
    String isShowHeadNotice2;// 新页眉开关
    String isShowCommendUser;// 用户推荐开关

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIsShowUploadAudioInformation() {
        return isShowUploadAudioInformation;
    }

    public void setIsShowUploadAudioInformation(String isShowUploadAudioInformation) {
        this.isShowUploadAudioInformation = isShowUploadAudioInformation;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRegTime() {
        return regTime;
    }

    public void setRegTime(String regTime) {
        this.regTime = regTime;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getIsQaSwitch() {
        return isQaSwitch;
    }

    public void setIsQaSwitch(String isQaSwitch) {
        this.isQaSwitch = isQaSwitch;
    }

    public List<String> getRecallTags() {
        return recallTags;
    }

    public void setRecallTags(List<String> recallTags) {
        this.recallTags = recallTags;
    }

    public List<String> getListName() {
        return listName;
    }

    public void setListName(List<String> listName) {
        this.listName = listName;
    }

    public List<UserBase> getListUser() {
        return listUser;
    }

    public void setListUser(List<UserBase> listUser) {
        this.listUser = listUser;
    }

    public User getUserPandora() {
        return userPandora;
    }

    public void setUserPandora(User userPandora) {
        this.userPandora = userPandora;
    }

    public String getIntroduceTemplate() {
        return introduceTemplate;
    }

    public void setIntroduceTemplate(String introduceTemplate) {
        this.introduceTemplate = introduceTemplate;
    }

    public String getIsInterceptSteps2() {
        return isInterceptSteps2;
    }

    public void setIsInterceptSteps2(String isInterceptSteps2) {
        this.isInterceptSteps2 = isInterceptSteps2;
    }

    public String getIsShowOneYuanDialog() {
        return isShowOneYuanDialog;
    }

    public void setIsShowOneYuanDialog(String isShowOneYuanDialog) {
        this.isShowOneYuanDialog = isShowOneYuanDialog;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getIsQuickSayHello() {
        return isQuickSayHello;
    }

    public void setIsQuickSayHello(String isQuickSayHello) {
        this.isQuickSayHello = isQuickSayHello;
    }

    public String getIsShowHeadNotice() {
        return isShowHeadNotice;
    }

    public void setIsShowHeadNotice(String isShowHeadNotice) {
        this.isShowHeadNotice = isShowHeadNotice;
    }

    public String getIsShowHeadNotice2() {
        return isShowHeadNotice2;
    }

    public void setIsShowHeadNotice2(String isShowHeadNotice2) {
        this.isShowHeadNotice2 = isShowHeadNotice2;
    }

    public String getIsShowCommendUser() {
        return isShowCommendUser;
    }

    public void setIsShowCommendUser(String isShowCommendUser) {
        this.isShowCommendUser = isShowCommendUser;
    }

    @Override
    public String toString() {
        return "Register{" +
                "userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", regTime='" + regTime + '\'' +
                ", token='" + token + '\'' +
                ", isQaSwitch='" + isQaSwitch + '\'' +
                ", recallTags=" + recallTags +
                ", listName=" + listName +
                ", listUser=" + listUser +
                ", userPandora=" + userPandora +
                ", introduceTemplate='" + introduceTemplate + '\'' +
                ", isInterceptSteps2='" + isInterceptSteps2 + '\'' +
                ", isShowOneYuanDialog='" + isShowOneYuanDialog + '\'' +
                ", isShowUploadAudioInformation='" + isShowUploadAudioInformation + '\'' +
                ", sessionId='" + sessionId + '\'' +
                ", type='" + type + '\'' +
                ", isQuickSayHello='" + isQuickSayHello + '\'' +
                ", isShowHeadNotice='" + isShowHeadNotice + '\'' +
                ", isShowHeadNotice2='" + isShowHeadNotice2 + '\'' +
                ", isShowCommendUser='" + isShowCommendUser + '\'' +
                '}';
    }

}
