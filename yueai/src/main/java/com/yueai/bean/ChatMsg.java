package com.yueai.bean;

/**
 * 聊天信息对象
 * Created by zhangdroid on 2016/8/23.
 */
public class ChatMsg {
    private String id;
    private long localUserId;// 己方id
    private long remoteUserId;// 对方id
    private int chatStatus;// 聊天状态类型 1、开始写信 2、发信 3、收信 4、配对
    private int unreadCount;// 未读消息数
    private String lastTime;// 最新消息的时间
    private String lastContent;// 最新消息
    private String addTime;// 收到新消息的时间
    private long addTimeMills;// 首次收到消息的时间
    private int fromChannel;// 渠道号
    private String country;
    private String language;
    private long  lastTimeMills;//最新收到的消息时间


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getLocalUserId() {
        return localUserId;
    }

    public void setLocalUserId(long localUserId) {
        this.localUserId = localUserId;
    }

    public long getRemoteUserId() {
        return remoteUserId;
    }

    public void setRemoteUserId(long remoteUserId) {
        this.remoteUserId = remoteUserId;
    }

    public int getChatStatus() {
        return chatStatus;
    }

    public void setChatStatus(int chatStatus) {
        this.chatStatus = chatStatus;
    }

    public int getUnreadCount() {
        return unreadCount;
    }

    public void setUnreadCount(int unreadCount) {
        this.unreadCount = unreadCount;
    }

    public String getLastTime() {
        return lastTime;
    }

    public void setLastTime(String lastTime) {
        this.lastTime = lastTime;
    }

    public String getLastContent() {
        return lastContent;
    }

    public void setLastContent(String lastContent) {
        this.lastContent = lastContent;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public long getAddTimeMills() {
        return addTimeMills;
    }
    public long getLastTimeMills() {
        return lastTimeMills;
    }
    public void setLastTimeMills(long lastTimeMills) {
        this.lastTimeMills = lastTimeMills;
    }
    public void setAddTimeMills(long addTimeMills) {
        this.addTimeMills = addTimeMills;
    }

    public int getFromChannel() {
        return fromChannel;
    }

    public void setFromChannel(int fromChannel) {
        this.fromChannel = fromChannel;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Override
    public String toString() {
        return "ChatMsg{" +
                "id=" + id +
                ", localUserId=" + localUserId +
                ", remoteUserId=" + remoteUserId +
                ", chatStatus=" + chatStatus +
                ", unreadCount=" + unreadCount +
                ", lastTime='" + lastTime + '\'' +
                ", lastContent='" + lastContent + '\'' +
                ", addTime='" + addTime + '\'' +
                ", addTimeMills=" + addTimeMills +
                ", fromChannel=" + fromChannel +
                ", country='" + country + '\'' +
                ", language='" + language + '\'' +
                '}';
    }

}
