package com.yueai.bean;

import com.yueai.online.DaoSession;
import com.yueai.online.QaQuestionDao;

import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.JoinProperty;
import org.greenrobot.greendao.annotation.Keep;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.ToMany;

import java.util.List;
import com.yueai.online.QaAnswerDao;

/**
 * Created by zhangdroid on 2016/11/25.
 */
@Entity
public class QaQuestion {
    @Id(autoincrement = true)
    private Long _id;
    private String id;
    @NotNull
    private String content;
    private String strategyType;
    @ToMany(joinProperties = {@JoinProperty(name = "id", referencedName = "id")})
    private List<QaAnswer> listQaAnswer;
    /** Used to resolve relations */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;
    /** Used for active entity operations. */
    @Generated(hash = 1397039887)
    private transient QaQuestionDao myDao;

    @Generated(hash = 1112320801)
    public QaQuestion(Long _id, String id, @NotNull String content,
            String strategyType) {
        this._id = _id;
        this.id = id;
        this.content = content;
        this.strategyType = strategyType;
    }

    @Generated(hash = 1457455800)
    public QaQuestion() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getStrategyType() {
        return strategyType;
    }

    public void setStrategyType(String strategyType) {
        this.strategyType = strategyType;
    }

    @Keep
    public List<QaAnswer> getListQaAnswer() {
        return listQaAnswer;
    }

    @Keep
    public void setListQaAnswer(List<QaAnswer> listQaAnswer) {
        this.listQaAnswer = listQaAnswer;
    }

    @Override
    public String toString() {
        return "QaQuestion{" +
                "id='" + id + '\'' +
                ", content='" + content + '\'' +
                ", strategyType='" + strategyType + '\'' +
                ", listQaAnswer=" + listQaAnswer +
                '}';
    }

    public Long get_id() {
        return this._id;
    }

    public void set_id(Long _id) {
        this._id = _id;
    }

    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    @Generated(hash = 504936636)
    public synchronized void resetListQaAnswer() {
        listQaAnswer = null;
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 1012586183)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getQaQuestionDao() : null;
    }
}
