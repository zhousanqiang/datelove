package com.yueai.bean;

/**
 * 临时保存个人资料项
 * Modified by zhangdroid on 2017/03/10.
 */
public class ChangeMyInfoBean {
    private String monologue;
    private String nickname;
    private String birthday;
    private String sign;
    private String area;
    private String income;
    private String height;
    private String work;
    private String education;
    private String marriage;
    private String wantBaby;
    private String sport;
    private String personal;
    private String wantToGo;
    private String bookCartoon;
    private String interest;

    public String getMonologue() {
        return monologue;
    }

    public void setMonologue(String monologue) {
        this.monologue = monologue;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getMarriage() {
        return marriage;
    }

    public void setMarriage(String marriage) {
        this.marriage = marriage;
    }

    public String getWantBaby() {
        return wantBaby;
    }

    public void setWantBaby(String wantBaby) {
        this.wantBaby = wantBaby;
    }

    public String getSport() {
        return sport;
    }

    public void setSport(String sport) {
        this.sport = sport;
    }

    public String getPersonal() {
        return personal;
    }

    public void setPersonal(String personal) {
        this.personal = personal;
    }

    public String getWantToGo() {
        return wantToGo;
    }

    public void setWantToGo(String wantToGo) {
        this.wantToGo = wantToGo;
    }

    public String getBookCartoon() {
        return bookCartoon;
    }

    public void setBookCartoon(String bookCartoon) {
        this.bookCartoon = bookCartoon;
    }

    public String getInterest() {
        return interest;
    }

    public void setInterest(String interest) {
        this.interest = interest;
    }

    @Override
    public String toString() {
        return "ChangeMyInfoBean{" +
                "monologue='" + monologue + '\'' +
                ", nickname='" + nickname + '\'' +
                ", birthday='" + birthday + '\'' +
                ", sign='" + sign + '\'' +
                ", area='" + area + '\'' +
                ", income='" + income + '\'' +
                ", height='" + height + '\'' +
                ", work='" + work + '\'' +
                ", education='" + education + '\'' +
                ", marriage='" + marriage + '\'' +
                ", wantBaby='" + wantBaby + '\'' +
                ", sport='" + sport + '\'' +
                ", personal='" + personal + '\'' +
                ", wantToGo='" + wantToGo + '\'' +
                ", bookCartoon='" + bookCartoon + '\'' +
                ", interest='" + interest + '\'' +
                '}';
    }

}
