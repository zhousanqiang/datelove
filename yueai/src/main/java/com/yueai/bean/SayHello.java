package com.yueai.bean;

/**
 * 打招呼配对
 * Created by zhangdroid on 2016/7/9.
 */
public class SayHello extends BaseModel {
    String isMatched;// 是否配对成功
    User matchUser;// 配对成功用户
    long cutDownTime;// 赠送like倒计时时间戳(毫秒数)
    int currentLikeValue;// 当前likeValue数量

    public String getIsMatched() {
        return isMatched;
    }

    public void setIsMatched(String isMatched) {
        this.isMatched = isMatched;
    }

    public User getMatchUser() {
        return matchUser;
    }

    public void setMatchUser(User matchUser) {
        this.matchUser = matchUser;
    }

    public long getCutDownTime() {
        return cutDownTime;
    }

    public void setCutDownTime(long cutDownTime) {
        this.cutDownTime = cutDownTime;
    }

    public int getCurrentLikeValue() {
        return currentLikeValue;
    }

    public void setCurrentLikeValue(int currentLikeValue) {
        this.currentLikeValue = currentLikeValue;
    }

    @Override
    public String toString() {
        return "SayHello{" +
                "isMatched='" + isMatched + '\'' +
                ", matchUser=" + matchUser +
                ", cutDownTime=" + cutDownTime +
                ", currentLikeValue=" + currentLikeValue +
                '}';
    }

}
