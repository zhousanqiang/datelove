package com.yueai.bean;

import android.os.Parcel;
import android.os.Parcelable;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Transient;

/**
 * 图片对象
 * Created by zhangdroid on 2016/6/23.
 */
@Entity
public class Image implements Parcelable {
    @Id(autoincrement = true)
    private Long _id;
    private String id;// 图片id
    private int state;// 图片状态,0->close 1->open
    private int isMain;// 图片类型,3->已设置头像,9->不允许设置头像,10->审核中
    private String thumbnailUrl; // 缩略图
    private String thumbnailUrlM;// 缩略图（大）
    private String imageUrl;// 原图
    private String status;// 审核状态
    @Transient
    private String language;
    @Transient
    private String country;
    @Transient
    private String localPath;

    @Generated(hash = 229324438)
    public Image(Long _id, String id, int state, int isMain, String thumbnailUrl,
                 String thumbnailUrlM, String imageUrl, String status) {
        this._id = _id;
        this.id = id;
        this.state = state;
        this.isMain = isMain;
        this.thumbnailUrl = thumbnailUrl;
        this.thumbnailUrlM = thumbnailUrlM;
        this.imageUrl = imageUrl;
        this.status = status;
    }

    @Generated(hash = 1590301345)
    public Image() {
    }

    public Long get_id() {
        return this._id;
    }

    public void set_id(Long _id) {
        this._id = _id;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getState() {
        return this.state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getIsMain() {
        return this.isMain;
    }

    public void setIsMain(int isMain) {
        this.isMain = isMain;
    }

    public String getThumbnailUrl() {
        return this.thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getThumbnailUrlM() {
        return this.thumbnailUrlM;
    }

    public void setThumbnailUrlM(String thumbnailUrlM) {
        this.thumbnailUrlM = thumbnailUrlM;
    }

    public String getImageUrl() {
        return this.imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Image{" +
                "_id=" + _id +
                ", id='" + id + '\'' +
                ", state=" + state +
                ", isMain=" + isMain +
                ", thumbnailUrl='" + thumbnailUrl + '\'' +
                ", thumbnailUrlM='" + thumbnailUrlM + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", status='" + status + '\'' +
                ", language='" + language + '\'' +
                ", country='" + country + '\'' +
                ", localPath='" + localPath + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeInt(this.state);
        dest.writeInt(this.isMain);
        dest.writeString(this.thumbnailUrl);
        dest.writeString(this.thumbnailUrlM);
        dest.writeString(this.imageUrl);
        dest.writeString(this.status);
        dest.writeString(this.language);
        dest.writeString(this.country);
        dest.writeString(this.localPath);
    }

    protected Image(Parcel in) {
        this.id = in.readString();
        this.state = in.readInt();
        this.isMain = in.readInt();
        this.thumbnailUrl = in.readString();
        this.thumbnailUrlM = in.readString();
        this.imageUrl = in.readString();
        this.status = in.readString();
        this.language = in.readString();
        this.country = in.readString();
        this.localPath = in.readString();
    }

    public static final Parcelable.Creator<Image> CREATOR = new Parcelable.Creator<Image>() {
        @Override
        public Image createFromParcel(Parcel source) {
            return new Image(source);
        }

        @Override
        public Image[] newArray(int size) {
            return new Image[size];
        }
    };

}
