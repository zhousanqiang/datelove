package com.yueai.bean;

import java.util.List;

/**
 * Created by Administrator on 2017/1/10.
 */

public class MarkPhotoBean {
    public String isSucceed;
    public String isShowMarkPhoto;
    public List<MarkPhoto> markPhotos;
    public String markPhotoSize;

    public String getIsSucceed() {
        return isSucceed;
    }

    public void setIsSucceed(String isSucceed) {
        this.isSucceed = isSucceed;
    }

    public String getIsShowMarkPhoto() {
        return isShowMarkPhoto;
    }

    public void setIsShowMarkPhoto(String isShowMarkPhoto) {
        this.isShowMarkPhoto = isShowMarkPhoto;
    }

    public List<MarkPhoto> getMarkPhotos() {
        return markPhotos;
    }

    public void setMarkPhotos(List<MarkPhoto> markPhotos) {
        this.markPhotos = markPhotos;
    }

    public String getMarkPhotoSize() {
        return markPhotoSize;
    }

    public void setMarkPhotoSize(String markPhotoSize) {
        this.markPhotoSize = markPhotoSize;
    }

    @Override
    public String toString() {
        return "MarkPhotoBean{" +
                "isSucceed='" + isSucceed + '\'' +
                ", isShowMarkPhoto='" + isShowMarkPhoto + '\'' +
                ", markPhotos=" + markPhotos +
                ", markPhotoSize='" + markPhotoSize + '\'' +
                '}';
    }
}
