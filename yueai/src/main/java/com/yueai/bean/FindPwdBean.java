package com.yueai.bean;

/**
 * Created by Administrator on 2016/10/29.
 */

public class FindPwdBean extends BaseModel{
    public String description;
    public String content;
    public String account;
    public String password;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "FindPwdBean{" +
                "description='" + description + '\'' +
                ", content='" + content + '\'' +
                ", account='" + account + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
