package com.yueai.bean;

/**
 * 征友匹配条件
 * Created by zhangdroid on 2016/6/30.
 */
public class MatcherInfo {
    private Area area;// 征友居住区域
    private int minAge;// 征友最小年龄
    private int maxAge;// 征友最大年龄
    private String minHeight;// 征友最低身高（英寸）
    private String maxHeight;// 征友最高身高（英寸）
    private int minHeightCm;// 征友最低身高（厘米）
    private int maxHeightCm;// 征友最高身高（厘米）
    private int minimumEducation;// 征友最低学历
    private int income;// 征友收入
    private int ethnicity;// 种族
    private int faith;// 信仰
    private String minEducationEnglish;
    private String incomeEnglish;
    private String language;// 语言
    private int relationShip;// 婚姻状况
    private int sign;// 星座
    private int occupation;// 职业
    private int wantKids;// 是否想要小孩

    public MatcherInfo() {
        this.area = new Area();
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public int getMinAge() {
        return minAge;
    }

    public void setMinAge(int minAge) {
        this.minAge = minAge;
    }

    public int getMaxAge() {
        return maxAge;
    }

    public void setMaxAge(int maxAge) {
        this.maxAge = maxAge;
    }

    public String getMinHeight() {
        return minHeight;
    }

    public void setMinHeight(String minHeight) {
        this.minHeight = minHeight;
    }

    public String getMaxHeight() {
        return maxHeight;
    }

    public void setMaxHeight(String maxHeight) {
        this.maxHeight = maxHeight;
    }

    public int getMinHeightCm() {
        return minHeightCm;
    }

    public void setMinHeightCm(int minHeightCm) {
        this.minHeightCm = minHeightCm;
    }

    public int getMaxHeightCm() {
        return maxHeightCm;
    }

    public void setMaxHeightCm(int maxHeightCm) {
        this.maxHeightCm = maxHeightCm;
    }

    public int getMinimumEducation() {
        return minimumEducation;
    }

    public void setMinimumEducation(int minimumEducation) {
        this.minimumEducation = minimumEducation;
    }

    public int getIncome() {
        return income;
    }

    public void setIncome(int income) {
        this.income = income;
    }

    public int getEthnicity() {
        return ethnicity;
    }

    public void setEthnicity(int ethnicity) {
        this.ethnicity = ethnicity;
    }

    public int getFaith() {
        return faith;
    }

    public void setFaith(int faith) {
        this.faith = faith;
    }

    public String getMinEducationEnglish() {
        return minEducationEnglish;
    }

    public void setMinEducationEnglish(String minEducationEnglish) {
        this.minEducationEnglish = minEducationEnglish;
    }

    public String getIncomeEnglish() {
        return incomeEnglish;
    }

    public void setIncomeEnglish(String incomeEnglish) {
        this.incomeEnglish = incomeEnglish;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public int getRelationShip() {
        return relationShip;
    }

    public void setRelationShip(int relationShip) {
        this.relationShip = relationShip;
    }

    public int getSign() {
        return sign;
    }

    public void setSign(int sign) {
        this.sign = sign;
    }

    public int getOccupation() {
        return occupation;
    }

    public void setOccupation(int occupation) {
        this.occupation = occupation;
    }

    public int getWantKids() {
        return wantKids;
    }

    public void setWantKids(int wantKids) {
        this.wantKids = wantKids;
    }

    @Override
    public String toString() {
        return "MatcherInfo{" +
                "area=" + area +
                ", minAge=" + minAge +
                ", maxAge=" + maxAge +
                ", minHeight='" + minHeight + '\'' +
                ", maxHeight='" + maxHeight + '\'' +
                ", minHeightCm=" + minHeightCm +
                ", maxHeightCm=" + maxHeightCm +
                ", minimumEducation=" + minimumEducation +
                ", income=" + income +
                ", ethnicity=" + ethnicity +
                ", faith=" + faith +
                ", minEducationEnglish=" + minEducationEnglish +
                ", incomeEnglish=" + incomeEnglish +
                ", language=" + language +
                ", relationShip=" + relationShip +
                ", sign=" + sign +
                ", occupation=" + occupation +
                ", wantKids=" + wantKids +
                '}';
    }

}
