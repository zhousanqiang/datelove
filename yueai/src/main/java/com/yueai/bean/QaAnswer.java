package com.yueai.bean;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by Administrator on 2016/11/25.
 */
@Entity
public class QaAnswer {
    @Id(autoincrement = true)
    private Long _id;
    private String id;
    @NotNull
    private String content;

    @Generated(hash = 422415186)
    public QaAnswer(Long _id, String id, @NotNull String content) {
        this._id = _id;
        this.id = id;
        this.content = content;
    }

    @Generated(hash = 1559417987)
    public QaAnswer() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "QaAnswer{" +
                "id='" + id + '\'' +
                ", content='" + content + '\'' +
                '}';
    }

    public Long get_id() {
        return this._id;
    }

    public void set_id(Long _id) {
        this._id = _id;
    }
}
