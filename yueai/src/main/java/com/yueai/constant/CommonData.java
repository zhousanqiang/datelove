package com.yueai.constant;

import com.library.utils.TimeUtil;
import com.yueai.utils.ParamsUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>公用数据，用于滚轮选择器数据源，格式统一为List<String>.</p>
 * <p/>
 * <p>1、年龄选择列表</p>
 * <p>2、日期选择列表</p>
 * <p>3、省份/城市选择列表</p>
 * Created by zhangdroid on 2016/6/24.
 */
public class CommonData {

    /**
     * 获得年龄列表
     */
    public static List<String> getAgeList() {
        List<String> ageList = new ArrayList<String>();
        for (int i = 0; i < 45; i++) {
            ageList.add(String.valueOf(i + 18));
        }
        return ageList;
    }

    /**
     * 获得年份列表
     *
     * @return
     */
    public static List<String> getYearList() {
        List<String> yearList = new ArrayList<String>();
        for (int i = 1997; i >= 1950; i--) {
            yearList.add(String.valueOf(i));
        }
        return yearList;
    }

    /**
     * 获得月份列表
     *
     * @return
     */
    public static List<String> getMonthList() {
        List<String> monthList = new ArrayList<String>();
        for (int i = 1; i <= 12; i++) {
            monthList.add(TimeUtil.pad(i));
        }
        return monthList;
    }

    /**
     * 获得天数列表
     *
     * @param maxDays 该月的最大天数
     * @return
     */
    public static List<String> getDayList(int maxDays) {
        List<String> dayList = new ArrayList<String>();
        for (int i = 1; i <= maxDays; i++) {
            dayList.add(TimeUtil.pad(i));
        }
        return dayList;
    }

    /**
     * 获得省份列表
     *
     * @return
     */
    public static List<String> getProvinceList() {
        return ParamsUtils.getProvinceMapValue();
    }


    /**
     * 获得个性List
     *
     * @return
     */
    public static List<String> getWeightList() {
        List<String> list = new ArrayList<String>();
        for (int i = 0; i < 60; i++) {
            list.add(i + 40 + "Kg");
        }
        return list;
    }
}
