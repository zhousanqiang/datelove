package com.datelove.online.International.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * 国家对应渠道号工具类
 * Created by zhangdroid on 2016/11/23.
 */
public class CountryFidUtils {

    /**
     * @return 国家和渠道号对应map
     */
    public static Map<String, String> getCountryFidMap() {
        Map<String, String> map = new HashMap<>();
        // 添加13个英语国家
        map.put("America", "10801");
        map.put("Australia", "10811");
        map.put("India", "10812");
        map.put("Indonesia", "10813");
        map.put("UnitedKingdom", "10814");
        map.put("Canada", "10815");
        map.put("NewZealand", "10816");
        map.put("Ireland", "10817");
        map.put("SouthAfrica", "10818");
        map.put("Singapore", "10820");
        map.put("Pakistan", "10824");
        map.put("Philippines", "10825");
        map.put("HongKong", "10833");
        map.put("France", "10858");
        map.put("Belgium", "10889");
        map.put("Luxembourg", "10888");
        map.put("Monaco", "10862");
        map.put("IvoryCoast", "10863");
        map.put("Rwanda", "10886");
        map.put("Centrafrique", "10864");
        map.put("Togo", "10865");
        map.put("Gabon", "10866");
        map.put("Guinea", "10867");
        map.put("Mali", "10868");
        map.put("BurkinaFaso", "10869");
        map.put("CongoDemocratic", "10870");
        map.put("Cameroun", "10885");
        map.put("CongoBrasseville", "10872");
        map.put("Benin", "10873");
        map.put("Niger", "10874");
        map.put("Burundi", "10875");
        map.put("Senegal", "10876");
        map.put("Djibouti", "10877");
        map.put("Madagascar", "10878");
        map.put("Comores", "10879");
        map.put("Seychelles", "10884");
        map.put("CanadaQuebec", "10880");
        map.put("Haiti", "10881");
        map.put("Vanuatu", "10883");
        map.put("Spain", "10835");
        map.put("Andorra", "10857");
        map.put("Argentina", "10836");
        map.put("Honduras", "10845");
        map.put("Bolivia", "10843");
        map.put("Chile", "10839");
        map.put("Colombia", "10834");
        map.put("CostaRica", "10849");
        map.put("Cuba", "10842");
        map.put("ominicanRepublic", "10844");
        map.put("Ecuador", "10840");
        map.put("ElSalvador", "10847");
        map.put("Guatemala", "10841");
        map.put("Mexico", "10802");
        map.put("Nicaragua", "10848");
        map.put("Panama", "10851");
        map.put("Uruguay", "10852");
        map.put("Paraguay", "10846");
        map.put("Peru", "10837");
        map.put("Venezuela", "10838");
        map.put("EquatorialGuinea", "10853");
        map.put("Japan", "10887");
        map.put("Germany", "10887");
        map.put("Austria", "10892");
        map.put("Suisse", "10859");
        return map;
    }
    public static Map<String, String> getLanguageMap() {
        Map<String, String> map = new HashMap<>();
        // 添加13个英语国家
        map.put("America", "English");
        map.put("Australia", "English");
        map.put("India", "English");
        map.put("Indonesia", "English");
        map.put("UnitedKingdom", "English");
        map.put("Canada", "English");
        map.put("NewZealand", "English");
        map.put("Ireland", "English");
        map.put("SouthAfrica", "English");
        map.put("Singapore", "English");
        map.put("Pakistan", "English");
        map.put("Philippines", "English");
        map.put("HongKong", "English");
        map.put("Cameroun", "English");
        map.put("France", "French");
        map.put("Monaco", "French");
        map.put("IvoryCoast", "French");
        map.put("Rwanda", "French");
        map.put("Centrafrique", "French");
        map.put("Togo", "French");
        map.put("Gabon", "French");
        map.put("Guinea", "French");
        map.put("Mali", "French");
        map.put("BurkinaFaso", "French");
        map.put("CongoDemocratic", "French");
        map.put("CongoBrasseville", "French");
        map.put("Benin", "French");
        map.put("Niger", "French");
        map.put("Burundi", "French");
        map.put("Senegal", "French");
        map.put("Djibouti", "French");
        map.put("Madagascar", "French");
        map.put("Comores", "French");
        map.put("Seychelles", "French");
        map.put("CanadaQuebec", "French");
        map.put("Haiti", "French");
        map.put("Vanuatu", "French");
        map.put("Spain", "Spanish");
        map.put("Andorra", "Spanish");
        map.put("Argentina", "Spanish");
        map.put("Honduras", "Spanish");
        map.put("Bolivia", "Spanish");
        map.put("Chile", "Spanish");
        map.put("Colombia", "Spanish");
        map.put("CostaRica", "Spanish");
        map.put("Cuba", "Spanish");
        map.put("ominicanRepublic", "Spanish");
        map.put("Ecuador", "Spanish");
        map.put("ElSalvador", "Spanish");
        map.put("Guatemala", "Spanish");
        map.put("Mexico", "Spanish");
        map.put("Nicaragua", "Spanish");
        map.put("Panama", "Spanish");
        map.put("Uruguay", "Spanish");
        map.put("Paraguay", "Spanish");
        map.put("Peru", "Spanish");
        map.put("Venezuela", "Spanish");
        map.put("EquatorialGuinea", "Spanish");
        map.put("Japan", "Japanese");
        map.put("Germany", "German");
        map.put("Austria", "German");
        map.put("Suisse", "French");
        map.put("Belgium", "French");
        map.put("Luxembourg", "French");
        return map;
    }

}
