package com.datelove.online.International.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.datelove.online.International.R;
import com.datelove.online.International.bean.Image;
import com.datelove.online.International.bean.NearByUser;
import com.datelove.online.International.bean.UserBase;
import com.datelove.online.International.constant.IConfigConstant;
import com.library.adapter.recyclerview.CommonRecyclerViewAdapter;
import com.library.adapter.recyclerview.RecyclerViewHolder;
import com.library.imageloader.CircleTransformation;
import com.library.imageloader.ImageLoader;
import com.library.imageloader.ImageLoaderUtil;
import com.library.utils.DensityUtil;

import java.util.List;

/**
 * 附近的人适配器
 * Created by zhangdroid on 2016/11/2.
 */
public class NearbyAdapter extends CommonRecyclerViewAdapter<NearByUser> {

    public NearbyAdapter(Context context, int layoutResId) {
        super(context, layoutResId);
    }

    public NearbyAdapter(Context context, int layoutResId, List<NearByUser> list) {
        super(context, layoutResId, list);
    }

    @Override
    protected void convert(int position, RecyclerViewHolder viewHolder, NearByUser bean) {
        LinearLayout linearLayout = (LinearLayout) viewHolder.getView(R.id.item_nearby);
        ViewGroup.LayoutParams lp1 = linearLayout.getLayoutParams();
        if (position == 1) {
            lp1.height = DensityUtil.dip2px(mContext, 180);
        } else {
            lp1.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        }
        linearLayout.setLayoutParams(lp1);
        if (bean != null) {
            String distance = bean.getDistance();
            if (!TextUtils.isEmpty(distance)) {
                viewHolder.setText(R.id.distance, distance);
            }
            final UserBase userBase = bean.getUserBaseEnglish();
            if (userBase != null) {
                ImageView ivAvatar = (ImageView) viewHolder.getView(R.id.item_nearby_avatar);
                Image image = userBase.getImage();
                if (image != null) {
                    String imgUrl = image.getThumbnailUrl();
                    if (!TextUtils.isEmpty(imgUrl)) {
                        ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(imgUrl).placeHolder(IConfigConstant.COMMON_DEFAULT_AVATAR_CIRCLE)
                                .transform(new CircleTransformation()).imageView(ivAvatar).build());
                    } else {
                        ivAvatar.setImageResource(IConfigConstant.COMMON_DEFAULT_AVATAR_CIRCLE);
                    }
                } else {
                    ivAvatar.setImageResource(IConfigConstant.COMMON_DEFAULT_AVATAR_CIRCLE);
                }
                // 昵称
                String nickname = userBase.getNickName();
                if (!TextUtils.isEmpty(nickname)) {
                    viewHolder.setText(R.id.item_nearby_nickname, nickname);
                }
            }
        }
    }

}
