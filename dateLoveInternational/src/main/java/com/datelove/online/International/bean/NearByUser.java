package com.datelove.online.International.bean;

import java.util.List;

/**
 * 附近的人用户对象
 * Created by zhangdroid on 2016/6/29.
 */
public class NearByUser {
    private String isSayHello;// 是否打过招呼，1->已打招呼, 0->未打招呼,
    private String distance;// 距离
    private List<String> listLabel;// 推荐标签
    private int imgCount;// 照片数量
    private UserBase userBaseEnglish;// 用户对象基础信息

    public String getIsSayHello() {
        return isSayHello;
    }

    public void setIsSayHello(String isSayHello) {
        this.isSayHello = isSayHello;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public List<String> getListLabel() {
        return listLabel;
    }

    public void setListLabel(List<String> listLabel) {
        this.listLabel = listLabel;
    }

    public int getImgCount() {
        return imgCount;
    }

    public void setImgCount(int imgCount) {
        this.imgCount = imgCount;
    }

    public UserBase getUserBaseEnglish() {
        return userBaseEnglish;
    }

    public void setUserBaseEnglish(UserBase userBaseEnglish) {
        this.userBaseEnglish = userBaseEnglish;
    }

    @Override
    public String toString() {
        return "NearByUser{" +
                "isSayHello='" + isSayHello + '\'' +
                ", distance='" + distance + '\'' +
                ", listLabel=" + listLabel +
                ", imgCount=" + imgCount +
                ", userEnglish=" + userBaseEnglish +
                '}';
    }

}
