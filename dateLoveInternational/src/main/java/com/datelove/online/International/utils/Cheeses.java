/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.datelove.online.International.utils;


import com.datelove.online.International.R;
import com.datelove.online.International.base.BaseApplication;

import java.util.HashMap;
import java.util.Map;

public class Cheeses {


	public static final char[] LETTERS = new char[]{
            '#','A', 'B', 'C', 'D', 'E', 'F',
			'G', 'H', 'I', 'J', 'K', 'L',
			'M', 'N', 'O', 'P', 'Q', 'R',
			'S', 'T', 'U', 'V', 'W', 'X',
			'Y', 'Z'};
//    日语
    public static final char[] LETTERS_JA = new char[]{
        'ア', 'イ', 'ウ', 'エ', 'オ', 'カ', 'キ',
        'ク', 'ケ', 'コ', 'サ', 'シ', 'ス', 'セ',
        'ソ', 'タ', 'チ', 'ツ', 'テ', 'ト', 'ナ', 'ニ',
        'ヌ', 'ネ', 'ノ', 'ハ', 'ヒ', 'フ',
        'ヘ', 'ホ', 'マ', 'ミ', 'ム', 'メ', 'モ', 'ヤ',
        'ユ', 'ヨ', 'ラ', 'リ', 'ル', 'レ', 'ロ'};

    //    西语
    public static final char[] LETTERS_ES = new char[]{
            '#','A', 'B', 'C', 'D', 'E', 'F',
            'G', 'H', 'I', 'J', 'K', 'L',
            'M', 'N', 'Ñ', 'O', 'P', 'Q', 'R',
            'S', 'T', 'U', 'V', 'W', 'X',
            'Y', 'Z'};
//    印地语
    public static final char[] LETTERS_HI = new char[]{
        'अ', 'आ', 'इ', 'ई', 'उ', 'ऊ', 'ऋ', 'ए', 'ऐ', 'ओ', 'औ',
        'क', 'ख', 'ग', 'घ', 'ङ',
        'च', 'छ', 'ज', 'झ', 'ञ',
        'ट', 'ठ', 'ड', 'ढ', 'ण',
        'त', 'थ', 'द', 'ध', 'न',
        'प', 'फ', 'ब', 'भ', 'म',
        'य', 'र', 'ल', 'व', 'श', 'ष', 'स', 'ह'};
//    'अ', 'आ', 'इ', 'ई', 'उ', 'ऊ', 'ऋ', 'ए', 'ऐ', 'ओ', 'औ',
//    'क', 'ख', 'ग', 'घ', 'ङ',
//    'च', 'छ', 'ज', 'झ', 'ञ',
//    'ट', 'ठ', 'ड', 'ढ', 'ण',
//    'त', 'थ', 'द', 'ध', 'न',
//    'प', 'फ', 'ब', 'भ', 'म',
//    'य', 'र', 'ल', 'व', 'श', 'ष', 'स', 'ह'
//    德语
    public static final char[] LETTERS_DE = new char[]{
        '#','A', 'B', 'C', 'D', 'E', 'F',
            'G', 'H', 'I', 'J', 'K', 'L',
            'M', 'N', 'O', 'P', 'Q', 'R',
            'S', 'T', 'U', 'V', 'W', 'X',
            'Y', 'Z','Ä','Ö','Ü'};
//    法语
    public static final char[] LETTERS_RU = new char[]{
        '#','A', 'B', 'C', 'D', 'E', 'F',
            'G', 'H', 'I', 'J', 'K', 'L',
            'M', 'N', 'O', 'P', 'Q', 'R',
            'S', 'T', 'U', 'V', 'W', 'X',
            'Y', 'Z'};
	/**西班牙语**/
//	public static final String[] GOUNTRY = new String[] { "España", "Costa Rica", "Argentina",
//			"Paraguay", "Uruguay", "México", "Panamá", "Dominican Repúblic", "Venezuela", "Honduras", "Chile", "Cuba", "Ecuador",
//			"EL Salvador", "Nicaragua", "Perú", "Guatemala", "Bolivia", "Colombia", "Guinea Ecuatorial" };
//	public static final char[] LETTERS = new char[]{
//			'A', 'B', 'Ch', 'C', 'D', 'E', 'F',
//			'G', 'H', 'I', 'J', 'K', 'L', 'Ll',
//			'M', 'N', 'Ñ', 'O', 'P', 'Q', 'R', 'Rr',
//			'S', 'T', 'U', 'V', 'W', 'X',
//			'Y', 'Z'};
//    static String france=String.valueOf(R.string.france);
//            法语国家
    static String france=BaseApplication.getGlobalContext().getString(R.string.france);
    static String suisse=BaseApplication.getGlobalContext().getString(R.string.suisse);
    static String belgique=BaseApplication.getGlobalContext().getString(R.string.belgique);
    static String luxembourg=BaseApplication.getGlobalContext().getString(R.string.luxembourg);
    static String monaco=BaseApplication.getGlobalContext().getString(R.string.monaco);
    static String côte_divoire=BaseApplication.getGlobalContext().getString(R.string.cote_divoire);
    static String rwanda=BaseApplication.getGlobalContext().getString(R.string.rwanda);
    static String centrafrique=BaseApplication.getGlobalContext().getString(R.string.centrafrique);
    static String togo=BaseApplication.getGlobalContext().getString(R.string.togo);
    static String gabon=BaseApplication.getGlobalContext().getString(R.string.gabon);
    static String guinée=BaseApplication.getGlobalContext().getString(R.string.guinee);
    static String mali=BaseApplication.getGlobalContext().getString(R.string.mali);
    static String burkina_faso=BaseApplication.getGlobalContext().getString(R.string.burkina_faso);
    static String congo_démocratique=BaseApplication.getGlobalContext().getString(R.string.congo_democratique);
    static String cameroun=BaseApplication.getGlobalContext().getString(R.string.cameroun);
    static String congo_brasseville=BaseApplication.getGlobalContext().getString(R.string.congo_brasseville);
    static String benin=BaseApplication.getGlobalContext().getString(R.string.benin);
    static String niger=BaseApplication.getGlobalContext().getString(R.string.niger);
    static String burundi=BaseApplication.getGlobalContext().getString(R.string.burundi);
    static String senega=BaseApplication.getGlobalContext().getString(R.string.senega);
    static String djibouti=BaseApplication.getGlobalContext().getString(R.string.djibouti);
    static String madagascar=BaseApplication.getGlobalContext().getString(R.string.madagascar);
    static String comores=BaseApplication.getGlobalContext().getString(R.string.comores);
    static String seychelles=BaseApplication.getGlobalContext().getString(R.string.seychelles);
    static String canada_quebec=BaseApplication.getGlobalContext().getString(R.string.canada_quebec);
    static String haiti=BaseApplication.getGlobalContext().getString(R.string.haiti);
    static String vanuatu=BaseApplication.getGlobalContext().getString(R.string.vanuatu);
//    西语
    static String spain=BaseApplication.getGlobalContext().getString(R.string.spain);
    static String andorra=BaseApplication.getGlobalContext().getString(R.string.andorra);
    static String argentina=BaseApplication.getGlobalContext().getString(R.string.argentina);
    static String honduras=BaseApplication.getGlobalContext().getString(R.string.honduras);
    static String bolivia=BaseApplication.getGlobalContext().getString(R.string.bolivia);
    static String chile=BaseApplication.getGlobalContext().getString(R.string.chile);
    static String colombia=BaseApplication.getGlobalContext().getString(R.string.colombia);
    static String costa_rica=BaseApplication.getGlobalContext().getString(R.string.costa_rica);
    static String cuba=BaseApplication.getGlobalContext().getString(R.string.cuba);
    static String dominica=BaseApplication.getGlobalContext().getString(R.string.dominica);
    static String ecuador=BaseApplication.getGlobalContext().getString(R.string.ecuador);
    static String el_salvador=BaseApplication.getGlobalContext().getString(R.string.el_salvador);
    static String guatemala=BaseApplication.getGlobalContext().getString(R.string.guatemala);
    static String mexico=BaseApplication.getGlobalContext().getString(R.string.mexico);
    static String nicaragua=BaseApplication.getGlobalContext().getString(R.string.nicaragua);
    static String panama=BaseApplication.getGlobalContext().getString(R.string.panama);
    static String uruguay=BaseApplication.getGlobalContext().getString(R.string.uruguay);
    static String paraguay=BaseApplication.getGlobalContext().getString(R.string.paraguay);
    static String peru=BaseApplication.getGlobalContext().getString(R.string.peru);
    static String venezuela=BaseApplication.getGlobalContext().getString(R.string.venezuela);
//    static String belize=BaseApplication.getGlobalContext().getString(R.string.belize);
    static String equatorial_guinea=BaseApplication.getGlobalContext().getString(R.string.equatorial_guinea);
    //英语国家
    static String united_states=BaseApplication.getGlobalContext().getString(R.string.united_states);
    static String australia=BaseApplication.getGlobalContext().getString(R.string.australia);
    static String india=BaseApplication.getGlobalContext().getString(R.string.india);
    static String indonesia=BaseApplication.getGlobalContext().getString(R.string.indonesia);
    static String canada=BaseApplication.getGlobalContext().getString(R.string.canada);
    static String unitedkingdom=BaseApplication.getGlobalContext().getString(R.string.unitedkingdom);
    static String newzealand=BaseApplication.getGlobalContext().getString(R.string.newzealand);
    static String ireland=BaseApplication.getGlobalContext().getString(R.string.ireland);
    static String southafrica=BaseApplication.getGlobalContext().getString(R.string.southafrica);
    static String singapore=BaseApplication.getGlobalContext().getString(R.string.singapore);
    static String pakistan=BaseApplication.getGlobalContext().getString(R.string.pakistan);
    static String philippines=BaseApplication.getGlobalContext().getString(R.string.philippines);
    static String hongkong=BaseApplication.getGlobalContext().getString(R.string.hongkong);
    static String japan=BaseApplication.getGlobalContext().getString(R.string.japan);
    static String germany=BaseApplication.getGlobalContext().getString(R.string.germany);
    static String austria=BaseApplication.getGlobalContext().getString(R.string.austria);




/**数组GOUNTRY和数组EN_GOUNTRY存在一一对应的关系，每个数据位置只能同时改变，不能单独改变。**/
public static final String[] GOUNTRY = new String[] {france,suisse,belgique,luxembourg,monaco,côte_divoire,rwanda,centrafrique,togo
        ,gabon,guinée,mali,burkina_faso,congo_démocratique,cameroun,congo_brasseville,benin,niger,burundi,
        senega,djibouti,madagascar,comores,seychelles
        ,canada_quebec,haiti,vanuatu,
        spain,andorra,argentina,honduras,bolivia,chile,colombia,costa_rica,cuba,dominica,ecuador,el_salvador,
        guatemala,mexico,nicaragua,panama,uruguay,paraguay,peru,venezuela,equatorial_guinea,
        united_states,australia,india,indonesia,canada,unitedkingdom,newzealand,ireland,southafrica,singapore,pakistan,
        philippines,hongkong,japan,germany,austria};
    public static final String[] EN_GOUNTRY = new String[] {"France","Suisse","Belgium","Luxembourg",
            "Monaco","IvoryCoast","Rwanda","Centrafrique","Togo","Gabon","Guinea","Mali",
            "BurkinaFaso","CongoDemocratic","Cameroun","CongoBrasseville", "Benin","Niger","Burundi",
            "Senegal","Djibouti","Madagascar","Comores","Seychelles","CanadaQuebec","Haiti","Vanuatu",
            "Spain","Andorra","Argentina","Honduras","Bolivia","Chile","Colombia","CostaRica","Cuba",
            "ominicanRepublic","Ecuador","ElSalvador", "Guatemala","Mexico","Nicaragua","Panama",
            "Uruguay","Paraguay","Peru","Venezuela", "EquatorialGuinea", "UnitedStates","Australia",
            "India","Indonesia","Canada","UnitedKingdom","NewZealand", "Ireland","SouthAfrica",
            "Singapore","Pakistan", "Philippines","HongKong","Japan","Germany","Austria"};
    /**
     * @return 国家和渠道号对应map
     */
    public static Map<String, String> getCountryMap() {
        Map<String, String> map = new HashMap<>();
        // 添加13个英语国家
        map.put(france, "France");
        map.put(suisse, "Suisse");
        map.put(belgique, "Belgium");
        map.put(luxembourg, "Luxembourg");
        map.put(monaco, "Monaco");
        map.put(côte_divoire, "IvoryCoast");
        map.put(rwanda, "Rwanda");
        map.put(centrafrique, "Centrafrique");
        map.put(togo, "Togo");
        map.put(gabon, "Gabon");
        map.put(guinée, "Guinea");
        map.put(mali, "Mali");
        map.put(burkina_faso, "BurkinaFaso");
        map.put(congo_démocratique, "CongoDemocratic");
        map.put(cameroun, "Cameroun");
        map.put(congo_brasseville, "CongoBrasseville");
        map.put(benin, "Benin");
        map.put(niger, "Niger");
        map.put(burundi, "Burundi");
        map.put(senega, "Senegal");
        map.put(djibouti, "Djibouti");
        map.put(madagascar, "Madagascar");
        map.put(comores, "Comores");
        map.put(seychelles, "Seychelles");
        map.put(canada_quebec, "CanadaQuebec");
        map.put(haiti, "Haiti");
        map.put(vanuatu, "Vanuatu");
        map.put(spain, "Spain");
        map.put(andorra, "Andorra");
        map.put(argentina, "Argentina");
        map.put(honduras, "Honduras");
        map.put(bolivia, "Bolivia");
        map.put(chile, "Chile");
        map.put(colombia, "Colombia");
        map.put(costa_rica, "CostaRica");
        map.put(cuba, "Cuba");
        map.put(dominica, "ominicanRepublic");
        map.put(ecuador, "Ecuador");
        map.put(el_salvador, "ElSalvador");
        map.put(guatemala, "Guatemala");
        map.put(mexico, "Mexico");
        map.put(nicaragua, "Nicaragua");
        map.put(panama, "Panama");
        map.put(uruguay, "Uruguay");
        map.put(paraguay, "Paraguay");
        map.put(peru, "Peru");
        map.put(venezuela, "Venezuela");
        map.put(equatorial_guinea, "EquatorialGuinea");
        map.put(united_states, "UnitedStates");
        map.put(australia, "Australia");
        map.put(india, "India");
        map.put(indonesia, "Indonesia");
        map.put(canada, "Canada");
        map.put(unitedkingdom, "UnitedKingdom");
        map.put(newzealand, "NewZealand");
        map.put(ireland, "Ireland");
        map.put(southafrica, "SouthAfrica");
        map.put(singapore, "Singapore");
        map.put(pakistan, "Pakistan");
//        map.put("Belize", "10882");
        map.put(philippines, "Philippines");
        map.put(hongkong, "HongKong");
        map.put(japan, "Japan");
        map.put(germany, "Germany");
        map.put(austria, "Austria");
        return map;
    }
}
