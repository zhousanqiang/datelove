package com.datelove.online.International.bean;

/**
 * 聊天消息对象
 * Created by zhangdroid on 2016/6/22.
 */
public class Message {
    private String id;// 消息id
    private String language;// 语言
    private String country;// 国家
    private String uid;// 会员id
    // 1->对方文本消息，2->对方语音消息，3->自己文本消息，4->文本消息, 7->语音信件,8->qa问答信,9->小助手问题
    // 10->图片, 11->qa解锁信）
    private String msgType;
    private String content;// 消息内容
    private String audioUrl;// 语音url
    private String audioTime;// 语音时间
    private String recevStatus;// 语音状态(0是未读，1是已读)
    private String createDate;// 发送时间
    private long createDateMills;// 发送时间（毫秒数）
    private QaQuestion qaQuestion;// QA对象
    private Image chatImage;// 图片对象

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAudioUrl() {
        return audioUrl;
    }

    public void setAudioUrl(String audioUrl) {
        this.audioUrl = audioUrl;
    }

    public String getAudioTime() {
        return audioTime;
    }

    public void setAudioTime(String audioTime) {
        this.audioTime = audioTime;
    }

    public String getRecevStatus() {
        return recevStatus;
    }

    public void setRecevStatus(String recevStatus) {
        this.recevStatus = recevStatus;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public long getCreateDateMills() {
        return createDateMills;
    }

    public void setCreateDateMills(long createDateMills) {
        this.createDateMills = createDateMills;
    }

    public QaQuestion getQaQuestion() {
        return qaQuestion;
    }

    public void setQaQuestion(QaQuestion qaQuestion) {
        this.qaQuestion = qaQuestion;
    }

    public Image getChatImage() {
        return chatImage;
    }

    public void setChatImage(Image chatImage) {
        this.chatImage = chatImage;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id='" + id + '\'' +
                ", language='" + language + '\'' +
                ", country='" + country + '\'' +
                ", uid='" + uid + '\'' +
                ", msgType='" + msgType + '\'' +
                ", content='" + content + '\'' +
                ", audioUrl='" + audioUrl + '\'' +
                ", audioTime='" + audioTime + '\'' +
                ", recevStatus='" + recevStatus + '\'' +
                ", createDate='" + createDate + '\'' +
                ", createDateMills=" + createDateMills +
                ", qaQuestion=" + qaQuestion +
                ", chatImage=" + chatImage +
                '}';
    }

}
