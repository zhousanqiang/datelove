package com.datelove.online.International.event;

/**
 * Created by Administrator on 2016/12/27.
 */
public class YeMeiIntoUserInfoDetailEvent {
    public int mCurrDisplayItem;

    public YeMeiIntoUserInfoDetailEvent(int mCurrDisplayItem) {
        this.mCurrDisplayItem = mCurrDisplayItem;
    }

    public int getUnreadMsgCount() {
        return mCurrDisplayItem;
    }

    public void setUnreadMsgCount(int mCurrDisplayItem) {
        this.mCurrDisplayItem = mCurrDisplayItem;
    }

}
