package com.datelove.online.International.constant;

import com.datelove.online.International.utils.ParamsUtils;
import com.library.utils.TimeUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>公用数据，用于滚轮选择器数据源，格式统一为List<String>.</p>
 * <p/>
 * <p>1、年龄选择列表</p>
 * <p>2、日期选择列表</p>
 * <p>3、省份/城市选择列表</p>
 * Created by zhangdroid on 2016/6/24.
 */
public class CommonData {

    /**
     * 获得年龄列表
     */
    public static List<String> getAgeList() {
        List<String> ageList = new ArrayList<String>();
        for (int i = 0; i < 45; i++) {
            ageList.add(String.valueOf(i + 18));
        }
        return ageList;
    }

    /**
     * 获得年份列表
     *
     * @return
     */
    public static List<String> getYearList() {
        List<String> yearList = new ArrayList<String>();
        for (int i = 1997; i >= 1950; i--) {
            yearList.add(String.valueOf(i));
        }
        return yearList;
    }

    /**
     * 获得月份列表
     *
     * @return
     */
    public static List<String> getMonthList() {
        List<String> monthList = new ArrayList<String>();
        for (int i = 1; i <= 12; i++) {
            monthList.add(TimeUtil.pad(i));
        }
        return monthList;
    }

    /**
     * 获得天数列表
     *
     * @param maxDays 该月的最大天数
     * @return
     */
    public static List<String> getDayList(int maxDays) {
        List<String> dayList = new ArrayList<String>();
        for (int i = 1; i <= maxDays; i++) {
            dayList.add(TimeUtil.pad(i));
        }
        return dayList;
    }

    /**
     * 获得省份列表
     *
     * @return
     */
    public static List<String> getProvinceList() {
        return ParamsUtils.getProvinceMapValue();
    }

    /**
     * 获得国家列表
     *
     * @return
     */
    public static List<String> getCountryList() {
        List<String> list = new ArrayList<String>();
        list.add("UnitedStates");
        list.add("America");
        list.add("Australia");
        list.add("India");
        list.add("Indonesia");
        list.add("UnitedKingdom");
        list.add("Canada");
        list.add("NewZealand");
        list.add("Ireland");
        list.add("SouthAfrica");
        list.add("Singapore");
        list.add("Pakistan");
        list.add("Philippines");
        list.add("HongKong");
        return list;
    }
//    西语国家
    public static List<String> getSpanishCountryList() {
        List<String> list = new ArrayList<String>();
        list.add("Spain");
        list.add("Andorra");
        list.add("Argentina");
        list.add("Honduras");
        list.add("Bolivia");
        list.add("Chile");
        list.add("Colombia");
        list.add("CostaRica");
        list.add("Cuba");
        list.add("ominicanRepublic");
        list.add("Ecuador");
        list.add("ElSalvador");
        list.add("Guatemala");
        list.add("Mexico");
        list.add("Nicaragua");
        list.add("Panama");
        list.add("Uruguay");
        list.add("Paraguay");
        list.add("Peru");
        list.add("Venezuela");
        list.add("EquatorialGuinea");
        return list;
    }

    /**
     * @return 国家语言缩写对照表
     */
    public static Map<String, String> getCountryLanguageMap() {
        Map<String, String> countryLanguageMap = new HashMap<String, String>();
        // 语言
//        “EN”"FR""SP""JA""GE""IN"

        countryLanguageMap.put("EN","English");
        countryLanguageMap.put("SP","Spanish");
        countryLanguageMap.put("TR", "Traditional");
        countryLanguageMap.put("FR","French");
        countryLanguageMap.put("JA", "Japanese");
        countryLanguageMap.put("HI", "Hindi");
        countryLanguageMap.put("GE", "German");
        // 添加63个国家
        countryLanguageMap.put("US", "America");
        countryLanguageMap.put("AU", "Australia");
        countryLanguageMap.put("IN", "India");
        countryLanguageMap.put("ID", "Indonesia");
        countryLanguageMap.put("GB", "UnitedKingdom");
        countryLanguageMap.put("CA", "Canada");
        countryLanguageMap.put("NZ", "NewZealand");
        countryLanguageMap.put("IE", "Ireland");
        countryLanguageMap.put("ZA", "SouthAfrica");
        countryLanguageMap.put("SG", "Singapore");
        countryLanguageMap.put("PK", "Pakistan");
        countryLanguageMap.put("PH", "Philippines");
        countryLanguageMap.put("HK", "HongKong");
        countryLanguageMap.put("CA","Cameroun");
        countryLanguageMap.put("CM","Cameroun");
        countryLanguageMap.put("FR","France");
        countryLanguageMap.put("MC","Monaco");
        countryLanguageMap.put("KT","IvoryCoast");
        countryLanguageMap.put("RW","Rwanda");
        countryLanguageMap.put("CF","Centrafrique");
        countryLanguageMap.put("TG","Togo");
        countryLanguageMap.put("GA","Gabon");
        countryLanguageMap.put("GN","Guinea");
        countryLanguageMap.put("ML","Mali");
        countryLanguageMap.put("BF","BurkinaFaso");
        countryLanguageMap.put("CD","CongoDemocratic");
        countryLanguageMap.put("CB","CongoBrasseville");
        countryLanguageMap.put("BJ","Benin");
        countryLanguageMap.put("NE","Niger");
        countryLanguageMap.put("BI","Burundi");
        countryLanguageMap.put("SN","Senegal");
        countryLanguageMap.put("DJ","Djibouti");
        countryLanguageMap.put("MG","Madagascar");
        countryLanguageMap.put("CO","Comores");
        countryLanguageMap.put("SC","Seychelles");
        countryLanguageMap.put("CQ","CanadaQuebec");
        countryLanguageMap.put("HT","Haiti");
        countryLanguageMap.put("VA","Vanuatu");
        countryLanguageMap.put("ES","Spain");
        countryLanguageMap.put("AD","Andorra");
        countryLanguageMap.put("AR","Argentina");
        countryLanguageMap.put("HN","Honduras");
        countryLanguageMap.put("BO","Bolivia");
        countryLanguageMap.put("CL","Chile");
        countryLanguageMap.put("CI","Colombia");
        countryLanguageMap.put("CR","CostaRica");
        countryLanguageMap.put("CU","Cuba");
        countryLanguageMap.put("DO","ominicanRepublic");
        countryLanguageMap.put("EC","Ecuador");
        countryLanguageMap.put("SV","ElSalvador");
        countryLanguageMap.put("GT","Guatemala");
        countryLanguageMap.put("MX","Mexico");
        countryLanguageMap.put("NI","Nicaragua");
        countryLanguageMap.put("PA","Panama");
        countryLanguageMap.put("UY","Uruguay");
        countryLanguageMap.put("PY","Paraguay");
        countryLanguageMap.put("PE","Peru");
        countryLanguageMap.put("VE","Venezuela");
        countryLanguageMap.put("EG","EquatorialGuinea");
        countryLanguageMap.put("JP","Japan");
        countryLanguageMap.put("DE","Germany");
        countryLanguageMap.put("AT","Austria");
        countryLanguageMap.put("SU","Suisse");
        countryLanguageMap.put("BE","Belgium");
        countryLanguageMap.put("LU","Luxembourg");

        return countryLanguageMap;
    }

}
