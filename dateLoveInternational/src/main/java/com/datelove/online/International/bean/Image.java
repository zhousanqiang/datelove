package com.datelove.online.International.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 图片对象
 * Created by zhangdroid on 2016/6/23.
 */
public class Image implements Parcelable {
    public String id;// 图片id
    public int state;// 图片状态,0->close 1->open
    public int isMain;// 图片类型,3->已设置头像,9->不允许设置头像,10->审核中
    public String thumbnailUrl; // 缩略图
    public String thumbnailUrlM;// 缩略图（大）
    public String imageUrl;// 原图
    public String status;// 审核状态
    public String language;
    public String country;
    public String localPath;

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLocalPath() {
        return localPath;
    }

    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getIsMain() {
        return isMain;
    }

    public void setIsMain(int isMain) {
        this.isMain = isMain;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getThumbnailUrlM() {
        return thumbnailUrlM;
    }

    public void setThumbnailUrlM(String thumbnailUrlM) {
        this.thumbnailUrlM = thumbnailUrlM;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return "Image{" +
                "id='" + id + '\'' +
                ", state=" + state +
                ", isMain=" + isMain +
                ", thumbnailUrl='" + thumbnailUrl + '\'' +
                ", thumbnailUrlM='" + thumbnailUrlM + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", status='" + status + '\'' +
                ", language='" + language + '\'' +
                ", country='" + country + '\'' +
                ", localPath='" + localPath + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        return this.id == ((Image) obj).getId();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeInt(this.state);
        dest.writeInt(this.isMain);
        dest.writeString(this.thumbnailUrl);
        dest.writeString(this.thumbnailUrlM);
        dest.writeString(this.imageUrl);
        dest.writeString(this.status);
        dest.writeString(this.language);
        dest.writeString(this.country);
        dest.writeString(this.localPath);
    }

    public Image() {
    }

    protected Image(Parcel in) {
        this.id = in.readString();
        this.state = in.readInt();
        this.isMain = in.readInt();
        this.thumbnailUrl = in.readString();
        this.thumbnailUrlM = in.readString();
        this.imageUrl = in.readString();
        this.status = in.readString();
        this.language = in.readString();
        this.country = in.readString();
        this.localPath = in.readString();
    }

    public static final Creator<Image> CREATOR = new Creator<Image>() {
        @Override
        public Image createFromParcel(Parcel source) {
            return new Image(source);
        }

        @Override
        public Image[] newArray(int size) {
            return new Image[size];
        }
    };

}
