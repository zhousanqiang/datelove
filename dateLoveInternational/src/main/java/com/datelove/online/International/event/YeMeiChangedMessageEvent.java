package com.datelove.online.International.event;

/**
 * Created by Administrator on 2017/8/2.
 */

public class YeMeiChangedMessageEvent {
    boolean isYeMei;
    public  YeMeiChangedMessageEvent(boolean isYeMei){
        this.isYeMei=isYeMei;
    }
    public boolean getYeMeiChangedMessageEvent(){
        return isYeMei;
    }
    public void setYeMeiChangedMessageEvent(boolean isYeMei){
        this.isYeMei=isYeMei;
    }
}
