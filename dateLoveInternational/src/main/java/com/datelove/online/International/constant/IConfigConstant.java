package com.datelove.online.International.constant;

import android.os.Environment;

import com.datelove.online.International.R;
import com.datelove.online.International.base.BaseApplication;

import java.io.File;

/**
 * 全局参数，统一配置
 * Created by zhangdroid on 2016/6/22.
 */
public interface IConfigConstant {
    /**
     * appsFlyer dev_key
     */
    String APPSFLYER_DEV_KEY = "7EhHiNjd6ef8TGUyZSGzF3";
    /**
     * 百度推送api_key
     */
    String BAIDU_PUSH_API_KEY = "FagQMtrzEGgt3nGsx3k4sHeT";

    /**
     * Google pay app key
     */
    String GOOGLE_APP_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuvZZk+QnPHozBkNWnCgcGz8TYtHwTEvbYf3tCiJDkznJTss7kjx65SVQgYya18zChEAY8fh3RIAbqPOq56nD4OCDi4ZLeV4m3thDgGOVa1S2pO2dZTulsygBAQYwQMPwPNhcmSRV7JKHDiHmY5pK7DAF+1siWnVaEDAquCMK7mOdgBZCOiPY3jSRf6Q/fMLMfwwH3PkXcsg+mceMTR2VcgQ6iP0BPvCqNF6m2IzldiBD88/OVJcb+yyQdGDNnkkhOaobT9S1Hxm1DdJ6f9mDN5BlzXlcx+uInwB4FeiUXcBuiOaGVw2TtmN3dGbfPv4AuIfTTFrTI/T5y90JtaZyEQIDAQAB";
    /**
     * 产品号
     */
    String PRODUCT_ID = "08";

    /**
     * 渠道号
     */
    String F_ID = "10801";
//    String F_ID = "1080101";

    /**
     * 客服邮箱
     */
    String E_MAIL = "Dating1420@yahoo.com";

    // Google支付SKU
//    String SKU_BEAN300 = "bean1";
//    String SKU_BEAN600 = "bean2";
//    String SKU_BEAN1500 = "bean3";
//    String SKU_MONTH1 = "onemonth";
//    String SKU_MONTH3 = "threemonths";
//    String SKU_MONTH12 = "oneyear";
//    String SKU_Standard_MONTH1 = "standardonemonth";
//    String SKU_Standard_MONTH3 = "standardthreemonths";
//    String SKU_Standard_MONTH12 = "standardoneyear";
    String SKU_BEAN300 = "bean1";
    String SKU_BEAN600 = "bean2";
    String SKU_BEAN1500 = "bean3";
    String SKU_MONTH1 = "viponemonth";
    String SKU_MONTH3 = "vipthreemonths";
    String SKU_MONTH12 = "viponeyear";
    String SKU_Standard_MONTH1 = "onemonth1";
    String SKU_Standard_MONTH3 = "svipthreemonths";
    String SKU_Standard_MONTH12 = "sviponeyear";

//    onemonth1
//    svipthreemonths
//    sviponeyear
//    viponemonth
//    vipthreemonths
//    viponeyear
    String SKU_ONEMONTH1 = "onemonth1";
    String SKU_SVIPTHREE = "svipthreemonths";
    String SKU_SVIPONEYEAR = "sviponeyear";
    String SKU_VIPONE = "viponemonth";
    String SKU_VIPTHREE = "vipthreemonths";
    String SKU_VIPYEAR = "viponeyear";

    /**
     * 支付来源：用户详情页
     */
    String PAY_TAG_FROM_DETAIL = "2";
    /**
     * 支付来源：聊天页
     */
    String PAY_TAG_FROM_CHAT = "3";
    /**
     * 支付来源：弹窗
     */
    String PAY_TAG_FROM_POPUP = "6";
    /**
     * 支付入口：包月
     */
    String PAY_WAY_MONTH = "1";
    /**
     * 支付入口：豆币
     */
    String PAY_WAY_BEAN = "0";
    /**
     * 支付入口: URL
     */
    String PAY_WAY_ALL = "-1";

    /**
     * 本地头像保存路径（Android/data/包名/files/Pictures）
     */
    String LOCAL_AVATAR_PATH = BaseApplication.getGlobalContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
            + File.separator + "avatar.jpg";
    /**
     * 本地照片墙保存路径（Android/data/包名/files/Pictures）
     */
    String LOCAL_wall_PATH = BaseApplication.getGlobalContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
            + File.separator + "wall.jpg";

    /**
     * 本地图片保存路径（Android/data/包名/files/Pictures）
     */
    String LOCAL_PIC_PATH = BaseApplication.getGlobalContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
            + File.separator + "photo.jpg";

    /**
     * 设置注册默认头像
     */
    int COMMON_AVATAR = R.drawable.register_head;
    /**
     * 全局默认圆形头像
     */
    int COMMON_DEFAULT_AVATAR_CIRCLE = R.drawable.img_default_circle;

    /**
     * 全局默认头像
     */
    int COMMON_DEFAULT_AVATAR = R.drawable.img_default;

}
