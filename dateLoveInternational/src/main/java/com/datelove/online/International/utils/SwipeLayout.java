package com.datelove.online.International.utils;


import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ViewDragHelper;
import android.support.v4.widget.ViewDragHelper.Callback;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

/**
 * 侧拉删除工具类
 * Created by Tian on 2017/8/22.
 *
 * **/
public class SwipeLayout extends FrameLayout {

	public enum Status{
		OPENED,CLOSED,DRAGING;
	}
	
	private Status status = Status.CLOSED;
	
	public Status getStatus() {
		return status;
	}
	
	public interface OnStatusChangeListener {
		void onClosed(SwipeLayout swipeLayout);
		void onOpened(SwipeLayout swipeLayout);
		void onDraging(SwipeLayout swipeLayout);
		void onStartOpen(SwipeLayout swipeLayout);
		void onStartClose(SwipeLayout swipeLayout);
	}
	
	private OnStatusChangeListener onStatusChangeListener;
	
	public void setOnStatusChangeListener(OnStatusChangeListener onStatusChangeListener) {
		this.onStatusChangeListener = onStatusChangeListener;
	}
	
	private ViewDragHelper mDragHelper;
	private ViewGroup mButtonView;
	private ViewGroup mContentView;
	private int mWidth;
	private int mHeight;
	private int mRange;

	public SwipeLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initView();
	}

	public SwipeLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		initView();
	}

	public SwipeLayout(Context context) {
		super(context);
		initView();
	}

	// 初始化要使用到资源
	private void initView() {
		mDragHelper = ViewDragHelper.create(this, new MyCallback());
	}
	
	@Override
	// 由 ViewDragHelper 帮助分析是否 要拦截触摸事件
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		return mDragHelper.shouldInterceptTouchEvent(ev);
	}
	
	@Override
	// 由 ViewDragHelper 帮助分析，子控件要如何移动
	public boolean onTouchEvent(MotionEvent event) {
		mDragHelper.processTouchEvent(event);
		return true;
	}
	
	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		System.out.println("SwipeLayout.onFinishInflate,");
		// 健壮性检查
		if (getChildCount()!=2) {
			throw new RuntimeException("SwipeLayout 只能放置两个子控件");
		}
		if (!(getChildAt(0) instanceof ViewGroup)||!(getChildAt(1) instanceof ViewGroup)) {
			throw new RuntimeException("SwipeLayout 的子控件只能是 ViewGroup");
		}
		
		mButtonView = (ViewGroup) getChildAt(0);
		mContentView = (ViewGroup) getChildAt(1);
	}
	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		mWidth = w;
		mHeight = h;
		mRange = mButtonView.getMeasuredWidth();
		System.out.println("SwipeLayout.onSizeChanged,mWidth="+mWidth+":mHeight="+mHeight+":mRange="+mRange);
	}

	@Override
	protected void onLayout(boolean changed, int left, int top, int right,
			int bottom) {
		System.out.println("SwipeLayout.onLayout,");
		super.onLayout(changed, left, top, right, bottom);
		
		// 将按钮部分放置到屏幕的右侧
		mButtonView.layout(mWidth, 0, mWidth + mRange, mHeight);
	}
	
	private final class MyCallback extends Callback {
		@Override
		// 返回true则说明child允许被移动
		public boolean tryCaptureView(View child, int pointerId) {
			return true;
		}
		
		@Override
		// 是在child移动之前被调用。返回的left 将是child要被移动到的x位置
		public int clampViewPositionHorizontal(View child, int left, int dx) {
			if (child == mContentView) {
				if (left>0) {
					left = 0;
				}else if (left< -mRange) {
					left = -mRange;
				}
			}else {
				// 侧栏按钮
				if (left < mWidth - mRange) {
					left = mWidth - mRange;
				}else if (left > mWidth) {
					left = mWidth;
				}
			}
			
			return left;
		}
		
		@Override
		// 在 changedView 移动之后被调用，可以在这里处理联动效果
		public void onViewPositionChanged(View changedView, int left, int top,
				int dx, int dy) {
			if (changedView == mContentView) {
				mButtonView.offsetLeftAndRight(dx);
			}else {
				mContentView.offsetLeftAndRight(dx);
			}
			
			
			handlerListener();
			
			// 为了兼容 2.3 等低版本，当控件位置变化后，需要手动的刷新界面
			invalidate();
		}
		
		@Override
		// 当用户松手时被调用。 
		// xvel 大于0，说明是向右滑动，应该关闭面板。
		// xvel 小于0，说明是向左滑动，应该打开面板。
		// xvel 等于0，如果面板打开不到一半则自动关闭面板，否则就打开面板
		public void onViewReleased(View releasedChild, float xvel, float yvel) {
//			System.out.println("SwipeLayout.onViewReleased,xvel="+xvel);
			if (xvel >0 || (xvel ==0 && mContentView.getLeft() > -mRange / 2 )) {
				// 关闭面板
				close();
			}else {
				// 打开面板
				open();
			}
		}

		@Override
		// 返回可拖拽的移动范围
		public int getViewHorizontalDragRange(View child) {
			return mRange;
		}
	}

	// 关闭面板
	public void close() {
		updateContentLeft(0);
	}

	// 根据面板的打开状态，调用对应的监听方法
	public void handlerListener() {
		if (onStatusChangeListener == null) {
			return;
		}
		
		
		Status preStatus = status; // 记录旧状态
		status = updateStatus(); // 获取新状态
		
		// 如果是重复的状态则不处理
		if (preStatus==status) {
			return;
		}
		
		// 根据不同的状态变化，调用不同的监听方法
		if (status == Status.OPENED) {
			// 当前是打开状态
			onStatusChangeListener.onOpened(this);
		}else if (status == Status.CLOSED) {
			// 当前是关闭状态
			onStatusChangeListener.onClosed(this);
		}else {
			// 当前是拖拽状态
			onStatusChangeListener.onDraging(this);
			if (preStatus == Status.OPENED) {
				// 从打开状态变为拖拽，说明用户想要关闭面板
				onStatusChangeListener.onStartClose(this);
			}else if (preStatus==Status.CLOSED) {
				// 从关闭状态变为拖拽，说明用户想要打开面板
				onStatusChangeListener.onStartOpen(this);
			}
		}
	}

	// 根据面板的位置，计算打开状态
	private Status updateStatus() {
		if (mContentView.getLeft() == -mRange) {
			return Status.OPENED;
		}
		
		if (mContentView.getLeft() == 0) {
			return Status.CLOSED;
		}
		
		
		return Status.DRAGING;
	}

	// 打开面板
	private void open() {
		updateContentLeft(-mRange);
	}

	// 修改正文的位置，并同步修改侧栏按钮的位置
	private void updateContentLeft(int contentleft) {
		
		// 开启平滑滚动
		if (mDragHelper.smoothSlideViewTo(mContentView, contentleft, 0)) {
			ViewCompat.postInvalidateOnAnimation(this);
		}
	}
	
	@Override
	public void computeScroll() {
		super.computeScroll();
		
		// 如果continueSettling返回true则说明还没有滚动到结束位置，需要继续滚动
		if (mDragHelper.continueSettling(true)) {
			ViewCompat.postInvalidateOnAnimation(this);
		}
	}
}
