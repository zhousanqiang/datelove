package com.datelove.online.International.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.datelove.online.International.R;
import com.datelove.online.International.bean.Image;
import com.datelove.online.International.bean.User;
import com.datelove.online.International.bean.UserBase;
import com.datelove.online.International.constant.IConfigConstant;
import com.datelove.online.International.utils.CommonRequestUtil;
import com.datelove.online.International.utils.ParamsUtils;
import com.library.adapter.recyclerview.CommonRecyclerViewAdapter;
import com.library.adapter.recyclerview.RecyclerViewHolder;
import com.library.imageloader.CircleTransformation;
import com.library.imageloader.ImageLoader;
import com.library.imageloader.ImageLoaderUtil;
import com.library.utils.HeightUtils;

import java.util.List;

/**
 * 搜索适配器
 * Created by zhangdroid on 2016/10/29.
 */
public class CareAdapter extends CommonRecyclerViewAdapter<User> {

    public CareAdapter(Context context, int layoutResId) {
        super(context, layoutResId);
    }

    public CareAdapter(Context context, int layoutResId, List list) {
        super(context, layoutResId, list);
    }

    @Override
    protected void convert(final int position, RecyclerViewHolder viewHolder, final User bean) {
        if (bean != null) {
            final UserBase userBase = bean.getUserBaseEnglish();
            if (userBase != null) {
                ImageView ivAvatar = (ImageView) viewHolder.getView(R.id.item_care_avatar);
                Image image = userBase.getImage();
                if (image != null) {
                    String imgUrl = image.getThumbnailUrl();
                    if (!TextUtils.isEmpty(imgUrl)) {
                        ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(imgUrl).imageView(ivAvatar)
                                .placeHolder(IConfigConstant.COMMON_DEFAULT_AVATAR_CIRCLE).transform(new CircleTransformation()).build());
                    } else {
                        ivAvatar.setImageResource(IConfigConstant.COMMON_DEFAULT_AVATAR_CIRCLE);
                    }
                } else {
                    ivAvatar.setImageResource(IConfigConstant.COMMON_DEFAULT_AVATAR_CIRCLE);
                }
                // 昵称
                String nickname = userBase.getNickName();
                if (!TextUtils.isEmpty(nickname)) {
                    viewHolder.setText(R.id.item_care_nickname, nickname);
                }
                // 年龄、身高
                int age = userBase.getAge();
                if (age < 18) {
                    age = 18;
                }
                StringBuilder age_height = new StringBuilder();
                age_height.append(age)
//                        .append(mContext.getString(R.string.years))
                        .append("  ");
                String heightCm = userBase.getHeightCm();
                if (!TextUtils.isEmpty(heightCm)) {
                    age_height.append(HeightUtils.getInchCmByCm(heightCm));
                }
                viewHolder.setText(R.id.item_care_age_height, age_height.toString());
                // 地区，收入
                StringBuilder area_income = new StringBuilder();
                String area = userBase.getArea().getProvinceName();
                if (!TextUtils.isEmpty(area)) {
                    area_income.append(area);
                }
                String income = ParamsUtils.getIncomeMap().get(String.valueOf(userBase.getIncome()));
                if (!TextUtils.isEmpty(income)) {
                    if (!TextUtils.isEmpty(area)) {
                        area_income.append("  ");
                    }
                    area_income.append(income);
                }
                viewHolder.setText(R.id.item_care_location_income, area_income.toString());
                // 取消关注
                final TextView unfollow = (TextView) viewHolder.getView(R.id.item_care_unfollow);
                unfollow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        CommonRequestUtil.cancelFollow(mContext, userBase.getId(),
                                true, new CommonRequestUtil.OnCommonListener() {

                                    public void onSuccess() {
                                        removeItem(bean);
                                    }

                                    @Override
                                    public void onFail() {

                                    }
                                });
                    }
                });
            }
        }
    }

}
