package com.datelove.online.International.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.datelove.online.International.R;
import com.datelove.online.International.base.BaseTitleActivity;
import com.datelove.online.International.bean.BaseModel;
import com.datelove.online.International.constant.IConfigConstant;
import com.datelove.online.International.constant.IUrlConstant;
import com.datelove.online.International.event.CloseMainActivityEvent;
import com.datelove.online.International.utils.FileUtil;
import com.datelove.online.International.utils.Utils;
import com.datelove.online.International.xml.PlatformInfoXml;
import com.datelove.online.International.xml.UserInfoXml;
import com.library.dialog.OnDoubleDialogClickListener;
import com.library.utils.DialogUtil;
import com.library.utils.SharedPreferenceUtil;
import com.library.utils.ToastUtil;
import com.library.utils.Util;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.Callback;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.Response;

/**
 * 设置
 * Created by Administrator on 2016/10/22.
 */
public class SettingActivity extends BaseTitleActivity {
    @BindView(R.id.uid)
    TextView mTvUid;
    @BindView(R.id.setting_email)
    TextView mTvEmail;
    @BindView(R.id.change_pwd)
    LinearLayout mLlChangePwd;
    @BindView(R.id.log_out)
    Button mBtnLogOut;
    @BindView(R.id.cancel)
    Button mbtnCancel;
    @BindView(R.id.user_name)
    TextView userName;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_setting;
    }

    @Override
    protected String getCenterTitle() {
        return getString(R.string.my_set);
    }

    @Override
    protected void initViewsAndVariables() {
        mTvUid.setText(UserInfoXml.getUID());
        userName.setText(UserInfoXml.getUsername());
        mTvEmail.setText(IConfigConstant.E_MAIL);
    }

    @Override
    protected void addListeners() {
        mBtnLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 删除用户本地头像
                FileUtil.deleteFile(IConfigConstant.LOCAL_AVATAR_PATH);
                String username = UserInfoXml.getUsername();
                String password = UserInfoXml.getPassword();
                SharedPreferenceUtil.clearXml(SettingActivity.this, UserInfoXml.XML_NAME);
                UserInfoXml.setUsername(username);
                UserInfoXml.setPassword(password);
                // 关闭MainActivity事件
                EventBus.getDefault().post(new CloseMainActivityEvent());
                SharedPreferenceUtil.setBooleanValue(Utils.getContext(), "INTERCEPT_DIALOG", "DIALOG", true);
//                删除定位信息
                SharedPreferenceUtil.setStringValue(Utils.getContext(),"LOCATION","getLatitude",null);
                SharedPreferenceUtil.setStringValue(Utils.getContext(),"LOCATION","getLongitude",null);

//                Util.gotoActivity(SettingActivity.this, LoginActivity.class, true);
                Intent intent = new Intent(SettingActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.login_anim_in, R.anim.login_anim_out);
            }
        });
        mLlChangePwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.gotoActivity(SettingActivity.this, ChangePwdActivity.class, false);
            }
        });
        mbtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogUtil.showDoubleBtnDialog(getSupportFragmentManager(), getString(R.string.switch_cancel), getString(R.string.cancel_warn), getString(R.string.sure), getString(R.string.cancel), false, new OnDoubleDialogClickListener() {
                    @Override
                    public void onPositiveClick(View view) {
                        OkHttpUtils.post()
                                .url(IUrlConstant.URL_LOG_OUT)
                                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                                .build()
                                .execute(new Callback<BaseModel>() {
                                    @Override
                                    public BaseModel parseNetworkResponse(Response response, int id) throws Exception {
                                        String resultJson = response.body().string();
                                        if (!TextUtils.isEmpty(resultJson)) {
                                            return JSON.parseObject(resultJson, BaseModel.class);
                                        }
                                        return null;
                                    }

                                    @Override
                                    public void onError(Call call, Exception e, int id) {
//                                        ToastUtil.showShortToast(Utils.getContext(), Utils.getContext().getString(R.string.log_out_success));

                                    }

                                    @Override
                                    public void onResponse(BaseModel response, int id) {
                                        if (response != null) {
                                            String isSucceed = response.getIsSucceed();
                                            if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
                                                ToastUtil.showShortToast(Utils.getContext(), Utils.getContext().getString(R.string.log_out_success));
                                                // 删除用户本地头像
                                                FileUtil.deleteFile(IConfigConstant.LOCAL_AVATAR_PATH);
                                                String username = UserInfoXml.getUsername();
                                                String password = UserInfoXml.getPassword();
                                                SharedPreferenceUtil.clearXml(SettingActivity.this, UserInfoXml.XML_NAME);
                                                UserInfoXml.setUsername(username);
                                                UserInfoXml.setPassword(password);
                                                // 关闭MainActivity事件
                                                EventBus.getDefault().post(new CloseMainActivityEvent());
                                                SharedPreferenceUtil.setBooleanValue(Utils.getContext(), "INTERCEPT_DIALOG", "DIALOG", true);
                                                Intent intent = new Intent(SettingActivity.this, RegisterActivity.class);
                                                startActivity(intent);
                                                finish();
                                                overridePendingTransition(R.anim.login_anim_in, R.anim.login_anim_out);
                                            }
                                        }
                                    }
                                });

                    }

                    @Override
                    public void onNegativeClick(View view) {


                    }
                });
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
