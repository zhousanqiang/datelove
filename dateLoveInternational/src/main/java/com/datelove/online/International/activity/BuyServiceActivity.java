package com.datelove.online.International.activity;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.alibaba.fastjson.JSON;
import com.datelove.online.International.R;
import com.datelove.online.International.base.BaseTitleActivity;
import com.datelove.online.International.bean.PayWay;
import com.datelove.online.International.bean.User;
import com.datelove.online.International.constant.IConfigConstant;
import com.datelove.online.International.constant.IUrlConstant;
import com.datelove.online.International.event.RefreshChatDialogEvent;
import com.datelove.online.International.event.RefreshChatEvent;
import com.datelove.online.International.event.UpdatePayInfoEvent;
import com.datelove.online.International.payUtil.IabHelper;
import com.datelove.online.International.payUtil.IabResult;
import com.datelove.online.International.payUtil.Purchase;
import com.datelove.online.International.utils.AppsflyerUtils;
import com.datelove.online.International.utils.CommonRequestUtil;
import com.datelove.online.International.utils.KochavaUtils;
import com.datelove.online.International.utils.Utils;
import com.datelove.online.International.xml.PlatformInfoXml;
import com.datelove.online.International.xml.UserInfoXml;
import com.library.utils.DialogUtil;
import com.library.utils.PayUtils;
import com.library.utils.SharedPreferenceUtil;
import com.library.utils.TimeUtil;
import com.library.utils.ToastUtil;
import com.library.utils.Util;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.Callback;
import com.zhy.http.okhttp.callback.StringCallback;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.Response;

/**
 * 支付页面
 * Modified by zhangdroid on 2017/03/09.
 */
public class BuyServiceActivity extends BaseTitleActivity implements View.OnClickListener {
    @BindView(R.id.ll_month)
    LinearLayout ll_month;
    @BindView(R.id.ll_bean)
    LinearLayout ll_bean;
    @BindView(R.id.month1)
    LinearLayout month1;
    @BindView(R.id.month3)
    LinearLayout month3;
    @BindView(R.id.month12)
    LinearLayout month12;
    @BindView(R.id.month1_payall)
    Button month1_payall;
    @BindView(R.id.month3_payall)
    Button month3_payall;
    @BindView(R.id.month12_payall)
    Button month12_payall;
    @BindView(R.id.bean1)
    Button bean1;
    @BindView(R.id.bean2)
    Button bean2;
    @BindView(R.id.bean3)
    Button bean3;
    @BindView(R.id.bean3_pay)
    LinearLayout bean3Pay;
    @BindView(R.id.bean2_pay)
    LinearLayout bean2Pay;
    @BindView(R.id.bean1_pay)
    LinearLayout bean1Pay;
    @BindView(R.id.ll_intorduce)
    LinearLayout llIntorduce;

//    @BindView(R.id.beans)
//    TextView beans;// 豆币数

    private String[] skus = {IConfigConstant.SKU_BEAN300, IConfigConstant.SKU_BEAN600, IConfigConstant.SKU_BEAN1500,
            IConfigConstant.SKU_MONTH1, IConfigConstant.SKU_MONTH3, IConfigConstant.SKU_MONTH12,
            IConfigConstant.SKU_Standard_MONTH1, IConfigConstant.SKU_Standard_MONTH3, IConfigConstant.SKU_Standard_MONTH12};


    private static final String INTENT_KEY_TYPE = "intent_key_type";
    private static final String INTENT_KEY_FROM_TAG = "intent_key_from_tag";
    private static final String INTENT_KEY_SERVICE_WAY = "intent_key_service_way";
    public static final String INTENT_FROM_INTERCEPT = "intent_key_intercept";
    public static final String INTENT_FROM_BEAN = "intent_key_bean";
    public static final String INTENT_FROM_MONTH = "intent_key_month";

    private IabHelper mHelper;
    private static final int RC_REQUEST = 10001;
    private boolean iap_is_ok = false;
    private String sku1;
    String fromTag = "";

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_buy_service;
    }

    @Override
    protected String getCenterTitle() {
        return getString(R.string.recharge);
    }

    /**
     * 跳转支付页面
     *
     * @param from       Activity对象
     * @param sourceTag  支付入口
     * @param serviceWay 支付类型
     */
    public static void toBuyServiceActivity(Activity from, String type, String sourceTag, String serviceWay) {
        Map<String, String> map = new HashMap<String, String>();
        map.put(INTENT_KEY_TYPE, type);
        map.put(INTENT_KEY_FROM_TAG, sourceTag);
        map.put(INTENT_KEY_SERVICE_WAY, serviceWay);
        Util.gotoActivity(from, BuyServiceActivity.class, false, map);
    }

    @Override
    protected void initViewsAndVariables() {
        String type = INTENT_FROM_INTERCEPT;

        String serviceWay = "-1";
        Intent intent = getIntent();
        if (intent != null) {
            type = intent.getStringExtra(INTENT_KEY_TYPE);
            fromTag = intent.getStringExtra(INTENT_KEY_FROM_TAG);
            serviceWay = intent.getStringExtra(INTENT_KEY_SERVICE_WAY);
        }
        switch (type) {
            case INTENT_FROM_INTERCEPT:// 支付拦截
                ll_bean.setVisibility(View.VISIBLE);
                ll_month.setVisibility(View.VISIBLE);
//                beans.setVisibility(View.GONE);
                break;

            case INTENT_FROM_BEAN:// 豆币
                ll_bean.setVisibility(View.VISIBLE);
                ll_month.setVisibility(View.GONE);
                llIntorduce.setVisibility(View.GONE);
//                beans.setVisibility(View.VISIBLE);
//                beans.setText(getString(R.string.beancounts, String.valueOf(UserInfoXml.getBeanCount())));
                break;

            case INTENT_FROM_MONTH:// 包月
                ll_bean.setVisibility(View.GONE);
                ll_month.setVisibility(View.VISIBLE);
                llIntorduce.setVisibility(View.VISIBLE);
//                beans.setVisibility(View.GONE);
                break;
        }
        // 价格本地化
        setPriceByCountry();
        // 支付统计和支付方式
        getPayWay(fromTag, serviceWay);

        mHelper = new IabHelper(BuyServiceActivity.this, IConfigConstant.GOOGLE_APP_KEY);
        mHelper.enableDebugLogging(false);
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    return;
                }
                iap_is_ok = true;
            }
        });
    }

    private void setPriceByCountry() {
        String mo1 = getResources().getString(R.string.month1);
        String mo3 = getResources().getString(R.string.month3);
        String mo12 = getResources().getString(R.string.month12);
        String b1 = getResources().getString(R.string.bean1);
        String b2 = getResources().getString(R.string.bean2);
        String b3 = getResources().getString(R.string.bean3);
        String mo1All = getResources().getString(R.string.month1_pay);
        String mo3All = getResources().getString(R.string.month2_pay);
        String mo12All = getResources().getString(R.string.month3_pay);
        switch (PlatformInfoXml.getCountry()) {
            case "America":
//                month1.setText(String.format(mo1, "$16"));
//                month3.setText(String.format(mo3, "$10.6"));
//                month12.setText(String.format(mo12, "$6.6"));
                month1_payall.setText(String.format(mo1All, "$19.99"));
                month3_payall.setText(String.format(mo3All, "$38.99"));
                month12_payall.setText(String.format(mo12All, "$89.99"));
                bean1.setText(String.format(b1, "$14.99"));
                bean2.setText(String.format(b2, "$29.99"));
                bean3.setText(String.format(b3, "$74.99"));
                break;
//            澳大利亚
            case "Australia":
//                month1.setText(String.format(mo1, "$21.99"));
//                month3.setText(String.format(mo3, "$14.6"));
//                month12.setText(String.format(mo12, "$9.2"));
                month1_payall.setText(String.format(mo1All, "AUD28.99"));
                month3_payall.setText(String.format(mo3All, "AUD55.99"));
                month12_payall.setText(String.format(mo12All, "AUD129.99"));
                bean1.setText(String.format(b1, "AUD20.99"));
                bean2.setText(String.format(b2, "AUD42.99"));
                bean3.setText(String.format(b3, "AUD104.99"));
                break;
//            印度
            case "India":
//                month1.setText(String.format(mo1, "Rs1,100"));
//                month3.setText(String.format(mo3, "Rs733"));
//                month12.setText(String.format(mo12, "Rs450"));
                month1_payall.setText(String.format(mo1All, "Rs1,300"));
                month3_payall.setText(String.format(mo3All, "Rs2,500"));
                month12_payall.setText(String.format(mo12All, "Rs5,800"));
                bean1.setText(String.format(b1, "Rs990"));
                bean2.setText(String.format(b2, "Rs1,950"));
                bean3.setText(String.format(b3, "Rs4,850"));
                break;
//            印度尼西亚
            case "Indonesia":
//                month1.setText(String.format(mo1, "Rp 219,000"));
//                month3.setText(String.format(mo3, "Rp 143000"));
//                month12.setText(String.format(mo12, "Rp 88723"));
                month1_payall.setText(String.format(mo1All, "Rp269,000"));
                month3_payall.setText(String.format(mo3All, "Rp499,000"));
                month12_payall.setText(String.format(mo12All, "Rp1,220,579"));
                bean1.setText(String.format(b1, "Rp199,000"));
                bean2.setText(String.format(b2, "Rp409,000"));
                bean3.setText(String.format(b3, "Rp1,017,127"));
                break;
//            英国
            case "UnitedKingdom":
//                month1.setText(String.format(mo1, "£15.49"));
//                month3.setText(String.format(mo3, "£10.49"));
//                month12.setText(String.format(mo12, "£6.41"));
                month1_payall.setText(String.format(mo1All, "£17.99"));
                month3_payall.setText(String.format(mo3All, "£35.49"));
                month12_payall.setText(String.format(mo12All, "£80.99"));
                bean1.setText(String.format(b1, "£13.49"));
                bean2.setText(String.format(b2, "£26.99"));
                bean3.setText(String.format(b3, "£67.99"));
                break;
            case "Canada":
//                month1.setText(String.format(mo1, "$21.99"));
//                month3.setText(String.format(mo3, "$14.33"));
//                month12.setText(String.format(mo12, "$8.74"));
                month1_payall.setText(String.format(mo1All, "CAD25.99"));
                month3_payall.setText(String.format(mo3All, "CAD49.99"));
                month12_payall.setText(String.format(mo12All, "CAD114.99"));
                bean1.setText(String.format(b1, "CAD18.99"));
                bean2.setText(String.format(b2, "CAD37.99"));
                bean3.setText(String.format(b3, "CAD95.99"));
                break;
//            新西兰
            case "NewZealand":
//                month1.setText(String.format(mo1, "$26.99"));
//                month3.setText(String.format(mo3, "$18.33"));
//                month12.setText(String.format(mo12, "$10.83"));
                month1_payall.setText(String.format(mo1All, "NZD32.99"));
                month3_payall.setText(String.format(mo3All, "NZD64.99"));
                month12_payall.setText(String.format(mo12All, "NZD149.99"));
                bean1.setText(String.format(b1, "NZD24.99"));
                bean2.setText(String.format(b2, "NZD49.99"));
                bean3.setText(String.format(b3, "NZD124.99"));
                break;
//            爱尔兰
            case "Ireland":
//                month1.setText(String.format(mo1, "18.99€"));
//                month3.setText(String.format(mo3, "12.66€"));
//                month12.setText(String.format(mo12, "7.9€"));
                month1_payall.setText(String.format(mo1All, "20.99€"));
                month3_payall.setText(String.format(mo3All, "40.99€"));
                month12_payall.setText(String.format(mo12All, "94.99€"));
                bean1.setText(String.format(b1, "15.99€"));
                bean2.setText(String.format(b2, "30.99€"));
                bean3.setText(String.format(b3, "79.99€"));
                break;
//            南非
            case "SouthAfrica":
//                month1.setText(String.format(mo1, "R219.99"));
//                month3.setText(String.format(mo3, "R146.66"));
//                month12.setText(String.format(mo12, "R91.66"));
                month1_payall.setText(String.format(mo1All, "R279.99"));
                month3_payall.setText(String.format(mo3All, "R549.99"));
                month12_payall.setText(String.format(mo12All, "R1,249.99"));
                bean1.setText(String.format(b1, "R209.99"));
                bean2.setText(String.format(b2, "R419.99"));
                bean3.setText(String.format(b3, "R1,049.99"));
                break;
//            新加坡
            case "Singapore":
//                month1.setText(String.format(mo1, "S$22.99"));
//                month3.setText(String.format(mo3, "S$15.33"));
//                month12.setText(String.format(mo12, "S$9.16"));
                month1_payall.setText(String.format(mo1All, "S$26.99"));
                month3_payall.setText(String.format(mo3All, "S$52.99"));
                month12_payall.setText(String.format(mo12All, "S$119.99"));
                bean1.setText(String.format(b1, "S$19.99"));
                bean2.setText(String.format(b2, "S$40.98"));
                bean3.setText(String.format(b3, "S$99.99"));
                break;
//            巴基斯坦
            case "Pakistan":
//                month1.setText(String.format(mo1, "Rs1,700"));
//                month3.setText(String.format(mo3, "Rs1116"));
//                month12.setText(String.format(mo12, "Rs691"));
                month1_payall.setText(String.format(mo1All, "Rs2,100"));
                month3_payall.setText(String.format(mo3All, "Rs4,100"));
                month12_payall.setText(String.format(mo12All, "Rs9,500"));
                bean1.setText(String.format(b1, "Rs1,600"));
                bean2.setText(String.format(b2, "Rs3,150"));
                bean3.setText(String.format(b3, "Rs57,900"));
                break;
            case "Philippines":
//                month1.setText(String.format(mo1, "₱795"));
//                month3.setText(String.format(mo3, "₱533"));
//                month12.setText(String.format(mo12, "₱329"));
                month1_payall.setText(String.format(mo1All, "₱1,050"));
                month3_payall.setText(String.format(mo3All, "₱2,000"));
                month12_payall.setText(String.format(mo12All, "₱4,650"));
                bean1.setText(String.format(b1, "₱775"));
                bean2.setText(String.format(b2, "₱1,550"));
                bean3.setText(String.format(b3, "₱3,900"));
                break;
//            香港
            case "HongKong":
//                month1.setText(String.format(mo1, "HK$128.00"));
//                month3.setText(String.format(mo3, "HK$82.66"));
//                month12.setText(String.format(mo12, "HK$50.75"));
                month1_payall.setText(String.format(mo1All, "HK$158"));
                month3_payall.setText(String.format(mo3All, "HK$308"));
                month12_payall.setText(String.format(mo12All, "HK$699"));
                bean1.setText(String.format(b1, "HK$118"));
                bean2.setText(String.format(b2, "HK$238"));
                bean3.setText(String.format(b3, "HK$588"));
                break;
//            墨西哥
            case "Mexico":
                month1_payall.setText(String.format(mo1All, " MXN379"));
                month3_payall.setText(String.format(mo3All, "MXN739"));
                month12_payall.setText(String.format(mo12All, "MXN1,699"));
                bean1.setText(String.format(b1, "MXN285"));
                bean2.setText(String.format(b2, "MXN569"));
                bean3.setText(String.format(b3, "MXN1,449"));
                break;
//            奥地利
            case "Austria":
                month1_payall.setText(String.format(mo1All, "EUR19.99"));
                month3_payall.setText(String.format(mo3All, "EUR39.99"));
                month12_payall.setText(String.format(mo12All, "EUR89.99"));
                bean1.setText(String.format(b1, "EUR14.99"));
                bean2.setText(String.format(b2, "EUR29.99"));
                bean3.setText(String.format(b3, "EUR74.99"));
                break;
//            智利
            case "Chile":
                month1_payall.setText(String.format(mo1All, "CLP12,6"));
                month3_payall.setText(String.format(mo3All, "CLP24,6"));
                month12_payall.setText(String.format(mo12All, "CLP57,0"));
                bean1.setText(String.format(b1, "CLP9,4"));
                bean2.setText(String.format(b2, "CLP18,9"));
                bean3.setText(String.format(b3, "CLP47,2"));
                break;
//            玻利维亚
            case "Bolivia":
                month1_payall.setText(String.format(mo1All, "BOB139.99"));
                month3_payall.setText(String.format(mo3All, "BOB269.99"));
                month12_payall.setText(String.format(mo12All, "BOB619.99"));
                bean1.setText(String.format(b1, "BOB104.99"));
                bean2.setText(String.format(b2, "BOB209.99"));
                bean3.setText(String.format(b3, "BOB519.99"));
                break;
//            瑞士
            case "Suisse":
                month1_payall.setText(String.format(mo1All, "CHF20"));
                month3_payall.setText(String.format(mo3All, "CHF39"));
                month12_payall.setText(String.format(mo12All, "CHF90"));
                bean1.setText(String.format(b1, "CHF15"));
                bean2.setText(String.format(b2, "CHF30"));
                bean3.setText(String.format(b3, "CHF75"));
                break;
//            秘鲁
            case "Peru":
                month1_payall.setText(String.format(mo1All, "PEN64.99"));
                month3_payall.setText(String.format(mo3All, "PEN124.99"));
                month12_payall.setText(String.format(mo12All, "PEN289.99"));
                bean1.setText(String.format(b1, "PEN47.99"));
                bean2.setText(String.format(b2, "PEN96.99"));
                bean3.setText(String.format(b3, "PEN244.99"));
                break;
//            德国
            case "Germany":
                month1_payall.setText(String.format(mo1All, "EUR19.99"));
                month3_payall.setText(String.format(mo3All, "EUR38.99"));
                month12_payall.setText(String.format(mo12All, "EUR89.99"));
                bean1.setText(String.format(b1, "EUR14.99"));
                bean2.setText(String.format(b2, "EUR29.99"));
                bean3.setText(String.format(b3, "EUR74.99"));
                break;
//            日本
            case "Japan":
                month1_payall.setText(String.format(mo1All, "JPY2,260"));
                month3_payall.setText(String.format(mo3All, "JPY4,420"));
                month12_payall.setText(String.format(mo12All, "JPY10,200"));
                bean1.setText(String.format(b1, "JPY1,700"));
                bean2.setText(String.format(b2, "JPY3,400"));
                bean3.setText(String.format(b3, "JPY8,500"));
                break;
//            法国
            case "France":
                month1_payall.setText(String.format(mo1All, "EUR19.99"));
                month3_payall.setText(String.format(mo3All, "EUR39.99"));
                month12_payall.setText(String.format(mo12All, "EUR89.99"));
                bean1.setText(String.format(b1, "EUR14.99"));
                bean2.setText(String.format(b2, "EUR29.99"));
                bean3.setText(String.format(b3, "EUR74.99"));
                break;
//            西班牙
            case "Spain":
                month1_payall.setText(String.format(mo1All, "EUR19.99"));
                month3_payall.setText(String.format(mo3All, "EUR39.99"));
                month12_payall.setText(String.format(mo12All, "EUR89.99"));
                bean1.setText(String.format(b1, "EUR14.99"));
                bean2.setText(String.format(b2, "EUR30.99"));
                bean3.setText(String.format(b3, "EUR74.99"));
                break;

            default:
//                month1.setText(String.format(mo1, "$16"));
//                month3.setText(String.format(mo3, "$10.6"));
//                month12.setText(String.format(mo12, "$6.6"));
                month1_payall.setText(String.format(mo1All, "$19.99"));
                month3_payall.setText(String.format(mo3All, "$38.99"));
                month12_payall.setText(String.format(mo12All, "$89.99"));
                bean1.setText(String.format(b1, "$14.99"));
                bean2.setText(String.format(b2, "$29.99"));
                bean3.setText(String.format(b3, "$74.99"));
                break;
        }
    }

    private void getPayWay(String fromTag, String serviceWay) {
        OkHttpUtils.post()
                .url(IUrlConstant.URL_PAY_WAY)
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .addHeader("token", PlatformInfoXml.getToken())
                .addParams("fromTag", fromTag)
                .addParams("serviceWay", serviceWay)
                .build()
                .execute(new Callback<PayWay>() {
                    @Override
                    public PayWay parseNetworkResponse(Response response, int id) throws Exception {
                        String resultJson = response.body().string();
                        if (!TextUtils.isEmpty(resultJson)) {
                            return JSON.parseObject(resultJson, PayWay.class);
                        }
                        return null;
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                    }

                    @Override
                    public void onResponse(PayWay response, int id) {
                    }
                });
    }

    @Override
    protected void addListeners() {
        month1.setOnClickListener(this);
        month3.setOnClickListener(this);
        month12.setOnClickListener(this);
        bean1.setOnClickListener(this);
        bean2.setOnClickListener(this);
        bean3.setOnClickListener(this);
        bean1Pay.setOnClickListener(this);
        bean2Pay.setOnClickListener(this);
        bean3Pay.setOnClickListener(this);
        month1_payall.setOnClickListener(this);
        month3_payall.setOnClickListener(this);
        month12_payall.setOnClickListener(this);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bean1_pay:
//                payTest(PlatformInfoXml.getPlatformInfo().getFid() + "101", "1");
//                AppsflyerUtils.click300Bean();
//                iapHandler.sendEmptyMessage(0);
                AppsflyerUtils.clickStandard1Month();
                iapHandler.sendEmptyMessage(6);
                break;
            case R.id.bean2_pay:
//                AppsflyerUtils.click600Bean();
//                iapHandler.sendEmptyMessage(1);
                AppsflyerUtils.clickStandard3Month();
                iapHandler.sendEmptyMessage(7);
                break;
            case R.id.bean3_pay:
//                AppsflyerUtils.click1500Bean();
//                iapHandler.sendEmptyMessage(2);
                AppsflyerUtils.clickStandardOneYear();
                iapHandler.sendEmptyMessage(8);
                break;
            case R.id.bean1:
////                payTest(PlatformInfoXml.getPlatformInfo().getFid() + "101", "1");
////                AppsflyerUtils.click300Bean();
////                iapHandler.sendEmptyMessage(0);
                AppsflyerUtils.clickStandard1Month();
                iapHandler.sendEmptyMessage(6);
                break;
            case R.id.bean2:
////                AppsflyerUtils.click600Bean();
////                iapHandler.sendEmptyMessage(1);
//                AppsflyerUtils.clickStandard3Month();
//                iapHandler.sendEmptyMessage(7);
                AppsflyerUtils.clickStandard3Month();
                iapHandler.sendEmptyMessage(7);
                break;
            case R.id.bean3:
////                AppsflyerUtils.click1500Bean();
////                iapHandler.sendEmptyMessage(2);
//                AppsflyerUtils.clickStandardOneYear();
//                iapHandler.sendEmptyMessage(8);
                AppsflyerUtils.clickStandardOneYear();
                iapHandler.sendEmptyMessage(8);
                break;
            case R.id.month1:
//                AppsflyerUtils.click1Month();
                KochavaUtils.kochavaSvipOnemonth();
                iapHandler.sendEmptyMessage(3);
                break;
            case R.id.month3:
//                AppsflyerUtils.click3Month();
                KochavaUtils.kochavaSvipthreemonths();
                iapHandler.sendEmptyMessage(4);
                break;
            case R.id.month12:
//                AppsflyerUtils.clickOneYear();
                KochavaUtils.kochavaSviponeyear();
                iapHandler.sendEmptyMessage(5);
                break;
            case R.id.month1_payall:
//                AppsflyerUtils.click1Month();
                KochavaUtils.kochavaSvipOnemonth();
                iapHandler.sendEmptyMessage(3);
                break;
            case R.id.month3_payall:
//                AppsflyerUtils.click3Month();
                iapHandler.sendEmptyMessage(4);
                break;
            case R.id.month12_payall:
                AppsflyerUtils.clickOneYear();
                iapHandler.sendEmptyMessage(5);
                break;
        }
    }

    /**
     * 根据SKU购买商品
     *
     * @param sku 商品对应的SKU
     */
    private void purchase(String sku) {
        if (isAppInstalled(BuyServiceActivity.this, "com.android.vending") && mHelper != null && iap_is_ok) {
            sku1 = sku;
            mHelper.handleActivityResult(mHelper.mRequestCode, Activity.RESULT_CANCELED, null);
//            mHelper.launchPurchaseFlow(BuyServiceActivity.this, sku, RC_REQUEST, mPurchaseFinishedListener);
            mHelper.launchSubscriptionPurchaseFlow(BuyServiceActivity.this, sku, RC_REQUEST, mPurchaseFinishedListener);

        } else {
            showMessage(getString(R.string.pay_tip_message));
        }
    }

    Handler iapHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    purchase(skus[0]);
                    break;
                case 1:
                    purchase(skus[1]);
                    break;
                case 2:
                    purchase(skus[2]);
                    break;
                case 3:
                    purchase(skus[3]);
                    break;
                case 4:
                    purchase(skus[4]);
                    break;
                case 5:
                    purchase(skus[5]);
                    break;
                case 6:
                    purchase(skus[6]);
                    break;
                case 7:
                    purchase(skus[7]);
                    break;
                case 8:
                    purchase(skus[8]);
                    break;
            }
        }
    };

    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            if(purchase==null){

//                if (sku1.equals(skus[0])) {
//                    pay(purchase, PlatformInfoXml.getPlatformInfo().getFid() + "101", "2");
//                    mHelper.consumeAsync(purchase, mConsumeFinishedListener);
//                } else if (sku1.equals(skus[1])) {
//                    pay(purchase, PlatformInfoXml.getPlatformInfo().getFid() + "102", "2");
//                    mHelper.consumeAsync(purchase, mConsumeFinishedListener);
//                } else if (sku1.equals(skus[2])) {
//                    pay(purchase, PlatformInfoXml.getPlatformInfo().getFid() + "103", "2");
//                    mHelper.consumeAsync(purchase, mConsumeFinishedListener);
//                } else if (sku1.equals(skus[3])) {
//                    pay(purchase, PlatformInfoXml.getPlatformInfo().getFid() + "002", "1");
//                    mHelper.consumeAsync(purchase, mConsumeFinishedListener);
//                } else if (sku1.equals(skus[4])) {
//                    pay(purchase, PlatformInfoXml.getPlatformInfo().getFid() + "001", "1");
//                    mHelper.consumeAsync(purchase, mConsumeFinishedListener);
//                } else if (sku1.equals(skus[5])) {
//                    pay(purchase, PlatformInfoXml.getPlatformInfo().getFid() + "003", "1");
//                    mHelper.consumeAsync(purchase, mConsumeFinishedListener);
//                } else if (sku1.equals(skus[6])) {
//                    pay(purchase, PlatformInfoXml.getPlatformInfo().getFid() + "202", "1");
//                    mHelper.consumeAsync(purchase, mConsumeFinishedListener);
//                } else if (sku1.equals(skus[7])) {
//                    pay(purchase, PlatformInfoXml.getPlatformInfo().getFid() + "201", "1");
//                    mHelper.consumeAsync(purchase, mConsumeFinishedListener);
//                } else if (sku1.equals(skus[8])) {
//                    pay(purchase, PlatformInfoXml.getPlatformInfo().getFid() + "203", "1");
//                    mHelper.consumeAsync(purchase, mConsumeFinishedListener);
//                }
            }
            if (result.isFailure()) {
                int response = result.getResponse();
                if(response==-1008) {

                }
                return;
            }




            if (purchase.getSku().equals(skus[0])) {
                pay(purchase, PlatformInfoXml.getPlatformInfo().getFid() + "101", "2");
                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
            } else if (purchase.getSku().equals(skus[1])) {
                pay(purchase, PlatformInfoXml.getPlatformInfo().getFid() + "102", "2");
                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
            } else if (purchase.getSku().equals(skus[2])) {
                pay(purchase, PlatformInfoXml.getPlatformInfo().getFid() + "103", "2");
                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
            } else if (purchase.getSku().equals(skus[3])) {
                pay(purchase, PlatformInfoXml.getPlatformInfo().getFid() + "002", "1");
                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
            } else if (purchase.getSku().equals(skus[4])) {
                pay(purchase, PlatformInfoXml.getPlatformInfo().getFid() + "001", "1");
                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
            } else if (purchase.getSku().equals(skus[5])) {
                pay(purchase, PlatformInfoXml.getPlatformInfo().getFid() + "003", "1");
                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
            } else if (purchase.getSku().equals(skus[6])) {
                pay(purchase, PlatformInfoXml.getPlatformInfo().getFid() + "202", "1");
                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
            } else if (purchase.getSku().equals(skus[7])) {
                pay(purchase, PlatformInfoXml.getPlatformInfo().getFid() + "201", "1");
                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
            } else if (purchase.getSku().equals(skus[8])) {
                pay(purchase, PlatformInfoXml.getPlatformInfo().getFid() + "203", "1");
                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
            }
        }
    };

    // Called when consumption is complete
    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, IabResult result) {
            if (result.isSuccess()) {
                updateUserInfo();
                if (purchase.getSku().equals(skus[0])) {
                    showMessage(getString(R.string.pay_bean300_success));
                    AppsflyerUtils.purchase300Bean();
                } else if (purchase.getSku().equals(skus[1])) {
                    showMessage(getString(R.string.pay_bean600_success));
                    AppsflyerUtils.purchase600Bean();
                } else if (purchase.getSku().equals(skus[2])) {
                    showMessage(getString(R.string.pay_bean1500_success));
                    AppsflyerUtils.purchase1500Bean();
                } else if (purchase.getSku().equals(skus[3])) {
                    showMessage(getString(R.string.pay_month1_success));
                    KochavaUtils.kochavaVipOnemonth();
//                    AppsflyerUtils.purchase1Month();
                } else if (purchase.getSku().equals(skus[4])) {
                    showMessage(getString(R.string.pay_month3_success));
                    KochavaUtils.kochavaVipthreemonths();
//                    AppsflyerUtils.purchase3Month();
                } else if (purchase.getSku().equals(skus[5])) {
                    showMessage(getString(R.string.pay_month12_success));
                    KochavaUtils.kochavaviponeyear();
//                    AppsflyerUtils.purchaseOneYear();
                } else if (purchase.getSku().equals(skus[6])) {
                    showMessage(getString(R.string.pay_standard_month1_success));

                    KochavaUtils.kochavaSvipOnemonth();
//                    AppsflyerUtils.purchaseStandard1Month();
                } else if (purchase.getSku().equals(skus[7])) {
                    showMessage(getString(R.string.pay_standard_months3_success));
                    KochavaUtils.kochavaSvipthreemonths();
//                    AppsflyerUtils.purchaseStandard3Month();
                } else if (purchase.getSku().equals(skus[8])) {
                    showMessage(getString(R.string.pay_standard_months12_success));
                    KochavaUtils.kochavaSviponeyear();
//                    AppsflyerUtils.purchaseStandardOneYear();
                }
            }
        }
    };

    private void updateUserInfo() {
        CommonRequestUtil.loadUserInfo(new CommonRequestUtil.OnLoadUserInfoListener() {
            @Override
            public void onSuccess(User user) {
                // 更新用户支付信息
                EventBus.getDefault().post(new UpdatePayInfoEvent());
            }

            @Override
            public void onFail() {
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mHelper != null)
            mHelper.dispose();
        mHelper = null;
    }

    private void showMessage(String message) {
        DialogUtil.showSingleBtnDialog(getSupportFragmentManager(), getString(R.string.pay_tip), message, getString(R.string.sure), true, null);
    }

    private void pay(final Purchase purchase, final String serviceid, final String payType) {
        if(purchase!=null){
            OkHttpUtils.post()
                    .url(IUrlConstant.GOOGLE_PAY)
                    .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                    .addParams("token", PlatformInfoXml.getToken())
                    .addParams("transactionId", purchase.getOrderId())
                    .addParams("payTime", purchase.getPurchaseTime() + "")
                    .addParams("clientTime", TimeUtil.getCurrentTime())
                    .addParams("userGoogleId", " ")
                    .addParams("payType", payType)
                    .addParams("googleId", purchase.getPackageName())
                    .addParams("serviceId", serviceid)
                    .addParams("userId", UserInfoXml.getUID())
                    .addParams("transactionToken", PayUtils.encryptPay(PlatformInfoXml.getPlatformInfo().getPid(), purchase.getOrderId()))
                    //后台验证支付和续费使用 具体key 跟后台协商
                    .addParams("purchaseToken", purchase.getToken())
                    .addParams("packageName", purchase.getPackageName())
                    .addParams("productId", purchase.getSku())
                    .build()
                    .execute(new PayCallback());
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

   /* private void payTest(final String serviceid, final String payType) {
        OkHttpUtils.post()
                .url(IUrlConstant.GOOGLE_PAY)
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .addParams("token", PlatformInfoXml.getToken())
                .addParams("transactionId", "GPA.1234-5678-9012-34567")
                .addParams("payTime", System.currentTimeMillis() + "")
                .addParams("clientTime", TimeUtil.getCurrentTime())
                .addParams("userGoogleId", "123")
                .addParams("googleId", "123")
                .addParams("payType", payType)
                .addParams("serviceId", serviceid)
                .addParams("userId", UserInfoXml.getUID())
                .addParams("transactionToken", PayUtils.encryptPay(PlatformInfoXml.getPlatformInfo().getPid(), "GPA.1234-5678-9012-34567"))
                .build()
                .execute(new PayCallback());
    }*/

    public class PayCallback extends StringCallback {

        @Override
        public void onError(Call call, Exception e, int id) {
            ToastUtil.showShortToast(BuyServiceActivity.this, getString(R.string.pay_fail) + e.toString());
        }

        @Override
        public void onResponse(String response, int id) {
            if (!TextUtils.isEmpty(response) && "success".equals(response)) {
                ToastUtil.showShortToast(BuyServiceActivity.this, getString(R.string.pay_success));
                if(fromTag.equals(IConfigConstant.PAY_TAG_FROM_CHAT)){
                    /**去除读信拦截**/
                    SharedPreferenceUtil.setBooleanValue(Utils.getContext(), "INTERCEPT_DIALOG", "DIALOG", false);

                    // 发送事件，刷新当前聊天记录的拦截信对话框
                    EventBus.getDefault().post(new RefreshChatDialogEvent());
                    }
            } else {
                ToastUtil.showShortToast(BuyServiceActivity.this, getString(R.string.pay_fail));
            }
        }
    }

    private boolean isAppInstalled(Context context, String packagename) {
        PackageInfo packageInfo;
        try {
            packageInfo = context.getPackageManager().getPackageInfo(packagename, 0);
        } catch (PackageManager.NameNotFoundException e) {
            packageInfo = null;
            e.printStackTrace();
        }
        return packageInfo != null;
    }

}












//package com.datelove.online.International.activity;
//
//import android.app.Activity;
//import android.content.Context;
//import android.content.Intent;
//import android.content.pm.PackageInfo;
//import android.content.pm.PackageManager;
//import android.os.Bundle;
//import android.os.Handler;
//import android.os.Message;
//import android.text.TextUtils;
//import android.view.View;
//import android.widget.Button;
//import android.widget.LinearLayout;
//
//import com.alibaba.fastjson.JSON;
//import com.datelove.online.International.R;
//import com.datelove.online.International.base.BaseTitleActivity;
//import com.datelove.online.International.bean.PayWay;
//import com.datelove.online.International.bean.User;
//import com.datelove.online.International.constant.IConfigConstant;
//import com.datelove.online.International.constant.IUrlConstant;
//import com.datelove.online.International.event.UpdatePayInfoEvent;
//import com.datelove.online.International.payUtil.IabHelper;
//import com.datelove.online.International.payUtil.IabResult;
//import com.datelove.online.International.payUtil.Purchase;
//import com.datelove.online.International.utils.AppsflyerUtils;
//import com.datelove.online.International.utils.CommonRequestUtil;
//import com.datelove.online.International.xml.PlatformInfoXml;
//import com.datelove.online.International.xml.UserInfoXml;
//import com.library.utils.DialogUtil;
//import com.library.utils.PayUtils;
//import com.library.utils.TimeUtil;
//import com.library.utils.ToastUtil;
//import com.library.utils.Util;
//import com.zhy.http.okhttp.OkHttpUtils;
//import com.zhy.http.okhttp.callback.Callback;
//import com.zhy.http.okhttp.callback.StringCallback;
//
//import org.greenrobot.eventbus.EventBus;
//
//import java.util.HashMap;
//import java.util.Map;
//
//import butterknife.BindView;
//import butterknife.ButterKnife;
//import okhttp3.Call;
//import okhttp3.Response;
//
///**
// * 支付页面
// * Modified by zhangdroid on 2017/03/09.
// */
//public class BuyServiceActivity extends BaseTitleActivity implements View.OnClickListener {
//    @BindView(R.id.ll_month)
//    LinearLayout ll_month;
//    @BindView(R.id.ll_bean)
//    LinearLayout ll_bean;
//    @BindView(R.id.month1)
//    LinearLayout month1;
//    @BindView(R.id.month3)
//    LinearLayout month3;
//    @BindView(R.id.month12)
//    LinearLayout month12;
//    @BindView(R.id.month1_payall)
//    Button month1_payall;
//    @BindView(R.id.month3_payall)
//    Button month3_payall;
//    @BindView(R.id.month12_payall)
//    Button month12_payall;
//    @BindView(R.id.bean1)
//    Button bean1;
//    @BindView(R.id.bean2)
//    Button bean2;
//    @BindView(R.id.bean3)
//    Button bean3;
//    @BindView(R.id.bean3_pay)
//    LinearLayout bean3Pay;
//    @BindView(R.id.bean2_pay)
//    LinearLayout bean2Pay;
//    @BindView(R.id.bean1_pay)
//    LinearLayout bean1Pay;
//    @BindView(R.id.ll_intorduce)
//    LinearLayout llIntorduce;
//
////    @BindView(R.id.beans)
////    TextView beans;// 豆币数
//
//    private String[] skus = {IConfigConstant.SKU_BEAN300, IConfigConstant.SKU_BEAN600, IConfigConstant.SKU_BEAN1500,
//            IConfigConstant.SKU_MONTH1, IConfigConstant.SKU_MONTH3, IConfigConstant.SKU_MONTH12,
//            IConfigConstant.SKU_Standard_MONTH1, IConfigConstant.SKU_Standard_MONTH3, IConfigConstant.SKU_Standard_MONTH12};
//
//    private static final String INTENT_KEY_TYPE = "intent_key_type";
//    private static final String INTENT_KEY_FROM_TAG = "intent_key_from_tag";
//    private static final String INTENT_KEY_SERVICE_WAY = "intent_key_service_way";
//    public static final String INTENT_FROM_INTERCEPT = "intent_key_intercept";
//    public static final String INTENT_FROM_BEAN = "intent_key_bean";
//    public static final String INTENT_FROM_MONTH = "intent_key_month";
//
//    private IabHelper mHelper;
//    private static final int RC_REQUEST = 10001;
//    private boolean iap_is_ok = false;
//    private String sku1;
//
//    @Override
//    protected int getLayoutResId() {
//        return R.layout.activity_buy_service;
//    }
//
//    @Override
//    protected String getCenterTitle() {
//        return getString(R.string.recharge);
//    }
//
//    /**
//     * 跳转支付页面
//     *
//     * @param from       Activity对象
//     * @param sourceTag  支付入口
//     * @param serviceWay 支付类型
//     */
//    public static void toBuyServiceActivity(Activity from, String type, String sourceTag, String serviceWay) {
//        Map<String, String> map = new HashMap<String, String>();
//        map.put(INTENT_KEY_TYPE, type);
//        map.put(INTENT_KEY_FROM_TAG, sourceTag);
//        map.put(INTENT_KEY_SERVICE_WAY, serviceWay);
//        Util.gotoActivity(from, BuyServiceActivity.class, false, map);
//    }
//
//    @Override
//    protected void initViewsAndVariables() {
//        String type = INTENT_FROM_INTERCEPT;
//        String fromTag = "";
//        String serviceWay = "-1";
//        Intent intent = getIntent();
//        if (intent != null) {
//            type = intent.getStringExtra(INTENT_KEY_TYPE);
//            fromTag = intent.getStringExtra(INTENT_KEY_FROM_TAG);
//            serviceWay = intent.getStringExtra(INTENT_KEY_SERVICE_WAY);
//        }
//        switch (type) {
//            case INTENT_FROM_INTERCEPT:// 支付拦截
//                ll_bean.setVisibility(View.VISIBLE);
//                ll_month.setVisibility(View.VISIBLE);
////                beans.setVisibility(View.GONE);
//                break;
//
//            case INTENT_FROM_BEAN:// 豆币
//                ll_bean.setVisibility(View.VISIBLE);
//                ll_month.setVisibility(View.GONE);
//                llIntorduce.setVisibility(View.GONE);
////                beans.setVisibility(View.VISIBLE);
////                beans.setText(getString(R.string.beancounts, String.valueOf(UserInfoXml.getBeanCount())));
//                break;
//
//            case INTENT_FROM_MONTH:// 包月
//                ll_bean.setVisibility(View.GONE);
//                ll_month.setVisibility(View.VISIBLE);
//                llIntorduce.setVisibility(View.VISIBLE);
////                beans.setVisibility(View.GONE);
//                break;
//        }
//        // 价格本地化
//        setPriceByCountry();
//        // 支付统计和支付方式
//        getPayWay(fromTag, serviceWay);
//
//        mHelper = new IabHelper(BuyServiceActivity.this, IConfigConstant.GOOGLE_APP_KEY);
//        mHelper.enableDebugLogging(false);
//        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
//            public void onIabSetupFinished(IabResult result) {
//                if (!result.isSuccess()) {
//                    return;
//                }
//                iap_is_ok = true;
//            }
//        });
//    }
//
//    private void setPriceByCountry() {
//        String mo1 = getResources().getString(R.string.month1);
//        String mo3 = getResources().getString(R.string.month3);
//        String mo12 = getResources().getString(R.string.month12);
//        String b1 = getResources().getString(R.string.bean1);
//        String b2 = getResources().getString(R.string.bean2);
//        String b3 = getResources().getString(R.string.bean3);
//        String mo1All = getResources().getString(R.string.month1_pay);
//        String mo3All = getResources().getString(R.string.month2_pay);
//        String mo12All = getResources().getString(R.string.month3_pay);
//        switch (PlatformInfoXml.getCountry()) {
//            case "America":
////                month1.setText(String.format(mo1, "$16"));
////                month3.setText(String.format(mo3, "$10.6"));
////                month12.setText(String.format(mo12, "$6.6"));
//                month1_payall.setText(String.format(mo1All, "$16"));
//                month3_payall.setText(String.format(mo3All, "$32"));
//                month12_payall.setText(String.format(mo12All, "$79"));
//                bean1.setText(String.format(b1, "$9.99"));
//                bean2.setText(String.format(b2, "$19.99"));
//                bean3.setText(String.format(b3, "$49.99"));
//                break;
//            case "Australia":
////                month1.setText(String.format(mo1, "$21.99"));
////                month3.setText(String.format(mo3, "$14.6"));
////                month12.setText(String.format(mo12, "$9.2"));
//                month1_payall.setText(String.format(mo1All, "$21.99"));
//                month3_payall.setText(String.format(mo3All, "$43.99"));
//                month12_payall.setText(String.format(mo12All, "$109.99"));
//                bean1.setText(String.format(b1, "$14.99"));
//                bean2.setText(String.format(b2, "$26.99"));
//                bean3.setText(String.format(b3, "$65.99"));
//                break;
//            case "India":
////                month1.setText(String.format(mo1, "Rs1,100"));
////                month3.setText(String.format(mo3, "Rs733"));
////                month12.setText(String.format(mo12, "Rs450"));
//                month1_payall.setText(String.format(mo1All, "Rs1,100"));
//                month3_payall.setText(String.format(mo3All, "Rs2,200"));
//                month12_payall.setText(String.format(mo12All, "Rs5,400"));
//                bean1.setText(String.format(b1, "Rs620"));
//                bean2.setText(String.format(b2, "Rs1200"));
//                bean3.setText(String.format(b3, "Rs3100"));
//                break;
//            case "Indonesia":
////                month1.setText(String.format(mo1, "Rp 219,000"));
////                month3.setText(String.format(mo3, "Rp 143000"));
////                month12.setText(String.format(mo12, "Rp 88723"));
//                month1_payall.setText(String.format(mo1All, "Rp 219,000"));
//                month3_payall.setText(String.format(mo3All, "Rp 429,000"));
//                month12_payall.setText(String.format(mo12All, "Rp 1,064,683"));
//                bean1.setText(String.format(b1, "Rp 149000"));
//                bean2.setText(String.format(b2, "Rp 266000"));
//                bean3.setText(String.format(b3, "Rp 666000"));
//                break;
//            case "United Kingdom":
////                month1.setText(String.format(mo1, "£15.49"));
////                month3.setText(String.format(mo3, "£10.49"));
////                month12.setText(String.format(mo12, "£6.41"));
//                month1_payall.setText(String.format(mo1All, "£15.49"));
//                month3_payall.setText(String.format(mo3All, "£31.49"));
//                month12_payall.setText(String.format(mo12All, "£76.99"));
//                bean1.setText(String.format(b1, "£7.99"));
//                bean2.setText(String.format(b2, "£17.49"));
//                bean3.setText(String.format(b3, "£44.99"));
//                break;
//            case "Canada":
////                month1.setText(String.format(mo1, "$21.99"));
////                month3.setText(String.format(mo3, "$14.33"));
////                month12.setText(String.format(mo12, "$8.74"));
//                month1_payall.setText(String.format(mo1All, "$21.99"));
//                month3_payall.setText(String.format(mo3All, "$42.99"));
//                month12_payall.setText(String.format(mo12All, "$104.99"));
//                bean1.setText(String.format(b1, "$13.99"));
//                bean2.setText(String.format(b2, "$27.99"));
//                bean3.setText(String.format(b3, "$69.99"));
//                break;
//            case "New Zealand":
////                month1.setText(String.format(mo1, "$26.99"));
////                month3.setText(String.format(mo3, "$18.33"));
////                month12.setText(String.format(mo12, "$10.83"));
//                month1_payall.setText(String.format(mo1All, "$26.99"));
//                month3_payall.setText(String.format(mo3All, "$54.99"));
//                month12_payall.setText(String.format(mo12All, "$129.99"));
//                bean1.setText(String.format(b1, "$14.99"));
//                bean2.setText(String.format(b2, "$29.99"));
//                bean3.setText(String.format(b3, "$74.99"));
//                break;
//            case "Ireland":
////                month1.setText(String.format(mo1, "18.99€"));
////                month3.setText(String.format(mo3, "12.66€"));
////                month12.setText(String.format(mo12, "7.9€"));
//                month1_payall.setText(String.format(mo1All, "18.99€"));
//                month3_payall.setText(String.format(mo3All, "37.99€"));
//                month12_payall.setText(String.format(mo12All, "94.99€"));
//                bean1.setText(String.format(b1, "9.99€"));
//                bean2.setText(String.format(b2, "19.99€"));
//                bean3.setText(String.format(b3, "49.99€"));
//                break;
//            case "South Africa":
////                month1.setText(String.format(mo1, "R219.99"));
////                month3.setText(String.format(mo3, "R146.66"));
////                month12.setText(String.format(mo12, "R91.66"));
//                month1_payall.setText(String.format(mo1All, "R219.99"));
//                month3_payall.setText(String.format(mo3All, "R439.99"));
//                month12_payall.setText(String.format(mo12All, "R1099.99"));
//                bean1.setText(String.format(b1, "R149.99"));
//                bean2.setText(String.format(b2, "R299.99"));
//                bean3.setText(String.format(b3, "R799.99"));
//                break;
//            case "Singapore":
////                month1.setText(String.format(mo1, "S$22.99"));
////                month3.setText(String.format(mo3, "S$15.33"));
////                month12.setText(String.format(mo12, "S$9.16"));
//                month1_payall.setText(String.format(mo1All, "S$22.99"));
//                month3_payall.setText(String.format(mo3All, "S$45.99"));
//                month12_payall.setText(String.format(mo12All, "S$109.99"));
//                bean1.setText(String.format(b1, "S$14.99"));
//                bean2.setText(String.format(b2, "S$28.99"));
//                bean3.setText(String.format(b3, "S$68.99"));
//                break;
//            case "Pakistan":
////                month1.setText(String.format(mo1, "Rs1,700"));
////                month3.setText(String.format(mo3, "Rs1116"));
////                month12.setText(String.format(mo12, "Rs691"));
//                month1_payall.setText(String.format(mo1All, "Rs1,700"));
//                month3_payall.setText(String.format(mo3All, "Rs3,350"));
//                month12_payall.setText(String.format(mo12All, "Rs8,300"));
//                bean1.setText(String.format(b1, "Rs1,000"));
//                bean2.setText(String.format(b2, "Rs2,000"));
//                bean3.setText(String.format(b3, "Rs5,000"));
//                break;
//            case "Philippines":
////                month1.setText(String.format(mo1, "₱795"));
////                month3.setText(String.format(mo3, "₱533"));
////                month12.setText(String.format(mo12, "₱329"));
//                month1_payall.setText(String.format(mo1All, "₱795"));
//                month3_payall.setText(String.format(mo3All, "₱1600"));
//                month12_payall.setText(String.format(mo12All, "₱3950"));
//                bean1.setText(String.format(b1, "₱499"));
//                bean2.setText(String.format(b2, "₱999"));
//                bean3.setText(String.format(b3, "₱2490"));
//                break;
//            case "Hong Kong":
////                month1.setText(String.format(mo1, "HK$128.00"));
////                month3.setText(String.format(mo3, "HK$82.66"));
////                month12.setText(String.format(mo12, "HK$50.75"));
//                month1_payall.setText(String.format(mo1All, "HK$128.00"));
//                month3_payall.setText(String.format(mo3All, "HK$248"));
//                month12_payall.setText(String.format(mo12All, "HK$609"));
//                bean1.setText(String.format(b1, "HK$78"));
//                bean2.setText(String.format(b2, "HK$158"));
//                bean3.setText(String.format(b3, "HK$398"));
//                break;
//            default:
////                month1.setText(String.format(mo1, "$16"));
////                month3.setText(String.format(mo3, "$10.6"));
////                month12.setText(String.format(mo12, "$6.6"));
//                month1_payall.setText(String.format(mo1All, "$16"));
//                month3_payall.setText(String.format(mo3All, "$32"));
//                month12_payall.setText(String.format(mo12All, "$79"));
//                bean1.setText(String.format(b1, "$9.99"));
//                bean2.setText(String.format(b2, "$19.99"));
//                bean3.setText(String.format(b3, "$49.99"));
//                break;
//        }
//    }
//
//    private void getPayWay(String fromTag, String serviceWay) {
//        OkHttpUtils.post()
//                .url(IUrlConstant.URL_PAY_WAY)
//                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
//                .addHeader("token", PlatformInfoXml.getToken())
//                .addParams("fromTag", fromTag)
//                .addParams("serviceWay", serviceWay)
//                .build()
//                .execute(new Callback<PayWay>() {
//                    @Override
//                    public PayWay parseNetworkResponse(Response response, int id) throws Exception {
//                        String resultJson = response.body().string();
//                        if (!TextUtils.isEmpty(resultJson)) {
//                            return JSON.parseObject(resultJson, PayWay.class);
//                        }
//                        return null;
//                    }
//
//                    @Override
//                    public void onError(Call call, Exception e, int id) {
//                    }
//
//                    @Override
//                    public void onResponse(PayWay response, int id) {
//                    }
//                });
//    }
//
//    @Override
//    protected void addListeners() {
//        month1.setOnClickListener(this);
//        month3.setOnClickListener(this);
//        month12.setOnClickListener(this);
//        bean1.setOnClickListener(this);
//        bean2.setOnClickListener(this);
//        bean3.setOnClickListener(this);
//        bean1Pay.setOnClickListener(this);
//        bean2Pay.setOnClickListener(this);
//        bean3Pay.setOnClickListener(this);
//        month1_payall.setOnClickListener(this);
//        month3_payall.setOnClickListener(this);
//        month12_payall.setOnClickListener(this);
//    }
//
//    public void onClick(View view) {
//        switch (view.getId()) {
//            case R.id.bean1_pay:
////                payTest(PlatformInfoXml.getPlatformInfo().getFid() + "101", "1");
////                AppsflyerUtils.click300Bean();
////                iapHandler.sendEmptyMessage(0);
//                AppsflyerUtils.clickStandard1Month();
//                iapHandler.sendEmptyMessage(6);
//                break;
//            case R.id.bean2_pay:
////                AppsflyerUtils.click600Bean();
////                iapHandler.sendEmptyMessage(1);
//                AppsflyerUtils.clickStandard3Month();
//                iapHandler.sendEmptyMessage(7);
//                break;
//            case R.id.bean3_pay:
////                AppsflyerUtils.click1500Bean();
////                iapHandler.sendEmptyMessage(2);
//                AppsflyerUtils.clickStandardOneYear();
//                iapHandler.sendEmptyMessage(8);
//                break;
//            case R.id.bean1:
////                payTest(PlatformInfoXml.getPlatformInfo().getFid() + "101", "1");
////                AppsflyerUtils.click300Bean();
////                iapHandler.sendEmptyMessage(0);
//                AppsflyerUtils.clickStandard1Month();
//                iapHandler.sendEmptyMessage(6);
//                break;
//            case R.id.bean2:
////                AppsflyerUtils.click600Bean();
////                iapHandler.sendEmptyMessage(1);
//                AppsflyerUtils.clickStandard3Month();
//                iapHandler.sendEmptyMessage(7);
//                break;
//            case R.id.bean3:
////                AppsflyerUtils.click1500Bean();
////                iapHandler.sendEmptyMessage(2);
//                AppsflyerUtils.clickStandardOneYear();
//                iapHandler.sendEmptyMessage(8);
//                break;
//            case R.id.month1:
//                AppsflyerUtils.click1Month();
//                iapHandler.sendEmptyMessage(3);
//                break;
//            case R.id.month3:
//                AppsflyerUtils.click3Month();
//                iapHandler.sendEmptyMessage(4);
//                break;
//            case R.id.month12:
//                AppsflyerUtils.clickOneYear();
//                iapHandler.sendEmptyMessage(5);
//                break;
//            case R.id.month1_payall:
//                AppsflyerUtils.click1Month();
//                iapHandler.sendEmptyMessage(3);
//                break;
//            case R.id.month3_payall:
//                AppsflyerUtils.click3Month();
//                iapHandler.sendEmptyMessage(4);
//                break;
//            case R.id.month12_payall:
//                AppsflyerUtils.clickOneYear();
//                iapHandler.sendEmptyMessage(5);
//                break;
//        }
//    }
//
//    /**
//     * 根据SKU购买商品
//     *
//     * @param sku 商品对应的SKU
//     */
//    private void purchase(String sku) {
//        if (isAppInstalled(BuyServiceActivity.this, "com.android.vending") && mHelper != null && iap_is_ok) {
//            mHelper.handleActivityResult(mHelper.mRequestCode, Activity.RESULT_CANCELED, null);
//             sku1 = sku;
//            mHelper.launchPurchaseFlow(BuyServiceActivity.this, sku, RC_REQUEST, mPurchaseFinishedListener);
//        } else {
//            showMessage(getString(R.string.pay_tip_message));
//        }
//    }
//
//    Handler iapHandler = new Handler() {
//        public void handleMessage(Message msg) {
//            switch (msg.what) {
//                case 0:
//                    purchase(skus[0]);
//                    break;
//                case 1:
//                    purchase(skus[1]);
//                    break;
//                case 2:
//                    purchase(skus[2]);
//                    break;
//                case 3:
//                    purchase(skus[3]);
//                    break;
//                case 4:
//                    purchase(skus[4]);
//                    break;
//                case 5:
//                    purchase(skus[5]);
//                    break;
//                case 6:
//                    purchase(skus[6]);
//                    break;
//                case 7:
//                    purchase(skus[7]);
//                    break;
//                case 8:
//                    purchase(skus[8]);
//                    break;
//            }
//        }
//    };
//
//    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
//        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
//            if(purchase==null){
//
////                if (sku1.equals(skus[0])) {
////                    pay(purchase, PlatformInfoXml.getPlatformInfo().getFid() + "101", "2");
////                    mHelper.consumeAsync(purchase, mConsumeFinishedListener);
////                } else if (sku1.equals(skus[1])) {
////                    pay(purchase, PlatformInfoXml.getPlatformInfo().getFid() + "102", "2");
////                    mHelper.consumeAsync(purchase, mConsumeFinishedListener);
////                } else if (sku1.equals(skus[2])) {
////                    pay(purchase, PlatformInfoXml.getPlatformInfo().getFid() + "103", "2");
////                    mHelper.consumeAsync(purchase, mConsumeFinishedListener);
////                } else if (sku1.equals(skus[3])) {
////                    pay(purchase, PlatformInfoXml.getPlatformInfo().getFid() + "002", "1");
////                    mHelper.consumeAsync(purchase, mConsumeFinishedListener);
////                } else if (sku1.equals(skus[4])) {
////                    pay(purchase, PlatformInfoXml.getPlatformInfo().getFid() + "001", "1");
////                    mHelper.consumeAsync(purchase, mConsumeFinishedListener);
////                } else if (sku1.equals(skus[5])) {
////                    pay(purchase, PlatformInfoXml.getPlatformInfo().getFid() + "003", "1");
////                    mHelper.consumeAsync(purchase, mConsumeFinishedListener);
////                } else if (sku1.equals(skus[6])) {
////                    pay(purchase, PlatformInfoXml.getPlatformInfo().getFid() + "202", "1");
////                    mHelper.consumeAsync(purchase, mConsumeFinishedListener);
////                } else if (sku1.equals(skus[7])) {
////                    pay(purchase, PlatformInfoXml.getPlatformInfo().getFid() + "201", "1");
////                    mHelper.consumeAsync(purchase, mConsumeFinishedListener);
////                } else if (sku1.equals(skus[8])) {
////                    pay(purchase, PlatformInfoXml.getPlatformInfo().getFid() + "203", "1");
////                    mHelper.consumeAsync(purchase, mConsumeFinishedListener);
////                }
//            }
//            if (result.isFailure()) {
//                int response = result.getResponse();
//                if(response==-1008) {
//
//                }
//                return;
//            }
//
//
//
//            if (purchase.getSku().equals(skus[0])) {
//                pay(purchase, PlatformInfoXml.getPlatformInfo().getFid() + "101", "2");
//                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
//            } else if (purchase.getSku().equals(skus[1])) {
//                pay(purchase, PlatformInfoXml.getPlatformInfo().getFid() + "102", "2");
//                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
//            } else if (purchase.getSku().equals(skus[2])) {
//                pay(purchase, PlatformInfoXml.getPlatformInfo().getFid() + "103", "2");
//                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
//            } else if (purchase.getSku().equals(skus[3])) {
//                pay(purchase, PlatformInfoXml.getPlatformInfo().getFid() + "002", "1");
//                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
//            } else if (purchase.getSku().equals(skus[4])) {
//                pay(purchase, PlatformInfoXml.getPlatformInfo().getFid() + "001", "1");
//                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
//            } else if (purchase.getSku().equals(skus[5])) {
//                pay(purchase, PlatformInfoXml.getPlatformInfo().getFid() + "003", "1");
//                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
//            } else if (purchase.getSku().equals(skus[6])) {
//                pay(purchase, PlatformInfoXml.getPlatformInfo().getFid() + "202", "1");
//                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
//            } else if (purchase.getSku().equals(skus[7])) {
//                pay(purchase, PlatformInfoXml.getPlatformInfo().getFid() + "201", "1");
//                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
//            } else if (purchase.getSku().equals(skus[8])) {
//                pay(purchase, PlatformInfoXml.getPlatformInfo().getFid() + "203", "1");
//                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
//            }
//        }
//    };
//
//    // Called when consumption is complete
//    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
//        public void onConsumeFinished(Purchase purchase, IabResult result) {
//            if (result.isSuccess()) {
//                updateUserInfo();
//                if (purchase.getSku().equals(skus[0])) {
//                    showMessage(getString(R.string.pay_bean300_success));
//                    AppsflyerUtils.purchase300Bean();
//                } else if (purchase.getSku().equals(skus[1])) {
//                    showMessage(getString(R.string.pay_bean600_success));
//                    AppsflyerUtils.purchase600Bean();
//                } else if (purchase.getSku().equals(skus[2])) {
//                    showMessage(getString(R.string.pay_bean1500_success));
//                    AppsflyerUtils.purchase1500Bean();
//                } else if (purchase.getSku().equals(skus[3])) {
//                    showMessage(getString(R.string.pay_month1_success));
//                    AppsflyerUtils.purchase1Month();
//                } else if (purchase.getSku().equals(skus[4])) {
//                    showMessage(getString(R.string.pay_month3_success));
//                    AppsflyerUtils.purchase3Month();
//                } else if (purchase.getSku().equals(skus[5])) {
//                    showMessage(getString(R.string.pay_month12_success));
//                    AppsflyerUtils.purchaseOneYear();
//                } else if (purchase.getSku().equals(skus[6])) {
//                    showMessage(getString(R.string.pay_standard_month1_success));
//                    AppsflyerUtils.purchaseStandard1Month();
//                } else if (purchase.getSku().equals(skus[7])) {
//                    showMessage(getString(R.string.pay_standard_months3_success));
//                    AppsflyerUtils.purchaseStandard3Month();
//                } else if (purchase.getSku().equals(skus[8])) {
//                    showMessage(getString(R.string.pay_standard_months12_success));
//                    AppsflyerUtils.purchaseStandardOneYear();
//                }
//            }
//        }
//    };
//
//    private void updateUserInfo() {
//        CommonRequestUtil.loadUserInfo(new CommonRequestUtil.OnLoadUserInfoListener() {
//            @Override
//            public void onSuccess(User user) {
//                // 更新用户支付信息
//                EventBus.getDefault().post(new UpdatePayInfoEvent());
//            }
//
//            @Override
//            public void onFail() {
//            }
//        });
//    }
//
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
//            super.onActivityResult(requestCode, resultCode, data);
//        }
//    }
//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        if (mHelper != null)
//            mHelper.dispose();
//        mHelper = null;
//    }
//
//    private void showMessage(String message) {
//        DialogUtil.showSingleBtnDialog(getSupportFragmentManager(), getString(R.string.pay_tip), message, getString(R.string.sure), true, null);
//    }
//
//    private void pay(final Purchase purchase, final String serviceid, final String payType) {
//        if(purchase!=null){
//            OkHttpUtils.post()
//                    .url(IUrlConstant.GOOGLE_PAY)
//                    .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
//                    .addParams("token", PlatformInfoXml.getToken())
//                    .addParams("transactionId", purchase.getOrderId())
//                    .addParams("payTime", purchase.getPurchaseTime() + "")
//                    .addParams("clientTime", TimeUtil.getCurrentTime())
//                    .addParams("userGoogleId", " ")
//                    .addParams("payType", payType)
//                    .addParams("googleId", purchase.getPackageName())
//                    .addParams("serviceId", serviceid)
//                    .addParams("userId", UserInfoXml.getUID())
//                    .addParams("transactionToken", PayUtils.encryptPay(PlatformInfoXml.getPlatformInfo().getPid(), purchase.getOrderId()))
//                    //后台验证支付和续费使用 具体key 跟后台协商
//                    .addParams("purchaseToken", purchase.getToken())
//                    .addParams("packageName", purchase.getPackageName())
//                    .addParams("productId", purchase.getSku())
//                    .build()
//                    .execute(new PayCallback());
//        }
//
//    }
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        // TODO: add setContentView(...) invocation
//        ButterKnife.bind(this);
//    }
//
//   /* private void payTest(final String serviceid, final String payType) {
//        OkHttpUtils.post()
//                .url(IUrlConstant.GOOGLE_PAY)
//                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
//                .addParams("token", PlatformInfoXml.getToken())
//                .addParams("transactionId", "GPA.1234-5678-9012-34567")
//                .addParams("payTime", System.currentTimeMillis() + "")
//                .addParams("clientTime", TimeUtil.getCurrentTime())
//                .addParams("userGoogleId", "123")
//                .addParams("googleId", "123")
//                .addParams("payType", payType)
//                .addParams("serviceId", serviceid)
//                .addParams("userId", UserInfoXml.getUID())
//                .addParams("transactionToken", PayUtils.encryptPay(PlatformInfoXml.getPlatformInfo().getPid(), "GPA.1234-5678-9012-34567"))
//                .build()
//                .execute(new PayCallback());
//    }*/
//
//    public class PayCallback extends StringCallback {
//
//        @Override
//        public void onError(Call call, Exception e, int id) {
//            ToastUtil.showShortToast(BuyServiceActivity.this, getString(R.string.pay_fail) + e.toString());
//        }
//
//        @Override
//        public void onResponse(String response, int id) {
//            if (!TextUtils.isEmpty(response) && "success".equals(response)) {
//                ToastUtil.showShortToast(BuyServiceActivity.this, getString(R.string.pay_success));
//            } else {
//                ToastUtil.showShortToast(BuyServiceActivity.this, getString(R.string.pay_fail));
//            }
//        }
//    }
//
//    private boolean isAppInstalled(Context context, String packagename) {
//        PackageInfo packageInfo;
//        try {
//            packageInfo = context.getPackageManager().getPackageInfo(packagename, 0);
//        } catch (PackageManager.NameNotFoundException e) {
//            packageInfo = null;
//            e.printStackTrace();
//        }
//        return packageInfo != null;
//    }
//
//}
