package com.datelove.online.International.adapter;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;

import com.datelove.online.International.R;
import com.datelove.online.International.activity.BuyServiceActivity;
import com.datelove.online.International.base.BaseApplication;
import com.datelove.online.International.bean.FateUser;
import com.datelove.online.International.bean.Image;
import com.datelove.online.International.bean.UserBase;
import com.datelove.online.International.constant.IConfigConstant;
import com.datelove.online.International.haoping.ActionSheetDialog;
import com.datelove.online.International.haoping.OnOperItemClickL;
import com.datelove.online.International.utils.ParamsUtils;
import com.datelove.online.International.utils.Utils;
import com.library.adapter.abslistview.BaseAdapterHelper;
import com.library.adapter.abslistview.CommonAbsListViewAdapter;
import com.library.imageloader.ImageLoader;
import com.library.imageloader.ImageLoaderUtil;
import com.library.utils.HeightUtils;
import com.library.utils.SharedPreferenceUtil;
import com.library.utils.Util;

import java.util.List;

/**
 * 缘分页左右滑动适配器
 * Created by zhangdroid on 2016/8/10.
 */
public class SwipeFlingViewAdapter extends CommonAbsListViewAdapter<FateUser> {

    private int count;
    private Context mContext;
    private ImageView ivAvatar;

    public SwipeFlingViewAdapter(Context context, int layoutResId) {
        this(context, layoutResId, null);
        this.mContext=context;

    }

    public SwipeFlingViewAdapter(Context context, int layoutResId, List<FateUser> list) {
        super(context, layoutResId, list);
    }

    @Override
    protected void convert(int position, BaseAdapterHelper helper, FateUser bean) {
        if (bean != null) {
                UserBase userBase = bean.getUserBaseEnglish();
                if (userBase != null) {
                     ivAvatar = (ImageView) helper.getView(R.id.item_swipefling_avatar);
                    Image image = userBase.getImage();
                    if (image != null) {
                        String imgUrl = image.getThumbnailUrlM();
                        if (!TextUtils.isEmpty(imgUrl)) {
//                        Utils.display(mContext,ivAvatar, imgUrl);
                            ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(imgUrl).imageView(ivAvatar)
                                    .placeHolder(IConfigConstant.COMMON_DEFAULT_AVATAR).build());
                        } else {
                            ivAvatar.setImageResource(IConfigConstant.COMMON_DEFAULT_AVATAR);
                        }
                    } else {
                        ivAvatar.setImageResource(IConfigConstant.COMMON_DEFAULT_AVATAR);
                    }

                    // 用户姓名/年龄
                    String nickname = userBase.getNickName();
                    int age = userBase.getAge();
                    if (age < 18) {
                        age = 18;
                    }
                    StringBuilder nickname_age = new StringBuilder();
                    if (!TextUtils.isEmpty(nickname)) {
                        nickname_age.append(nickname)
                                .append("，");
                    }
                    nickname_age.append(age);
//                        .append(mContext.getString(R.string.years));
                    helper.setText(R.id.item_swipefling_nickname_age, nickname_age.toString());

                    // 职业/身高/收入
                    StringBuilder profile = new StringBuilder();
                    String occupation = ParamsUtils.getWorkMap().get(String.valueOf(userBase.getOccupation()));
                    if (!TextUtils.isEmpty(occupation)) {
                        profile.append(occupation)
                                .append("，");
                    } else {
                        profile.append(mContext.getResources().getString(R.string.other))
                                .append("，");
                    }
                    String heightCm = userBase.getHeightCm();
                    if (!TextUtils.isEmpty(heightCm)) {
                        profile.append(HeightUtils.getInchCmByCm(heightCm))
                                .append("\n");
                    }
                    String income = ParamsUtils.getIncomeMap().get(String.valueOf(userBase.getIncome()));
                    if (!TextUtils.isEmpty(income)) {
                        profile.append(income);
                    }
                    helper.setText(R.id.item_swipefling_profile, profile.toString());
                }
            }

//        }
    }


}
