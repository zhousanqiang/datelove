package com.datelove.online.International.utils;

import android.text.TextUtils;

import com.alibaba.fastjson.JSON;
import com.datelove.online.International.bean.ParamsInit;
import com.datelove.online.International.xml.UserInfoXml;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * 数据字典工具类
 * Created by jzrh on 2016/6/29.
 */
public class ParamsUtils {

    public static ParamsInit getParamsInit() {
        if (!TextUtils.isEmpty(UserInfoXml.getParamsInit())) {
            return JSON.parseObject(UserInfoXml.getParamsInit(), ParamsInit.class);
        } else {
            return new ParamsInit();
        }
    }
//


//    public static ParamsInit2 getParamsInit2() {
//        if (!TextUtils.isEmpty(UserInfoXml.getParamsInit())) {
//            return JSON.parseObject(UserInfoXml.getParamsInit(), ParamsInit2.class);
//        } else {
//            return new ParamsInit2();
//        }
//    }
//    /**
//     * 获得学历Map
//     *
//     * @return
//     */
//    public static Map<String, String> getEducationMap1() {
//        return getParamsInit2().getEducationList();
//    }

//
    /**
     * 将map的key排序后，返回map的value生成的list
     *
     * @param map
     */
    private static List<String> getMapValue(Map<String, String> map) {
        if(map!=null){
            Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
            // 对key排序
            List<String> keyList = new ArrayList<String>();
            while (iterator.hasNext()) {
                keyList.add(iterator.next().getKey());
            }
            Collections.sort(keyList);
            List<String> list = new ArrayList<String>();
            for (int i = 0; i < keyList.size(); i++) {
                list.add(map.get(keyList.get(i)));
            }
            return list;
        }
        return null;

    }

    /**
     * 根据map和value获得对应的key
     *
     * @param map
     * @param value
     */
    private static String getMapKeyByValue(Map<String, String> map, String value) {
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, String> entry = iterator.next();
            if (entry.getValue().equals(value)) {
                return entry.getKey();
            }
        }
        return null;
    }

    /**
     * 获得默认城市
     */
    public static String getDefaultCity() {
        return getParamsInit().getDefaultCity();
    }

    /**
     * 获得地区Map
     *
     * @return
     */
    public static Map<String, String> getProvinceMap() {
        return getParamsInit().getDistractMap2();
    }

    /**
     * 获得地区List
     *
     * @return
     */
    public static List<String> getProvinceMapValue() {
        return getMapValue(getProvinceMap());
    }

    /**
     * 根据value获得key(地区)
     *
     * @param value
     * @return
     */
    public static String getProvinceMapKeyByValue(String value) {
        return getMapKeyByValue(getProvinceMap(), value);
    }

    /**
     * 获得星座Map
     *
     * @return
     */
    public static Map<String, String> getConstellationMap() {
        return getParamsInit().getConstellationMap2();
    }

    /**
     * 获得星座List
     *
     * @return
     */
    public static List<String> getConstellationMapValue() {
        return getMapValue(getConstellationMap());
    }

    /**
     * 根据value获得key(星座)
     *
     * @param value
     * @return
     */
    public static String getConstellationMapKeyByValue(String value) {
        return getMapKeyByValue(getConstellationMap(), value);
    }

    /**
     * 获得工作Map
     *
     * @return
     */
    public static Map<String, String> getWorkMap() {
        return getParamsInit().getWorkMap2();
    }

    /**
     * 获得工作List
     *
     * @return
     */
    public static List<String> getWorkMapValue() {
        return getMapValue(getWorkMap());
    }

    /**
     * 根据value获得key(工作)
     *
     * @param value
     * @return
     */
    public static String getWorkMapKeyByValue(String value) {
        return getMapKeyByValue(getWorkMap(), value);
    }

    /**
     * 获得兴趣Map
     *
     * @return
     */
    public static Map<String, String> getInterestMap() {
        return getParamsInit().getInterestMap2();
    }

    /**
     * 获得兴趣List
     *
     * @return
     */
    public static List<String> getInterestMapValue() {
        return getMapValue(getInterestMap());
    }

    /**
     * 根据value获得key(兴趣)
     *
     * @param value
     * @return
     */
    public static String getInterestMapKeyByValue(String value) {
        return getMapKeyByValue(getInterestMap(), value);
    }

    /**
     * 获得收入Map
     *
     * @return
     */
    public static Map<String, String> getIncomeMap() {
        return getParamsInit().getWorkMoneyMap2();
    }

    /**
     * 获得收入List
     *
     * @return
     */
    public static List<String> getIncomeMapValue() {
        return getMapValue(getIncomeMap());
    }

    /**
     * 根据value获得key(收入)
     *
     * @param value
     * @return
     */
    public static String getIncomeMapKeyByValue(String value) {
        return getMapKeyByValue(getIncomeMap(), value);
    }

    /**
     * 获得喜爱的运动Map
     *
     * @return
     */
    public static Map<String, String> getSportMap() {
        return getParamsInit().getSportMap2();
    }

    /**
     * 获得喜爱的运动List
     *
     * @return
     */
    public static List<String> getSportMapValue() {
        return getMapValue(getSportMap());
    }

    /**
     * 根据value获得key(喜爱的运动)
     *
     * @param value
     * @return
     */
    public static String getSportMapKeyByValue(String value) {
        return getMapKeyByValue(getSportMap(), value);
    }

    /**
     * 获得喜爱的宠物Map
     *
     * @return
     */
    public static Map<String, String> getPetsMap() {
        return getParamsInit().getPetsMap2();
    }

    /**
     * 获得喜爱的宠物List
     *
     * @return
     */
    public static List<String> getPetsMapValue() {
        return getMapValue(getPetsMap());
    }

    /**
     * 根据value获得key(喜爱的宠物)
     *
     * @param value
     * @return
     */
    public static String getPetsMapKeyByValue(String value) {
        return getMapKeyByValue(getPetsMap(), value);
    }

    /**
     * 获得书和动漫Map
     *
     * @return
     */
    public static Map<String, String> getBookCartoonMap() {
        return getParamsInit().getBooksMap2();
    }

    /**
     * 获得书和动漫List
     *
     * @return
     */
    public static List<String> getBookCartoonMapValue() {
        return getMapValue(getBookCartoonMap());
    }

    /**
     * 根据value获得key(书和动漫)
     *
     * @param value
     * @return
     */
    public static String getBookCartoonMapKeyByValue(String value) {
        return getMapKeyByValue(getBookCartoonMap(), value);
    }

    /**
     * 获得想去的地方Map
     *
     * @return
     */
    public static Map<String, String> getTravelMap() {
        return getParamsInit().getTravelMap2();
    }

    /**
     * 获得想去的地方List
     *
     * @return
     */
    public static List<String> getTravelMapValue() {
        return getMapValue(getTravelMap());
    }

    /**
     * 根据value获得key(想去的地方)
     *
     * @param value
     * @return
     */
    public static String getTravelMapKeyByValue(String value) {
        return getMapKeyByValue(getTravelMap(), value);
    }

    /**
     * 获得学历Map
     *
     * @return
     */
    public static Map<String, String> getEducationMap() {
        return getParamsInit().getEducationMap2();
    }

    /**
     * 获得学历List
     *
     * @return
     */
    public static List<String> getEducationMapValue() {
        return getMapValue(getEducationMap());
    }

    /**
     * 根据value获得key(学历)
     *
     * @param value
     * @return
     */
    public static String getEducationMapKeyByValue(String value) {
        return getMapKeyByValue(getEducationMap(), value);
    }

    /**
     * 获得婚姻Map
     *
     * @return
     */
    public static Map<String, String> getMarriageMap() {
        return getParamsInit().getMarriageMap2();
    }

    /**
     * 获得婚姻List
     *
     * @return
     */
    public static List<String> getMarriageMapValue() {
        return getMapValue(getMarriageMap());
    }

    /**
     * 根据value获得key(婚姻)
     *
     * @param value
     * @return
     */
    public static String getMarriageMapKeyByValue(String value) {
        return getMapKeyByValue(getMarriageMap(), value);
    }

    /**
     * 获得想要小孩Map
     *
     * @return
     */
    public static Map<String, String> getWantBabyMap() {
        return getParamsInit().getNeedBabeMap2();
    }

    /**
     * 获得想要小孩List
     *
     * @return
     */
    public static List<String> getWantBabyMapValue() {
        return getMapValue(getWantBabyMap());
    }

    /**
     * 根据value获得key(想要小孩)
     *
     * @param value
     * @return
     */
    public static String getWantBabyMapKeyByValue(String value) {
        return getMapKeyByValue(getWantBabyMap(), value);
    }

    /**
     * 获得个性Map
     *
     * @return
     */
    public static Map<String, String> getPersonalMap() {
        return getParamsInit().getCharacteristicsMap2();
    }

    /**
     * 获得个性List
     *
     * @return
     */
    public static List<String> getPersonalMapValue() {
        return getMapValue(getPersonalMap());
    }

    /**
     * 根据value获得key(个性)
     *
     * @param value
     * @return
     */

    public static String getPersonalMapKeyByValue(String value) {
        return getMapKeyByValue(getPersonalMap(), value);
    }

    /**
     * 获得血型Map
     *
     * @return
     */
    public static Map<String, String> getBloodMap() {
        return getParamsInit().getBloodMap2();
    }

    /**
     * 获得血型List
     *
     * @return
     */
    public static List<String> getBloodMapValue() {
        return getMapValue(getBloodMap());
    }

    /**
     * 根据value获得key(血型)
     *
     * @param value
     * @return
     */
    public static String getBloodMapKeyByValue(String value) {
        return getMapKeyByValue(getBloodMap(), value);
    }

    /**
     * 获得魅力Map
     *
     * @return
     */
    public static Map<String, String> getCharmMap() {
        return getParamsInit().getCharmMap2();
    }

    /**
     * 获得魅力List
     *
     * @return
     */
    public static List<String> getCharmMapValue() {
        return getMapValue(getCharmMap());
    }

    /**
     * 根据value获得key(魅力)
     *
     * @param value
     * @return
     */

    public static String getCharmMapKeyByValue(String value) {
        return getMapKeyByValue(getCharmMap(), value);
    }

    /**
     * 获得异地恋Map
     *
     * @return
     */
    public static Map<String, String> getDiffAreaLoveMap() {
        return getParamsInit().getDiffAreaLoveIdMap2();

    }

    /**
     * 获得异地恋List
     *
     * @return
     */
    public static List<String> getDiffAreaLoveMapValue() {
        return getMapValue(getDiffAreaLoveMap());
    }

    /**
     * 根据value获得key(异地恋)
     *
     * @param value
     * @return
     */

    public static String getDiffAreaLoveMapKeyByValue(String value) {
        return getMapKeyByValue(getDiffAreaLoveMap(), value);
    }

    /**
     * 获得喜欢的食物Map
     *
     * @return
     */
    public static Map<String, String> getFoodsMap() {
        return getParamsInit().getPetsMap2();
    }

    /**
     * 获得喜欢的食物List
     *
     * @return
     */
    public static List<String> getFoodsMapValue() {
        return getMapValue(getFoodsMap());
    }

    /**
     * 根据value获得key(喜欢的食物)
     *
     * @param value
     * @return
     */
    public static String getFoodsMapKeyByValue(String value) {
        return getMapKeyByValue(getFoodsMap(), value);
    }

    /**
     * 获得住房状况Map
     *
     * @return
     */
    public static Map<String, String> getHouseMap() {
        return getParamsInit().getHouseMap2();
    }

    /**
     * 获得住房状况List
     *
     * @return
     */
    public static List<String> getHouseMapValue() {
        return getMapValue(getHouseMap());
    }

    /**
     * 根据value获得key(住房状况)
     *
     * @param value
     * @return
     */
    public static String getHouseMapKeyByValue(String value) {
        return getMapKeyByValue(getHouseMap(), value);
    }

    /**
     * 获得与父母同住Map
     *
     * @return
     */
    public static Map<String, String> getLiveWithParentMap() {
        return getParamsInit().getLiveWithParentMap2();
    }

    /**
     * 获得与父母同住List
     *
     * @return
     */
    public static List<String> getLiveWithParentMapValue() {
        return getMapValue(getLiveWithParentMap());
    }

    /**
     * 根据value获得key(与父母同住)
     *
     * @param value
     * @return
     */
    public static String getLiveWithParentMapKeyByValue(String value) {
        return getMapKeyByValue(getLiveWithParentMap(), value);
    }

    /**
     * 喜欢的类型Map（男）
     *
     * @return
     */
    public static Map<String, String> getLoverTypeMapM() {
        return getParamsInit().getLoverTypeMap2M();
    }

    /**
     * 获得喜欢的类型List（男）
     *
     * @return
     */
    public static List<String> getLoverTypeMapMValue() {
        return getMapValue(getLoverTypeMapM());
    }

    /**
     * 根据value获得key(喜欢的类型)（男）
     *
     * @param value
     * @return
     */
    public static String getLoverTypeMapMKeyByValue(String value) {
        return getMapKeyByValue(getLoverTypeMapM(), value);
    }

    /**
     * 获得喜欢的类型Map（女）
     *
     * @return
     */
    public static Map<String, String> getLoverTypeMapF() {
        return getParamsInit().getLoverTypeMap2F();
    }

    /**
     * 获得喜欢的类型List（女）
     *
     * @return
     */
    public static List<String> getLoverTypeMapFValue() {
        return getMapValue(getLoverTypeMapF());
    }

    /**
     * 根据value获得key(喜欢的类型)（女）
     *
     * @param value
     * @return
     */
    public static String getLoverTypeMapFKeyByValue(String value) {
        return getMapKeyByValue(getLoverTypeMapF(), value);
    }

    /**
     * 获得喜欢的音乐Map
     *
     * @return
     */
    public static Map<String, String> getMusicMap() {
        return getParamsInit().getMusicMap2();
    }

    /**
     * 获得喜欢的音乐List
     *
     * @return
     */
    public static List<String> getMusicMapValue() {
        return getMapValue(getMusicMap());
    }

    /**
     * 根据value获得key(喜欢的音乐)
     *
     * @param value
     * @return
     */
    public static String getMusicMapKeyByValue(String value) {
        return getMapKeyByValue(getMusicMap(), value);
    }

    /**
     * 获得喜欢的电影Map
     *
     * @return
     */
    public static Map<String, String> getMoviesMap() {
        return getParamsInit().getMoviesMap2();
    }

    /**
     * 获得喜欢的电影List
     *
     * @return
     */
    public static List<String> getMoviesMapValue() {
        return getMapValue(getMoviesMap());
    }

    /**
     * 根据value获得key(喜欢的电影)
     *
     * @param value
     * @return
     */
    public static String getMoviesMapKeyByValue(String value) {
        return getMapKeyByValue(getMoviesMap(), value);
    }

    /**
     * 获得亲密行为Map
     *
     * @return
     */
    public static Map<String, String> getSexBefMarriedMap() {
        return getParamsInit().getSexBefMarriedMap2();
    }

    /**
     * 获得亲密行为List
     *
     * @return
     */
    public static List<String> getSexBefMarriedMapValue() {
        return getMapValue(getSexBefMarriedMap());
    }

    /**
     * 根据value获得key(亲密行为)
     *
     * @param value
     * @return
     */
    public static String getSexBefMarriedMapKeyByValue(String value) {
        return getMapKeyByValue(getSexBefMarriedMap(), value);
    }

    /**
     * 获得种族Map
     *
     * @return
     */
    public static Map<String, String> getEthnicityMap() {
        return getParamsInit().getEthnicityMap2();
    }

    /**
     * 获得种族List
     *
     * @return
     */
    public static List<String> getEthnicityMapValue() {
        return getMapValue(getEthnicityMap());
    }

    /**
     * 根据value获得key(种族)
     *
     * @param value
     * @return
     */
    public static String getEthnicityMapKeyByValue(String value) {
        return getMapKeyByValue(getEthnicityMap(), value);
    }

    /**
     * 获得锻炼习惯Map
     *
     * @return
     */
    public static Map<String, String> getExerciseHabitsMap() {
        return getParamsInit().getExerciseHabitsMap2();
    }

    /**
     * 获得锻炼习惯List
     *
     * @return
     */
    public static List<String> getExerciseHabitsMapValue() {
        return getMapValue(getExerciseHabitsMap());
    }

    /**
     * 根据value获得key(锻炼习惯)
     *
     * @param value
     * @return
     */
    public static String getExerciseHabitsMapKeyByValue(String value) {
        return getMapKeyByValue(getExerciseHabitsMap(), value);
    }

}
