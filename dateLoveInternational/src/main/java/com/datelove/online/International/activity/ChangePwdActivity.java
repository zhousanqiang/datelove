package com.datelove.online.International.activity;

import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.alibaba.fastjson.JSON;
import com.datelove.online.International.R;
import com.datelove.online.International.base.BaseTitleActivity;
import com.datelove.online.International.bean.BaseModel;
import com.datelove.online.International.constant.IUrlConstant;
import com.datelove.online.International.xml.PlatformInfoXml;
import com.datelove.online.International.xml.UserInfoXml;
import com.library.utils.ToastUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.Callback;

import butterknife.BindView;
import okhttp3.Call;
import okhttp3.Response;

/**
 * Created by Administrator on 2016/10/22.
 */
public class ChangePwdActivity extends BaseTitleActivity {
    @BindView(R.id.old_pwd)
    EditText old_pwd;
    @BindView(R.id.new_pwd)
    EditText new_pwd;
    @BindView(R.id.sure_new_pwd)
    EditText sure_new_pwd;
    @BindView(R.id.change_pwd)
    Button change_pwd;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_change_pwd;
    }

    @Override
    protected String getCenterTitle() {
        return getString(R.string.change_pwd_title);
    }

    @Override
    protected void initViewsAndVariables() {
        old_pwd.setText(UserInfoXml.getPassword());
    }

    @Override
    protected void addListeners() {
        change_pwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changePwd();
            }
        });
    }

    public void changePwd() {
        ToastUtil.showLongToast(getApplicationContext(), "old" + TextUtils.isEmpty(old_pwd.getText()));
        if (!TextUtils.isEmpty(old_pwd.getText()) && !TextUtils.isEmpty(new_pwd.getText()) && !TextUtils.isEmpty(sure_new_pwd.getText())) {
            if (new_pwd.getText().toString().equals(sure_new_pwd.getText().toString())) {
                if (new_pwd.getText().length() > 5 && new_pwd.getText().length() < 9) {
                    OkHttpUtils.post()
                            .url(IUrlConstant.URL_MODIFY_PWD)
                            .addHeader("token", PlatformInfoXml.getToken())
                            .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                            .addParams("password", old_pwd.getText().toString())
                            .addParams("newPassword", new_pwd.getText().toString())
                            .build()
                            .execute(new Callback<BaseModel>() {
                                @Override
                                public BaseModel parseNetworkResponse(Response response, int id) throws Exception {
                                    String resultJson = response.body().string();
                                    if (!TextUtils.isEmpty(resultJson)) {
                                        return JSON.parseObject(resultJson, BaseModel.class);
                                    }
                                    return null;
                                }

                                @Override
                                public void onError(Call call, Exception e, int id) {

                                }

                                @Override
                                public void onResponse(BaseModel response, int id) {
                                    if (response != null) {
                                        if (response.getMsg() != null) {
                                            ToastUtil.showLongToast(ChangePwdActivity.this, response.getMsg());
                                        }
                                        if (response.getIsSucceed() != null && response.getIsSucceed().equals("1")) {
                                            UserInfoXml.setPassword(new_pwd.getText().toString());
                                            finish();
                                        }
                                    }
                                }
                            });
                } else {
                    ToastUtil.showShortToast(ChangePwdActivity.this, getString(R.string.new_pwd_hint));
                }
            } else {
                ToastUtil.showLongToast(ChangePwdActivity.this, getString(R.string.two_not_same));
            }
        }
    }
}
