package com.datelove.online.International.activity;

import android.os.Environment;
import android.view.View;
import android.widget.TextView;

import com.datelove.online.International.R;
import com.datelove.online.International.base.BaseApplication;
import com.datelove.online.International.base.BaseTitleActivity;
import com.datelove.online.International.bean.Image;
import com.datelove.online.International.constant.IConfigConstant;
import com.datelove.online.International.utils.CommonRequestUtil;
import com.datelove.online.International.utils.FileUtil;
import com.datelove.online.International.utils.Utils;
import com.datelove.online.International.xml.UserInfoXml;
import com.library.utils.SharedPreferenceUtil;
import com.library.utils.Util;

import java.io.File;

import butterknife.BindView;

/**
 * Created by Administrator on 2016/12/28.
 */
public class UploadPhotoActivity extends BaseTitleActivity {
    @BindView(R.id.tv_up_photo)
    TextView tv_up_photo;
    @BindView(R.id.skip)
    TextView skip;

    @Override
    protected String getCenterTitle() {
        return getString(R.string.up_headicon);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_upphoto;
    }

    @Override
    protected void initViewsAndVariables() {
        mIvLeft.setVisibility(View.GONE);
    }

    @Override
    protected void addListeners() {
        tv_up_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetPhotoActivity.toGetPhotoActivity(UploadPhotoActivity.this, IConfigConstant.LOCAL_AVATAR_PATH, new GetPhotoActivity.OnGetPhotoListener() {
                    @Override
                    public void getSelectedPhoto(File file) {
                        if (file != null) {
                            showLoading();
                            CommonRequestUtil.uploadImage(true, file, true, new CommonRequestUtil.OnUploadImageListener() {
                                @Override
                                public void onSuccess(final Image image) {
                                    dismissLoading();
                                    gotoMainOrUploadAudio();
                                    new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            FileUtil.downWallFile(image.getImageUrl(), BaseApplication.getGlobalContext().
                                                    getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath(), "wall.jpg",true);
                                        }
                                    }).start();
                                    SharedPreferenceUtil.setBooleanValue(UploadPhotoActivity.this,"mIvUserAvatar","get_mIvUserAvatar",true);
                                }

                                @Override
                                public void onFail() {
                                    dismissLoading();
                                }
                            });
                        }
                    }
                });
            }
        });
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoMainOrUploadAudio();
            }
        });
    }

    private void gotoMainOrUploadAudio() {
        // 女性用户注册
        if (!UserInfoXml.isMale()) {
            boolean booleanValue = SharedPreferenceUtil.getBooleanValue(Utils.getContext(), "SELF_INTRODUCE", "self_introduce", false);
            if(booleanValue){
                Util.gotoActivity(UploadPhotoActivity.this, MainActivity.class, true);
            }else{
                Util.gotoActivity(UploadPhotoActivity.this, SelfIntroduceActivity.class, true);
            }
        } else {
            Util.gotoActivity(UploadPhotoActivity.this, MainActivity.class, true);
        }
//        if (CommonData.getCountryList().contains(PlatformInfoXml.getCountry())) {
//            Util.gotoActivity(UploadPhotoActivity.this, AnswerQuestionActivity.class, true);
//        } else if (!UserInfoXml.isMale()) {
//            Util.gotoActivity(UploadPhotoActivity.this, SelfIntroduceActivity.class, true);
//        } else {
//            Util.gotoActivity(UploadPhotoActivity.this, MainActivity.class, true);
//        }
    }

}
