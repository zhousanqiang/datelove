package com.datelove.online.International.bean;

/**
 * 用户对象基础信息
 * Created by zhangdroid on 2016/6/23.
 */
public class UserBase {
    private String id;// 用户id
    private long regTime; // 注册时间
    private String nickName;// 昵称
    private int age;// 年龄
    private int sex;// 性别(1->女, 0－>男)
    private String height;// 身高（英寸）
    private String heightCm;// 身高（厘米）
    private String bodyType;// 体型
    private int sign;// 星座
    private int education;// 学历
    private int occupation;// 职业
    private int income;// 收入
    private int maritalStatus; // 婚姻状况
    private String monologue; // 内心独白
    private int isBeanUser;// 1->表示为豆币用户,0->未开通
    private int isMonthly;// 1->表示开通“写信包月”,0->未开通
    private int isVip;// 1->VIP,0->非VIP
    private int level;// 诚信等级
    private int isAuthentication;// 1->已验证身份，0->未验证
    private int isLock;// 头像锁,1->锁定头像，0->公开头像
    private String country;
    private String stat;
    private String district;
    private String language;
    private String spokenLanguage;
    private Image image; // 图片对象
    private Area area;// 区域对象

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getRegTime() {
        return regTime;
    }

    public void setRegTime(long regTime) {
        this.regTime = regTime;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getHeightCm() {
        return heightCm;
    }

    public void setHeightCm(String heightCm) {
        this.heightCm = heightCm;
    }

    public String getBodyType() {
        return bodyType;
    }

    public void setBodyType(String bodyType) {
        this.bodyType = bodyType;
    }

    public int getSign() {
        return sign;
    }

    public void setSign(int sign) {
        this.sign = sign;
    }

    public int getEducation() {
        return education;
    }

    public void setEducation(int education) {
        this.education = education;
    }

    public int getOccupation() {
        return occupation;
    }

    public void setOccupation(int occupation) {
        this.occupation = occupation;
    }

    public int getIncome() {
        return income;
    }

    public void setIncome(int income) {
        this.income = income;
    }

    public int getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(int maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getMonologue() {
        return monologue;
    }

    public void setMonologue(String monologue) {
        this.monologue = monologue;
    }

    public int getIsBeanUser() {
        return isBeanUser;
    }

    public void setIsBeanUser(int isBeanUser) {
        this.isBeanUser = isBeanUser;
    }

    public int getIsMonthly() {
        return isMonthly;
    }

    public void setIsMonthly(int isMonthly) {
        this.isMonthly = isMonthly;
    }

    public int getIsVip() {
        return isVip;
    }

    public void setIsVip(int isVip) {
        this.isVip = isVip;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getIsAuthentication() {
        return isAuthentication;
    }

    public void setIsAuthentication(int isAuthentication) {
        this.isAuthentication = isAuthentication;
    }

    public int getIsLock() {
        return isLock;
    }

    public void setIsLock(int isLock) {
        this.isLock = isLock;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getSpokenLanguage() {
        return spokenLanguage;
    }

    public void setSpokenLanguage(String spokenLanguage) {
        this.spokenLanguage = spokenLanguage;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    @Override
    public String toString() {
        return "UserBase{" +
                "id='" + id + '\'' +
                ", regTime=" + regTime +
                ", nickName='" + nickName + '\'' +
                ", age=" + age +
                ", sex=" + sex +
                ", height='" + height + '\'' +
                ", heightCm='" + heightCm + '\'' +
                ", bodyType='" + bodyType + '\'' +
                ", sign=" + sign +
                ", education=" + education +
                ", occupation=" + occupation +
                ", income=" + income +
                ", maritalStatus=" + maritalStatus +
                ", monologue='" + monologue + '\'' +
                ", isBeanUser=" + isBeanUser +
                ", isMonthly=" + isMonthly +
                ", isVip=" + isVip +
                ", level=" + level +
                ", isAuthentication=" + isAuthentication +
                ", isLock=" + isLock +
                ", country='" + country + '\'' +
                ", stat='" + stat + '\'' +
                ", district='" + district + '\'' +
                ", language='" + language + '\'' +
                ", spokenLanguage='" + spokenLanguage + '\'' +
                ", image=" + image +
                ", area=" + area +
                '}';
    }

}
