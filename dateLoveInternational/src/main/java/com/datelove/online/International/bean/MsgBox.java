package com.datelove.online.International.bean;

/**
 * 单条聊天记录对象
 * Created by zhangdroid on 2016/10/29.
 */
public class MsgBox {
    private long id;
    private int isRead;// 是否已读，1->已读，0->未读
    private String type;// 消息类型，1->手写信, 2->招呼信, 3->管理员信，4->语音消息，6->QA问答
    private String time;// 收到消息的时间
    private long timeMills;// 收到消息的时间，毫秒数
    private long ownedUid;
    private String msg;// 消息内容
    private String audioUrl;// 语音消息url
    private String audioTime;// 语音时长
    private UserBase userBaseEnglish;// 发消息的用户对象
    private String noHeadImg;
    private String diffProvinces;
    private String noConformAge;
    private String noVerifyIdentity;
    private String language;
    private String country;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getIsRead() {
        return isRead;
    }

    public void setIsRead(int isRead) {
        this.isRead = isRead;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public long getTimeMills() {
        return timeMills;
    }

    public void setTimeMills(long timeMills) {
        this.timeMills = timeMills;
    }

    public long getOwnedUid() {
        return ownedUid;
    }

    public void setOwnedUid(long ownedUid) {
        this.ownedUid = ownedUid;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getAudioUrl() {
        return audioUrl;
    }

    public void setAudioUrl(String audioUrl) {
        this.audioUrl = audioUrl;
    }

    public String getAudioTime() {
        return audioTime;
    }

    public void setAudioTime(String audioTime) {
        this.audioTime = audioTime;
    }

    public String getNoHeadImg() {
        return noHeadImg;
    }

    public void setNoHeadImg(String noHeadImg) {
        this.noHeadImg = noHeadImg;
    }

    public String getDiffProvinces() {
        return diffProvinces;
    }

    public void setDiffProvinces(String diffProvinces) {
        this.diffProvinces = diffProvinces;
    }

    public String getNoConformAge() {
        return noConformAge;
    }

    public void setNoConformAge(String noConformAge) {
        this.noConformAge = noConformAge;
    }

    public String getNoVerifyIdentity() {
        return noVerifyIdentity;
    }

    public void setNoVerifyIdentity(String noVerifyIdentity) {
        this.noVerifyIdentity = noVerifyIdentity;
    }

    public UserBase getUserBaseEnglish() {
        return userBaseEnglish;
    }

    public void setUserBaseEnglish(UserBase userBaseEnglish) {
        this.userBaseEnglish = userBaseEnglish;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return "MsgBox{" +
                "id=" + id +
                ", isRead=" + isRead +
                ", type='" + type + '\'' +
                ", time='" + time + '\'' +
                ", timeMills=" + timeMills +
                ", ownedUid=" + ownedUid +
                ", msg='" + msg + '\'' +
                ", audioUrl='" + audioUrl + '\'' +
                ", audioTime='" + audioTime + '\'' +
                ", userBaseEnglish=" + userBaseEnglish +
                ", noHeadImg='" + noHeadImg + '\'' +
                ", diffProvinces='" + diffProvinces + '\'' +
                ", noConformAge='" + noConformAge + '\'' +
                ", noVerifyIdentity='" + noVerifyIdentity + '\'' +
                ", language='" + language + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}
