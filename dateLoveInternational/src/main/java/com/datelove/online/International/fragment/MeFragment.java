package com.datelove.online.International.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.datelove.online.International.R;
import com.datelove.online.International.activity.BuyServiceActivity;
import com.datelove.online.International.activity.GetPhotoActivity;
import com.datelove.online.International.activity.MatchInfoActivity;
import com.datelove.online.International.activity.MyCareActivity;
import com.datelove.online.International.activity.MyInfoActivity;
import com.datelove.online.International.activity.PictureBrowseActivity;
import com.datelove.online.International.activity.RecentVistorsActivity;
import com.datelove.online.International.activity.SettingActivity;
import com.datelove.online.International.adapter.SeeMeAdapter;
import com.datelove.online.International.base.BaseApplication;
import com.datelove.online.International.base.BaseTitleFragment;
import com.datelove.online.International.bean.Image;
import com.datelove.online.International.bean.SeeMe;
import com.datelove.online.International.bean.SeeMeList;
import com.datelove.online.International.bean.User;
import com.datelove.online.International.bean.UserBase;
import com.datelove.online.International.bean.UserDetail;
import com.datelove.online.International.constant.CommonData;
import com.datelove.online.International.constant.IConfigConstant;
import com.datelove.online.International.constant.IUrlConstant;
import com.datelove.online.International.dialog.AlbumMoreDialog;
import com.datelove.online.International.event.SetAvatarEvent;
import com.datelove.online.International.event.UpdatePayInfoEvent;
import com.datelove.online.International.event.UpdateUserInfoEvent;
import com.datelove.online.International.utils.Cheeses;
import com.datelove.online.International.utils.CommonRequestUtil;
import com.datelove.online.International.utils.FileUtil;
import com.datelove.online.International.utils.MyLinearLayout;
import com.datelove.online.International.utils.ParamsUtils;
import com.datelove.online.International.utils.SlideMenu;
import com.datelove.online.International.utils.Utils;
import com.datelove.online.International.xml.PlatformInfoXml;
import com.datelove.online.International.xml.UserInfoXml;
import com.library.adapter.recyclerview.CommonRecyclerViewAdapter;
import com.library.adapter.recyclerview.RecyclerViewHolder;
import com.library.imageloader.CircleTransformation;
import com.library.imageloader.ImageLoader;
import com.library.imageloader.ImageLoaderUtil;
import com.library.utils.DensityUtil;
import com.library.utils.HeightUtils;
import com.library.utils.SharedPreferenceUtil;
import com.library.utils.Util;
import com.library.widgets.CollapsingTextView;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.Callback;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.Call;
import okhttp3.Response;

import static com.squareup.picasso.Picasso.with;

/**
 * 我
 */
public class MeFragment extends BaseTitleFragment implements View.OnClickListener {
    @BindView(R.id.userDetail_CollapsingToolbarLayout)
    CollapsingToolbarLayout mCollapsingToolbarLayout;
    //    @BindView(R.id.userDetail_toolbar)
//    Toolbar mToolbar;
    // 用户头像和照片墙
    @BindView(R.id.userDetail_avatar)
    ImageView mIvUserAvatar;
    @BindView(R.id.userDetail_pic_wall)
    RecyclerView mPicRecyclerView;
    // 展示的基本信息
    @BindView(R.id.userDetail_base_info)
    TextView mTvBaseInfo;
    // 内心独白
    @BindView(R.id.userDetail_monologue_ll)
    LinearLayout mLlMonologue;
    @BindView(R.id.userDetail_monologue)
    CollapsingTextView mMonologue;
    // 用户个人资料
    // 国家
    @BindView(R.id.userDetail_country_ll)
    LinearLayout mLlCountry;
    @BindView(R.id.userDetail_country)
    TextView mTvCountry;
    @BindView(R.id.userDetail_country_divider)
    View mCountryDivider;
    // 地区
    @BindView(R.id.userDetail_area_ll)
    LinearLayout mLlArea;
    @BindView(R.id.userDetail_area)
    TextView mTvArea;
    @BindView(R.id.userDetail_area_divider)
    View mAreaDivider;
    // 星座
    @BindView(R.id.userDetail_sign_ll)
    LinearLayout mLlSign;
    @BindView(R.id.userDetail_sign)
    TextView mTvSign;
    @BindView(R.id.userDetail_sign_divider)
    View mSignDivider;
    // 学历
    @BindView(R.id.userDetail_education_ll)
    LinearLayout mLlEducation;
    @BindView(R.id.userDetail_education)
    TextView mTvEducation;
    @BindView(R.id.userDetail_education_divider)
    View mEducationDivider;
    // 收入
    @BindView(R.id.userDetail_income_ll)
    LinearLayout mLlIncome;
    @BindView(R.id.userDetail_income)
    TextView mTvIncome;
    @BindView(R.id.userDetail_income_divider)
    View mIncomeDivider;
    // 职业
    @BindView(R.id.userDetail_occupation_ll)
    LinearLayout mLlOccupation;
    @BindView(R.id.userDetail_occupation)
    TextView mTvOccupation;
    @BindView(R.id.userDetail_occupation_divider)
    View mOccupationDivider;
    // 是否想要小孩
    @BindView(R.id.userDetail_wantBaby_ll)
    LinearLayout mLlWantBaby;
    @BindView(R.id.userDetail_wantBaby)
    TextView mTvWantBaby;
    @BindView(R.id.userDetail_want_baby_divider)
    View mWantBabyDivider;
    // 喜欢的运动
    @BindView(R.id.userDetail_sport_ll)
    LinearLayout mLlSport;
    @BindView(R.id.userDetail_sport)
    TextView mTvSport;
    @BindView(R.id.userDetail_sport_divider)
    View mSportDivider;
    // 喜欢的宠物
    @BindView(R.id.userDetail_pets_ll)
    LinearLayout mLlPets;
    @BindView(R.id.userDetail_pets)
    TextView mTvPets;
    @BindView(R.id.userDetail_pets_divider)
    View mPetsDivider;
    // 种族
    @BindView(R.id.userDetail_ethnicity_ll)
    LinearLayout mLlEthnicity;
    @BindView(R.id.userDetail_ethnicity)
    TextView mTvEthnicity;
    @BindView(R.id.userDetail_ethnicity_divider)
    View mEthnicityDivider;
    // 锻炼习惯
    @BindView(R.id.userDetail_exerciseHabits_ll)
    LinearLayout mLlExerciseHabits;
    @BindView(R.id.userDetail_exerciseHabits)
    TextView mTvExerciseHabits;
    @BindView(R.id.userDetail_exerciseHabits_divider)
    View mExerciseHabitsDivider;
    // 兴趣爱好
    @BindView(R.id.userDetail_hobby_ll)
    LinearLayout mLlHobby;
    @BindView(R.id.userDetail_hobby)
    TextView mTvHobby;
    @BindView(R.id.userDetail_hobby_divider)
    View mHobbyDivider;


    //    @BindView(R.id.head_icon)
//    ImageView head_icon;
//    @BindView(R.id.nick_name)
//    TextView nick_name;
//    @BindView(R.id.user_info)
//    TextView user_info;
    @BindView(R.id.recyclerview_seeme)
    RecyclerView mRecyclerView;
    @BindView(R.id.buy_bean)
    TextView buy_bean;
    @BindView(R.id.buy_month)
    TextView buy_month;
    @BindView(R.id.tv_photo)
    TextView tv_photo;

    //    @BindView(R.id.my_info)
//    TextView my_info;
//    @BindView(R.id.me_vip)
//    TextView me_vip;
//    @BindView(R.id.my_photo)
//    TextView my_photo;
    @BindView(R.id.my_match)
    TextView my_match;
    @BindView(R.id.my_care)
    TextView my_care;
    @BindView(R.id.my_set)
    TextView my_set;
    @BindView(R.id.no_person)
    TextView no_person;
    @BindView(R.id.my_linearlayout)
    MyLinearLayout my_linearlayout;
    @BindView(R.id.iv_icon)
    ImageView ivIcon;
    @BindView(R.id.main_left_menu)
    ImageView mainLeftMenu;
    @BindView(R.id.main_right_menu)
    ImageView mainRightMenu;
    @BindView(R.id.slideMenu_slide)
    SlideMenu slideMenu;
    @BindView(R.id.name)
    TextView name;
    Unbinder unbinder;
    @BindView(R.id.tv_introduce)
    TextView tvIntroduce;
    @BindView(R.id.rl_introduce)
    RelativeLayout rlIntroduce;


    private SeeMeAdapter mSeeMeAdapter;
    private String uId;
    private String nickname;
    private ImageWallAdapter mImageWallAdapter;
    private List<Image> mImageList = new ArrayList<>();
    private boolean slideOpen = false;
    private int i;
    private boolean first=true;
    private boolean isFirst=true;
    private int n=0;

    @Override
    public void onResume() {
        super.onResume();
        System.out.println("wwwwwwMeFragment");
    }

//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        slideMenu = (SlideMenu) View.inflate(getActivity(), R.layout.activity_main, null);
//        setContentView(slideMenu);
//    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_me;
    }


    @Override
    protected void initViewsAndVariables() {
//        女用户更换背景图
        String genter = SharedPreferenceUtil.getStringValue(getActivity(), "REGISTER_GENDER", "genter", "0");
        if (!genter.equals("0")) {
            slideMenu.setBackgroundResource(R.drawable.me_woman_bacground);
//            改变字体颜色
            buy_bean.setTextColor(getActivity().getResources().getColor(R.color.mask_color));
            buy_month.setTextColor(getActivity().getResources().getColor(R.color.mask_color));
            tv_photo.setTextColor(getActivity().getResources().getColor(R.color.mask_color));
            no_person.setTextColor(getActivity().getResources().getColor(R.color.mask_color));
            my_care.setTextColor(getActivity().getResources().getColor(R.color.mask_color));
            my_match.setTextColor(getActivity().getResources().getColor(R.color.mask_color));
            my_set.setTextColor(getActivity().getResources().getColor(R.color.mask_color));
        }
        //设置SlideMenu
        my_linearlayout.setSlideMenu(slideMenu);
//        去除最上面的标题栏
        deleteBar();
//        setTitle(getString(R.string.tab_me));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mSeeMeAdapter = new SeeMeAdapter(getActivity(), R.layout.item_seeme);
        mRecyclerView.setAdapter(mSeeMeAdapter);
        uId = UserInfoXml.getUID();
        // 初始化照片墙RecyclerView
        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(getActivity());
        linearLayoutManager1.setOrientation(LinearLayoutManager.HORIZONTAL);
        mPicRecyclerView.setLayoutManager(linearLayoutManager1);
        mImageWallAdapter = new ImageWallAdapter(getActivity(), R.layout.item_pic_wall);
        // 添加HeadView
        ImageView headView = new ImageView(getActivity());
        ViewGroup.MarginLayoutParams layoutParams = new ViewGroup.MarginLayoutParams(DensityUtil.dip2px(getActivity(), 60), DensityUtil.dip2px(getActivity(), 60));
        int margin = DensityUtil.dip2px(getActivity(), 5);
        layoutParams.setMargins(margin, margin, margin, margin);
        headView.setLayoutParams(layoutParams);
        headView.setImageResource(R.drawable.upload_photo_me);

        mImageWallAdapter.addHeaderView(headView, false);

        mPicRecyclerView.setAdapter(mImageWallAdapter);

        setMyInfo();
        // 加载用户详情
        showLoading();
        loadUserDetailInfo(false);
        getSeeMe();
        getPhotoList();
        if (CommonData.getCountryList().contains(PlatformInfoXml.getCountry())) {
            getSelfIntroduce();
        }

    }

    private void setMyInfo() {
        if (UserInfoXml.isMale()) {
//            head_icon.setImageResource(R.drawable.icon_me_male);
            ivIcon.setImageResource(R.drawable.icon_me_male);
        } else {
//            head_icon.setImageResource(R.drawable.icon_me_female);
            ivIcon.setImageResource(R.drawable.icon_me_female);
        }
         File avatarFile = new File(IConfigConstant.LOCAL_AVATAR_PATH);
        if (avatarFile != null && avatarFile.exists()) {
            ImageLoaderUtil.getInstance().loadCircleImage(getActivity(), ivIcon, avatarFile);
        } else {
            ImageLoaderUtil.getInstance().loadImage(getActivity(), new ImageLoader.Builder().url(UserInfoXml.getAvatarThumbnailUrl()).
                    imageView(ivIcon).transform(new CircleTransformation()).build());
            // 下载头像，缓存到本地
            new Thread(new Runnable() {
                @Override
                public void run() {
                    String imageUrl = UserInfoXml.getAvatarUrl();
                    if (!TextUtils.isEmpty(imageUrl)) {
                        FileUtil.downloadFile(imageUrl, BaseApplication.getGlobalContext().
                                getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath(), "avatar.jpg");
                    }
                }
            }).start();
        }

        boolean booleanValue = SharedPreferenceUtil.getBooleanValue(getActivity(), "mIvUserAvatar", "get_mIvUserAvatar", false);
        if (booleanValue) {
            File wallrFile = new File(IConfigConstant.LOCAL_wall_PATH);

            if (wallrFile != null && wallrFile.exists()) {
                ImageLoaderUtil.getInstance().loadLocalImage(getActivity(), mIvUserAvatar, wallrFile);
            } else {
                String imgUrl = UserInfoXml.getAvatarUrl();
                if (!TextUtils.isEmpty(imgUrl)) {
                    ImageLoaderUtil.getInstance().loadImage(getActivity(), new ImageLoader.Builder()
                            .url(imgUrl).imageView(mIvUserAvatar).build());
                }
            }
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                     File avatarFile = new File(IConfigConstant.LOCAL_AVATAR_PATH);
                    if (avatarFile != null && avatarFile.exists()) {
                        ImageLoaderUtil.getInstance().loadCircleImage(getActivity(), ivIcon, avatarFile);
                    }
                    File wallrFile = new File(IConfigConstant.LOCAL_wall_PATH);
                    if (wallrFile != null && wallrFile.exists()) {
                        ImageLoaderUtil.getInstance().loadLocalImage(getActivity(), mIvUserAvatar, wallrFile);
                    } else {
                        String imgUrl = UserInfoXml.getAvatarUrl();
                        if (!TextUtils.isEmpty(imgUrl)) {
                            ImageLoaderUtil.getInstance().loadImage(getActivity(), new ImageLoader.Builder()
                                    .url(imgUrl).imageView(mIvUserAvatar).build());
                        }
                    }
                }
            },5000);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                     File avatarFile = new File(IConfigConstant.LOCAL_AVATAR_PATH);
                    if (avatarFile != null && avatarFile.exists()) {
                        ImageLoaderUtil.getInstance().loadCircleImage(getActivity(), ivIcon, avatarFile);
                    }
                    File wallrFile = new File(IConfigConstant.LOCAL_wall_PATH);
                    if (wallrFile != null && wallrFile.exists()) {
                        ImageLoaderUtil.getInstance().loadLocalImage(getActivity(), mIvUserAvatar, wallrFile);
                    } else {
                        String imgUrl = UserInfoXml.getAvatarUrl();
                        if (!TextUtils.isEmpty(imgUrl)) {
                            ImageLoaderUtil.getInstance().loadImage(getActivity(), new ImageLoader.Builder()
                                    .url(imgUrl).imageView(mIvUserAvatar).build());
                        }
                    }
                }
            },10000);



        }




//        nick_name.setText(UserInfoXml.getNickName());
//        user_info.setText(UserInfoXml.getAge() + getString(R.string.years) + "\t\t" + UserInfoXml.getProvinceName());
        boolean beanUser = UserInfoXml.isBeanUser();
        UserInfoXml.isMonthly();
        buy_bean.setSelected(UserInfoXml.isBeanUser());
        buy_month.setSelected(UserInfoXml.isMonthly());
    }

    @Override
    protected void addListeners() {
        buy_bean.setOnClickListener(this);
        buy_month.setOnClickListener(this);
        mIvUserAvatar.setOnClickListener(this);
//        my_photo.setOnClickListener(this);
        my_match.setOnClickListener(this);
        my_care.setOnClickListener(this);
        my_set.setOnClickListener(this);
//        no_person.setOnClickListener(this);
//        head_icon.setOnClickListener(this);
        ivIcon.setOnClickListener(this);
        mainLeftMenu.setOnClickListener(this);
        mainRightMenu.setOnClickListener(this);
        //设置拖拽的监听器
        slideMenu.setDragListener(new SlideMenu.OnDragListener() {
            @Override
            public void onOpen() {
//                Toast.makeText(getActivity(), "open", Toast.LENGTH_SHORT).show();
//                int i = new Random().nextInt(4);
//                menuListView.smoothScrollToPosition(i);
            }

            @Override
            public void onClose() {
//                Toast.makeText(getActivity(), "close", Toast.LENGTH_SHORT).show();
//                ViewCompat.animate(iv_head)
//                        .setInterpolator(new CycleInterpolator(10))
//                        .translationXBy(25)
//                        .setDuration(1000)
//                        .start();

                ViewCompat.animate(mainLeftMenu)
                        .rotationBy(360)
                        .setInterpolator(new OvershootInterpolator(4))
                        .setDuration(1000)
                        .start();
            }

            @Override
            public void onDragging(float fraction) {
//                Log.e(TAG, "onDragging: "+fraction);
//                iv_head.setAlpha(1-fraction);
                mainLeftMenu.setRotation(360 * fraction);
            }
        });

//        头像和照片的监听
        mImageWallAdapter.setOnHeaderViewClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetPhotoActivity.toGetPhotoActivity(getActivity(), IConfigConstant.LOCAL_PIC_PATH, new GetPhotoActivity.OnGetPhotoListener() {
                    @Override
                    public void getSelectedPhoto(File file) {
                        if (file != null) {
                            uploadImage(file);
                            updateUserInfo();
                        }
                    }
                });
            }
        });
        mImageWallAdapter.setOnItemClickListener(new CommonRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, RecyclerViewHolder viewHolder) {
                PictureBrowseActivity.toPictureBrowseActivity(getActivity(), position, mImageList, true);
            }
        });
        mImageWallAdapter.setOnItemLongClickListener(new CommonRecyclerViewAdapter.OnItemLongClickListener() {
            @Override
            public void onItemLongClick(View view, final int position, RecyclerViewHolder viewHolder) {
                final Image image = mImageWallAdapter.getItemByPosition(position);
                AlbumMoreDialog.newInstance(new AlbumMoreDialog.OnAlbumMoreClickLister() {
                    @Override
                    public void setToAvatar() {
                        CommonRequestUtil.setAvatar(image.getId(), true, new CommonRequestUtil.OnCommonListener() {
                            @Override
                            public void onSuccess() {
//                                删除本地缓存
                                FileUtil.deleteFile(IConfigConstant.LOCAL_AVATAR_PATH);
                                FileUtil.deleteFile(IConfigConstant.LOCAL_wall_PATH);
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        FileUtil.downWallFile(image.imageUrl, BaseApplication.getGlobalContext().
                                                getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath(), "wall.jpg",false);
                                    }
                                }).start();
                                SharedPreferenceUtil.setBooleanValue(getActivity(),"mIvUserAvatar","get_mIvUserAvatar",true);

                                updateUserInfo();

                            }

                            @Override
                            public void onFail() {
                            }
                        });
                    }

                    @Override
                    public void deleteImage() {
                        CommonRequestUtil.deleteImage(image.getId(), true, new CommonRequestUtil.OnCommonListener() {
                            @Override
                            public void onSuccess() {
                                if (mImageWallAdapter != null) {
                                    mImageWallAdapter.removeItem(position);
                                    getPhotoList();
                                }
                            }

                            @Override
                            public void onFail() {
                            }
                        });
                    }
                }).show(getActivity().getSupportFragmentManager(), "album");
            }
        });


    }


    private void getSeeMe() {
        OkHttpUtils.post()
                .url(IUrlConstant.URL_GET_GUEST_LIST)
                .addHeader("token", PlatformInfoXml.getToken())
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .addParams("pageNum", "1")
                .addParams("pageSize", "15")
                .build()
                .execute(new Callback<SeeMeList>() {
                    @Override
                    public SeeMeList parseNetworkResponse(Response response, int id) throws Exception {
                        String resultJson = response.body().string();
                        if (!TextUtils.isEmpty(resultJson)) {
                            return JSON.parseObject(resultJson, SeeMeList.class);
                        }
                        return null;
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(SeeMeList response, int id) {
                        if (response != null) {
                            List<SeeMe> seeMeList = response.getSeeMeList();
                            if (!Util.isListEmpty(seeMeList)) {
                                if (mRecyclerView != null && no_person != null && mSeeMeAdapter != null) {
                                    mRecyclerView.setVisibility(View.VISIBLE);
                                    no_person.setVisibility(View.GONE);
                                    mSeeMeAdapter.replaceAll(seeMeList);
                                    mSeeMeAdapter.setOnItemClickListener(new CommonRecyclerViewAdapter.OnItemClickListener() {
                                        @Override
                                        public void onItemClick(View view, int position, RecyclerViewHolder viewHolder) {
                                            SeeMe seeMe = mSeeMeAdapter.getItemByPosition(position);
                                            if (seeMe != null) {
//                    UserInfoDetailActivity.toUserInfoDetailActivity(getActivity(), seeMe.getUserBaseEnglish().getId(), null, UserInfoDetailActivity.SOURCE_FROM_SEEME);
//                    EventBus.getDefault().post(new RecentVistorsEvent());
                                                Intent intent = new Intent(getActivity(), RecentVistorsActivity.class);
                                                startActivity(intent);
                                            }
                                        }
                                    });
                                }

                            }
                        }
                    }
                });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
//            case R.id.head_icon:
//                GetPhotoActivity.toGetPhotoActivity(getActivity(), IConfigConstant.LOCAL_AVATAR_PATH, new GetPhotoActivity.OnGetPhotoListener() {
//                    @Override
//                    public void getSelectedPhoto(final File file) {
//                        if (file != null) {
//                            showLoading();
//                            CommonRequestUtil.uploadImage(true, file, true, new CommonRequestUtil.OnUploadImageListener() {
//                                @Override
//                                public void onSuccess(Image image) {
//                                    if (image != null) {
//                                        // 保存头像信息
//                                        UserInfoXml.setAvatarUrl(image.getImageUrl());
//                                        UserInfoXml.setAvatarThumbnailUrl(image.getThumbnailUrl());
//                                        UserInfoXml.setAvatarStatus(image.getStatus());
//                                    }
//                                    // 设置新头像
////                                    ImageLoaderUtil.getInstance().loadCircleImage(getActivity(), head_icon, file);
//                                    ImageLoaderUtil.getInstance().loadCircleImage(getActivity(), ivIcon, file);
//                                    dismissLoading();
//                                }
//
//                                @Override
//                                public void onFail() {
//                                    dismissLoading();
//                                }
//                            });
//                        }
//                    }
//                });
//                break;
//            case R.id.userDetail_avatar:
//                PictureBrowseActivity.toPictureBrowseActivity(getActivity(), position, mImageList,true);
//                break;
            case R.id.iv_icon:
                slideMenu.open();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
         //                打开侧滑菜单
                        slideMenu.open();
                    }
                },4000);
                GetPhotoActivity.toGetPhotoActivity(getActivity(), IConfigConstant.LOCAL_AVATAR_PATH, new GetPhotoActivity.OnGetPhotoListener() {
                    @Override
                    public void getSelectedPhoto(final File file) {
                        if (file != null) {
                            slideMenu.open();
                            showLoading();
                            CommonRequestUtil.uploadImage(true, file, true, new CommonRequestUtil.OnUploadImageListener() {
                                @Override
                                public void onSuccess(final Image image) {
                                    slideMenu.open();
                                    if (image != null) {
                                        // 保存头像信息
                                        UserInfoXml.setAvatarUrl(image.getImageUrl());
                                        UserInfoXml.setAvatarThumbnailUrl(image.getThumbnailUrl());
                                        UserInfoXml.setAvatarStatus(image.getStatus());
                                    }
                                    // 设置新头像
//                                    ImageLoaderUtil.getInstance().loadCircleImage(getActivity(), head_icon, file);
                                    ImageLoaderUtil.getInstance().loadCircleImage(getActivity(), ivIcon, file);
                                    with(getActivity()).load(file).skipMemoryCache().noPlaceholder().into(mIvUserAvatar);
                                    SharedPreferenceUtil.setBooleanValue(getActivity(),"mIvUserAvatar","get_mIvUserAvatar",true);
                                    dismissLoading();
                                    new Thread(new Runnable() {
                                        @Override
                                       public void run() {
                              FileUtil.downWallFile(image.getImageUrl(), BaseApplication.getGlobalContext().
                                getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath(), "wall.jpg",false);
                                        }
                                    }).start();

                                     getPhotoList();
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            slideMenu.open();
                                        }
                                    },1000);


                                }

                                @Override
                                public void onFail() {
                                    dismissLoading();
                                    slideMenu.open();
                                }
                            });
                        }
                    }
                });
                break;
            case R.id.buy_bean:
                BuyServiceActivity.toBuyServiceActivity(getActivity(), BuyServiceActivity.INTENT_FROM_BEAN,
                        IConfigConstant.PAY_TAG_FROM_DETAIL, IConfigConstant.PAY_WAY_BEAN);
                break;
            case R.id.buy_month:
                BuyServiceActivity.toBuyServiceActivity(getActivity(), BuyServiceActivity.INTENT_FROM_MONTH,
                        IConfigConstant.PAY_TAG_FROM_DETAIL, IConfigConstant.PAY_WAY_MONTH);
                break;
//            case R.id.me_vip:
//                BuyServiceActivity.toBuyServiceActivity(getActivity(), BuyServiceActivity.INTENT_FROM_INTERCEPT,
//                        IConfigConstant.PAY_TAG_FROM_DETAIL, IConfigConstant.PAY_WAY_ALL);
//                break;
//
//            case R.id.my_info:
//                Util.gotoActivity(getActivity(), MyInfoActivity.class, false);
//                break;
//            case R.id.my_photo:
//                Util.gotoActivity(getActivity(), MyPhotoActivity.class, false);
//                break;
            case R.id.my_match:
                Util.gotoActivity(getActivity(), MatchInfoActivity.class, false);
                break;
            case R.id.my_care:
                Util.gotoActivity(getActivity(), MyCareActivity.class, false);
                break;
            case R.id.my_set:
                Util.gotoActivity(getActivity(), SettingActivity.class, false);
                break;
//            case R.id.no_person:
//                EventBus.getDefault().post(new NoSeeMeEvent());
//                break;
            case R.id.main_left_menu:
//                左边按钮
                slideMenu.open();

                break;
            case R.id.main_right_menu:
//                右边按钮
                Util.gotoActivity(getActivity(), MyInfoActivity.class, false);
                break;

        }
    }


    /**
     * 设置用户信息
     */
    private void setUserInfo(User user,boolean setAvatar) {
        if (user != null) {
            if(setAvatar){
                setUserPicWall(user.getListImage());
                slideMenu.open();
            }else {
                UserBase userBase = user.getUserBaseEnglish();
                if (userBase != null) {
                    // 更新ID
                    uId = userBase.getId();
                    // 设置用户名为标题
                    nickname = userBase.getNickName();
                    if (!TextUtils.isEmpty(nickname)) {
                        mCollapsingToolbarLayout.setTitle(nickname);
                    }
                    // 设置头像
                    Image image = userBase.getImage();
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        first = false;
//                    }
//                }, 8000);
//                if(!first) {
//                    if (image != null) {

//                    boolean booleanValue = SharedPreferenceUtil.getBooleanValue(getActivity(), "mIvUserAvatar", "get_mIvUserAvatar", false);
//                    if (booleanValue) {
//                        File avatarFile = new File(IConfigConstant.LOCAL_wall_PATH);
//                        if (avatarFile != null && avatarFile.exists()) {
//                            ImageLoaderUtil.getInstance().loadCircleImage(getActivity(), mIvUserAvatar, avatarFile);
//                        } else {
//                            String imgUrl = image.getImageUrl();
//                            if (!TextUtils.isEmpty(imgUrl)) {
//                                ImageLoaderUtil.getInstance().loadImage(getActivity(), new ImageLoader.Builder()
//                                        .url(imgUrl).imageView(mIvUserAvatar).build());
//                            }
//                        }
//
//
//
//                    }


//                        } else {
//                            mIvUserAvatar.setImageResource(IConfigConstant.COMMON_DEFAULT_AVATAR);
//                        }

//                    } else {
//                        mIvUserAvatar.setImageResource(IConfigConstant.COMMON_DEFAULT_AVATAR);
//                    }
//                }

                    // 设置年龄，身高，婚姻状况为展示基本信息
                    StringBuilder stringBuilder = new StringBuilder();
                    int age = userBase.getAge();
                    if (age < 18) {
                        age = 18;
                    }
                    stringBuilder.append(age);
//                        .append(getString(R.string.years));
                    String heightCm = userBase.getHeightCm();
                    if (!TextUtils.isEmpty(heightCm)) {
                        stringBuilder.append(" | ")
                                .append(HeightUtils.getInchCmByCm(heightCm));
                    }
                    if (ParamsUtils.getMarriageMap() != null) {
                        String marriage = ParamsUtils.getMarriageMap().get(String.valueOf(userBase.getMaritalStatus()));
                        if (!TextUtils.isEmpty(marriage)) {
                            if (!TextUtils.isEmpty(heightCm)) {
                                stringBuilder.append(" | ");
                            }
                            stringBuilder.append(marriage);
                        }
                    }

                    mTvBaseInfo.setText(stringBuilder.toString());
                    name.setText(userBase.getNickName());
                    // 内心独白
                    String monologue = userBase.getMonologue();
                    if (!TextUtils.isEmpty(monologue)) {
                        mMonologue.setText(monologue);
                        mLlMonologue.setVisibility(View.VISIBLE);
                    } else {
                        mLlMonologue.setVisibility(View.GONE);
                    }
                    // 国家
                    String country = userBase.getCountry();
                    if ("America".equals(country)) {
                        country = "United States";
                    }
                    if (country.contains(" ")) {
                        country = country.replaceAll(" ", "");
                    }
                    for (int i = 0; i < Cheeses.EN_GOUNTRY.length; i++) {
                        if (country.equals(Cheeses.EN_GOUNTRY[i])) {
                            country = Cheeses.EN_GOUNTRY[i];
                        }
                    }
                    setTextInfo(mLlCountry, mTvCountry, mCountryDivider, country);
                    // 地区
                    setTextInfo(mLlArea, mTvArea, mAreaDivider, userBase.getArea().getProvinceName());
                    // 星座
                    setTextInfo(mLlSign, mTvSign, mSignDivider, ParamsUtils.getConstellationMap().get(String.valueOf(userBase.getSign())));
                    // 学历
//                setTextInfo(mLlEducation, mTvEducation, mEducationDivider, ParamsUtils.getEducationMap().get(String.valueOf(userBase.getEducation())));
                    // 收入
                    setTextInfo(mLlIncome, mTvIncome, mIncomeDivider, ParamsUtils.getIncomeMap().get(String.valueOf(userBase.getIncome())));
                    // 职业
                    setTextInfo(mLlOccupation, mTvOccupation, mOccupationDivider, ParamsUtils.getWorkMap().get(String.valueOf(userBase.getOccupation())));
                }
                // 设置照片墙
                setUserPicWall(user.getListImage());
                // 设置用户详细信息
                setUserDetailInfo(user);
            }
        }
    }

    /**
     * 设置用户照片墙
     */
    private void setUserPicWall(List<Image> imageList) {
        if (!Util.isListEmpty(imageList) && mImageWallAdapter != null) {
            mImageWallAdapter.replaceAll(imageList);
        }
    }

    /**
     * 设置用户详细资料
     */
    private void setUserDetailInfo(User user) {
        // 是否想要小孩
        setTextInfo(mLlWantBaby, mTvWantBaby, mWantBabyDivider, ParamsUtils.getWantBabyMap().get(String.valueOf(user.getWantsKids())));
        // 喜欢的运动
        setTextInfo(mLlSport, mTvSport, mSportDivider, getValue(user.getSports(), ParamsUtils.getSportMap()));
        // 喜欢的宠物
        setTextInfo(mLlPets, mTvPets, mPetsDivider, getValue(user.getPets(), ParamsUtils.getPetsMap()));
        // 种族
        setTextInfo(mLlEthnicity, mTvEthnicity, mEthnicityDivider, ParamsUtils.getEthnicityMap().get(user.getEthnicity()));
        // 锻炼习惯
        setTextInfo(mLlExerciseHabits, mTvExerciseHabits, mExerciseHabitsDivider, ParamsUtils.getExerciseHabitsMap().get(String.valueOf(user.getExerciseHabits())));
        // 兴趣爱好
        setTextInfo(mLlHobby, mTvHobby, mHobbyDivider, getValue(user.getListHobby(), ParamsUtils.getInterestMap()));
    }

    private void setTextInfo(LinearLayout llContainer, TextView tvInfo, View dividerView, String text) {
        if (!TextUtils.isEmpty(text) && !"null".equals(text) && !"".equals(text)) {
            tvInfo.setText(text);
            llContainer.setVisibility(View.VISIBLE);
            dividerView.setVisibility(View.VISIBLE);
        } else {
            llContainer.setVisibility(View.GONE);
            dividerView.setVisibility(View.GONE);
        }
    }

    private String getValue(String key, Map<String, String> map) {
        if (key != null) {
            String[] spit = key.split("\\|");
            StringBuffer value = new StringBuffer();
            for (int i = 0; i < spit.length; i++) {
                if (i < spit.length - 1) {
                    value.append(map.get(spit[i]) + ",");
                } else {
                    value.append(map.get(spit[i]));
                }
            }
            return value.toString();
        }
        return null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    private class ImageWallAdapter extends CommonRecyclerViewAdapter<Image> {

        public ImageWallAdapter(Context context, int layoutResId) {
            this(context, layoutResId, null);
        }

        public ImageWallAdapter(Context context, int layoutResId, List<Image> list) {
            super(context, layoutResId, list);
        }

        @Override
        protected void convert(final int position, RecyclerViewHolder viewHolder, Image bean) {
            final ImageView ivPic = (ImageView) viewHolder.getView(R.id.item_user_pic);
            ImageView ivLock = (ImageView) viewHolder.getView(R.id.iv_small_lock);
            ivLock.setVisibility(View.GONE);
            if (bean != null) {
                final String imgUrl = bean.getImageUrl();
                if (!TextUtils.isEmpty(imgUrl)) {

//                    if(position==0){
//                        if(isFirst){
//                            isFirst=false;
//                            File avatarFile = new File(IConfigConstant.LOCAL_AVATAR_PATH);
//                            if (avatarFile != null && avatarFile.exists()) {
////            ImageLoaderUtil.getInstance().loadCircleImage(getActivity(), head_icon, avatarFile);
//                                ImageLoaderUtil.getInstance().loadCircleImage(getActivity(), ivPic, avatarFile);
//                            }else{
//                                ivPic.setImageResource(IConfigConstant.COMMON_DEFAULT_AVATAR_CIRCLE);
//                            }
//                        }
//
//                    }else{
                    showImage(ivPic, imgUrl);
//                    }


                } else {
                    ivPic.setImageResource(IConfigConstant.COMMON_DEFAULT_AVATAR_CIRCLE);
//                    if(position!=0 && interceptLock){
//                        returnBitmap(imgUrl, ivPic);
//                    }
                    Log.v("Tian==","url地址为空");

                }
            } else {
                ivPic.setImageResource(IConfigConstant.COMMON_DEFAULT_AVATAR_CIRCLE);
                Log.v("Tian==","bean地址为空");

            }
        }

        private void showImage(final ImageView ivPic, final String imgUrl) {

            ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder()
                    .url(imgUrl).placeHolder(IConfigConstant.COMMON_DEFAULT_AVATAR_CIRCLE).transform(new CircleTransformation()).imageView(ivPic).build(), new ImageLoaderUtil.OnImageLoadListener() {
                @Override
                public void onCompleted() {
                    Log.v("Tian==","显示成功");
                }

                @Override
                public void onFailed() {
                    Log.v("Tian==","显示失败");
                        showImage(ivPic, imgUrl);
                }
            });
        }
    }

    private void loadUserDetailInfo(final boolean setAvatar) {
        OkHttpUtils.post()
                .url(IUrlConstant.URL_GET_USER_INFO)
                .addHeader("token", PlatformInfoXml.getToken())
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .addParams("uid", TextUtils.isEmpty(uId) ? "" : uId)
                .addParams("isRecord", "0")
                .addParams("sourceTag", "1")
                .build()
                .execute(new Callback<UserDetail>() {
                    @Override
                    public UserDetail parseNetworkResponse(Response response, int id) throws Exception {
                        String resultJson = response.body().string();
                        if (!TextUtils.isEmpty(resultJson)) {
                            return JSON.parseObject(resultJson, UserDetail.class);
                        }
                        return null;
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        stopRefresh(3);
                    }

                    @Override
                    public void onResponse(final UserDetail response, int id) {
                        if (response != null) {
                            String isSucceed = response.getIsSucceed();
                            if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
                               setUserInfo(response.getUserEnglish(),setAvatar);

                            }
                        }
                        stopRefresh(1);
                    }
                });
    }

    private void stopRefresh(int seconds) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dismissLoading();
            }
        }, seconds * 1000);
    }


    @Override
    protected void doRegister() {
        EventBus.getDefault().register(this);
    }

    @Override
    protected void unregister() {
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onEvent(UpdatePayInfoEvent event) {
        buy_bean.setSelected(UserInfoXml.isBeanUser());
        buy_month.setSelected(UserInfoXml.isMonthly());
        /**去除读信拦截**/
        SharedPreferenceUtil.setBooleanValue(Utils.getContext(), "INTERCEPT_DIALOG", "DIALOG", false);

    }
    @Subscribe
    public void onEvent(SetAvatarEvent event) {
        File wallrFile = new File(IConfigConstant.LOCAL_wall_PATH);
        if (wallrFile != null && wallrFile.exists()) {
            ImageLoaderUtil.getInstance().loadLocalImage(getActivity(), mIvUserAvatar, wallrFile);
        } else {
            String imgUrl = UserInfoXml.getAvatarUrl();
            if (!TextUtils.isEmpty(imgUrl)) {
                ImageLoaderUtil.getInstance().loadImage(getActivity(), new ImageLoader.Builder()
                        .url(imgUrl).imageView(mIvUserAvatar).build());
            }
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                File wallrFile = new File(IConfigConstant.LOCAL_wall_PATH);
                if (wallrFile != null && wallrFile.exists()) {
                    ImageLoaderUtil.getInstance().loadLocalImage(getActivity(), mIvUserAvatar, wallrFile);
                } else {
                    String imgUrl = UserInfoXml.getAvatarUrl();
                    if (!TextUtils.isEmpty(imgUrl)) {
                        ImageLoaderUtil.getInstance().loadImage(getActivity(), new ImageLoader.Builder()
                                .url(imgUrl).imageView(mIvUserAvatar).build());
                    }
                }
            }
        },2000);
    }


    @Subscribe
    public void onEvent(UpdateUserInfoEvent event) {
        CommonRequestUtil.loadUserInfo(new CommonRequestUtil.OnLoadUserInfoListener() {
            @Override
            public void onSuccess(User user) {
                setMyInfo();
                loadUserDetailInfo(false);
            }

            @Override
            public void onFail() {

            }
        });

    }

    //    更新用户信息
    public void updateUserInfo() {
        CommonRequestUtil.loadUserInfo(new CommonRequestUtil.OnLoadUserInfoListener() {
            @Override
            public void onSuccess(User user) {
                setMyInfo();
                loadUserDetailInfo(false);
            }

            @Override
            public void onFail() {

            }
        });
    }

    //    获取由注册引导产生的自我介绍
    private void getSelfIntroduce() {
        int answer_1 = SharedPreferenceUtil.getIntValue(getActivity(), "QUESTION", "answer_1", 0);
        int answer_2 = SharedPreferenceUtil.getIntValue(getActivity(), "QUESTION", "answer_2", 0);
        int answer_3 = SharedPreferenceUtil.getIntValue(getActivity(), "QUESTION", "answer_3", 0);
        int answer_4 = SharedPreferenceUtil.getIntValue(getActivity(), "QUESTION", "answer_4", 0);
        int answer_5 = SharedPreferenceUtil.getIntValue(getActivity(), "QUESTION", "answer_5", 0);
        String answer1;
        String answer2;
        String answer3;
        String answer4;
        String answer5;
        String answer6;
        if (answer_1 == 0) {
            answer1 = getString(R.string.result1_a);
        } else if (answer_1 == 1) {
            answer1 = getString(R.string.result1_b);
        } else {
            answer1 = getString(R.string.result1_c);
        }
        if (answer_2 == 0) {
            answer2 = getString(R.string.result2_a);
        } else if (answer_2 == 1) {
            answer2 = getString(R.string.result2_b);
        } else {
            answer2 = "";
        }
        if (answer_3 == 0) {
            answer3 = getString(R.string.result3_a);
        } else if (answer_3 == 1) {
            answer3 = getString(R.string.result3_b);
        } else {
            answer3 = "";
        }
        if (answer_4 == 0) {
            answer4 = getString(R.string.result4_a);
        } else if (answer_4 == 1) {
            answer4 = getString(R.string.result4_b);
        } else {
            answer4 = getString(R.string.result4_c);
        }
        if (answer_5 == 0) {
            answer5 = getString(R.string.result5_a);
        } else if (answer_5 == 1) {
            answer5 = getString(R.string.result5_b);
        } else if (answer_5 == 2) {
            answer5 = getString(R.string.result5_c);
        } else {
            answer5 = getString(R.string.result5_d);
        }
        answer6 = getString(R.string.result);
        rlIntroduce.setVisibility(View.GONE);
//        tvIntroduce.setText(answer1 + answer2 + answer3 + answer4 + answer5 + answer6);


    }

//    @Subscribe
//    public void onEvent(UpdateUserInfoEvent event) {
//        // 加载用户详情
//        showLoading();
//        loadUserDetailInfo();
//
//    }

    //    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        // TODO: inflate a fragment view
//        View rootView = super.onCreateView(inflater, container, savedInstanceState);
//        slideMenu = (SlideMenu) View.inflate(getActivity(), R.layout.fragment_me, null);
//        my_linearlayout.setSlideMenu(slideMenu);
//        unbinder = ButterKnife.bind(this, rootView);
//        uId = UserInfoXml.getUID();
////        initViewsAndVariables();
////        addListeners();
//        // 初始化照片墙RecyclerView
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
//        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
//        mPicRecyclerView.setLayoutManager(linearLayoutManager);
//        mImageWallAdapter = new ImageWallAdapter(getActivity(), R.layout.item_pic_wall);
//        // 添加FootView
//        ImageView headView = new ImageView(getActivity());
//        ViewGroup.MarginLayoutParams layoutParams = new ViewGroup.MarginLayoutParams(DensityUtil.dip2px(getActivity(), 60), DensityUtil.dip2px(getActivity(), 60));
//        int margin = DensityUtil.dip2px(getActivity(), 5);
//        layoutParams.setMargins(margin, margin, margin, margin);
//        headView.setLayoutParams(layoutParams);
//        headView.setImageResource(R.drawable.upload_photo_me);
//
//        mImageWallAdapter.addHeaderView(headView, false);
//
//        mPicRecyclerView.setAdapter(mImageWallAdapter);
//
////        addListeners();
//        // 加载用户详情
//        showLoading();
//        loadUserDetailInfo();
//        return rootView;
//    }
//更新照片
    private void uploadImage(File file) {
        showLoading();
        CommonRequestUtil.uploadImage(false, file, false, new CommonRequestUtil.OnUploadImageListener() {
            @Override
            public void onSuccess(final Image image) {
                if (image != null) {
                    // 由于上传图片为异步操作，延时加载上传后图片
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mImageList.add(image);
                            mImageWallAdapter.replaceAll(mImageList);
                            if(mImageList.size()==1){
//                                如果只有一张照片，设置为头像
//                                删除本地缓存
                                CommonRequestUtil.setAvatar(image.getId(), true, new CommonRequestUtil.OnCommonListener() {
                                    @Override
                                    public void onSuccess() {
                                        FileUtil.deleteFile(IConfigConstant.LOCAL_AVATAR_PATH);
                                        FileUtil.deleteFile(IConfigConstant.LOCAL_wall_PATH);
                                        SharedPreferenceUtil.setBooleanValue(getActivity(),"mIvUserAvatar","get_mIvUserAvatar",true);

                                        new Thread(new Runnable() {
                                            @Override
                                            public void run() {
                                                FileUtil.downWallFile(image.imageUrl, BaseApplication.getGlobalContext().
                                                        getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath(), "wall.jpg",false);

                                                updateUserInfo();

                                            }
                                        }).start();


                                    }

                                    @Override
                                    public void onFail() {
                                    }
                                });

                            }
                        }
                    }, 1 * 1000);
                }
                dismissLoading();
            }

            @Override
            public void onFail() {
                dismissLoading();
            }
        });
    }

    private void getPhotoList() {
        showLoading();
        CommonRequestUtil.loadUserInfo(new CommonRequestUtil.OnLoadUserInfoListener() {
            @Override
            public void onSuccess(User user) {
                if (user != null) {
                    mImageList = user.getListImage();
                    if (!Util.isListEmpty(mImageList)) {
                        mImageWallAdapter.replaceAll(mImageList);
                    }
                }
                dismissLoading();
            }

            @Override
            public void onFail() {
                dismissLoading();
            }
        });
    }

//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        EventBus.getDefault().register(this);
//        setContentView(R.layout.activity_user_info);
//        ButterKnife.bind(this);
//        // 透明状态栏
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {// 5.0及以上
//            View decorView = getWindow().getDecorView();
//            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
//            // 设置状态栏背景
//            getWindow().setStatusBarColor(Color.TRANSPARENT);
//        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {// 4.4
//            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//        }
//        setSupportActionBar(mToolbar);
//        ActionBar actionBar = getSupportActionBar();
//        if (actionBar != null) {
//            actionBar.setDisplayShowHomeEnabled(true);
//        }
//
//        Intent intent = getIntent();
//        if (intent != null) {
//            uId = intent.getStringExtra(INTENT_KEY_UID);
//            isRecord = intent.getStringExtra(INTENT_KEY_ISRECORD);
//            sourceTag = intent.getStringExtra(INTENT_KEY_SOURCETAG);
//        }
//        uId=UserInfoXml.getUID();
//
//        // 初始化照片墙RecyclerView
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
//        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
//        mPicRecyclerView.setLayoutManager(linearLayoutManager);
//        mImageWallAdapter = new ImageWallAdapter(getActivity(), R.layout.item_pic_wall);
//        mPicRecyclerView.setAdapter(mImageWallAdapter);
//
////        addListeners();
//        // 加载用户详情
//        showLoading();
//        loadUserDetailInfo();
//    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        if (!Util.isListEmpty(mImageList)) {
            mImageList.clear();
            mImageList = null;
        }
    }

}
