package com.datelove.online.International.fragment;

import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;

import com.alibaba.fastjson.JSON;
import com.datelove.online.International.R;
import com.datelove.online.International.activity.BuyServiceActivity;
import com.datelove.online.International.activity.UserInfoDetailActivity;
import com.datelove.online.International.adapter.SwipeFlingViewAdapter;
import com.datelove.online.International.base.BaseTitleFragment;
import com.datelove.online.International.bean.Fate;
import com.datelove.online.International.bean.FateUser;
import com.datelove.online.International.bean.UserBase;
import com.datelove.online.International.constant.IConfigConstant;
import com.datelove.online.International.constant.IUrlConstant;
import com.datelove.online.International.event.InterceptDialogEvent;
import com.datelove.online.International.event.InterceptEvent;
import com.datelove.online.International.event.SayHelloEvent;
import com.datelove.online.International.event.UpdateUserProvince;
import com.datelove.online.International.haoping.ActionSheetDialog;
import com.datelove.online.International.haoping.OnOperItemClickL;
import com.datelove.online.International.utils.CommonRequestUtil;
import com.datelove.online.International.utils.Utils;
import com.datelove.online.International.xml.PlatformInfoXml;
import com.flingswipe.FlingCardListener;
import com.flingswipe.SwipeFlingAdapterView;
import com.library.utils.SharedPreferenceUtil;
import com.library.utils.Util;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.Callback;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.Call;
import okhttp3.Response;

/**
 * 缘分页左右滑动卡片
 * Created by zhangdroid on 2016/8/11.
 */
public class FateCardFragment extends BaseTitleFragment implements SwipeFlingAdapterView.onFlingListener {
    @BindView(R.id.fragment_fate_SwipeView)
    SwipeFlingAdapterView mSwipeFlingAdapterView;
    @BindView(R.id.fragment_fate_dislike)
    ImageView mIvDislike;
    @BindView(R.id.fragment_fate_like)
    ImageView mIvLike;
    private SwipeFlingViewAdapter mSwipeFlingViewAdapter;
    private int pageNum = 1;
    private final String pageSize = "20";
    private int count;
    private boolean isFirst=true;
    private boolean getIntercept=false;
    private int count1;
    private boolean isIntercept=true;
    private boolean scrollIntercept=false;
    private String country;
    private int i=0;
    private long randomNum=1;


    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_fate_card;
    }

    @Override
    protected void initViewsAndVariables() {

        setTitle(getString(R.string.tab_fate));
        mSwipeFlingViewAdapter = new SwipeFlingViewAdapter(getActivity(), R.layout.item_swipefling_cardview);
        mSwipeFlingAdapterView.setAdapter(mSwipeFlingViewAdapter);
         country = PlatformInfoXml.getPlatformInfo().getCountry().trim();
        // 加载缘分列表
        setBtnVisibility(false);
        showLoading();
        loadYuanFenData(false);

    }

    /**
     * 设置是否显示喜欢/不喜欢按钮
     *
     * @param isVisible
     */
    private void setBtnVisibility(boolean isVisible) {
        if(mIvDislike!=null && mIvLike!=null){
            mIvDislike.setVisibility(isVisible ? View.VISIBLE : View.GONE);
            mIvLike.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        }

    }

    @Override
    protected void addListeners() {
        mSwipeFlingAdapterView.setFlingListener(this);
        mSwipeFlingAdapterView.setOnItemClickListener(new SwipeFlingAdapterView.OnItemClickListener() {

            @Override
            public void onItemClicked(int itemPosition, Object dataObject) {
                FateUser fateUser = (FateUser) dataObject;
                if (fateUser != null) {
                    UserInfoDetailActivity.toUserInfoDetailActivity(getActivity(), fateUser.getUserBaseEnglish().getId(), null, UserInfoDetailActivity.SOURCE_FROM_FATE);
                }
            }
        });
    }

    @OnClick({R.id.fragment_fate_dislike, R.id.fragment_fate_like})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fragment_fate_dislike:// 左滑不感兴趣
                if (mSwipeFlingAdapterView != null) {
//                    设置美国不拦截
                    if(!(country.equals(getString(R.string.united_states).trim()) || country.equals("America"))){
                        count++;
                        /**加载读信拦截对话框**/
                        boolean isIntercept = SharedPreferenceUtil.getBooleanValue(Utils.getContext(), "INTERCEPT_DIALOG", "DIALOG", true);
                        if(count>=10 && isIntercept){
//                        弹出拦截对话框
                            interceptDialog();
                            Log.v("interceptDialog()","143");


                        }else{
                            View selectedView = mSwipeFlingAdapterView.getSelectedView();
                            if (selectedView != null) {
                                ImageView ivDislike = (ImageView) selectedView.findViewById(R.id.item_swipefling_dislike);
                                ivDislike.setAlpha(1f);
                            }
                            FlingCardListener flingCardListener = mSwipeFlingAdapterView.getTopCardListener();
                            if (flingCardListener != null) {
                                flingCardListener.selectLeft();
                            }
                        }
                    }else{
                        View selectedView = mSwipeFlingAdapterView.getSelectedView();
                        if (selectedView != null) {
                            ImageView ivDislike = (ImageView) selectedView.findViewById(R.id.item_swipefling_dislike);
                            ivDislike.setAlpha(1f);
                        }
                        FlingCardListener flingCardListener = mSwipeFlingAdapterView.getTopCardListener();
                        if (flingCardListener != null) {
                            flingCardListener.selectLeft();
                        }
                    }


                }
                break;

            case R.id.fragment_fate_like:// 右滑喜欢
                // 打招呼
 //             设置一部分拦截
                if(!(country.equals(getString(R.string.united_states).trim()) || country.equals("America"))){
                    count++;
                    /**加载读信拦截对话框**/
                    boolean isIntercept = SharedPreferenceUtil.getBooleanValue(Utils.getContext(), "INTERCEPT_DIALOG", "DIALOG", true);
                    if(count>=10 && isIntercept){
//                        弹出拦截对话框
                        interceptDialog();
                        Log.v("interceptDialog()","183");

                    }else {
                        FateUser fateUser = (FateUser) mSwipeFlingAdapterView.getSelectedItem();
                        if (fateUser != null) {
                            UserBase userBase = fateUser.getUserBaseEnglish();
                            if (userBase != null) {
                                sayHello(userBase.getId());
                            }
                        }
                        if (mSwipeFlingAdapterView != null) {
                            View selectedView = mSwipeFlingAdapterView.getSelectedView();
                            if (selectedView != null) {
                                ImageView ivLike = (ImageView) selectedView.findViewById(R.id.item_swipefling_like);
                                ivLike.setAlpha(1f);
                            }
                            FlingCardListener flingCardListener = mSwipeFlingAdapterView.getTopCardListener();
                            if (flingCardListener != null) {
                                flingCardListener.selectRight();
                            }
                        }
                    }
                }else{
                    FateUser fateUser = (FateUser) mSwipeFlingAdapterView.getSelectedItem();
                    if (fateUser != null) {
                        UserBase userBase = fateUser.getUserBaseEnglish();
                        if (userBase != null) {
                            sayHello(userBase.getId());
                        }
                    }
                    if (mSwipeFlingAdapterView != null) {
                        View selectedView = mSwipeFlingAdapterView.getSelectedView();
                        if (selectedView != null) {
                            ImageView ivLike = (ImageView) selectedView.findViewById(R.id.item_swipefling_like);
                            ivLike.setAlpha(1f);
                        }
                        FlingCardListener flingCardListener = mSwipeFlingAdapterView.getTopCardListener();
                        if (flingCardListener != null) {
                            flingCardListener.selectRight();
                        }
                    }
                }

                break;
        }
    }

    public void interceptDialog() {
        if(isIntercept){
            isIntercept=false;
            SharedPreferenceUtil.setBooleanValue(Utils.getContext(), "INTERCEPT_DIALOG", "INTERCEPT", true);
            i++;
//            if(i==1 || i>4){
                boolean isSetIntercept = SharedPreferenceUtil.getBooleanValue(Utils.getContext(), "INTERCEPT_DIALOG", "DIALOG", true);
                if(isSetIntercept){
                    scrollIntercept=true;
                }else{
                    scrollIntercept=false;
                }
            final String[] stringItems = {getString(R.string.buy_vip), getString(R.string.buy_vip_service)};
            final ActionSheetDialog dialog = new ActionSheetDialog(getActivity(), stringItems, mIvDislike);
                mSwipeFlingAdapterView.setIntercept();

                dialog.isTitleShow(false).show();
                dialog.setCanceledOnTouchOutside(false);
                dialog.setOnOperItemClickL(new OnOperItemClickL() {
                    @Override
                    public void onOperItemClick(AdapterView<?> parent, View view, int position, long id) {
                        if (position == 1 || position == 0) {
//                    展示十个用户，之后跳转到付费界面
                            isIntercept=true;
                            BuyServiceActivity.toBuyServiceActivity(getActivity(), BuyServiceActivity.INTENT_FROM_INTERCEPT,
                                    IConfigConstant.PAY_TAG_FROM_CHAT, IConfigConstant.PAY_WAY_ALL);
                        }
                        dialog.dismiss();
                    }
                });
//            }

        }


    }



    private void loadYuanFenData(final boolean getProvince) {
        OkHttpUtils.post()
                .url(IUrlConstant.URL_FATE)
                .addHeader("token", PlatformInfoXml.getToken())
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .addParams("pageNum", String.valueOf(pageNum))
                .addParams("randomNum", String.valueOf(randomNum))


                .addParams("pageSize", pageSize)
                .build()
                .execute(new Callback<Fate>() {
                    @Override
                    public Fate parseNetworkResponse(Response response, int id) throws Exception {
                        String resultJson = response.body().string();
                        if (!TextUtils.isEmpty(resultJson)) {
                            return JSON.parseObject(resultJson, Fate.class);
                        }
                        return null;
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        if (mSwipeFlingViewAdapter != null && mSwipeFlingViewAdapter.isEmpty()) {
                            setBtnVisibility(false);
                        }
                        loadYuanFenData(false);
                        stopRefresh(3);
                    }

                    @Override
                    public void onResponse(Fate response, int id) {
                        if (response != null) {
                            String isSucceed = response.getIsSucceed();
                            if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
                                 randomNum = response.getRandomNum();
                                List<FateUser> list = response.getListUser();
                                if (!Util.isListEmpty(list)) {
                                    setBtnVisibility(true);
                                    if (mSwipeFlingViewAdapter != null) {
                                        if (pageNum == 1) {
                                            mSwipeFlingViewAdapter.replaceAll(list);
                                        } else {
                                            mSwipeFlingViewAdapter.appendToList(list);
                                        }
                                    }
                                    if(getProvince) {
                                        if (mSwipeFlingAdapterView != null) {
                                            View selectedView = mSwipeFlingAdapterView.getSelectedView();
                                            if (selectedView != null) {
                                                ImageView ivDislike = (ImageView) selectedView.findViewById(R.id.item_swipefling_dislike);
                                                ivDislike.setAlpha(1f);
                                            }
                                            FlingCardListener flingCardListener = mSwipeFlingAdapterView.getTopCardListener();
                                            if (flingCardListener != null) {
                                                flingCardListener.selectLeft();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        stopRefresh(1);
                    }
                });
    }

    /**
     * 隐藏加载对话框
     */
    private void stopRefresh(int secs) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dismissLoading();
            }
        }, secs * 1000);
    }

    @Override
    public void removeFirstObjectInAdapter() {
        if (mSwipeFlingViewAdapter != null) {
            mSwipeFlingViewAdapter.removeItem(0);
        }
    }

    @Override
    public void onLeftCardExit(Object dataObject) {
        // 拉黑，不喜欢
/*        FateUser FateUser = (FateUser) dataObject;
        if (FateUser != null) {

            UserBase userBase = FateUser.getUserBaseEnglish();
            if (userBase != null) {
                CommonRequestUtil.pull2Black(userBase.getId(), false, new CommonRequestUtil.OnPull2BlackListener() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onFail() {

                    }
                });
            }
        }*/
    }

    @Override
    public void onRightCardExit(Object dataObject) {
        // 打招呼
        FateUser FateUser = (FateUser) dataObject;

        if (FateUser != null) {
            UserBase userBase = FateUser.getUserBaseEnglish();
            if (userBase != null) {
                sayHello(userBase.getId());
            }
        }
    }

    @Override
    public void onAdapterAboutToEmpty(int itemsInAdapter) {
        if (itemsInAdapter == 9) {// 剩余10条数据时加载更多
            pageNum++;
            loadYuanFenData(false);
        }
    }

    @Override
    public void onScroll(float scrollProgressPercent) {
                if (mSwipeFlingAdapterView != null) {
                    if(scrollIntercept){
                        interceptDialog();
                        Log.v("interceptDialog()","400");

                    }
                    View selectedView = mSwipeFlingAdapterView.getSelectedView();
                    if (selectedView != null) {
                        // 右滑
                        ImageView ivLike = (ImageView) selectedView.findViewById(R.id.item_swipefling_like);
                        ivLike.setAlpha(scrollProgressPercent > 0 ? scrollProgressPercent : 0);
                        // 左滑
                        ImageView ivDislike = (ImageView) selectedView.findViewById(R.id.item_swipefling_dislike);
                        ivDislike.setAlpha(scrollProgressPercent < 0 ? -scrollProgressPercent : 0);

                    }
                }
//            }
//        }

    }

    private void sayHello(String userId) {
        CommonRequestUtil.sayHello(getActivity(), userId, CommonRequestUtil.SAY_HELLO_TYPE_FATE,
                true, new CommonRequestUtil.OnCommonListener() {

                    @Override
                    public void onSuccess() {
                        // 发送事件，更新状态
                        EventBus.getDefault().post(new SayHelloEvent());

                    }

                    @Override
                    public void onFail() {

                    }
                });
    }
    @Subscribe
    public void onEvent(UpdateUserProvince event) {
        // 加载缘分列表
        setBtnVisibility(false);
        loadYuanFenData(true);
    }
    @Subscribe
    public void onEvent(InterceptDialogEvent event) {
        count=11;
        interceptDialog();
        Log.v("interceptDialog()","445");
    }
    @Subscribe
    public void onEvent(InterceptEvent event) {
        isIntercept=true;
    }

    @Override
    protected void doRegister() {
        EventBus.getDefault().register(this);
    }

    @Override
    protected void unregister() {
        EventBus.getDefault().unregister(this);
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }




}
