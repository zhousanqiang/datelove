package com.datelove.online.International.activity;

import android.os.Handler;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.datelove.online.International.R;
import com.datelove.online.International.adapter.MyPhotoAdapter;
import com.datelove.online.International.base.BaseTitleActivity;
import com.datelove.online.International.bean.Image;
import com.datelove.online.International.bean.User;
import com.datelove.online.International.constant.IConfigConstant;
import com.datelove.online.International.dialog.AlbumMoreDialog;
import com.datelove.online.International.event.UpdateUserInfoEvent;
import com.datelove.online.International.utils.CommonRequestUtil;
import com.datelove.online.International.utils.FileUtil;
import com.library.adapter.recyclerview.CommonRecyclerViewAdapter;
import com.library.adapter.recyclerview.RecyclerViewHolder;
import com.library.utils.DensityUtil;
import com.library.utils.Util;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 我的相册
 * Modified by zhangdroid on 2017/03/02.
 */
public class MyPhotoActivity extends BaseTitleActivity {
    @BindView(R.id.myPhoto_recyclerview)
    RecyclerView mPhotoRecyclerView;

    private MyPhotoAdapter mMyPhotoAdapter;
    private List<Image> mImageList = new ArrayList<>();

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_my_photo;
    }

    @Override
    protected String getCenterTitle() {
        return getString(R.string.my_photo);
    }

    @Override
    protected void initViewsAndVariables() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3);
        gridLayoutManager.setOrientation(GridLayoutManager.VERTICAL);
        mPhotoRecyclerView.setLayoutManager(gridLayoutManager);
        mMyPhotoAdapter = new MyPhotoAdapter(this, R.layout.item_photo);
        // 添加HeadView
        ImageView headView = new ImageView(this);
        ViewGroup.MarginLayoutParams layoutParams = new ViewGroup.MarginLayoutParams(DensityUtil.dip2px(this, 120), DensityUtil.dip2px(this, 120));
        int margin = DensityUtil.dip2px(this, 5);
        layoutParams.setMargins(margin, margin, margin, margin);
        headView.setLayoutParams(layoutParams);
        headView.setImageResource(R.drawable.upload_photo);
        mMyPhotoAdapter.addHeaderView(headView, false);
        mPhotoRecyclerView.setAdapter(mMyPhotoAdapter);
        // 加载图片列表
        getPhotoList();
    }

    @Override
    protected void addListeners() {
        mMyPhotoAdapter.setOnHeaderViewClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetPhotoActivity.toGetPhotoActivity(MyPhotoActivity.this, IConfigConstant.LOCAL_PIC_PATH, new GetPhotoActivity.OnGetPhotoListener() {
                    @Override
                    public void getSelectedPhoto(File file) {
                        if (file != null) {
                            uploadImage(file);
                            // 发送事件，更新头像
                            EventBus.getDefault().post(new UpdateUserInfoEvent());
                        }
                    }
                });
            }
        });
        mMyPhotoAdapter.setOnItemClickListener(new CommonRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, RecyclerViewHolder viewHolder) {
                PictureBrowseActivity.toPictureBrowseActivity(MyPhotoActivity.this, position, mImageList,true);
            }
        });
        mMyPhotoAdapter.setOnItemLongClickListener(new CommonRecyclerViewAdapter.OnItemLongClickListener() {
            @Override
            public void onItemLongClick(View view, final int position, RecyclerViewHolder viewHolder) {
                final Image image = mMyPhotoAdapter.getItemByPosition(position);
                AlbumMoreDialog.newInstance(new AlbumMoreDialog.OnAlbumMoreClickLister() {
                    @Override
                    public void setToAvatar() {
                        CommonRequestUtil.setAvatar(image.getId(), true, new CommonRequestUtil.OnCommonListener() {
                            @Override
                            public void onSuccess() {
                                // 先删除本地缓存的头像
                                FileUtil.deleteFile(IConfigConstant.LOCAL_AVATAR_PATH);
                                // 发送事件，更新头像
                                EventBus.getDefault().post(new UpdateUserInfoEvent());
                            }

                            @Override
                            public void onFail() {
                            }
                        });
                    }

                    @Override
                    public void deleteImage() {
                        CommonRequestUtil.deleteImage(image.getId(), true, new CommonRequestUtil.OnCommonListener() {
                            @Override
                            public void onSuccess() {
                                if (mMyPhotoAdapter != null) {
                                    mMyPhotoAdapter.removeItem(position);
                                }
                            }

                            @Override
                            public void onFail() {
                            }
                        });
                    }
                }).show(getSupportFragmentManager(), "album");
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (!Util.isListEmpty(mImageList)) {
            mImageList.clear();
            mImageList = null;
        }
    }

    private void getPhotoList() {
        showLoading();
        CommonRequestUtil.loadUserInfo(new CommonRequestUtil.OnLoadUserInfoListener() {
            @Override
            public void onSuccess(User user) {
                if (user != null) {
                    mImageList = user.getListImage();
                    if (!Util.isListEmpty(mImageList)) {
                        mMyPhotoAdapter.replaceAll(mImageList);
                    }
                }
                dismissLoading();
            }

            @Override
            public void onFail() {
                dismissLoading();
            }
        });
    }

    private void uploadImage(File file) {
        showLoading();
        CommonRequestUtil.uploadImage(false, file, false, new CommonRequestUtil.OnUploadImageListener() {
            @Override
            public void onSuccess(final Image image) {
                if (image != null) {
                    // 由于上传图片为异步操作，延时加载上传后图片
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mImageList.add(image);
                            mMyPhotoAdapter.replaceAll(mImageList);
                        }
                    }, 3 * 1000);
                }
                dismissLoading();
            }

            @Override
            public void onFail() {
                dismissLoading();
            }
        });
    }

}
