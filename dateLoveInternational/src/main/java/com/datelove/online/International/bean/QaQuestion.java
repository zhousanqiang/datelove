package com.datelove.online.International.bean;

import java.util.List;

/**
 * Created by Administrator on 2016/11/25.
 */

public class QaQuestion {
    private String id;
    private String content;
    private String strategyType;
    private List<QaAnswer> listQaAnswer;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getStrategyType() {
        return strategyType;
    }

    public void setStrategyType(String strategyType) {
        this.strategyType = strategyType;
    }

    public List<QaAnswer> getListQaAnswer() {
        return listQaAnswer;
    }

    public void setListQaAnswer(List<QaAnswer> listQaAnswer) {
        this.listQaAnswer = listQaAnswer;
    }

    @Override
    public String toString() {
        return "QaQuestion{" +
                "id='" + id + '\'' +
                ", content='" + content + '\'' +
                ", strategyType='" + strategyType + '\'' +
                ", listQaAnswer=" + listQaAnswer +
                '}';
    }
}
