package com.datelove.online.International.activity;

import android.content.Context;
import android.os.Bundle;
import android.os.Vibrator;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.alibaba.fastjson.JSON;
import com.datelove.online.International.R;
import com.datelove.online.International.base.BaseTitleActivity;
import com.datelove.online.International.bean.BaseModel;
import com.datelove.online.International.constant.IUrlConstant;
import com.datelove.online.International.utils.Utils;
import com.datelove.online.International.xml.PlatformInfoXml;
import com.library.utils.SharedPreferenceUtil;
import com.library.utils.Util;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.Callback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;
import okhttp3.Response;

public class SelfIntroduceActivity extends BaseTitleActivity {

    @BindView(R.id.et_introduce)
    EditText etIntroduce;
    @BindView(R.id.btn_upload)
    Button btnUpload;
    @BindView(R.id.rl_background)
    RelativeLayout rlBackground;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_self_introduce;
    }

    @Override
    protected String getCenterTitle() {
        return getString(R.string.write_introduction);
    }

    @Override
    protected void initViewsAndVariables() {

    }

    @Override
    protected void addListeners() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick({R.id.rl_background, R.id.btn_upload,R.id.et_introduce})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rl_background:
                rlBackground.setBackgroundDrawable(getResources().getDrawable(R.drawable.shape_introduce));
                break;
            case R.id.et_introduce:
                rlBackground.setBackgroundDrawable(getResources().getDrawable(R.drawable.shape_introduce));
                break;
            case R.id.btn_upload:
                if (etIntroduce == null || TextUtils.isEmpty(etIntroduce.getText())) {
                    rlBackground.setBackgroundDrawable(getResources().getDrawable(R.drawable.shape_introduce_bg));
                    Vibrator vibrator = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                    vibrator.vibrate(200);
                } else {
                    String content = String.valueOf(etIntroduce.getText());
                    OkHttpUtils.post()
                            .url(IUrlConstant.URL_UPLOAD_QA_WORDS)
                            .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                            .addParams("content", content)
                            .build()
                            .execute(new Callback<BaseModel>() {
                                @Override
                                public BaseModel parseNetworkResponse(Response response, int id) throws Exception {
                                    String resultJson = response.body().string();
                                    if (!TextUtils.isEmpty(resultJson)) {
                                        return JSON.parseObject(resultJson, BaseModel.class);
                                    }
                                    return null;
                                }

                                @Override
                                public void onError(Call call, Exception e, int id) {
                                    etIntroduce.setInputType(InputType.TYPE_NUMBER_VARIATION_NORMAL);
                                    Util.gotoActivity(SelfIntroduceActivity.this, UploadAudioActivity.class, true);
                                }

                                @Override
                                public void onResponse(BaseModel response, int id) {
                                    if (response != null) {
                                        String isSucceed = response.getIsSucceed();
                                        if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
//                                            ToastUtil.showShortToast(SelfIntroduceActivity.this,R.string.cancel_success);
                                            isSucceed = response.getIsSucceed();
                                            SharedPreferenceUtil.setBooleanValue(Utils.getContext(), "SELF_INTRODUCE", "self_introduce", true);

                                        }
                                    }
                                    etIntroduce.setInputType(InputType.TYPE_NUMBER_VARIATION_NORMAL);
                                    Util.gotoActivity(SelfIntroduceActivity.this, UploadAudioActivity.class, true);
                                }
                            });

                }
                break;
        }
    }
}
