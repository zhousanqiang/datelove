package com.datelove.online.International.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.design.widget.TextInputLayout;
import android.text.InputType;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.datelove.online.International.R;
import com.datelove.online.International.base.BaseFragmentActivity;
import com.datelove.online.International.bean.BaseModel;
import com.datelove.online.International.bean.Image;
import com.datelove.online.International.bean.PointBean;
import com.datelove.online.International.bean.Register;
import com.datelove.online.International.bean.UserBase;
import com.datelove.online.International.constant.CommonData;
import com.datelove.online.International.constant.IConfigConstant;
import com.datelove.online.International.constant.IUrlConstant;
import com.datelove.online.International.event.FinishEvent;
import com.datelove.online.International.event.SelectCountryEvent;
import com.datelove.online.International.utils.AppsflyerUtils;
import com.datelove.online.International.utils.Cheeses;
import com.datelove.online.International.utils.CommonRequestUtil;
import com.datelove.online.International.utils.CountryFidUtils;
import com.datelove.online.International.utils.Utils;
import com.datelove.online.International.utils.WheelView;
import com.datelove.online.International.xml.PlatformInfoXml;
import com.datelove.online.International.xml.UserInfoXml;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.library.dialog.OnSingleDialogClickListener;
import com.library.imageloader.ImageLoader;
import com.library.imageloader.ImageLoaderUtil;
import com.library.utils.DialogUtil;
import com.library.utils.SharedPreferenceUtil;
import com.library.utils.ToastUtil;
import com.library.utils.Util;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.Callback;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.Response;

/**
 * 注册页面
 * Midified by zhangdroid on 2017/03/09.
 */
public class RegisterActivity extends BaseFragmentActivity implements View.OnClickListener {
    @BindView(R.id.tv_sex)
    EditText tvSex;
    @BindView(R.id.tv_age)
    EditText tvAge;
    @BindView(R.id.tv_country)
    EditText tvCountry;
    @BindView(R.id.register)
    TextView register;
    @BindView(R.id.login)
    TextView login;
    @BindView(R.id.agreement)
    TextView mTvAgreement;
    @BindView(R.id.regist_headIcon)
    ImageView registHeadIcon;
    @BindView(R.id.tv_nickname)
    EditText tvNickname;
    @BindView(R.id.iv_loading_heart)
    ImageView ivLoadingHeart;
    @BindView(R.id.fl_loading)
    FrameLayout flLoading;
    @BindView(R.id.tv_loading)
    TextView tvLoading;
    @BindView(R.id.til_sex)
    TextInputLayout tilSex;
    @BindView(R.id.ll_sex)
    LinearLayout llSex;
    @BindView(R.id.ll_age)
    LinearLayout llAge;
    @BindView(R.id.ll_country)
    LinearLayout llCountry;
    @BindView(R.id.til_nickname)
    TextInputLayout tilNickname;
    @BindView(R.id.til_age)
    TextInputLayout tilAge;
    @BindView(R.id.til_country)
    TextInputLayout tilCountry;
//    @BindView(R.id.ll_nickname)
//    LinearLayout llNickname;
//    @BindView(R.id.ll_sex)
//    LinearLayout llSex;
//    @BindView(R.id.ll_age)
//    LinearLayout llAge;
//    @BindView(R.id.ll_country)
//    LinearLayout llCountry;

    private String gender;
    private String getCountry;
    private String locationCode;

    private List<String> list = new ArrayList<>();
    private File getFile = new File(IConfigConstant.LOCAL_AVATAR_PATH);
    private boolean getIcon = false;
    private Dialog mLoadingDialog;
    private File cropFile;
    private String birthday;
    private boolean isFirst = true;
    private File avatarFile;
    private boolean isSexDialog=true;
    private boolean isAgeDialog=true;
    private boolean isCountrySelcet=true;
    private FirebaseAnalytics mFirebaseAnalytics;


    @Override
    protected int getLayoutResId() {
        return R.layout.activity_register;
    }

    @Override
    protected void initViewsAndVariables() {
        //第三方工具Firebase的分析初始化
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        // 透明状态栏
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {// 5.0及以上
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            // 设置状态栏背景
            getWindow().setStatusBarColor(getResources().getColor(R.color.register_bg));
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {// 4.4
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        //新页面接收数据
        Bundle bundle = this.getIntent().getExtras();
        if (bundle != null) {
            getCountry = bundle.getString("getCountry");
            locationCode = bundle.getString("locationCode");
        }
        gender = "0";
//        register_male.setSelected(true);
//        register_female.setSelected(false);
        list = new ArrayList<String>();
        list.add(getString(R.string.local));
        list.add(getString(R.string.camera));

//        registHeadIcon.setImageResource(R.drawable.regist_photo);

//        禁止软键盘弹出
        tvSex.setInputType(InputType.TYPE_NULL);
        tvAge.setInputType(InputType.TYPE_NULL);
        tvCountry.setInputType(InputType.TYPE_NULL);
        tvSex.setInputType(InputType.TYPE_NULL);
        avatarFile = new File(IConfigConstant.LOCAL_AVATAR_PATH);
//        if (avatarFile != null && avatarFile.exists()) {
//            ImageLoaderUtil.getInstance().loadCircleImage(RegisterActivity.this, registHeadIcon, avatarFile);
//        } else {
            String avatarThumbnailUrl = UserInfoXml.getAvatarThumbnailUrl();
            if(TextUtils.isEmpty(avatarThumbnailUrl)){
                avatarThumbnailUrl="http://imagetw.ilove.ren/imagedataEn/201711/21/16/29/1511252981490AD89E80_c.jpg";
            }
//            ImageLoaderUtil.getInstance().loadImage(RegisterActivity.this, new ImageLoader.Builder().url(avatarThumbnailUrl).
//                    imageView(registHeadIcon).transform(new CircleTransformation()).build());
            // 下载头像，缓存到本地
//            new Thread(new Runnable() {
//                @Override
//                public void run() {
//                    String imageUrl = UserInfoXml.getAvatarUrl();
//                    if (TextUtils.isEmpty(imageUrl)) {
//                        imageUrl = "http://imagetw.ilove.ren/imagedataEn/201711/22/10/16/1511316973506A2DCE7B.jpg";
//                    }
//                        FileUtil.downloadFile(imageUrl, BaseApplication.getGlobalContext().
//                                getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath(), "avatar.jpg");
////                    FileUtil.downloadFile(imageUrl, BaseApplication.getGlobalContext().
////                            getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath(), "head.jpg");
//                }
//            }).start();
//        }


    }


    @Override
    protected void addListeners() {
//        register_male.setOnClickListener(this);
//        register_female.setOnClickListener(this);
//        tvSex.setText(getString(R.string.male));
//        tvAge.setText("24");
        login.setOnClickListener(this);
        register.setOnClickListener(this);

        tvSex.setOnClickListener(this);
        tvAge.setOnClickListener(this);
        mTvAgreement.setOnClickListener(this);
        registHeadIcon.setOnClickListener(this);
        tvCountry.setOnClickListener(this);
        tilSex.setOnClickListener(this);
        llAge.setOnClickListener(this);
        llSex.setOnClickListener(this);
        llCountry.setOnClickListener(this);


        for (int i = 0; i < Cheeses.EN_GOUNTRY.length; i++) {
            if (Cheeses.EN_GOUNTRY[i].equals(getCountry)) {
                tvCountry.setText(getCountry);
//                tvCountry.setVisibility(View.GONE);
                llCountry.setVisibility(View.GONE);
            }
        }

        tvSex.setOnTouchListener(new EtOnTouchListener());
        tvAge.setOnTouchListener(new EtOnTouchListener1());
        tvCountry.setOnTouchListener(new EtOnTouchListener2());



    }
    class EtOnTouchListener implements View.OnTouchListener {

        int touch_flag = 0;

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            touch_flag++;
            if (touch_flag == 2) {
                touch_flag = 0;
                showSexDialog();
            }
            return false;
        }
    }
    class EtOnTouchListener1 implements View.OnTouchListener {

        int touch_flag = 0;

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            touch_flag++;
            if (touch_flag == 2) {
                touch_flag = 0;
                showAgeDialog();
            }
            return false;
        }
    }
    class EtOnTouchListener2 implements View.OnTouchListener {

        int touch_flag = 0;

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            touch_flag++;
            if (touch_flag == 2) {
                touch_flag = 0;
                if(isCountrySelcet){
                    isCountrySelcet=false;
                    Intent intent = new Intent(RegisterActivity.this, SelectCountryActivity.class);
                    startActivity(intent);
                }

            }
            return false;
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_sex:
                showSexDialog();
                break;
            case R.id.tv_age:
                showAgeDialog();
                break;
            case R.id.tv_country:
                if(isCountrySelcet){
                    isCountrySelcet=false;
                    Intent intent = new Intent(RegisterActivity.this, SelectCountryActivity.class);
                    startActivity(intent);
                }
//
//                List<String> countryList = CommonData.getCountryList();
//                DialogUtil.showSingleWheelDialog(getSupportFragmentManager(), countryList, countryList.indexOf(tvCountry.getText().toString()),
//                        getString(R.string.country), getString(R.string.positive), getString(R.string.negative), true, new OneWheelDialog.OnOneWheelDialogClickListener() {
//                            @Override
//                            public void onPositiveClick(View view, String selectedText) {
//                                if (!TextUtils.isEmpty(selectedText)) {
//                                    tvCountry.setText(selectedText);
//                                    if (selectedText.contains(" ")) {
//                                        selectedText = selectedText.replaceAll(" ", "");
//                                    }
//                                    if ("United States".equals(selectedText)) {
//                                        selectedText = "America";
//                                    }
//                                    PlatformInfoXml.setCountry(selectedText);
//                                    PlatformInfoXml.setFid(CountryFidUtils.getCountryFidMap().get(selectedText));
//                                }
//                            }
//
//                            @Override
//                            public void onNegativeClick(View view) {
//
//                            }
//                        });
                break;

            case R.id.register:// 注册
//                if (cropFile == null) {
//                    ToastUtil.showShortToast(RegisterActivity.this, getString(R.string.register_icon));
//                    return;
//                }
                String tvNicknameText = String.valueOf(tvNickname.getText()).trim();
                String nickname = URLEncoder.encode(tvNicknameText);
                if (TextUtils.isEmpty(tvNicknameText)) {
                    ToastUtil.showShortToast(this, getString(R.string.register_nickname));
                    tilNickname.setError(getString(R.string.register_nickname));
                    tilNickname.setErrorEnabled(true);
                    Vibrator vibrator = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                    vibrator.vibrate(200);
                    return;
                }else{
                    tilNickname.setErrorEnabled(false);
                    nickname = getOsDisplay(nickname);
                }
                if (!((tvSex.getText().toString().equals(getString(R.string.male)))||(tvSex.getText().toString().equals(getString(R.string.famale)))) ) {
                    ToastUtil.showShortToast(this, getString(R.string.register_sex));
                    tilSex.setError(getString(R.string.register_sex));
                    tilSex.setErrorEnabled(true);
                    Vibrator vibrator = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                    vibrator.vibrate(200);
                    return;
                }else{
                    tilSex.setErrorEnabled(false);
                }
                if (TextUtils.isEmpty(tvAge.getText().toString()) || tvAge.getText().toString().equals(getString(R.string.age)) ) {
                    ToastUtil.showShortToast(this, getString(R.string.register_age));
                    tilAge.setError(getString(R.string.register_age));
                    tilAge.setErrorEnabled(true);
                    Vibrator vibrator = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                    vibrator.vibrate(200);
                    return;
                }else{
                    tilAge.setErrorEnabled(false);
                }
                if (TextUtils.isEmpty(tvCountry.getText().toString()) || tvCountry.getText().toString().equals(getString(R.string.country))) {
                    ToastUtil.showShortToast(this, getString(R.string.register_tip));
                    tilCountry.setError(getString(R.string.register_tip));
                    tilCountry.setErrorEnabled(true);
                    Vibrator vibrator = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                    vibrator.vibrate(200);
                    return;
                }else{
                    tilCountry.setErrorEnabled(false);
                }
//                13个英语国家
                if (CommonData.getCountryList().contains(tvCountry.getText().toString())) {
                    if ("United States".equals(tvCountry.getText().toString()) || "UnitedStates".equals(tvCountry.getText().toString())) {
                        PlatformInfoXml.setCountry("America");
                        PlatformInfoXml.setFid(CountryFidUtils.getCountryFidMap().get("America"));
                    } else {
                        String currentCountry = tvCountry.getText().toString();
                        if (currentCountry.contains(" ")) {
                            currentCountry = currentCountry.replaceAll(" ", "");
                        }
                        PlatformInfoXml.setCountry(currentCountry);
                        PlatformInfoXml.setFid(CountryFidUtils.getCountryFidMap().get(currentCountry));
                    }

                } else {
//                    非13个英语国家或者语言为非英语时
                    for (int i = 0; i < Cheeses.GOUNTRY.length; i++) {
                        String name = Cheeses.GOUNTRY[i];
                        if (name.equals(tvCountry.getText().toString())) {
                            String country = Cheeses.EN_GOUNTRY[i];
                            if ("UnitedStates".equals(country)) {
                                PlatformInfoXml.setCountry("America");
                                PlatformInfoXml.setFid(CountryFidUtils.getCountryFidMap().get("America"));
                            } else {
                                if (country.contains(" ")) {
                                    country = country.replaceAll(" ", "");
                                }
                                PlatformInfoXml.setCountry(country);
                                PlatformInfoXml.setFid(CountryFidUtils.getCountryFidMap().get(country));
                            }
                        }
                    }

                }
//                获取国家的语言
                String language = Locale.getDefault().getLanguage();
                String getLanguageFromCountry = CountryFidUtils.getLanguageMap().get(PlatformInfoXml.getCountry());
//               说两种语言的国家
                if (PlatformInfoXml.getCountry().equals("India")) {
                    if (language.equals("hi")) {
                        getLanguageFromCountry = "Indian";
                        PlatformInfoXml.setFid("10891");
                    }
                }
                if (PlatformInfoXml.getCountry().equals("Cameroun")) {
                    if (language.equals("fr")) {
                        getLanguageFromCountry = "French";
                        PlatformInfoXml.setFid("10871");
                    }
                }
                if (PlatformInfoXml.getCountry().equals("Austria")) {
                    if (language.equals("de")) {
                        getLanguageFromCountry = "German";
                        PlatformInfoXml.setFid("10892");
                    }
                }
                //		设置语言
                PlatformInfoXml.setLanguage(getLanguageFromCountry);


                if (!UserInfoXml.getIsClient()) {
//                    必须激活接口返回数据后才能调注册
                    activateAndLocation(locationCode);
                } else {
//                    说明此手机已经激活，直接走注册

                }
                String age = tvAge.getText().toString();
                if (!TextUtils.isEmpty(age)) {
                    if (isFirst) {
                        isFirst = false;
                        regist(gender, age, nickname);
//                        if(cropFile!=null && cropFile.exists()){
//                            register(gender, age, nickname, cropFile);
//                        }else{
//                            register(gender, age, nickname);
//                        }
                        SharedPreferenceUtil.setStringValue(RegisterActivity.this, "REGISTER_GENDER", "genter", gender);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                isFirst = true;
                            }
                        }, 3000);
                    }

                }

                break;

            case R.id.login:// 登录
                Util.gotoActivity(RegisterActivity.this, LoginActivity.class, false);
                break;

            case R.id.agreement:
                DialogUtil.showXiYiSingleBtnDialog(getSupportFragmentManager(), getString(R.string.register_xieyi), getString(R.string.register_xieyi_content),
                        getString(R.string.positive), true, new OnSingleDialogClickListener() {
                            @Override
                            public void onPositiveClick(View view) {
                            }
                        });
                break;
//            上传头像
            case R.id.regist_headIcon:
                GetPhotoActivity.toGetPhotoActivity(RegisterActivity.this, IConfigConstant.LOCAL_AVATAR_PATH, new GetPhotoActivity.OnGetPhotoListener() {
                    @Override
                    public void getSelectedPhoto(File file) {
                        cropFile = new File(IConfigConstant.LOCAL_AVATAR_PATH);
                        cropFile = file;

//                        if (file != null) {
//                            showLoading();
//                            CommonRequestUtil.uploadImage(true, file, true, new CommonRequestUtil.OnUploadImageListener() {
//                                @Override
//                                public void onSuccess(Image image) {
//                                    if (image != null) {
//                                        getIcon=true;
//                                        // 保存头像信息
//                                        UserInfoXml.setAvatarUrl(image.getImageUrl());
//                                        UserInfoXml.setAvatarThumbnailUrl(image.getThumbnailUrl());
//                                        UserInfoXml.setAvatarStatus(image.getStatus());
//                                    }
//                                    // 设置新头像
                        ImageLoaderUtil.getInstance().loadCircleImage(RegisterActivity.this, registHeadIcon, file);
//                                    dismissLoading();
//                                }
//
//                                @Override
//                                public void onFail() {
//                                    dismissLoading();
//                                }
//                            });
//                        }
                    }
                });
                break;
        }
    }

    private String getOsDisplay(String osStr) {
        //        String osStr = StringUtil.removeSpace(android.os.Build.DISPLAY);
        if (osStr.length() < osStr.getBytes().length) {
            try {
                return URLEncoder.encode(osStr, "UTF-8");
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }
        } else {
            return osStr;
        }
    }


    /**
     * 激活客户端和获取用户是否打开定位权限
     **/
    private void activateAndLocation(String LocationCode) {
        /**激活客户端**/
        OkHttpUtils.post()
                .url(IUrlConstant.URL_CLENT_ACTIVATION)
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .build()
                .execute(new Callback<BaseModel>() {
                    @Override
                    public BaseModel parseNetworkResponse(Response response, int id) throws Exception {
                        String resultJson = response.body().string();
                        if (!TextUtils.isEmpty(resultJson)) {
                            return JSON.parseObject(resultJson, BaseModel.class);
                        }
                        return null;
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                    }

                    @Override
                    public void onResponse(BaseModel response, int id) {
                        if (response != null) {
                            String isSucceed = response.getIsSucceed();
                            if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
                                UserInfoXml.setIsClient();
                                // appsflyer激活
                                AppsflyerUtils.activate();
//                                Firebase统计激活量
                                getActivateFirebase();
                            }
                        }
                    }
                });
/**获取用户是否打开了定位权限,埋点**/

        if (!TextUtils.isEmpty(LocationCode)) {
            OkHttpUtils.post()
                    .url(IUrlConstant.URL_GET_POINT)
                    .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                    .addParams("remoteId", "0")
                    .addParams("actionId", "500")
                    .addParams("extendId", LocationCode)
                    .build()
                    .execute(new Callback<PointBean>() {
                        @Override
                        public PointBean parseNetworkResponse(Response response, int id) throws Exception {
                            String resultJson = response.body().string();
                            if (!TextUtils.isEmpty(resultJson)) {
                                return JSON.parseObject(resultJson, PointBean.class);
                            }
                            return null;
                        }

                        @Override
                        public void onError(Call call, Exception e, int id) {
                        }

                        @Override
                        public void onResponse(PointBean response, int id) {
                            if (response != null) {
                                String isSucceed = response.getIsSucceed();
                                if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
                                    UserInfoXml.setIsClient();
                                }
                            }
                        }
                    });
        }


    }

    private void regist(String gender, String age, String nickName) {
        showLoad();
        OkHttpUtils.post()
                .url(IUrlConstant.URL_REGISTER)
                .addParams("pushToken", PlatformInfoXml.getPushToken())
                .addParams("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .addParams("gender", gender)
                .addParams("nickName", nickName)
                .addParams("age", age)
                .build()
                .execute(new Callback<Register>() {
                    @Override
                    public Register parseNetworkResponse(Response response, int id) throws Exception {
                        String resultJson = response.body().string();
                        if (!TextUtils.isEmpty(resultJson)) {
                            return JSON.parseObject(resultJson, Register.class);
                        }
                        return null;
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        dismissLoad();
                        ToastUtil.showShortToast(RegisterActivity.this, getString(R.string.register_fail));
                    }

                    @Override
                    public void onResponse(Register response, int id) {
                        if (response != null) {
                            String isSucceed = response.getIsSucceed();
                            if (!TextUtils.isEmpty(response.getMsg())) {
                                ToastUtil.showShortToast(RegisterActivity.this, response.getMsg());
                            }
                            if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
                                UserInfoXml.setUserInfo(response.getUserPandora());
                                PlatformInfoXml.setToken(response.getToken());
                                // 设置页眉和互推开关
                                UserInfoXml.setIsShowHeadMsg(response.getIsShowHeadNotice());
                                UserInfoXml.setIsShowHeadMsgNew(response.getIsShowHeadNotice2());
                                UserInfoXml.setIsShowRecommendUser(response.getIsShowCommendUser());
                                UserInfoXml.setSearchInterceptor(response.getSearchInterceptor());
                                // appsflyer注册
                                AppsflyerUtils.register();
                                // 初始化数据字典
                                CommonRequestUtil.initParamsDict(new CommonRequestUtil.OnCommonListener() {
                                    @Override
                                    public void onSuccess() {
                                        gotoQA();
                                    }

                                    @Override
                                    public void onFail() {
                                        gotoQA();
                                    }
                                });
                            } else {
                                dismissLoad();
                                ToastUtil.showShortToast(RegisterActivity.this, getString(R.string.register_fail));
                            }
                        } else {
                            dismissLoad();
                            ToastUtil.showShortToast(RegisterActivity.this, getString(R.string.register_fail));
                        }
                    }
                });
    }

    private void register(String gender, String age, String nickName, File file) {
        String birthday = 2017 - Integer.parseInt(age) + "-01-01";
        showLoad();
        register.setEnabled(false);
        OkHttpUtils.post()
                .url(IUrlConstant.API_REGISTER_HAVEPHOTO)
                .addHeader("gender", gender)
                .addHeader("pushToken", PlatformInfoXml.getPushToken())
                .addHeader("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .addHeader("birthday", birthday)
                .addHeader("nickName", nickName)
                .addHeader("isMain", "1")
                .addFile("file", "file", file)
                .build()
                .connTimeOut(60 * 1000)
                .readTimeOut(60 * 1000)
                .writeTimeOut(60 * 1000)
                .execute(new RegisterCallBack());
    }

    public class RegisterCallBack extends Callback<Register> {

        @Override
        public Register parseNetworkResponse(Response response, int id) throws Exception {
            setRegisterBtnEnable(true);
            String resultJson = response.body().string();

            if (!TextUtils.isEmpty(resultJson)) {
                return JSON.parseObject(resultJson, Register.class);
            }
            return null;
        }

        @Override
        public void onError(Call call, Exception e, int id) {
            setRegisterBtnEnable(true);
            dismissLoad();
            ToastUtil.showShortToast(RegisterActivity.this, getString(R.string.register_fail));
//            hideDialog(2);
        }

        @Override
        public void onResponse(final Register response, int id) {
            if (response != null) {
                String isSucceed = response.getIsSucceed();
                if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
                    if (!TextUtils.isEmpty(response.getMsg())) {
                        ToastUtil.showShortToast(RegisterActivity.this, response.getMsg());
                    }
                    // 缓存头像，延时5秒（上传头像的时间）
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            UserBase userBase = response.getUserPandora().getUserBaseEnglish();
                            if (userBase != null) {
                                Image image = userBase.getImage();
                                if (image != null) {
                                    ImageLoaderUtil.getInstance().loadImage(RegisterActivity.this, new ImageLoader.Builder()
                                            .url(image.getThumbnailUrl()).imageView(registHeadIcon).build());
//                                }
                                    // 保存头像信息
                                    UserInfoXml.setAvatarUrl(image.getImageUrl());
                                    UserInfoXml.setAvatarThumbnailUrl(image.getThumbnailUrl());
                                    UserInfoXml.setAvatarStatus(image.getStatus());
                                }
                                // 设置新头像
//                            ImageLoaderUtil.getInstance().loadCircleImage(RegisterActivity.this, registHeadIcon, file);
                            }
                            // 发送注册成功事件
//                            EventBus.getDefault().post(new RegisterSuccessEvent());
                        }
                    }, 5 * 1000);
                    UserInfoXml.setUserInfo(response.getUserPandora());
                    PlatformInfoXml.setToken(response.getToken());
                    // 设置页眉和互推开关
                    UserInfoXml.setIsShowHeadMsg(response.getIsShowHeadNotice());
                    UserInfoXml.setIsShowHeadMsgNew(response.getIsShowHeadNotice2());
                    UserInfoXml.setIsShowRecommendUser(response.getIsShowCommendUser());
                    UserInfoXml.setSearchInterceptor(response.getSearchInterceptor());
                    // appsflyer注册
                    AppsflyerUtils.register();
                    // 初始化数据字典
                    CommonRequestUtil.initParamsDict(new CommonRequestUtil.OnCommonListener() {
                        @Override
                        public void onSuccess() {
                            gotoQA();
                        }

                        @Override
                        public void onFail() {
                            gotoQA();
                        }
                    });
                } else {
                    dismissLoad();
                    ToastUtil.showShortToast(RegisterActivity.this, getString(R.string.register_fail));
                }
            } else {
                dismissLoad();
                ToastUtil.showShortToast(RegisterActivity.this, getString(R.string.register_fail));
            }
            dismissLoad();
        }
    }

    private void setRegisterBtnEnable(final boolean enable) {
        RegisterActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                register.setEnabled(enable);
            }
        });
    }

    //    private void hideDialog(int seconds) {
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                if (mLoadingDialog != null && mLoadingDialog.isShowing()) {
//                    mLoadingDialog.dismiss();
//                }
//            }
//        }, seconds * 1000);
//    }
    private void gotoQA() {
//        firebase统计注册量
        getRegisterFirebase();
//        设置默认头像
        SharedPreferenceUtil.setBooleanValue(RegisterActivity.this,"mIvUserAvatar","get_mIvUserAvatar",false);
//        去除上一个女用户的个人信息编辑
        SharedPreferenceUtil.setBooleanValue(Utils.getContext(), "SELF_INTRODUCE", "self_introduce", false);
//       设置男用户的拦截
        if(UserInfoXml.isMale()){
//            SharedPreferenceUtil.setBooleanValue(Utils.getContext(), "INTERCEPT_DIALOG", "DIALOG", true);
        }
        // 发送事件，finish其它Activity
        EventBus.getDefault().post(new FinishEvent());
        dismissLoad();
//        Util.gotoActivity(RegisterActivity.this, UploadPhotoActivity.class, true);

        if (CommonData.getCountryList().contains(PlatformInfoXml.getCountry())) {
            Util.gotoActivity(RegisterActivity.this, AnswerQuestionActivity.class, true);
        } else {
            Util.gotoActivity(RegisterActivity.this, UploadPhotoActivity.class, true);
//            Util.gotoActivity(RegisterActivity.this, MainActivity.class, true);
        }

    }

    @Override
    protected void doRegister() {
        super.doRegister();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void unregister() {
        super.unregister();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onEvent(FinishEvent event) {
        finish();
    }

    @Subscribe
    public void onEvent(SelectCountryEvent event) {
        String country = event.getmCountry();
        if(TextUtils.isEmpty(country) || country==null){
        }else{
            tvCountry.setText(country);
            tvCountry.setTextColor(Color.parseColor("#ffe6e6e6"));
        }

        tilCountry.setErrorEnabled(false);
        isCountrySelcet=true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    private void showAgeDialog() {
        if(isAgeDialog){
            isAgeDialog=false;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    isAgeDialog=true;
                }
            },2000);
            View view = getLayoutInflater().inflate(R.layout.dialog_select_age,
                    null);
            final Dialog dialog = new Dialog(this, R.style.transparentFrameWindowStyle);
            dialog.setContentView(view, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));
            Window window = dialog.getWindow();

            List<String> yearAgeList = new ArrayList<>();
            final WheelView yearWheelView = (WheelView) view.findViewById(R.id.dialog_year_wheelview);
            for (int i = 18; i < 66; i++) {
                yearAgeList.add(String.valueOf(i));
            }
            yearWheelView.setData(yearAgeList);
            if (!TextUtils.isEmpty(tvAge.getText().toString())) {
                yearWheelView.setDefaultIndex(6);
            }
            Button btnSure = (Button) view.findViewById(R.id.dialog_age_sure);
            btnSure.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tvAge.setText(yearWheelView.getSelectedText());
                    tvAge.setTextColor(Color.parseColor("#ffe6e6e6"));
                    tilAge.setErrorEnabled(false);
                    isAgeDialog=true;
                    dialog.dismiss();
                }
            });


            // 设置显示动画
            window.setWindowAnimations(R.style.main_menu_animstyle);
            WindowManager.LayoutParams wl = window.getAttributes();
            wl.x = 0;
            wl.y = getWindowManager().getDefaultDisplay().getHeight();
            // 以下这两句是为了保证按钮可以水平满屏
            wl.width = ViewGroup.LayoutParams.MATCH_PARENT;
            wl.height = ViewGroup.LayoutParams.WRAP_CONTENT;
            // 设置显示位置
            dialog.onWindowAttributesChanged(wl);
            // 设置点击外围解散
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }


    }

    private void showSexDialog() {

        if(isSexDialog ){
            isSexDialog=false;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    isSexDialog=true;
                }
            },2000);
            View view = getLayoutInflater().inflate(R.layout.sex_dialog,
                    null);
            TextView tvMan = (TextView) view.findViewById(R.id.tv_man);
            TextView tvWoman = (TextView) view.findViewById(R.id.tv_woman);
            Button btnCancel = (Button) view.findViewById(R.id.btn_cancel);
            final Dialog dialog = new Dialog(this, R.style.transparentFrameWindowStyle);
            dialog.setContentView(view, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));
            Window window = dialog.getWindow();
            // 设置显示动画
            window.setWindowAnimations(R.style.main_menu_animstyle);
            WindowManager.LayoutParams wl = window.getAttributes();
            wl.x = 0;
            wl.y = getWindowManager().getDefaultDisplay().getHeight();
            // 以下这两句是为了保证按钮可以水平满屏
            wl.width = ViewGroup.LayoutParams.MATCH_PARENT;
            wl.height = ViewGroup.LayoutParams.WRAP_CONTENT;

            // 设置显示位置
            dialog.onWindowAttributesChanged(wl);
            // 设置点击外围解散
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
            tvMan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    tvSex.setText(getString(R.string.male));
                    tvSex.setTextColor(Color.parseColor("#ffe6e6e6"));
                    tilSex.setErrorEnabled(false);
                    gender = "0";
                    isSexDialog=true;
                    dialog.dismiss();
                }
            });
            tvWoman.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    tvSex.setText(getString(R.string.famale));
                    tvSex.setTextColor(Color.parseColor("#ffe6e6e6"));
                    tilSex.setErrorEnabled(false);
                    gender = "1";
                    isSexDialog=true;
                    dialog.dismiss();

                }
            });
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    isSexDialog=true;
                    dialog.dismiss();
                }
            });
        }

    }

    private void dismissLoad() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                flLoading.setVisibility(View.GONE);
                tvLoading.setVisibility(View.GONE);
            }
        }, 1000);
    }

    private void showLoad() {
        flLoading.setVisibility(View.VISIBLE);
        tvLoading.setVisibility(View.VISIBLE);
        Animation hyperspaceJumpAnimation = AnimationUtils.loadAnimation(this, R.anim.anim_show_loading);
        ivLoadingHeart.startAnimation(hyperspaceJumpAnimation);
    }
    private void getRegisterFirebase() {
        Bundle params = new Bundle();
        params.putString("country", PlatformInfoXml.getPlatformInfo().getCountry());
        params.putString("user_Id", PlatformInfoXml.getPlatformInfo().getPid());
        mFirebaseAnalytics.logEvent("registered_successfully", params);
    }
    private void getActivateFirebase() {
        Bundle params = new Bundle();
        params.putString("country", PlatformInfoXml.getPlatformInfo().getCountry());
        params.putString("user_Id", PlatformInfoXml.getPlatformInfo().getPid());
        mFirebaseAnalytics.logEvent("Activate_successfully", params);
    }

}
