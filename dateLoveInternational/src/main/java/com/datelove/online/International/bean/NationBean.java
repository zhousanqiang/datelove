package com.datelove.online.International.bean;

/**
 * http请求后返回的相同字段
 * Created by Tian on 2017/7/8.
 */
public class NationBean {
    /**
     * 1代表请求成功
     */
    public String isSucceed;
    /**
     * 提示信息
     */
    public String country;

    public String getIsSucceed() {
        return isSucceed;
    }

    public void setIsSucceed(String isSucceed) {
        this.isSucceed = isSucceed;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return "BaseModel{" +
                "isSucceed='" + isSucceed + '\'' +
                ", country='" + country + '\'' +
                '}';
    }

}
