package com.datelove.online.International.utils;


import com.kochava.base.Tracker;
import com.kochava.base.Tracker.Event;

/**
 * Created by Administrator on 2017/11/17.
 */

public class KochavaUtils {
    public static void kochavaSvipOnemonth(){
        //Custom Event
        Tracker.sendEvent(new Event("Custom Event")
                //会员一个月
                .addCustom("sviponemonth", "sviponemonth")
                .setName("awesome_event")
        );
    }
    public static void kochavaSvipthreemonths(){
        //Custom Event
        Tracker.sendEvent(new Event("Custom Event")
//                会员3个月
                .addCustom("svipthreemonths", "svipthreemonths")
                .setName("awesome_event")
        );
    }
    public static void kochavaSviponeyear(){
        //Custom Event
//        会员一年
        Tracker.sendEvent(new Event("Custom Event")
                .addCustom("sviponeyear", "sviponeyear")
                .setName("awesome_event")
        );
    }
    public static void kochavaVipOnemonth(){
        //Custom Event
        Tracker.sendEvent(new Event("Custom Event")
                //会员一个月
                .addCustom("viponemonth", "viponemonth")
                .setName("awesome_event")
        );
    }
    public static void kochavaVipthreemonths(){
        //Custom Event
        Tracker.sendEvent(new Event("Custom Event")
//                会员3个月
                        .addCustom("vipthreemonths", "vipthreemonths")
                        .setName("awesome_event")
        );
    }
    public static void kochavaviponeyear(){
        //Custom Event
//        会员一年
        Tracker.sendEvent(new Event("Custom Event")
                .addCustom("viponeyear", "viponeyear")
                .setName("awesome_event")
        );
    }

}
