package com.datelove.online.International.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.datelove.online.International.R;
import com.datelove.online.International.activity.ChatActivity;
import com.datelove.online.International.activity.UserInfoDetailActivity;
import com.datelove.online.International.bean.Chat;
import com.datelove.online.International.bean.ChatMsg;
import com.datelove.online.International.bean.Image;
import com.datelove.online.International.bean.UserBase;
import com.datelove.online.International.constant.IConfigConstant;
import com.datelove.online.International.event.MessageChangedEvent;
import com.datelove.online.International.utils.CommonRequestUtil;
import com.datelove.online.International.utils.SwipeLayout;
import com.datelove.online.International.utils.Utils;
import com.library.adapter.recyclerview.CommonRecyclerViewAdapter;
import com.library.adapter.recyclerview.RecyclerViewHolder;
import com.library.imageloader.CircleTransformation;
import com.library.imageloader.ImageLoader;
import com.library.imageloader.ImageLoaderUtil;
import com.library.utils.SharedPreferenceUtil;
import com.library.utils.TimeUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * 聊天记录适配器
 * Created by zhangdroid on 2016/10/29.
 */
public class MessageAdapter extends CommonRecyclerViewAdapter<Chat> {

    private long mServerSystemTime;

    private Handler handler = new Handler();
    private long timeDiff;
    private Context mContext;
    /**加载读信拦截对话框**/
boolean isIntercept = SharedPreferenceUtil.getBooleanValue(Utils.getContext(), "INTERCEPT_DIALOG", "DIALOG", true);
//

    public MessageAdapter(Context context, int layoutResId) {
        super(context, layoutResId);
        mContext=context;
    }

    public MessageAdapter(Context context, int layoutResId, List list) {
        super(context, layoutResId, list);
    }

    public void setServerSystemTime(long serverSystemTime) {
        this.mServerSystemTime = serverSystemTime;
    }

    @Override
    protected void convert(final int position, final RecyclerViewHolder viewHolder, Chat bean) {
        if (bean != null) {
            final UserBase userBase = bean.getUserBaseEnglish();
            if (userBase != null) {

                SwipeLayout swipelayout = (SwipeLayout) viewHolder.getView(R.id.swipelayout);
                ImageView ivAvatar = (ImageView) viewHolder.getView(R.id.item_message_avatar);
                final TextView lastTime = (TextView)viewHolder.getView(R.id.item_message_last_time);
                final Image image = userBase.getImage();
                if (image != null) {
                    String imgUrl = image.getThumbnailUrl();
                    if (!TextUtils.isEmpty(imgUrl)) {
                        ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(imgUrl).imageView(ivAvatar)
                                .placeHolder(IConfigConstant.COMMON_DEFAULT_AVATAR_CIRCLE).transform(new CircleTransformation()).build());
                    } else {
                        ivAvatar.setImageResource(IConfigConstant.COMMON_DEFAULT_AVATAR_CIRCLE);
                    }
                } else {
                    ivAvatar.setImageResource(IConfigConstant.COMMON_DEFAULT_AVATAR_CIRCLE);
                }
                ivAvatar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // 查看详情
                        UserInfoDetailActivity.toUserInfoDetailActivity((Activity) mContext, userBase.getId(),
                                null, UserInfoDetailActivity.SOURCE_FROM_CHAT_LIST);
                    }
                });
                // 昵称
                String nickname = userBase.getNickName();
                if (!TextUtils.isEmpty(nickname)) {
                    viewHolder.setText(R.id.item_message_nickname, nickname);
                }
                // 最新一条消息内容和时间
                final ChatMsg chatMsg = bean.getChat();
                if (chatMsg != null) {
                    if ( chatMsg.getLastTimeMills()!=0) {
                        viewHolder.setText(R.id.item_message_last_time, TimeUtil.getLocalTime(mContext, mServerSystemTime,
                                chatMsg.getLastTimeMills()));
                         timeDiff = mServerSystemTime - chatMsg.getLastTimeMills();
                    }else{
                        viewHolder.setText(R.id.item_message_last_time, TimeUtil.getLocalTime(mContext, mServerSystemTime,
                                chatMsg.getAddTimeMills()));
                         timeDiff = mServerSystemTime - chatMsg.getAddTimeMills();
                    }

                    // 未读消息
                    int unreadCount=chatMsg.getUnreadCount();

                    if(unreadCount<=0){
                        String lastMsgContent = chatMsg.getLastContent();
                        viewHolder.getView(R.id.item_message_is_unread).setVisibility(View.GONE);
                        viewHolder.getView(R.id.item_message_lastmsg).setVisibility(View.INVISIBLE);
                    }else{
                        viewHolder.getView(R.id.item_message_is_unread).setVisibility(View.VISIBLE);
                        viewHolder.getView(R.id.item_message_lastmsg).setVisibility(View.INVISIBLE);
                        viewHolder.setText(R.id.item_message_is_unread, String.valueOf(unreadCount));
//                        /**加载读信拦截对话框**/
//                        boolean isIntercept = SharedPreferenceUtil.getBooleanValue(Utils.getContext(), "INTERCEPT_DIALOG", "DIALOG", true);
//                        if(!isIntercept){
//                            viewHolder.setText(R.id.item_message_lastmsg, Util.convertText(lastMsgContent));
//                        }

                        if(timeDiff >= 24 * 60 * 60 * 1000){
                            viewHolder.getView(R.id.item_message_is_unread).setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.shape_msgnum_circle_gray));
//                        24h之后的未读消息总数
//                            EventBus.getDefault().post(new UpdateMessageCountEvent(mTotalUnreadMsgCnt-unreadCount));
                        }else{
                            viewHolder.getView(R.id.item_message_is_unread).setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.shape_msgnum_circle));
                        }


                    }

                    SwipeLayout swipeLayout = (SwipeLayout)viewHolder.getView(R.id.swipelayout);
                    swipeLayout.setOnStatusChangeListener(new MyOnStatusChangeListener());
                    /**进入聊天界面**/
                    RelativeLayout tvNick = (RelativeLayout)viewHolder.getView(R.id.rv_item);
                    tvNick.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            String imgUrl = image.getThumbnailUrl();
                            if(imgUrl==null){
                                imgUrl="";
                            }
                            ChatActivity.toChatActivity((Activity) mContext, userBase.getId(), userBase.getNickName(), imgUrl);
                        }
                    });
                    /**点击删除按钮**/
                    FrameLayout flDelete = (FrameLayout) viewHolder.getView(R.id.item_del_container);
                    flDelete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            showSexDialog(userBase,position);
                        }
                    });


//
//                     Runnable runnable = new Runnable() {
//                        public void run() {
//                            this.update();
//                            handler.postDelayed(this, 1000*20);// 间隔20秒
//                        }
//                        void update() {
//                           String updateTime= TimeUtil.getLocalTime(mContext, mServerSystemTime, chatMsg.getAddTimeMills());
//                            String text = (String) lastTime.getText();
//                            if(updateTime!=text){
//                                //刷新出最新时间
//                                viewHolder.setText(R.id.item_message_last_time, TimeUtil.getLocalTime(mContext, mServerSystemTime, chatMsg.getAddTimeMills()));
//                            }
//
//                        }
//                    };
//                    handler.postDelayed(runnable, 1000*20); //开始刷新

                }
            }
        }
    }
    List<SwipeLayout> openingLayouts = new ArrayList<SwipeLayout>();



    private final class MyOnStatusChangeListener implements
            SwipeLayout.OnStatusChangeListener {

        @Override
        public void onStartOpen(SwipeLayout swipeLayout) {
            closeAll();
//            Log.e(tag  , "onStartOpen,list="+openingLayouts);
        }

        @Override
        public void onStartClose(SwipeLayout swipeLayout) {
//            Log.e(tag , "onStartClose");
        }

        @Override
        public void onOpened(SwipeLayout swipeLayout) {
            openingLayouts.add(swipeLayout);
//            Log.e(tag , "onOpened,list="+openingLayouts);
        }

        @Override
        public void onDraging(SwipeLayout swipeLayout) {
//            Log.e(tag , "onDraging");
        }

        @Override
        public void onClosed(SwipeLayout swipeLayout) {
            openingLayouts.remove(swipeLayout);
//            Log.e(tag , "onClosed,list="+openingLayouts);
        }
    }
    public void closeAll() {
        for (int i = 0; i < openingLayouts.size(); i++) {
            SwipeLayout swipeLayout = openingLayouts.get(i);
            swipeLayout.close();
        }
    }
    public void showSexDialog(final UserBase userBase, final int position) {
//        (Activity)mContext.layoutinfater.from(mcontext)
        LayoutInflater from = LayoutInflater.from(mContext);
        final View view = from.inflate(R.layout.delete_dialog, null);
//
//        View view = ((Activity)mContext)getLayoutInflater().inflate(R.layout.sex_dialog,
//                null);
        TextView delete = (TextView) view.findViewById(R.id.tv_delete);
        final TextView cancel = (TextView) view.findViewById(R.id.tv_cancel);
        final Dialog dialog = new Dialog(mContext, R.style.transparentFrameWindowStyle);
        dialog.setContentView(view, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        Window window = dialog.getWindow();
        // 设置显示动画
        window.setWindowAnimations(R.style.main_menu_animstyle);
        WindowManager.LayoutParams wl = window.getAttributes();
        wl.x = 0;
        wl.y = 800;
        // 以下这两句是为了保证按钮可以水平满屏
        wl.width = ViewGroup.LayoutParams.MATCH_PARENT;
        wl.height = ViewGroup.LayoutParams.WRAP_CONTENT;

        // 设置显示位置
        dialog.onWindowAttributesChanged(wl);
        // 设置点击外围解散
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonRequestUtil.deleteChatHistory(userBase.getId(), true, new CommonRequestUtil.OnCommonListener() {
                                @Override
                                public void onSuccess() {
                                   removeItem(position);
                                    // 发送事件，更新聊天列表
                                    EventBus.getDefault().post(new MessageChangedEvent());
                                }
                                @Override
                                public void onFail() {
                                }
                            });
                dialog.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

}
