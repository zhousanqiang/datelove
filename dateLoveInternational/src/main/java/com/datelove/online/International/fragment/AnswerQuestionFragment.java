package com.datelove.online.International.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.datelove.online.International.R;
import com.datelove.online.International.activity.UploadPhotoActivity;
import com.datelove.online.International.base.BaseApplication;
import com.datelove.online.International.base.BaseFragment;
import com.datelove.online.International.utils.CommonRequestUtil;
import com.datelove.online.International.utils.ParamsUtils;
import com.datelove.online.International.xml.UserInfoXml;
import com.library.adapter.recyclerview.CommonRecyclerViewAdapter;
import com.library.adapter.recyclerview.RecyclerViewHolder;
import com.library.utils.SharedPreferenceUtil;
import com.library.widgets.ScrollControlViewPager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;

/**
 * QA页
 * Created by zhangdroid on 2016/12/29.
 */
public class AnswerQuestionFragment extends BaseFragment {
    @BindView(R.id.iv_system_user_avatar)
    ImageView mIvSystemUserAvatar;
    @BindView(R.id.tv_question)
    TextView mTvQuestion;
    @BindView(R.id.answer_recyclerview)
    RecyclerView mAnswerRecyclerview;

    // 本地头像
    private int[] aq_images_female = new int[]{R.drawable.nv1, R.drawable.nv2,
            R.drawable.nv3, R.drawable.nv4, R.drawable.nv5};
    private int[] aq_images_male = new int[]{R.drawable.nan1, R.drawable.nan2,
            R.drawable.nan3, R.drawable.nan4, R.drawable.nan5};

    private static final String ARG_QUESTION_INDEX = "arg_QUESTION_INDEX";
    private FragmentActivity mContent;

    public static AnswerQuestionFragment newInstance(int index) {
        AnswerQuestionFragment answerQuestionFragment = new AnswerQuestionFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_QUESTION_INDEX, index);
        answerQuestionFragment.setArguments(bundle);
        return answerQuestionFragment;
    }

    // qa index
    private int index;
    private AnswerAdapter mAnswerAdapter;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_answer_question;
    }

    @Override
    protected void initViewsAndVariables() {
        Bundle bundle = getArguments();
        mContent= getActivity();
        if (bundle != null) {
            index = bundle.getInt(ARG_QUESTION_INDEX);
            mIvSystemUserAvatar.setImageResource(UserInfoXml.isMale() ? aq_images_female[index - 1] : aq_images_male[index - 1]);
            String question = "";
            List<String> answerList = new ArrayList<>();
            switch (index) {
                case 1:// 性经验
                    question ="Do you wish your partner to have more experience or less in sex?";
                    String[] s1=new String[]{"Less","More","It doesn’t matter"};
                    answerList = Arrays.asList(s1);
                    break;
                case 2:// 性行为
                    question ="Does sexual performance play an important role in choosing your the other half?";
                    String[] s2=new String[]{"Yes, it does","Not exactly,as long as we satisfied with mutual","No, not at all"};
                    answerList = Arrays.asList(s2);
                    break;
                case 3:// first date
                    question ="Which part of your body is the most sensitive?";
                    String[] s3=new String[]{"My five sense organs","My chest/breast","Others"};
                    answerList = Arrays.asList(s3);
                    break;
                case 4:// 日久生情
                    question ="What kind of foreplay do you like?";
                    String[] s4=new String[]{"Kissing the ears","Kissing the neck","I prefer to go ahead without foreplay"};
                    answerList = Arrays.asList(s4);
                    break;
                case 5:// 一夜情
                    question ="What kind of kissing do you like?";
                    String[] s5=new String[]{"Kissing the lip playfully","French kiss","Kissing on the forehead.","Others"};
                    answerList = Arrays.asList(s5);
                    break;
//
//                case 1:// 婚姻状况
//                    question = getString(R.string.question1);
//                    answerList = ParamsUtils.getMarriageMapValue();
//                    break;
//                case 2:// 教育
//                    question = getString(R.string.question2);
//                    answerList = ParamsUtils.getEducationMapValue();
//                    break;
//                case 3:// 是否想要孩子
//                    question = getString(R.string.question3);
//                    answerList = ParamsUtils.getWantBabyMapValue();
//                    break;
//                case 5:// 兴趣爱好
//                    question = getString(R.string.question4);
//                    answerList = ParamsUtils.getInterestMapValue();
//                    break;
//                case 4:// 职业
//                    question = getString(R.string.question5);
//                    answerList = ParamsUtils.getWorkMapValue();
//                    break;


            }
            mTvQuestion.setText(question);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            mAnswerRecyclerview.setLayoutManager(linearLayoutManager);
            mAnswerAdapter = new AnswerAdapter(getActivity(), R.layout.item_answer_list, answerList);
            mAnswerRecyclerview.setAdapter(mAnswerAdapter);
        }
    }

    @Override
    protected void addListeners() {
        mAnswerAdapter.setOnItemClickListener(new CommonRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, RecyclerViewHolder viewHolder) {
                String selectedItem = mAnswerAdapter.getItemByPosition(position);
                switch (index) {
                    case 1:// 婚姻状况
                        UserInfoXml.setMarriage(ParamsUtils.getMarriageMapKeyByValue(selectedItem));
                        SharedPreferenceUtil.setIntValue(getActivity(),"QUESTION","answer_1",position);
                        next();
                        break;
                    case 2:// 学历
                        UserInfoXml.setEducation(ParamsUtils.getEducationMapKeyByValue(selectedItem));
                        SharedPreferenceUtil.setIntValue(getActivity(),"QUESTION","answer_2",position);
                        next();
                        break;
                    case 3:// 是否想要孩子
                        UserInfoXml.setWantBaby(ParamsUtils.getWantBabyMapKeyByValue(selectedItem));
                        SharedPreferenceUtil.setIntValue(getActivity(),"QUESTION","answer_3",position);
                        next();
                        break;
                    case 4:// 兴趣爱好
                        UserInfoXml.setInterest(ParamsUtils.getInterestMapKeyByValue(selectedItem));
                        SharedPreferenceUtil.setIntValue(getActivity(),"QUESTION","answer_4",position);
                        next();
                        break;
                    case 5:// 职业
                        UserInfoXml.setWork(ParamsUtils.getWorkMapKeyByValue(selectedItem));
                        SharedPreferenceUtil.setIntValue(getActivity(),"QUESTION","answer_5",position);
//                        showLoading();
                        CommonRequestUtil.uploadUserInfo(false, new CommonRequestUtil.OnCommonListener() {
                            @Override
                            public void onSuccess() {
//                                dismissLoading();
//                                gotoMainOrUploadAudio();
//                                Util.gotoActivity(getActivity(), UploadPhotoActivity.class, true);

//                                Intent intent=new Intent(mContent,MainActivity.class);
//                                startActivity(intent);
//                                mContent.finish();
//                                mContent.overridePendingTransition(R.anim.login_anim_in,R.anim.login_anim_out);
                            }

                            @Override
                            public void onFail() {
//                                dismissLoading();
//                                Util.gotoActivity(getActivity(), MainActivity.class, true);
//                                gotoMainOrUploadAudio();
                            }
                        });
                        gotoMainOrUploadAudio();

                        break;
                }
            }
        });
    }
    private void gotoMainOrUploadAudio() {
//        上传头像
        FragmentActivity activity = getActivity();
        if(activity==null){
            activity= (FragmentActivity) BaseApplication.getGlobalContext();
        }
        Intent intent = new Intent(activity, UploadPhotoActivity.class);
        startActivity(intent);
        activity.finish();
//        Util.gotoActivity(getActivity(), UploadPhotoActivity.class, true);
        activity.overridePendingTransition(com.library.R.anim.login_anim_in, com.library.R.anim.login_anim_out);

//        // 女性用户注册
//        if (!UserInfoXml.isMale()) {
//            Util.gotoActivity(getActivity(), SelfIntroduceActivity.class, true);
////            Util.gotoActivity(getActivity(), UploadAudioActivity.class, true);
//        } else {
//            Util.gotoActivity(getActivity(), MainActivity.class, true);
//        }
    }


    private void next() {
        ScrollControlViewPager viewPager = (ScrollControlViewPager) getActivity().findViewById(R.id.answer_question_viewpager);
        viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, false);
    }

    private static class AnswerAdapter extends CommonRecyclerViewAdapter<String> {

        public AnswerAdapter(Context context, int layoutResId) {
            super(context, layoutResId);
        }

        public AnswerAdapter(Context context, int layoutResId, List<String> list) {
            super(context, layoutResId, list);
        }

        @Override
        protected void convert(int position, RecyclerViewHolder viewHolder, String bean) {
            if (!TextUtils.isEmpty(bean)) {
                viewHolder.setText(R.id.item_answer, bean);
            }
        }
    }

}
