package com.datelove.online.International.bean;

import java.util.List;

/**
 * 缘分列表对象
 * Created by zhangdroid on 2016/8/10.
 */
public class Fate extends BaseModel {
    int totalCount;// 总数
    int pageSize;// 该页内容数
    int pageNum; // 页码
    long randomNum;

    public long getRandomNum() {
        return randomNum;
    }

    public void setRandomNum(long randomNum) {
        this.randomNum = randomNum;
    }

    List<FateUser> listUser;// 缘分用户对象

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public List<FateUser> getListUser() {
        return listUser;
    }

    public void setListUser(List<FateUser> listUser) {
        this.listUser = listUser;
    }

    @Override
    public String toString() {
        return "Fate{" +
                "totalCount=" + totalCount +
                ", pageSize=" + pageSize +
                ", pageNum=" + pageNum +
                ", listUser=" + listUser +
                '}';
    }

}
