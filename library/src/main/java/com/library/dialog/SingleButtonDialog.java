package com.library.dialog;

import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.library.R;
import com.library.app.BaseDialogFragment;


/**
 * 只有一个按钮的对话框
 * Created by zhangdroid on 2016/6/24.
 */
public class SingleButtonDialog extends BaseDialogFragment {
    private OnSingleDialogClickListener mOnDialogClickListener;

    public static SingleButtonDialog newInstance(String title, String message, String positive, boolean isCancelable, OnSingleDialogClickListener listener) {
        SingleButtonDialog singleButtonDialog = new SingleButtonDialog();
        singleButtonDialog.mOnDialogClickListener = listener;
        singleButtonDialog.setArguments(getDialogBundle(title, message, positive, null, isCancelable));
        return singleButtonDialog;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.dialog_single_btn;
    }

    @Override
    protected void setDialogContentView(View view) {
        TextView tvTitle = (TextView) view.findViewById(R.id.dialog_one_btn_title);
        View dividerView = view.findViewById(R.id.dialog_one_btn_divider);// 标题栏下分割线
        TextView tvMessage = (TextView) view.findViewById(R.id.dialog_one_btn_content);
        Button btnPositive = (Button) view.findViewById(R.id.dialog_one_btn_sure);

        String title = getDialogTitle();
        if (!TextUtils.isEmpty((title))) {
            tvTitle.setText(title);
            tvTitle.setVisibility(View.VISIBLE);
            dividerView.setVisibility(View.VISIBLE);
        } else {
            tvTitle.setVisibility(View.GONE);
            dividerView.setVisibility(View.GONE);
        }

        String message = getDialogMessage();
        if (!TextUtils.isEmpty((message))) {
            tvMessage.setText(message);
            tvMessage.setVisibility(View.VISIBLE);
        } else {
            tvMessage.setVisibility(View.GONE);
        }

        String positive = getDialogPositive();
        if (!TextUtils.isEmpty((positive))) {
            btnPositive.setText(positive);
        }

        btnPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnDialogClickListener != null) {
                    mOnDialogClickListener.onPositiveClick(v);
                }
                dismiss();
            }
        });
    }

}
