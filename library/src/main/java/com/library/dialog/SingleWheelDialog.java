package com.library.dialog;


import com.library.widgets.WheelView;

import java.util.List;

/**
 * 单个滚轮选择对话框
 * Created by zhangdroid on 2016/6/24.
 */
public class SingleWheelDialog extends OneWheelDialog {
    private OnOneWheelDialogClickListener mOnDialogClickListener;
    // 数据源
    private List<String> mDataList;
    // 默认选项索引
    private int mDefaultIndex = 0;

    public static SingleWheelDialog newInstance(List<String> dataList, int defaultIndex, String title, String positive, String negative, boolean isCancelable, OnOneWheelDialogClickListener listener) {
        SingleWheelDialog singleWheelDialog = new SingleWheelDialog();
        singleWheelDialog.mOnDialogClickListener = listener;
        singleWheelDialog.mDataList = dataList;
        singleWheelDialog.mDefaultIndex = defaultIndex;
        singleWheelDialog.setArguments(getDialogBundle(title, null, positive, negative, isCancelable));
        return singleWheelDialog;
    }

    @Override
    protected void setWheelView(WheelView wheelView) {
        wheelView.setData(mDataList);
        if (mDefaultIndex < 0) {
            mDefaultIndex = 0;
        }
        wheelView.setDefaultIndex(mDefaultIndex);
    }

    @Override
    protected OnOneWheelDialogClickListener setOnDialogClickListener() {
        return mOnDialogClickListener;
    }

}
