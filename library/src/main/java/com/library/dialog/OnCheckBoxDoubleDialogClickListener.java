package com.library.dialog;

import android.view.View;

import java.util.List;

/**
 * Double button dialog click listener
 * Created by zhangdroid on 2016/6/25.
 */
public interface OnCheckBoxDoubleDialogClickListener {

    /**
     * Called when the dialog positive button has been clicked.
     *
     * @param view The View that was clicked.
     */
    void onPositiveClick(View view, List<String> list);

    /**
     * Called when the dialog negative button has been clicked.
     *
     * @param view The View that was clicked.
     */
    void onNegativeClick(View view);

}
