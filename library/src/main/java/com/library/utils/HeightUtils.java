package com.library.utils;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by jzrh on 2016/7/1.
 * Cm转英寸 英寸转Cm
 */
public class HeightUtils {

    /**
     * 根据厘米获得对应的"英寸（厘米）"
     */
    public static String getInchCmByCm(String cm) {
        Map<String, String> heightMap = getHeightMap();
        if (!TextUtils.isEmpty(cm)) {
            String inch = heightMap.get(cm);
            if (!TextUtils.isEmpty(inch)) {
                return inch + " (" + cm + "cm)";
            }
        }
        return "";
    }

    /**
     * 根据"英寸（厘米）"获得对应的厘米
     */
    public static String getCmByInchCm(String inchCm) {
        if (!TextUtils.isEmpty(inchCm) && inchCm.contains("cm")) {
            return inchCm.substring(inchCm.indexOf("(") + 1).replace("cm)", "");
        }
        return null;
    }

    /**
     * 生成身高英寸/厘米对应map
     */
    public static Map<String, String> getHeightMap() {
        Map<String, String> heightMap = new HashMap<String, String>();
//        heightMap.put("125", "4'2\"");
//        heightMap.put("126", "4'3\"");
//        heightMap.put("127", "4'3\"");
//        heightMap.put("128", "4'3\"");
//        heightMap.put("129", "4'4\"");
//        heightMap.put("130", "4'4\"");
//        heightMap.put("131", "4'4\"");
//        heightMap.put("132", "4'5\"");
//        heightMap.put("133", "4'5\"");
//        heightMap.put("134", "4'5\"");
//        heightMap.put("135", "4'6\"");
//        heightMap.put("136", "4'6\"");
//        heightMap.put("137", "4'6\"");
//        heightMap.put("138", "4'7\"");
//        heightMap.put("139", "4'7\"");
//        heightMap.put("140", "4'7\"");
//        heightMap.put("141", "4'8\"");
//        heightMap.put("142", "4'8\"");
//        heightMap.put("143", "4'8\"");
//        heightMap.put("144", "4'9\"");
        heightMap.put("145", "4'9\"");
        heightMap.put("146", "4'9\"");
        heightMap.put("147", "4'10\"");
        heightMap.put("148", "4'10\"");
        heightMap.put("149", "4'10\"");
        heightMap.put("150", "4'11\"");
        heightMap.put("151", "4'11\"");
        heightMap.put("152", "4'11\"");
        heightMap.put("153", "5'0\"");
        heightMap.put("154", "5'0\"");
        heightMap.put("155", "5'0\"");
        heightMap.put("156", "5'1\"");
        heightMap.put("157", "5'1\"");
        heightMap.put("158", "5'1\"");
        heightMap.put("159", "5'2\"");
        heightMap.put("160", "5'2\"");
        heightMap.put("161", "5'2\"");
        heightMap.put("162", "5'3\"");
        heightMap.put("163", "5'3\"");
        heightMap.put("164", "5'3\"");
        heightMap.put("165", "5'4\"");
        heightMap.put("166", "5'4\"");
        heightMap.put("167", "5'4\"");
        heightMap.put("168", "5'5\"");
        heightMap.put("169", "5'5\"");
        heightMap.put("170", "5'5\"");
        heightMap.put("171", "5'6\"");
        heightMap.put("172", "5'6\"");
        heightMap.put("173", "5'6\"");
        heightMap.put("174", "5'7\"");
        heightMap.put("175", "5'7\"");
        heightMap.put("176", "5'7\"");
        heightMap.put("177", "5'8\"");
        heightMap.put("178", "5'8\"");
        heightMap.put("179", "5'8\"");
        heightMap.put("180", "5'9\"");
        heightMap.put("181", "5'9\"");
        heightMap.put("182", "5'9\"");
        heightMap.put("183", "5'10\"");
        heightMap.put("184", "5'10\"");
        heightMap.put("185", "5'10\"");
        heightMap.put("186", "5'11\"");
        heightMap.put("187", "5'11\"");
        heightMap.put("188", "5'11\"");
        heightMap.put("189", "6'0\"");
        heightMap.put("190", "6'0\"");
        heightMap.put("191", "6'0\"");
        heightMap.put("192", "6'1\"");
        heightMap.put("193", "6'1\"");
        heightMap.put("194", "6'1\"");
        heightMap.put("195", "6'2\"");
        heightMap.put("196", "6'2\"");
        heightMap.put("197", "6'2\"");
        heightMap.put("198", "6'3\"");
        heightMap.put("199", "6'3\"");
        heightMap.put("200", "6'3\"");
        heightMap.put("201", "6'4\"");
        heightMap.put("202", "6'4\"");
        heightMap.put("203", "6'4\"");
        heightMap.put("204", "6'5\"");
        heightMap.put("205", "6'5\"");
        heightMap.put("206", "6'5\"");
        heightMap.put("207", "6'6\"");
        heightMap.put("208", "6'6\"");
        heightMap.put("209", "6'6\"");
        heightMap.put("210", "6'7\"");
        heightMap.put("211", "6'7\"");
        heightMap.put("212", "6'7\"");
        heightMap.put("213", "6'8\"");
        heightMap.put("214", "6'8\"");
        heightMap.put("215", "6'8\"");
        heightMap.put("216", "6'9\"");
        heightMap.put("217", "6'9\"");
        heightMap.put("218", "6'9\"");
        heightMap.put("219", "6'10\"");
        heightMap.put("220", "6'10\"");
        return heightMap;
    }

    /**
     * 生成英寸（厘米）List
     *
     * @return
     */
    public static List<String> getInchCmList() {
        Map<String, String> heightMap = getHeightMap();
        Iterator<Map.Entry<String, String>> iterator = heightMap.entrySet().iterator();
        // 对key排序
        List<String> heightKeyList = new ArrayList<String>();
        while (iterator.hasNext()) {
            heightKeyList.add(iterator.next().getKey());
        }
        Collections.sort(heightKeyList);
        List<String> heightList = new ArrayList<String>();
        for (int i = 0; i < heightKeyList.size(); i++) {
            String inchCm = getInchCmByCm(heightKeyList.get(i));
            if (!TextUtils.isEmpty(inchCm)) {
                heightList.add(inchCm);
            }
        }
        return heightList;
    }

}
