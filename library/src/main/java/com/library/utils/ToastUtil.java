package com.library.utils;

import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;

public class ToastUtil {
    private static Toast sDefault;

    public static void showShortToast(Context context, String message) {
        showToast(context, message, Toast.LENGTH_SHORT);
    }

    public static void showLongToast(Context context, String message) {
        showToast(context, message, Toast.LENGTH_LONG);
    }

    public static void showToast(Context context, String message, int duration) {
        if (TextUtils.isEmpty(message)) {
            return;
        }
        if (sDefault == null) {
            sDefault = Toast.makeText(context, message, duration);
        } else {
            sDefault.setText(message);
        }
        sDefault.show();
    }

}
