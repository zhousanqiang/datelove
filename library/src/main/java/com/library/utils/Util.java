package com.library.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Parcelable;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.library.R;
import com.spreada.utils.chinese.ZHConverter;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * 公用工具类
 * Created by zhangdroid on 2016/5/25.
 */
public class Util {

    public static boolean isListEmpty(List<?> list) {
        return list == null || list.size() == 0;
    }

    public static void gotoActivity(Activity from, Class<?> cls, boolean needFinish) {
        Intent intent = new Intent(from, cls);

        from.startActivity(intent);
        if (needFinish){
            from.finish();
        }
        from.overridePendingTransition(R.anim.login_anim_in,R.anim.login_anim_out);
    }

    public static void gotoActivity(Activity from, Class<?> cls, boolean needFinish, String key, boolean value) {
        Intent intent = new Intent(from, cls);
        intent.putExtra(key, value);
        from.startActivity(intent);
        if (needFinish) {
            from.finish();
        }
    }

    public static void gotoActivity(Activity from, Class<?> cls, boolean needFinish, String key, Serializable value) {
        Intent intent = new Intent(from, cls);
        if (value != null) {
            intent.putExtra(key, value);
        }
        if (needFinish) {
            from.finish();
        }
        from.startActivity(intent);
    }

    public static void gotoActivity(Activity from, Class<?> cls, boolean needFinish, String key, Parcelable value) {
        Intent intent = new Intent(from, cls);
        if (value != null) {
            intent.putExtra(key, value);
        }
        if (needFinish) {
            from.finish();
        }
        from.startActivity(intent);
    }

    public static void gotoActivity(Activity from, Class<?> cls, boolean needFinish, Map<String, String> map) {
        Intent intent = new Intent(from, cls);
        if (map != null && !map.isEmpty()) {
            Iterator<String> iterator = map.keySet().iterator();
            while (iterator.hasNext()) {
                String key = iterator.next();
                String value = map.get(key);
                if (!TextUtils.isEmpty(value))
                    intent.putExtra(key, value);
            }
        }
        if (needFinish) {
            from.finish();
        }
        from.startActivity(intent);
    }

    /**
     * 隐藏键盘
     */
    public static void hideKeyboard(Context context, View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /**
     * 显示键盘
     *
     * @param context
     */
    public static void showKeyboard(Context context) {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
    }

    /**
     * 将汉字从简体转换为繁体
     *
     * @param content
     * @return
     */
    public static String convertText(String content) {
        if (TextUtils.isEmpty(content)) {
            return null;
        }
        return ZHConverter.convert(content, ZHConverter.TRADITIONAL);
    }

    /**
     * 获取指定位数的小数
     *
     * @param number   double型原始数据
     * @param decimals 小数点后位数, 不能小于1
     */
    public static String getDecimal(double number, int decimals) {
        if (decimals < 1) {
            return number + "";
        }
        StringBuilder builder = new StringBuilder("0.");
        for (int i = 0; i < decimals; i++) {
            builder.append("0");
        }
        DecimalFormat format = new DecimalFormat(builder.toString().trim());
        return format.format(number);
    }

    /**
     * 获得AnimationDrawable，图片动画
     *
     * @param list         动画需要播放的图片集合
     * @param isRepeatable 是否可以重复
     * @param duration     帧间隔（毫秒）
     * @return
     */
    public static AnimationDrawable getFrameAnim(List<Drawable> list, boolean isRepeatable, int duration) {
        AnimationDrawable animationDrawable = new AnimationDrawable();
        animationDrawable.setOneShot(!isRepeatable);
        if (!isListEmpty(list)) {
            for (int i = 0; i < list.size(); i++) {
                animationDrawable.addFrame(list.get(i), duration);
            }
        }
        return animationDrawable;
    }

    /**
     * 读取assets目录下的json文件
     *
     * @param context  上下文对象
     * @param fileName assets目录下的json文件路径
     */
    public static String getJsonStringFromAssets(Context context, String fileName) {
        String jsonStr = null;
        if (!TextUtils.isEmpty(fileName)) {
            BufferedReader bufferedReader = null;
            try {
                bufferedReader = new BufferedReader(new InputStreamReader(context.getAssets().open(fileName)));
                String line;
                StringBuilder buffer = new StringBuilder();
                while (!TextUtils.isEmpty((line = bufferedReader.readLine()))) {
                    buffer.append(line);
                }
                jsonStr = buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (bufferedReader != null) {
                        bufferedReader.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return jsonStr;
    }

}
