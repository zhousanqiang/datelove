package com.library.utils;

/**
 * Created by Administrator on 2016/11/4.
 */

public class PayUtils {
    /**
     * 功能描述：加密算法
     *
     * @param pid
     * @param transactionId
     * @return
     *
     * @author lxl
     * @since 2016-10-28
     * @update:[变更日期YYYY-MM-DD][更改人姓名][变更描述]
     */
    public static  String encryptPay(String pid,String transactionId){
        String key = pid + transactionId;
        String md5Resault = MessageDigestUtil.encryptMD5(key.getBytes());
        return md5Resault;
    }
}
