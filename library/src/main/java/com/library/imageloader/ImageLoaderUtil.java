package com.library.imageloader;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.widget.ImageView;

import com.library.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;

import static com.squareup.picasso.Picasso.with;

/**
 * Image loader util, single instance pattern
 * Created by zhangdroid on 2016/5/31.
 */
public class ImageLoaderUtil {

    public static final int LARGE_PIC = 0;
    public static final int MEDIUM_PIC = 1;
    public static final int SMALL_PIC = 2;

    private static volatile ImageLoaderUtil sDefault;

    private ImageLoaderUtil() {
    }

    public static ImageLoaderUtil getInstance() {
        if (sDefault == null) {
            synchronized (ImageLoaderUtil.class) {
                if (sDefault == null) {
                    sDefault = new ImageLoaderUtil();
                }
            }
        }
        return sDefault;
    }

    /**
     * 图片加载监听器
     */
    public interface OnImageLoadListener {
        /**
         * 加载完成
         */
        void onCompleted();

        /**
         * 加载失败
         */
        void onFailed();
    }

    /**
     * 加载网络图片
     *
     * @param context
     * @param imageLoader
     */
    public void loadImage(Context context, ImageLoader imageLoader) {
        if (TextUtils.isEmpty(imageLoader.getUrl())) {
            return;
        }
        Picasso.with(context).load(imageLoader.getUrl())
                .placeholder(imageLoader.getPlaceHolder())
                .error(imageLoader.getPlaceHolder())
                .transform(imageLoader.getTransformation())
                .config(imageLoader.getConfig())
                .into(imageLoader.getImageView());
    }

    /**
     * 带有回调的加载网络图片
     *
     * @param context
     * @param imageLoader
     * @param listener
     */
    public void loadImage(Context context, ImageLoader imageLoader, final OnImageLoadListener listener) {
        if (TextUtils.isEmpty(imageLoader.getUrl())) {
            return;
        }
        with(context)
                .load(imageLoader.getUrl())
                .placeholder(imageLoader.getPlaceHolder())
                .error(imageLoader.getPlaceHolder())
                .transform(imageLoader.getTransformation())
                .config(imageLoader.getConfig())
                .into(imageLoader.getImageView(), new Callback() {
                    @Override
                    public void onSuccess() {
                        if (listener != null) {
                            listener.onCompleted();
                        }
                    }

                    @Override
                    public void onError() {
                        if (listener != null) {
                            listener.onFailed();
                        }
                    }
                });
    }

    /**
     * 加载圆形图片（drawable）
     *
     * @param context
     * @param imageView
     * @param resId
     */
    public void loadCircleImage(Context context, ImageView imageView, int resId) {
        with(context).load(resId).skipMemoryCache().noPlaceholder().transform(new CircleTransformation()).into(imageView);
    }

    /**
     * 加载圆形图片（本地File）
     *
     * @param context
     * @param imageView
     * @param file
     */
    public void loadCircleImage(Context context, ImageView imageView, File file) {
        with(context).load(file).skipMemoryCache().noPlaceholder().transform(new CircleTransformation()).into(imageView);
    }

    /**
     * 加载圆形图片（Uri）
     *
     * @param context
     * @param imageView
     * @param uri
     */
    public void loadCircleImage(Context context, ImageView imageView, Uri uri) {
        with(context).load(uri).skipMemoryCache().noPlaceholder().transform(new CircleTransformation()).into(imageView);
    }

    /**
     * 加载本地图片（drawable）
     *
     * @param context
     * @param imageView
     * @param resId
     */
    public void loadLocalImage(Context context, ImageView imageView, int resId) {
        with(context).load(resId).skipMemoryCache().noPlaceholder().into(imageView);
    }

    /**
     * 加载本地图片（本地File）
     *
     * @param context
     * @param imageView
     * @param file
     */
    public void loadLocalImage(Context context, ImageView imageView, File file) {
        with(context).load(file).skipMemoryCache().noPlaceholder().into(imageView);
    }

    /**
     * 加载本地图片（Uri）
     *
     * @param context
     * @param imageView
     * @param uri
     */
    public void loadLocalImage(Context context, ImageView imageView, Uri uri) {
        with(context).load(uri).skipMemoryCache().noPlaceholder().into(imageView);
    }
    public void loadLocalImage(Context context, ImageView imageView, Uri uri, final OnImageLoadListener listener) {
        with(context).load(uri).skipMemoryCache().noPlaceholder().into(imageView, new Callback() {
            @Override
            public void onSuccess() {
                if (listener != null) {
                    listener.onCompleted();
                }
            }

            @Override
            public void onError() {
                if (listener != null) {
                    listener.onFailed();
                }
            }
        });
    }
}
