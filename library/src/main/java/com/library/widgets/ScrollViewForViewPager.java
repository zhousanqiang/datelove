package com.library.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ScrollView;

/**
 * 可以嵌套ViewPager的Scroll
 * Created by zhangdroid on 2016/7/1.
 */
public class ScrollViewForViewPager extends ScrollView {
    // X轴和Y轴的滑动距离
    private float xDistance, yDistance;
    // 记录上次触摸的X轴和Y轴坐标
    private float xLast, yLast;

    public ScrollViewForViewPager(Context context) {
        super(context);
    }

    public ScrollViewForViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                xDistance = yDistance = 0f;
                xLast = ev.getX();
                yLast = ev.getY();
                break;
            case MotionEvent.ACTION_MOVE:
                final float curX = ev.getX();
                final float curY = ev.getY();
                xDistance += Math.abs(curX - xLast);
                yDistance += Math.abs(curY - yLast);
                xLast = curX;
                yLast = curY;

                if ((xDistance > yDistance) || (yDistance - xDistance > 0) && ((yDistance - xDistance < 30))) {// 横向滚动时不拦截
                    return false;
                }
        }
        return super.onInterceptTouchEvent(ev);
    }

}
